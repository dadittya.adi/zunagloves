const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
   // Javascript
   .js('resources/js/app.js', 'public/js')
   

   // SCSS
   .sass('resources/sass/app.scss', 'public/css')
   .sass('resources/sass/web.scss', 'public/css')

   // Jquery Zoom
   .copy('node_modules/jquery-zoom/jquery.zoom.min.js','public/js/jquery.zoom.min.js')

   // Images
   .copyDirectory('resources/images','public/images')
   
   .copy(
		"resources/fonts",
		"public/build/fonts"
   )
   
   .less(
      'resources/backend/less/app.less',
		'public/css/admin/app.css'
   )

   .combine(
	[
		"resources/backend/less/style.less",
		"resources/backend/less/gridlist.less",
		"resources/backend/less/custom.less",
		"resources/backend/less/media-queries.less",
		"resources/backend/less/media-news.less"
	],
	"public/css/custom.css"
   )
   
   .js(
		[
			//"resources/backend/js/jquery.min.js",
			"resources/backend/js/bootstrap.js",
			//"resources/backend/js/bootstrap-datepicker.js",
			//"resources/backend/js/mustache.js",
			//"resources/backend/js/is.js",
			"resources/backend/js/app.js",
			"resources/backend/js/formatMoney.js",
			"resources/backend/js/gridlist.js",
			//"resources/backend/js/commonmark.min.js",
			"resources/backend/js/jquery_ui.js"
		],
		"public/js/admin/app.js"
   )
   
   .js(
		[
			"resources/backend/js/master/subcategory_list_edit.js"
		],
		"public/js/admin/master/subcategory.js"
	).js(
		[
			"resources/backend/js/master/product_subcategory_dropdown.js",
			"resources/backend/js/master/product_productsize_render.js"
		],
		"public/js/admin/master/product.js"
	).js(
		[
			"resources/backend/js/master/productsize_list_edit.js"
		],
		"public/js/admin/master/productsize.js"
	)
	.version();

   
