<?php

return [

	'jumbotron' => storage_path() . '/app/jumbotron',
	'product' => storage_path() . '/app/product',
	'promo' => storage_path() . '/app/promo',
	'color' => storage_path() . '/app/color',
	'banknote' => storage_path() . '/app/banknote',
	'icon' => storage_path() . '/app/icon',
	'guide' => storage_path() . '/app/guide',
	'media' => public_path() . '/media',
	'report' => storage_path() . '/app/report',
	'customer' => storage_path() . '/app/customer',
	'product_thumb' => storage_path() . '/app/product_thumb',
	'subscribe' => storage_path() . '/app/subscribed_email'
];