<?php

return [
	'veritrans' => [
		'sandbox' => env('VERITRANS_SANDBOX', true),
		'production' => env('VERITRANS_PRODUCTION', false),
		'3d_secure' => env('VERITRANS_3D_SECURE', true),
		'merchant_id' => env('VERITRANS_MERCHANT_ID', ''),
		'client_key' => env('VERITRANS_CLIENT_KEY', ''),
		'server_key' => env('VERITRANS_SERVER_KEY', ''),
		'url' => env('VERITRANS_URL', '')
	]
];
