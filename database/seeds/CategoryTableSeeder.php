<?php

use Illuminate\Database\Seeder;
use App\Models\Category;
use App\Models\CategorySubcategory;

class CategoryTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Category::create([
			'name' => 'Accessories',
			'is_active' => 1,
			'show_order' => 1
		]);

		Category::create([
			'name' => 'Billiard',
			'is_active' => 1,
			'show_order' => 2
		]);

		Category::create([
			'name' => 'Cycling Gloves',
			'is_active' => 1,
			'show_order' => 3
		]);

		Category::create([
			'name' => 'Dress Gloves',
			'is_active' => 1,
			'show_order' => 4
		]);

		Category::create([
			'name' => 'Fisihing Gloves',
			'is_active' => 1,
			'show_order' => 5
		]);

		Category::create([
			'name' => 'Fitness Gloves',
			'is_active' => 1,
			'show_order' => 6
		]);

		Category::create([
			'name' => 'Golf Gloves',
			'is_active' => 1,
			'show_order' => 7
		]);

		Category::create([
			'name' => 'Horse Riding Gloves',
			'is_active' => 1,
			'show_order' => 8
		]);

		Category::create([
			'name' => 'Motorbike Gloves',
			'is_active' => 1,
			'show_order' => 9
		]);

		Category::create([
			'name' => 'Trekking Gloves',
			'is_active' => 1,
			'show_order' => 10
		]);
	}
}
