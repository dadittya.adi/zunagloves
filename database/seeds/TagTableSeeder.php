<?php

use Illuminate\Database\Seeder;
use App\Models\Tag;

class TagTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Tag::insert([
			[
				'id' => 1,
				'name' => 'sport',
				'state' => 1,
				'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
			],
			[
				'id' => 2,
				'name' => 'info',
				'state' => 1,
				'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
			],
			[
				'id' => 3,
				'name' => 'international',
				'state' => 1,
				'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
			],
			[
				'id' => 4,
				'name' => 'technology',
				'state' => 1,
				'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
			]
		]);
	}
}

