<?php

use Illuminate\Database\Seeder;
use App\Models\Productsize;

class ProductsizeTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Productsize::insert([
			[
				'id' => 1,
				'name' => 'Extra Small',
				'short_name' => 'XS',
				'order' => 1
			],
			[
				'id' => 2,
				'name' => 'Small',
				'short_name' => 'S',
				'order' => 2
			],
			[
				'id' => 3,
				'name' => 'Medium',
				'short_name' => 'M',
				'order' => 3
			],
			[
				'id' => 4,
				'name' => 'Large',
				'short_name' => 'L',
				'order' => 4
			],
			[
				'id' => 5,
				'name' => 'Extra Large',
				'short_name' => 'XL',
				'order' => 5
			]
		]);


	}
}
