<?php

use Illuminate\Database\Seeder;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Address;
use App\Models\Shipping;
use App\Models\Product;
use App\Models\Customer;

use Carbon\Carbon;

class OrderTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$customer = Customer::where('email', '=', 'testing+customer_panji@durenworks.com')->first();

		//address
		$addr = [
			'country' => 'Indonesia',
			'full_name' => $customer->full_name,
			'phone' => $customer->phone,
			'address' => 'Jalan Melati 99',
			'city' => 'Semarang',
			'province' => 'Jawa Tengah',
			'postal_code' => '12345',
			'type' => 'BILL',
		];
		$billaddr = Address::create($addr);

		$ship = $addr;
		$ship['type'] = 'SHIP';
		$shipaddr = Address::create($ship);

		$provider = Shipping::where('name', '=', 'JNE Reguler Indonesia')->first();
		$shipping = $provider->prices()->where('destination', '=', 'Semarang')->first();

		//order
		$order = Order::create([
			'domain' => 'en',
			'customer_id' => $customer->id,
			'state' => 'INV',
			'revision' => 0,
			'billing_addr' => $billaddr->id,
			'shipping_addr' => $shipaddr->id,
			'use_billing_addr' => 1,
			'provider' => $provider->name,
			'destination' => $shipping->destination,
			'shipping_price' => $shipping->price,
			'shipping_time' => $shipping->time,
			'payment_method' => 'ATM'
		]);

		$now = Carbon::now()->format('YmdHi');
		$order->order_number = $now . substr('00' . e($order->id), -2, 2);
		$order->save();

		//product 1
		$product = Product::active()->first();
		$color = $product->colors()->first();
		$size = $product->sizes()->first();

		$item = new OrderItem();
		$item->fill([
			'product_id' => $product->id,
			'productcolor_id' => $color->id,
			'color' => $color ? $color->name : null,
			'size' => $size ? $size->name : null,
			'qty' => 2,
			'price' => $product->price,
			'discount' => (2 * $product->price) * $product->disc_percent / 100,
			'total' => (2 * $product->price),
			'notes' => 'Lorem ipsum'
		]);

		$order->items()->save($item);

		//product 1
		$product = Product::active()->findOrFail($product->id + 1);
		$color = $product->colors()->first();
		$size = $product->sizes()->first();

		$item = new OrderItem();
		$item->fill([
			'product_id' => $product->id,
			'productcolor_id' => $color->id,
			'color' => $color ? $color->name : null,
			'size' => $size ? $size->name : null,
			'qty' => 1,
			'price' => $product->price,
			'disc_percent' => $product->disc_percent,
			'discount' => (1 * $product->price) * $product->disc_percent / 100,
			'total' => (1 * $product->price)
		]);

		$order->items()->save($item);
	}
}