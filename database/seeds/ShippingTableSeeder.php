<?php

use Illuminate\Database\Seeder;
use App\Models\Shipping;
use App\Models\ShippingPrice;
use App\Models\Country;

class ShippingTableSeeder extends Seeder {

	public function run()
	{
		$country = Country::where('name', '=', 'Indonesia')->first();

		//AMBIL SENDIRI
		$shipping = new Shipping();
		$shipping->name = 'Pick Up on Store';
		$shipping->display_name = 'Pick Up on Store';
		$shipping->country_id = $country->id;
		$shipping->show_order = 1;
		$shipping->save();
		
		$newprice = new ShippingPrice();
		$newprice->shipping_id = $shipping->id;
		$newprice->destination = 'Zuna - Jakarta';
		$newprice->price = 0;
		$newprice->time = '0';
		$newprice->save();

		$newprice = new ShippingPrice();
		$newprice->shipping_id = $shipping->id;
		$newprice->destination = 'Zuna - Semarang';
		$newprice->price = 0;
		$newprice->time = '0';
		$newprice->save();

		//JNE YES
		$shipping = new Shipping();
		$shipping->name = 'JNE YES Indonesia';
		$shipping->display_name = 'JNE YES';
		$shipping->country_id = $country->id;
		$shipping->show_order = 2;
		$shipping->save();

		$prices = array();
		$newprice = new ShippingPrice();
		$newprice->destination = 'Balikpapan';
		$newprice->price = 68000;
		$newprice->time = '1';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Bandar Lampung';
		$newprice->price = 44000;
		$newprice->time = '1';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Bandung';
		$newprice->price = 36000;
		$newprice->time = '1';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Banjarmasin';
		$newprice->price = 58000;
		$newprice->time = '1';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Batam';
		$newprice->price = 57000;
		$newprice->time = '1';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Bekasi';
		$newprice->price = 32000;
		$newprice->time = '1';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Bengkulu';
		$newprice->price = 54000;
		$newprice->time = '1';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Bogor';
		$newprice->price = 32000;
		$newprice->time = '1';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Cilegon';
		$newprice->price = 36000;
		$newprice->time = '1';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Cirebon';
		$newprice->price = 35000;
		$newprice->time = '1';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Denpasar';
		$newprice->price = 49000;
		$newprice->time = '1';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Depok';
		$newprice->price = 32000;
		$newprice->time = '1';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Jakarta';
		$newprice->price = 28000;
		$newprice->time = '1';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Jambi';
		$newprice->price = 45000;
		$newprice->time = '1';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Karawang';
		$newprice->price = 36000;
		$newprice->time = '1';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Kediri';
		$newprice->price = 40000;
		$newprice->time = '1';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Magelang';
		$newprice->price = 21000;
		$newprice->time = '1';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Malang';
		$newprice->price = 40000;
		$newprice->time = '1';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Mataram';
		$newprice->price = 55000;
		$newprice->time = '1';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Medan';
		$newprice->price = 63000;
		$newprice->time = '1';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Mojokerto';
		$newprice->price = 40000;
		$newprice->time = '1';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Padang';
		$newprice->price = 57000;
		$newprice->time = '1';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Palangkaraya';
		$newprice->price = 42000;
		$newprice->time = '1';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Palembang';
		$newprice->price = 45000;
		$newprice->time = '1';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Pangkal Pinang';
		$newprice->price = 47000;
		$newprice->time = '1';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Pekanbaru';
		$newprice->price = 59000;
		$newprice->time = '1';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Pontianak';
		$newprice->price = 56000;
		$newprice->time = '1';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Probolinggo';
		$newprice->price = 40000;
		$newprice->time = '1';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Semarang';
		$newprice->price = 27000;
		$newprice->time = '1';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Solo';
		$newprice->price = 27000;
		$newprice->time = '1';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Sukabumi';
		$newprice->price = 37000;
		$newprice->time = '1';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Surabaya';
		$newprice->price = 39000;
		$newprice->time = '1';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Tangerang';
		$newprice->price = 32000;
		$newprice->time = '1';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Ujung Pandang';
		$newprice->price = 73000;
		$newprice->time = '1';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Yogyakarta';
		$newprice->price = 7000;
		$newprice->time = '1';
		$prices[] = $newprice;

		$shipping->prices()->saveMany($prices);
	
		//JNE REGULER
		$shipping = new Shipping();
		$shipping->name = 'JNE REGULER Indonesia';
		$shipping->display_name = 'JNE REGULER';
		$shipping->country_id = $country->id;
		$shipping->show_order = 3;
		$shipping->save();

		$prices = array();
		$newprice = new ShippingPrice();
		$newprice->destination = 'Ambon';
		$newprice->price = 61000;
		$newprice->time = '5 - 6';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Ampana Kota';
		$newprice->price = 78000;
		$newprice->time = '6 - 7';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Balikpapan';
		$newprice->price = 39000;
		$newprice->time = '2 - 3';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Banda Aceh';
		$newprice->price = 45000;
		$newprice->time = '3 - 4';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Bandar Lampung';
		$newprice->price = 31000;
		$newprice->time = '2 - 3';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Bandung';
		$newprice->price = 19000;
		$newprice->time = '2 - 3';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Banjarmasin';
		$newprice->price = 35000;
		$newprice->time = '2 - 3';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Batam';
		$newprice->price = 35000;
		$newprice->time = '2 - 3';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Bekasi';
		$newprice->price = 21000;
		$newprice->time = '2 - 3';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Bengkulu';
		$newprice->price = 31000;
		$newprice->time = '2 - 3';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Benteng';
		$newprice->price = 62000;
		$newprice->time = '5 - 6';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Blangpidie';
		$newprice->price = 77000;
		$newprice->time = '5 - 6';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Bogor';
		$newprice->price = 21000;
		$newprice->time = '2 - 3';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Bontang';
		$newprice->price = 62000;
		$newprice->time = '3 - 4';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Burmeso';
		$newprice->price = 327000;
		$newprice->time = '> 10';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Cilacap';
		$newprice->price = 20000;
		$newprice->time = '2 - 3';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Cilegon';
		$newprice->price = 19000;
		$newprice->time = '2 - 3';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Cirebon';
		$newprice->price = 19000;
		$newprice->time = '2 - 3';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Denpasar';
		$newprice->price = 22000;
		$newprice->time = '2 - 3';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Depok';
		$newprice->price = 21000;
		$newprice->time = '2 - 3';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'FAK-FAK';
		$newprice->price = 183000;
		$newprice->time = '6 - 7';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Gorontalo';
		$newprice->price = 64000;
		$newprice->time = '4 - 5';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Jakarta';
		$newprice->price = 18000;
		$newprice->time = '2 - 3';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Jambi';
		$newprice->price = 31000;
		$newprice->time = '2 - 3';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Jayapura';
		$newprice->price = 67800;
		$newprice->time = '7';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Jayapura-Jember';
		$newprice->price = 26000;
		$newprice->time = '3 - 4';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Karawang';
		$newprice->price = 21000;
		$newprice->time = '3 - 4';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Kediri';
		$newprice->price = 26000;
		$newprice->time = '3 - 4';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Kendari';
		$newprice->price = 52000;
		$newprice->time = '4 - 5';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Kupang';
		$newprice->price = 44000;
		$newprice->time = '3 - 4';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Madiun';
		$newprice->price = 26000;
		$newprice->time = '3 - 4';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Magelang';
		$newprice->price = 9000;
		$newprice->time = '2 - 3';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Malang';
		$newprice->price = 26000;
		$newprice->time = '2 - 3';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Manado';
		$newprice->price = 52000;
		$newprice->time = '3 - 4';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Mataram';
		$newprice->price = 33000;
		$newprice->time = '2 - 3';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Medan';
		$newprice->price = 33000;
		$newprice->time = '2 - 3';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Mojokerto';
		$newprice->price = 26000;
		$newprice->time = '3 - 4';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Nabire';
		$newprice->price = 138000;
		$newprice->time = '5 - 6';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Ngamprah';
		$newprice->price = 19000;
		$newprice->time = '2 - 3';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Padang';
		$newprice->price = 31000;
		$newprice->time = '2 - 3';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Palangkaraya';
		$newprice->price = 35000;
		$newprice->time = '2 - 3';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Palembang';
		$newprice->price = 31000;
		$newprice->time = '2 - 3';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Palopo';
		$newprice->price = 62000;
		$newprice->time = '5 - 6';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Palu';
		$newprice->price = 63000;
		$newprice->time = '3 - 4';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Pandaan';
		$newprice->price = 26000;
		$newprice->time = '3 - 4';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Pangkal Pinang';
		$newprice->price = 34000;
		$newprice->time = '2 - 3';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Pangkalan Balai';
		$newprice->price = 52000;
		$newprice->time = '4 - 5';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Pangkalan Kerinci';
		$newprice->price = 58000;
		$newprice->time = '4 - 5';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Pekanbaru';
		$newprice->price = 31000;
		$newprice->time = '2 - 3';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Pemalang';
		$newprice->price = 15000;
		$newprice->time = '4 - 5';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Pontianak';
		$newprice->price = 34000;
		$newprice->time = '2 - 3';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Probolinggo';
		$newprice->price = 26000;
		$newprice->time = '3 - 4';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Ratahan';
		$newprice->price = 84000;
		$newprice->time = '6 - 7';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Samarinda';
		$newprice->price = 43000;
		$newprice->time = '2 - 3';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Sambas';
		$newprice->price = 60000;
		$newprice->time = '3 - 5';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Semarang';
		$newprice->price = 10000;
		$newprice->time = '2 - 3';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Solo';
		$newprice->price = 10000;
		$newprice->time = '2 - 3';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Sorong';
		$newprice->price = 164000;
		$newprice->time = '5 - 6';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Sukabumi';
		$newprice->price = 28000;
		$newprice->time = '3 - 4';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Sukadana-Lampung Timur';
		$newprice->price = 52000;
		$newprice->time = '3 - 4';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Surabaya';
		$newprice->price = 21000;
		$newprice->time = '2 - 3';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Tais';
		$newprice->price = 58000;
		$newprice->time = '4 - 5';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Tangerang';
		$newprice->price = 21000;
		$newprice->time = '2 - 3';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Tanjung Pandan';
		$newprice->price = 42000;
		$newprice->time = '3 - 4';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Tanjung Pinang';
		$newprice->price = 41000;
		$newprice->time = '3 - 4';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Tarakan';
		$newprice->price = 57000;
		$newprice->time = '2 - 3';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Tarutung';
		$newprice->price = 61000;
		$newprice->time = '4 - 5';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Ternate';
		$newprice->price = 65000;
		$newprice->time = '5 - 6';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Timika';
		$newprice->price = 133000;
		$newprice->time = '5 - 6';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Trenggalek';
		$newprice->price = 34000;
		$newprice->time = '4 - 5';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Tulungagung';
		$newprice->price = 34000;
		$newprice->time = '4 - 5';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Ujung Pandang';
		$newprice->price = 40000;
		$newprice->time = '2 - 3';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Wangiapu';
		$newprice->price = 87000;
		$newprice->time = '5 - 7';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Waris';
		$newprice->price = 174000;
		$newprice->time = '6 - 7';
		$prices[] = $newprice;

		$newprice = new ShippingPrice();
		$newprice->destination = 'Yogyakarta';
		$newprice->price = 4000;
		$newprice->time = '1 - 2';
		$prices[] = $newprice;

		$shipping->prices()->saveMany($prices);
	}
}