<?php

use Illuminate\Database\Seeder;
use App\Models\Admin;
use App\Models\AdminGroup;

class AdminTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Admin::insert([
			[
				'id' => 1,
				'full_name' => 'Administrator',
				'email' => 'admin@zunagloves.com',
				'password' => Hash::make('password'),
				'is_active' => 1,
			],
			[
				'id' => 2,
				'full_name' => 'Product Arranger',
				'email' => 'admin+product@example.com',
				'password' => Hash::make('password'),
				'is_active' => 1,
			],
			[
				'id' => 3,
				'full_name' => 'Order Manager',
				'email' => 'admin+order@example.com',
				'password' => Hash::make('password'),
				'is_active' => 1,
			]
			,
			[
				'id' => 4,
				'full_name' => 'Blog Writer',
				'email' => 'admin+blog@example.com',
				'password' => Hash::make('password'),
				'is_active' => 1,
			]
		]);

		$admins = Admin::get();
        AdminGroup::insert([
                [
                    'group_id' => 1,
                    'admin_id' => 1
                ],
                [
                    'group_id' => 2,
                    'admin_id' => 2
                ],
                [
                    'group_id' => 3,
                    'admin_id' => 3
                ],
                [
                    'group_id' => 4,
                    'admin_id' => 4
                ]
            ]);
        
	}
}
