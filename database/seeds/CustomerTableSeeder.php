<?php

use Illuminate\Database\Seeder;
use App\Models\Customer;

class CustomerTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$cust = new Customer();
		$cust->email = 'testing+customer_saras@durenworks.com';
		$cust->password = Hash::make('saraspass');
		$cust->full_name = 'Saras 008';
		$cust->phone = '008009010011';
		$cust->state = 'NEW';
		$cust->save();

		$cust = new Customer();
		$cust->email = 'testing+customer_panji@durenworks.com';
		$cust->password = Hash::make('panjipass');
		$cust->full_name = 'Panji Manusia Milenium';
		$cust->phone = '1234567890';
		$cust->state = 'ACT';
		$cust->save();

		$cust = new Customer();
		$cust->email = 'testing+customer_kotaro@durenworks.com';
		$cust->password = Hash::make('kotaropass');
		$cust->full_name = 'Kotaro Minami';
		$cust->phone = '0987654321';
		$cust->state = 'ACT';
		$cust->save();

		$cust = new Customer();
		$cust->email = 'testing+customer_deka@durenworks.com';
		$cust->password = Hash::make('dekapass');
		$cust->full_name = 'Dekanyo';
		$cust->phone = '09876123456';
		$cust->state = 'ACT';
		$cust->save();

		$cust = new Customer();
		$cust->email = 'testing+customer_yamin@durenworks.com';
		$cust->password = Hash::make('yaminpass');
		$cust->full_name = 'Mie Ayam Manis';
		$cust->phone = '12345';
		$cust->state = 'ACT';
		$cust->save();

		$cust = new Customer();
		$cust->email = 'testing+customer_yoyo@durenworks.com';
		$cust->password = Hash::make('yoyopass');
		$cust->full_name = 'Yoyo Yonpa';
		$cust->phone = '73737373';
		$cust->state = 'ACT';
		$cust->save();

		$cust = new Customer();
		$cust->email = 'testing+customer_nana@durenworks.com';
		$cust->password = Hash::make('nanapass');
		$cust->full_name = 'Nana';
		$cust->phone = '2929292';
		$cust->state = 'ACT';
		$cust->save();

		$cust = new Customer();
		$cust->email = 'testing+customer_rere@durenworks.com';
		$cust->password = Hash::make('rerepass');
		$cust->full_name = 'Rere mimi fafa';
		$cust->phone = '35353535';
		$cust->state = 'ACT';
		$cust->save();

		$cust = new Customer();
		$cust->email = 'testing+customer_marmut@durenworks.com';
		$cust->password = Hash::make('marmutpass');
		$cust->full_name = 'Marmut';
		$cust->phone = '727282964';
		$cust->state = 'ACT';
		$cust->save();
	}
}