<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

		DB::table('admins')->truncate();
		DB::table('customers')->truncate();
		DB::table('promos')->truncate();
		DB::table('vouchers')->truncate();
		DB::table('wishlists')->truncate();

		DB::table('products_productsizes')->truncate();
		DB::table('product_colors')->truncate();
		DB::table('products')->truncate();
		DB::table('productsizes')->truncate();

		//DB::table('category_subcategories')->truncate();
		DB::table('categories')->truncate();

		DB::table('countries')->truncate();
		DB::table('shippings')->truncate();
		DB::table('shipping_prices')->truncate();

		DB::table('addresses')->truncate();
		DB::table('orders')->truncate();
		DB::table('order_items')->truncate();
        DB::table('tags')->truncate();
        DB::table('blogs')->truncate();

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $this->call(AdminTableSeeder::class);
        $this->call(CategoryTableSeeder::class);
        $this->call(ZunaProductSeeder::class);
        //$this->call(ProductTableSeeder::class);
        //$this->call(ProductsizeTableSeeder::class);
        //$this->call(ProductColorTableSeeder::class);
        //$this->call(CountryTableSeeder::class);
        //$this->call(ShippingTableSeeder::class);
        $this->call(CustomerTableSeeder::class);
        $this->call(VoucherTableSeeder::class);
        //$this->call(OrderTableSeeder::class);
        $this->call(TagTableSeeder::class);
        $this->call(BlogTableSeeder::class);

        Model::reguard();
    }
}
