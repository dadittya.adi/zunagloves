<?php

use Illuminate\Database\Seeder;
use App\Models\Voucher;
use Carbon\Carbon;

class VoucherTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Voucher::create([
			'code' => 'CODE1',
			'promo' => 'MAGIC',
			'discount' => 5,
			'start_date' => Carbon::now()->startOfDay(),
			'end_date' => Carbon::now()->addDay()->endOfDay(),
			'minimum' => 30
		]);

		Voucher::create([
			'code' => 'CODE2',
			'promo' => 'YAYAYA',
			'discount' => 12.5,
			'start_date' => Carbon::now()->subDay(7)->startOfDay(),
			'end_date' => Carbon::now()->subDay()->endOfDay(),
			'minimum' => 0
		]);
	}
}