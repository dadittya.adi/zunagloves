<?php

use Illuminate\Database\Seeder;
use App\Models\Promo;

class PromoTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		copy(public_path() . '/images/1.png', Config::get('storage.promo') . '/expromo.png');
		Promo::insert([
			'id' => 1,
			'title' => 'Powerfully Responsive with A Clean Design',
			'image' => 'expromo.png',
			'mimetype' => 'image/png'
		]);

		copy(public_path() . '/images/2.png', Config::get('storage.promo') . '/expromo2.png');
		Promo::insert([
			'id' => 2,
			'title' => 'Face The Weather, Keep The Performance',
			'image' => 'expromo2.png',
			'mimetype' => 'image/png'
		]);

		copy(public_path() . '/images/3.png', Config::get('storage.promo') . '/expromo3.png');
		Promo::insert([
			'id' => 3,
			'title' => 'Amazing Peformance with A Unique Item',
			'image' => 'expromo3.png',
			'mimetype' => 'image/png'
		]);

		copy(public_path() . '/images/4.png', Config::get('storage.promo') . '/expromo4.png');
		Promo::insert([
			'id' => 4,
			'title' => 'Hit The Road With Performance',
			'image' => 'expromo4.png',
			'mimetype' => 'image/png'
		]);
	}

}