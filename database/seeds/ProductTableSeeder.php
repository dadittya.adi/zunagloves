<?php

use Illuminate\Database\Seeder;
use App\Models\Product;
use App\Models\Category;
use Carbon\Carbon;
use League\CommonMark\CommonMarkConverter;


class ProductTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$converter = new CommonMarkConverter();
		//GLOVES
		$subcategory = Category::first();

		//FITNESS GLOVES
		copy(public_path() . '/images/seed/product/1_1a.jpg', Config::get('storage.product') . '/exproduct1.jpg');
		copy(public_path() . '/images/seed/size_example.png', Config::get('storage.guide') . '/exsize.png');
		copy(public_path() . '/images/seed/media/1.jpg', Config::get('storage.media') . '/1.jpg');
		Product::insert([
			'id' => 1,
			'name' => 'Men Solid Basic',
			'is_active' => 1,
			'subcategory_id' => null,
			'description' =>  "We have enhanced our first batch of basic fitness gloves to become SOLID and rigid for you! More colours are available as Zuna Sport is staying true to being youthful and energetic. Palm area is still soft yet strong as promised. So don't forget to bring this SOLID fitness gloves to the gym",
			'feature_material' => '<img src=../media/1.jpg>',
			'guide_image' => 'exsize.png',
			'price_id' => 229500,
			'price_en' => 10,
			'genre' => 'MEN',
			'weight' => '500',
			'created_at' => Carbon::today()->subWeek(),
			'updated_at' => Carbon::today()->subWeek()
		]);

		$sizes = [3, 4, 5];
		$product = Product::find(1);
		$product->sizes()->sync($sizes);

		//CYCLING GLOVES
		copy(public_path() . '/images/seed/product/2_1a.jpg', Config::get('storage.product') . '/exproduct2.jpg');
		copy(public_path() . '/images/seed/size_example.png', Config::get('storage.guide') . '/exsize.png');
		copy(public_path() . '/images/seed/media/2.jpg', Config::get('storage.media') . '/2.jpg');
		Product::insert([
			'id' => 2,
			'name' => 'Ladies Spiderwomen',
			'is_active' => 1,
			'subcategory_id' => $subcategory->id+1,
			'description' => "Who says that ladies don't bike? This funky pair of gloves are designed for the energetic and outgoing ladies out there.
				Lightweight and fliexible top hand fabrics offer maximum comfort and performance.
				Intelligently placed foam help to distribute even pressure to relieve hand fatigue.
				So you don't have to worry about having palm ache the next day!",
			'feature_material' => "<img src=../media/2.jpg>",
			'guide_image' => 'exsize.png',
			'price_id' => 189500,
			'price_en' => 10,
			'genre' => 'WMN',
			'weight' => '500',
			'created_at' => Carbon::today()->subWeek(),
			'updated_at' => Carbon::today()->subWeek()
		]);

		$sizes = [2, 3, 4];
		$product = Product::find(2);
		$product->sizes()->sync($sizes);

		//BOXING GLOVES
		copy(public_path() . '/images/seed/product/3_1a.jpg', Config::get('storage.product') . '/exproduct3.jpg');
		copy(public_path() . '/images/seed/size_example.png', Config::get('storage.guide') . '/exsize.png');
		copy(public_path() . '/images/seed/media/3.jpg', Config::get('storage.media') . '/3.jpg');
		Product::insert([
			'id' => 3,
			'name' => 'Training Boxing',
			'is_active' => 1,
			'subcategory_id' => $subcategory->id+2,
			'description' => "Experience Men Training Boxing Gloves by Zuna Sports with strong padded material at bakchand.
- Designed casually with grey and black colour combination.
- Strong padded material at backhand.
- Available in 1 colour : grey.
- Available in 1 size : freestyle.",
			'feature_material' => "<img src=../media/3.jpg>",
			'guide_image' => 'exsize.png',
			'price_id' => 299500,
			'price_en' => 5,
			'genre' => 'MEN',
			'weight' => '500',
			'created_at' => Carbon::today()->subWeek(),
			'updated_at' => Carbon::today()->subWeek()
		]);

		$sizes = [];
		$product = Product::find(3);
		$product->sizes()->sync($sizes);

		//BILLIARD GLOVES
		copy(public_path() . '/images/seed/product/4_1a.jpg', Config::get('storage.product') . '/exproduct4.jpg');
		copy(public_path() . '/images/seed/size_example.png', Config::get('storage.guide') . '/exsize.png');
		copy(public_path() . '/images/seed/media/4.jpg', Config::get('storage.media') . '/4.jpg');
		Product::insert([
			'id' => 4,
			'name' => 'Billiard Basic',
			'is_active' => 1,
			'subcategory_id' => null,
			'description' => "We did not know that Indonesia loves billiard so much! Here, we create simple and chic billiard gloves for you. so those billiard lovers could look stylish and protect their hands from the friction.
- Made from fine lycra for maximum stretch and flexibility.
- Designed with X-Ray look for trendy and stylish appearance.",
			'feature_material' => "<img src=../media/4.jpg>",
			'guide_image' => 'exsize.png',
			'price_id' => 35000,
			'price_en' => 5,
			'genre' => 'UNI',
			'weight' => '500',
			'created_at' => Carbon::today()->subWeek(),
			'updated_at' => Carbon::today()->subWeek()
		]);

		$sizes = [];
		$product = Product::find(4);
		$product->sizes()->sync($sizes);

		//MULTIFUNCTION GLOVES
		copy(public_path() . '/images/seed/product/5_1a.jpg', Config::get('storage.product') . '/exproduct5.jpg');
		copy(public_path() . '/images/seed/size_example.png', Config::get('storage.guide') . '/exsize.png');
		copy(public_path() . '/images/seed/media/5.jpg', Config::get('storage.media') . '/5.jpg');
		Product::insert([
			'id' => 5,
			'name' => 'Men Air Cushion',
			'is_active' => 1,
			'subcategory_id' => null,
			'description' => "This pair of gloves are designed for any activities ranging from cross country, cycling, fitness and many more! Designed with air cushioned palm material plus thick foam padding, they give maximum protection for your hands. Enforced with Griptex®, they ensure good hand grip, so none will slip away from your hands!",
			'feature_material' => "<img src=../media/5.jpg>",
			'guide_image' => 'exsize.png',
			'price_id' => 149500,
			'price_en' => 5,
			'genre' => 'MEN',
			'weight' => '500',
			'created_at' => Carbon::today()->subWeek(),
			'updated_at' => Carbon::today()->subWeek()
		]);

		$sizes = [3, 4, 5];
		$product = Product::find(5);
		$product->sizes()->sync($sizes);

		//BAG
		$subcategory = Category::where('name', '=', 'Bag')->first()->subcategories()->first();

		//FITNESS
		copy(public_path() . '/images/seed/product/6_1a.jpg', Config::get('storage.product') . '/exproduct6.jpg');
		copy(public_path() . '/images/seed/size_example.jpg', Config::get('storage.guide') . '/exsize.jpg');
		copy(public_path() . '/images/seed/media/6.jpg', Config::get('storage.media') . '/6.jpg');
		Product::insert([
			'id' => 6,
			'name' => 'Bold Bag',
			'is_active' => 1,
			'subcategory_id' => null,
			'description' => "For those who don’t love plain, this is the perfect gym or outdoor bag for you! Suitable for men and ladies, they carry more than you can imagine! And don’t worry about the wet weather, as they are all water repellent and easy to wash.",
			'feature_material' => "<img src=../media/6.jpg>",
			'guide_image' => 'exsize.jpg',
			'price_id' => 359500,
			'price_en' => 10,
			'genre' => 'UNI',
			'weight' => '500',
			'created_at' => Carbon::today()->subWeek(),
			'updated_at' => Carbon::today()->subWeek()
		]);

		//MULTIFUNCTION
		copy(public_path() . '/images/seed/product/7_1a.jpg', Config::get('storage.product') . '/exproduct7.jpg');
		copy(public_path() . '/images/seed/size_example1.jpg', Config::get('storage.guide') . '/exsize1.jpg');
		copy(public_path() . '/images/seed/media/7.jpg', Config::get('storage.media') . '/7.jpg');
		Product::insert([
			'id' => 7,
			'name' => 'Hipster Bag',
			'is_active' => 1,
			'subcategory_id' =>null,
			'description' => "Get everything packed with Hipster Multifunction Bag by Zuna Sports and get surprised to see the number of objects that can be inside this bag. School, camping, hiking, travelling, working, YOU NAME IT! This bag is a magic bag and you can carry as many tools as you want, you need! Adding on with water repellent materials, this bag is known to be sturdy and strong as a bull",
			'feature_material' => "<img src=../media/7.jpg>",
			'guide_image' => 'exsize1.jpg',
			'price_id' => 399500,
			'price_en' => 5,
			'genre' => 'UNI',
			'weight' => '500',
			'created_at' => Carbon::today()->subWeek(),
			'updated_at' => Carbon::today()->subWeek()
		]);

	}

}
