<?php

use Illuminate\Database\Seeder;

use App\Models\Product;
use App\Models\ProductImage;
use App\Models\Category;
use Carbon\Carbon;

class ZunaProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = file_get_contents(storage_path('zuna_products.json'));
        $obj = json_decode($file, true);
        $genres = ['MEN','WMN','UNI'];

        usort($obj, function($a, $b) {
            return $a['category'] <=> $b['category'];
        });

        foreach ($obj as $key => $value) {
            $category = Category::where('name', $value['category'])->first();

            if ($category) {
                $slug = Illuminate\Support\Str::slug($value['title']);
                $image = public_path() . '/images/seeds/'.$value['image'];
                $genre = $genres[rand(0,2)];

                if ($image) {
                    copy(public_path() . '/images/seeds/'.$value['image'], Config::get('storage.product') . "/$slug.jpg");

                    $product = Product::create([
                        'id' => $key+1,
                        'name' => $value['title'],
                        'slug' => $slug,
                        'is_active' => 1,
                        'category_id' => $category->id,
                        'disc_percent' => 0,
                        'description' =>  $value['description'],
                        'description_html' =>  html_entity_decode($value['description']),
                        'feature_material' => '<img src=../media/1.jpg>',
                        'feature_html' => '<img src=../media/1.jpg>',
                        'guide_image' => "$slug.jpg",
                        'price_id' => $value['price'],
                        'price_en' => 10,
                        'genre' => $genre,
                        'weight' => '500',
                        'created_at' => Carbon::today()->subWeek(),
                        'updated_at' => Carbon::today()->subWeek()
                    ]);

                    ProductImage::create([
                        'product_id' => $product->id,
                        'image' => "$slug.jpg"
                    ]);
                }
            }
        }
    }
}
