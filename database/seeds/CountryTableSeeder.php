<?php

use Illuminate\Database\Seeder;
use App\Models\Country;

class CountryTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$countries = [
			'Indonesia',
			'Malaysia',
			'Singapura',
			'Thailand',
			'Brunei Darussalam',
			'Vietnam',
			'China',
			'Japan',
			'United Kingdom',
			'England',
			'Australia'
		];

		foreach ($countries as $key => $country) {
			Country::insert([
				'id' => $key + 1,
				'name' => $country
			]);
		}
	}
}
