<?php

use Illuminate\Database\Seeder;
use App\Models\Product;
use App\Models\ProductColor;
use App\Models\ProductColorImage;

class ProductColorTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$product = Product::active()->first();

		copy(public_path() . '/images/seed/color/1_Red.jpg', Config::get('storage.color') . '/1_Red.jpg');
		copy(public_path() . '/images/seed/product/1_1a.jpg', Config::get('storage.product') . '/1_1a.jpg');
		ProductColor::insert([
			'product_id' => $product->id,
			'name' => 'Red',
			'type' => 'COLOR',
			'color_image' => '1_Red.jpg',
			'product_image' => '1_1a.jpg',
			'stock_out' => 0
		]);

		copy(public_path() . '/images/seed/color/1_Black.jpg', Config::get('storage.color') . '/1_Black.jpg');
		copy(public_path() . '/images/seed/product/1_2a.jpg', Config::get('storage.product') . '/1_2a.jpg');
		ProductColor::insert([
			'product_id' => $product->id,
			'name' => 'Black',
			'type' => 'COLOR',
			'color_image' => '1_Black.jpg',
			'product_image' => '1_2a.jpg',
			'stock_out' => 0
		]);

		copy(public_path() . '/images/seed/color/1_White.jpg', Config::get('storage.color') . '/1_White.jpg');
		copy(public_path() . '/images/seed/product/1_3a.jpg', Config::get('storage.product') . '/1_3a.jpg');
		ProductColor::insert([
			'product_id' => $product->id,
			'name' => 'White',
			'type' => 'COLOR',
			'color_image' => '1_White.jpg',
			'product_image' => '1_3a.jpg',
			'stock_out' => 0
		]);

		copy(public_path() . '/images/seed/color/2_Green.jpg', Config::get('storage.color') . '/2_Green.jpg');
		copy(public_path() . '/images/seed/product/2_1a.jpg', Config::get('storage.product') . '/2_1a.jpg');
		ProductColor::insert([
			'product_id' => $product->id + 1,
			'name' => 'Green',
			'type' => 'COLOR',
			'color_image' => '2_Green.jpg',
			'product_image' => '2_1a.jpg',
			'stock_out' => 0
		]);

		copy(public_path() . '/images/seed/color/2_Pink.jpg', Config::get('storage.color') . '/2_Pink.jpg');
		copy(public_path() . '/images/seed/product/2_2a.jpg', Config::get('storage.product') . '/2_2a.jpg');
		ProductColor::insert([
			'product_id' => $product->id + 1,
			'name' => 'Pink',
			'type' => 'COLOR',
			'color_image' => '2_Pink.jpg',
			'product_image' => '2_2a.jpg',
			'stock_out' => 0
		]);

		copy(public_path() . '/images/seed/color/2_White.jpg', Config::get('storage.color') . '/2_White.jpg');
		copy(public_path() . '/images/seed/product/2_3a.jpg', Config::get('storage.product') . '/2_3a.jpg');
		ProductColor::insert([
			'product_id' => $product->id + 1,
			'name' => 'White',
			'type' => 'COLOR',
			'color_image' => '2_White.jpg',
			'product_image' => '2_3a.jpg',
			'stock_out' => 0
		]);

		copy(public_path() . '/images/seed/color/3_Blue.jpg', Config::get('storage.color') . '/3_Blue.jpg');
		copy(public_path() . '/images/seed/product/3_1a.jpg', Config::get('storage.product') . '/3_1a.jpg');
		ProductColor::insert([
			'product_id' => $product->id + 2,
			'name' => 'Blue',
			'type' => 'COLOR',
			'color_image' => '3_Blue.jpg',
			'product_image' => '3_1a.jpg',
			'stock_out' => 0
		]);

		copy(public_path() . '/images/seed/color/4_Black.jpg', Config::get('storage.color') . '/4_Black.jpg');
		copy(public_path() . '/images/seed/product/4_1a.jpg', Config::get('storage.product') . '/4_1a.jpg');
		ProductColor::insert([
			'product_id' => $product->id + 3,
			'name' => 'Black',
			'type' => 'COLOR',
			'color_image' => '4_Black.jpg',
			'product_image' => '4_1a.jpg',
			'stock_out' => 0
		]);

		copy(public_path() . '/images/seed/color/4_Blue.jpg', Config::get('storage.color') . '/4_Blue.jpg');
		copy(public_path() . '/images/seed/product/4_2a.jpg', Config::get('storage.product') . '/4_2a.jpg');
		ProductColor::insert([
			'product_id' => $product->id + 3,
			'name' => 'Blue',
			'type' => 'COLOR',
			'color_image' => '4_Blue.jpg',
			'product_image' => '4_2a.jpg',
			'stock_out' => 0
		]);

		copy(public_path() . '/images/seed/color/4_Navy.jpg', Config::get('storage.color') . '/4_Navy.jpg');
		copy(public_path() . '/images/seed/product/4_3a.jpg', Config::get('storage.product') . '/4_3a.jpg');
		ProductColor::insert([
			'product_id' => $product->id + 3,
			'name' => 'Navy',
			'type' => 'COLOR',
			'color_image' => '4_Navy.jpg',
			'product_image' => '4_3a.jpg',
			'stock_out' => 0
		]);

		copy(public_path() . '/images/seed/color/4_Yellow.jpg', Config::get('storage.color') . '/4_Yellow.jpg');
		copy(public_path() . '/images/seed/product/4_4a.jpg', Config::get('storage.product') . '/4_4a.jpg');
		ProductColor::insert([
			'product_id' => $product->id + 3,
			'name' => 'Yellow',
			'type' => 'COLOR',
			'color_image' => '4_Yellow.jpg',
			'product_image' => '4_4a.jpg',
			'stock_out' => 0
		]);

		copy(public_path() . '/images/seed/color/5_Green.jpg', Config::get('storage.color') . '/5_Green.jpg');
		copy(public_path() . '/images/seed/product/5_1a.jpg', Config::get('storage.product') . '/5_1a.jpg');
		ProductColor::insert([
			'product_id' => $product->id + 4,
			'name' => 'Green',
			'type' => 'COLOR',
			'color_image' => '5_Green.jpg',
			'product_image' => '5_1a.jpg',
			'stock_out' => 0
		]);

		copy(public_path() . '/images/seed/color/5_Grey.jpg', Config::get('storage.color') . '/5_Grey.jpg');
		copy(public_path() . '/images/seed/product/5_2a.jpg', Config::get('storage.product') . '/5_2a.jpg');
		ProductColor::insert([
			'product_id' => $product->id + 4,
			'name' => 'Grey',
			'type' => 'COLOR',
			'color_image' => '5_Grey.jpg',
			'product_image' => '5_2a.jpg',
			'stock_out' => 0
		]);

		copy(public_path() . '/images/seed/color/5_Red.jpg', Config::get('storage.color') . '/5_Red.jpg');
		copy(public_path() . '/images/seed/product/5_3a.jpg', Config::get('storage.product') . '/5_3a.jpg');
		ProductColor::insert([
			'product_id' => $product->id + 4,
			'name' => 'Red',
			'type' => 'COLOR',
			'color_image' => '5_Red.jpg',
			'product_image' => '5_3a.jpg',
			'stock_out' => 0
		]);

		copy(public_path() . '/images/seed/color/6_Purple.jpg', Config::get('storage.color') . '/6_Purple.jpg');
		copy(public_path() . '/images/seed/product/6_1a.jpg', Config::get('storage.product') . '/6_1a.jpg');
		ProductColor::insert([
			'product_id' => $product->id + 5,
			'name' => 'Purple',
			'type' => 'TEXTURE',
			'color_image' => '6_Purple.jpg',
			'product_image' => '6_1a.jpg',
			'stock_out' => 0
		]);

		copy(public_path() . '/images/seed/color/6_Green.jpg', Config::get('storage.color') . '/6_Green.jpg');
		copy(public_path() . '/images/seed/product/6_2a.jpg', Config::get('storage.product') . '/6_2a.jpg');
		ProductColor::insert([
			'product_id' => $product->id + 5,
			'name' => 'Green',
			'type' => 'TEXTURE',
			'color_image' => '6_Green.jpg',
			'product_image' => '6_2a.jpg',
			'stock_out' => 0
		]);

		copy(public_path() . '/images/seed/color/6_White.jpg', Config::get('storage.color') . '/6_White.jpg');
		copy(public_path() . '/images/seed/product/6_3a.jpg', Config::get('storage.product') . '/6_3a.jpg');
		ProductColor::insert([
			'product_id' => $product->id + 5,
			'name' => 'White',
			'type' => 'TEXTURE',
			'color_image' => '6_White.jpg',
			'product_image' => '6_3a.jpg',
			'stock_out' => 0
		]);

		copy(public_path() . '/images/seed/color/7_Blue.jpg', Config::get('storage.color') . '/7_Blue.jpg');
		copy(public_path() . '/images/seed/product/7_1a.jpg', Config::get('storage.product') . '/7_1a.jpg');
		ProductColor::insert([
			'product_id' => $product->id + 6,
			'name' => 'Blue',
			'type' => 'COLOR',
			'color_image' => '7_Blue.jpg',
			'product_image' => '7_1a.jpg',
			'stock_out' => 0
		]);

		copy(public_path() . '/images/seed/color/7_Red.jpg', Config::get('storage.color') . '/7_Red.jpg');
		copy(public_path() . '/images/seed/product/7_2a.jpg', Config::get('storage.product') . '/7_2a.jpg');
		ProductColor::insert([
			'product_id' => $product->id + 6,
			'name' => 'Red',
			'type' => 'COLOR',
			'color_image' => '7_Red.jpg',
			'product_image' => '7_2a.jpg',
			'stock_out' => 0
		]);

		$products = Product::get();
        foreach ($products as $key => $product) {
            foreach ($product->colors as $color) {
            	$color->sku=$product->sku;
                $color->save();
                ProductColorImage::insert([
                    [
                        'product_color_id' => $color->id,
                        'image' => $color->product_image
                    ]
                ]);
            } 
        }


		$productcolor = ProductColor::first();
        
        copy(public_path() . '/images/seed/product/6_1b.jpg', Config::get('storage.product') . '/6_1b.jpg');
		ProductColorImage::insert([
			'product_color_id' => $productcolor->id + 14,
			'image' => '6_1b.jpg'
		]);

		copy(public_path() . '/images/seed/product/6_1c.jpg', Config::get('storage.product') . '/6_1c.jpg');
		ProductColorImage::insert([
			'product_color_id' => $productcolor->id + 14,
			'image' => '6_1c.jpg'
		]);

		copy(public_path() . '/images/seed/product/6_2b.jpg', Config::get('storage.product') . '/6_2b.jpg');
		ProductColorImage::insert([
			'product_color_id' => $productcolor->id + 15,
			'image' => '6_2b.jpg'
		]);

		copy(public_path() . '/images/seed/product/6_2c.jpg', Config::get('storage.product') . '/6_2c.jpg');
		ProductColorImage::insert([
			'product_color_id' => $productcolor->id + 15,
			'image' => '6_2c.jpg'
		]);

		copy(public_path() . '/images/seed/product/6_3b.jpg', Config::get('storage.product') . '/6_3b.jpg');
		ProductColorImage::insert([
			'product_color_id' => $productcolor->id + 16,
			'image' => '6_3b.jpg'
		]);

		copy(public_path() . '/images/seed/product/6_3c.jpg', Config::get('storage.product') . '/6_3c.jpg');
		ProductColorImage::insert([
			'product_color_id' => $productcolor->id + 16,
			'image' => '6_3c.jpg'
		]);

		copy(public_path() . '/images/seed/product/7_1b.jpg', Config::get('storage.product') . '/7_1b.jpg');
		ProductColorImage::insert([
			'product_color_id' => $productcolor->id + 17,
			'image' => '7_1b.jpg'
		]);

		copy(public_path() . '/images/seed/product/7_1c.jpg', Config::get('storage.product') . '/7_1c.jpg');
		ProductColorImage::insert([
			'product_color_id' => $productcolor->id + 17,
			'image' => '7_1c.jpg'
		]);

		copy(public_path() . '/images/seed/product/7_2b.jpg', Config::get('storage.product') . '/7_2b.jpg');
		ProductColorImage::insert([
			'product_color_id' => $productcolor->id + 18,
			'image' => '7_2b.jpg'
		]);

		copy(public_path() . '/images/seed/product/7_2c.jpg', Config::get('storage.product') . '/7_2c.jpg');
		ProductColorImage::insert([
			'product_color_id' => $productcolor->id + 18,
			'image' => '7_2c.jpg'
		]);


		
	}
}
