<?php

use Illuminate\Database\Seeder;
use League\CommonMark\CommonMarkConverter;
use App\Models\Blog;

class BlogTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$converter = new CommonMarkConverter();
		Blog::insert([
			[
				'id' => 1,
				'title' => 'Welcome To Our Blogs',
				'permalink' => 'welcome-to-our-blogs',
				'content' => "# Welcome To Our Blog
## What is lorem ipsum?
**Lorem Ipsum** is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
",
				'content_html' => $converter->convertToHtml("# Welcome To Our Blog
## What is lorem ipsum?
**Lorem Ipsum** is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
"),
				'publish_state' => 'PUB',
				'publish_at' => date('Y-m-d H:i:s'),
				'writen_by' => 1,
				'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
			],
		]);
	}
}

