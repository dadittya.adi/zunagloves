<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductColorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_colors', function(Blueprint $table)
        {
        	$table->increments('id');
        	$table->integer('product_id')->unsigned();
        	$table->string('name', 127)->index();
        	$table->string('type', 7)->index();
        	$table->string('color_image', 127);
            $table->string('product_image', 127);
        	$table->timestamps();

        	$table->foreign('product_id')
        		->references('id')
        		->on('products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('product_colors');
    }
}
