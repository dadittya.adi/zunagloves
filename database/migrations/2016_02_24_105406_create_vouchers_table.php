<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVouchersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vouchers', function(Blueprint $table)
        {
        	$table->increments('id');
        	$table->string('code', 5)->index();
        	$table->string('promo', 255);
        	$table->decimal('discount', 15, 2);
        	$table->datetime('start_date');
        	$table->datetime('end_date');
        	$table->decimal('minimum', 15, 2)->default(0);
        	$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('vouchers');
    }
}
