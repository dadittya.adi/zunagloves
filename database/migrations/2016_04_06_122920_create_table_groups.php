<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\Models\Group;

class CreateTableGroups extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('groups', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('code');
            $table->timestamps();
        });

        Group::insert([
            [
                'name' => 'Admin',
                'code' => 'ADM'
            ],
            [
                'name' => 'Product',
                'code' => 'PRD'
            ],
            [
                'name' => 'Order',
                'code' => 'ORD'
            ],
            [
                'name' => 'Blog',
                'code' => 'BLO'
            ],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('groups');
    }
}
