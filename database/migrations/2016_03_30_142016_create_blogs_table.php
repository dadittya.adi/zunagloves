<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blogs', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('title', 255);
            $table->string('permalink', 255)->index();
            $table->text('content');
            $table->string('publish_state');
            $table->datetime('publish_at');
            $table->integer('writen_by')->unsigned();
            $table->timestamps();
            $table->foreign('writen_by')
                ->references('id')
                ->on('admins');
        });;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('blogs');
    }
}
