<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShippingPricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipping_prices', function(Blueprint $table)
        {
        	$table->increments('id');
        	$table->integer('shipping_id')->unsigned();
        	$table->string('destination', 255);
        	$table->decimal('price', 15, 2);
        	$table->string('time', 32);
        	$table->timestamps();

        	$table->foreign('shipping_id')
        		->references('id')
        		->on('shippings')
        		->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('shipping_prices');
    }
}
