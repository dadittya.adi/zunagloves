<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShippingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shippings', function(Blueprint $table)
        {
        	$table->increments('id');
        	$table->integer('country_id')->unsigned();
        	$table->string('name', 255);
        	$table->string('display_name', 255);
        	$table->integer('show_order');
        	$table->timestamps();

        	$table->foreign('country_id')
        		->references('id')
        		->on('countries');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('shippings');
    }
}
