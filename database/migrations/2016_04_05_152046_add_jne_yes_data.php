<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\Models\Shipping;
use App\Models\ShippingPrice;
use App\Models\Country;


use League\Csv\Reader;

class AddJneYesData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (App::environment('local'))
            return;
        
        try {
                $csv = Reader::createFromPath(app_path().'/../docs/tarif_jne.csv');                
                
            } catch (Exception $e) {
                return redirect()->action('Admin\ShippingController@index')
                ->with('No_File', true);
            }

            $shipping = new Shipping();
            $shipping->fill([
                'name' => 'JNE YES (Yakin Esok Sampai)',
                'display_name' => 'JNE YES',
                'country_id' => 1
            ]);

            $max = Shipping::max('show_order');
            if ($max == null)
                $max = 1;
            else
                $max += 1;

            $shipping->show_order = $max;

            $shipping->save();
            $csv->setOffset(1); //because we don't want to insert the header
            //var_dump($csv);die;
            $nbInsert = $csv->each(function ($row) use($shipping) {
                $data = explode(";", $row[0]);
                $Destinasi = $data[2].'('.$data[1].')';
                $Tarif = intval($data[9]);
                if($Tarif > 0){
                    $price = new ShippingPrice();
                    $price->fill([
                        'destination' => $Destinasi,
                        'price' => $Tarif,
                        'time' => 1
                    ]);

                    $shipping->prices()->save($price);
                }
            
            return true;
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
