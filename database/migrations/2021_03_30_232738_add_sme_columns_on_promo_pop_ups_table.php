<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSmeColumnsOnPromoPopUpsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('promo_pop_ups', function (Blueprint $table) {
            $table->integer('valid_in')->default(30);
            $table->integer('code_length')->default(5);
            $table->string('promo', 255);
            $table->string('type')->nullable();
        	$table->decimal('discount', 15, 2);
        	$table->decimal('minimum', 15, 2)->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('promo_pop_ups', function (Blueprint $table) {
            $table->dropColumn('valid_in');
            $table->dropColumn('code_length');
            $table->dropColumn('promo');
            $table->dropColumn('type');
        	$table->dropColumn('discount');
        	$table->dropColumn('minimum');
        });
    }
}
