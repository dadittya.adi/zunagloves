<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\AdminGroup;
use App\Models\Admin;

class CreateTableAdminGroups extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_groups', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('group_id')->unsigned();
            $table->integer('admin_id')->unsigned()->index();            
            $table->timestamps();
        });

        $admins = Admin::get();
        foreach ($admins as $key => $admin) {
            AdminGroup::insert([
                [
                    'group_id' => 1,
                    'admin_id' => $admin->id
                ]
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('admin_groups');
    }
}
