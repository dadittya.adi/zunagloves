<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SetFieldNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->string('destination')->nullable()->change();
            $table->decimal('shipping_price', 15, 2)->nullable()->change();
            $table->string('payment_method')->nullable()->change();
            $table->string('order_number')->nullable()->change();
            $table->dateTime('paid_at')->nullable()->change();
            $table->text('payment_data')->nullable()->change();
            //$table->string('snap_token')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
