<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Productsize;

class AddColumnOrderProductsizes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('productsizes', function (Blueprint $table) {
            $table->integer('order');
        });

        $sizes = Productsize::all();
        foreach ($sizes as $key => $size) {
            $size->order = $key+1;
            $size->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('productsizes', function (Blueprint $table) {
            $table->dropColumn('order');
        });
    }
}
