<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsProductsizesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products_productsizes', function(Blueprint $table) 
        {
        	$table->increments('id');
        	$table->integer('product_id')->unsigned();
        	$table->integer('productsize_id')->unsigned();
        	$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('products_productsizes');
    }
}
