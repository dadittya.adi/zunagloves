<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnToOrderItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_items', function (Blueprint $table) {
            $table->integer('product_productsize_id')->unsigned();
            $table->bigInteger('product_color_id')->unsigned();

            $table->foreign('product_productsize_id')
        		->references('id')
        		->on('products_productsizes');

            $table->foreign('product_color_id')
        		->references('id')
        		->on('product_colors');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_items', function (Blueprint $table) {
            //
        });
    }
}
