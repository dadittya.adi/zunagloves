<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Product;
use App\Models\ProductColor;

class AddSkuProductColor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_colors', function (Blueprint $table) {
            $table->string('sku', 127)->nullable();
        });

        $products = Product::get();
        foreach ($products as $key => $product) {
            foreach ($product->colors as $color) {
                $color->sku=$product->sku;
                $color->save();
            }
            
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_colors', function (Blueprint $table) {
            $table->dropColumn('sku');
        });
    }
}
