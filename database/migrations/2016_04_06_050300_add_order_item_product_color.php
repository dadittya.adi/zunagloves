<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Product;
use App\Models\ProductColor;

class AddOrderItemProductColor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::delete('DELETE FROM order_items');
        DB::delete('DELETE FROM orders');
        Schema::table('order_items', function (Blueprint $table) {
            $table->integer('productcolor_id')->unsigned();

            $table->foreign('productcolor_id')
                ->references('id')
                ->on('product_colors');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_items', function (Blueprint $table) {
            $table->dropForeign('order_items_productcolor_id_foreign');
            $table->dropColumn('productcolor_id');
        });
    }
}
