<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategorySubcategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category_subcategories', function(Blueprint $table)
        {
        	$table->increments('id');
        	$table->integer('category_id')->unsigned();
        	$table->string('name', 127);
        	//$table->string('icon', 127);
        	$table->timestamps();

        	$table->foreign('category_id')
        		->references('id')
        		->on('categories')
        		->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('category_subcategories');
    }
}
