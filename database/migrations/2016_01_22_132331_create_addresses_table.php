<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addresses', function(Blueprint $table) 
        {
        	$table->increments('id');
        	$table->string('full_name', 255);
        	$table->string('phone', 255);
        	$table->string('organization', 255)->nullable();
        	$table->text('address');
        	$table->string('city', 255);
        	$table->string('province', 255);
        	$table->string('country', 255);
        	$table->string('postal_code', 255);
        	$table->string('type', 4);
        	$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('addresses');
    }
}
