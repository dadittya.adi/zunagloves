<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activities', function(Blueprint $table)
        {
            $table->increments('id');
            $table->decimal('user_id', 5)->nullable();
            $table->string('user_name', 128)->nullable();
            $table->string('ip_address', 127);
            $table->text('user_agent');
            $table->text('message');
            $table->string('action', 32)->index();
            $table->string('type', 32)->index();
            $table->boolean('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('activities');
    }
}
