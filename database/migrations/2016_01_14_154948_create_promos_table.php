<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePromosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promos', function(Blueprint $table)
        {
        	$table->increments('id');
        	$table->string('image', 127);
        	$table->string('mimetype', 127);
        	$table->string('title', 127);
        	$table->string('link', 255)->nullable();
        	$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::drop('promos');
    }
}
