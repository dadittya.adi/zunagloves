<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddImageDetailToJumbotronsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('jumbotrons', function (Blueprint $table) {
            $table->string('image_detail', 127)->nullable();
            $table->string('image_detail_mimetype', 127)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('jumbotrons', function (Blueprint $table) {
            $table->dropColumn('image_detail');
            $table->dropColumn('image_detail_mimetype');
        });
    }
}
