<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use League\CommonMark\CommonMarkConverter;
use App\Models\Blog;
use App\Models\Admin;

class AddBlogsData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $converter = new CommonMarkConverter();
        $admin = Admin::orderBy('id')->first();

        if ($admin){
            $blog = new Blog();
            $blog->fill([
                'id' => 1,
                'title' => 'Welcome To Our Blogs',
                'permalink' => 'welcome-to-our-blogs',
                'content' => "# Welcome To Our Blog
## What is lorem ipsum?
**Lorem Ipsum** is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
",
                'content_html' => $converter->convertToHtml("# Welcome To Our Blog
## What is lorem ipsum?
**Lorem Ipsum** is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
"),
                'publish_state' => 'PUB',
                'publish_at' => date('Y-m-d H:i:s'),
                'writen_by' => $admin->id,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]);

            $blog->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
