<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductStocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_stocks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('product_id')->unsigned();
            $table->integer('product_color_id')->unsigned();
            $table->integer('product_productsize_id')->unsigned();
            $table->integer('stock')->default(0);
            $table->timestamps();

            $table->foreign('product_id')
                ->references('id')
                ->on('products');
            
            $table->foreign('product_color_id')
                ->references('id')
                ->on('product_colors');
            
            $table->foreign('product_productsize_id')
                ->references('id')
                ->on('products_productsizes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_stocks');
    }
}
