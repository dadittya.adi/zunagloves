<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function(Blueprint $table)
        {
        	$table->increments('id');
        	$table->integer('category_id')->unsigned();
        	$table->string('name', 255);
        	$table->boolean('is_active');
        	$table->string('sku', 127)->nullable();
        	$table->string('genre', 3);
        	$table->text('description');
        	$table->text('feature_material')->nullable();
        	$table->string('guide_image')->nullable();
            $table->boolean('stock_out')->default(0);
        	$table->decimal('price_id', 15, 2);
        	$table->decimal('price_en', 15, 2)->nullable();
        	$table->decimal('disc_percent', 5, 2);
        	$table->datetime('start_discount')->nullable();
        	$table->datetime('end_discount')->nullable();
            $table->string('status', 100)->nullable();
            $table->datetime('launch_date')->nullable();
        	$table->integer('weight');
        	$table->timestamps();

        	$table->foreign('category_id')
        		->references('id')
        		->on('categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('products');
    }
}
