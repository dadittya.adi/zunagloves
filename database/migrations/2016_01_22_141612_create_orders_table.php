<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function(Blueprint $table)
        {
        	$table->increments('id');
        	$table->string('domain', 2);
        	$table->string('order_number', 14);
        	$table->integer('customer_id')->unsigned();
        	$table->string('state', 3);
        	$table->integer('revision')->unsigned();
        	$table->integer('billing_addr')->unsigned();
        	$table->integer('shipping_addr')->unsigned();
        	$table->boolean('use_billing_addr');
        	$table->string('provider', 255);
        	$table->string('destination', 255);
        	$table->decimal('shipping_price', 15, 2);
        	$table->string('shipping_time', 32);
        	$table->integer('weight')->default(1);
        	$table->integer('voucher_id')->unsigned()->nullable();
        	$table->string('voucher_code', 5)->nullable();
        	$table->string('voucher_promo', 255)->nullable();
        	$table->decimal('voucher_discount', 15, 2)->default(0);
        	$table->decimal('voucher_minimum', 15, 2)->default(0);
        	$table->string('payment_method', 3);
        	$table->datetime('paid_at');
        	$table->decimal('paid_amount', 15, 4)->default(0);
        	$table->text('paid_notes')->nullable();
        	$table->string('bank_acc', 255)->nullable();
        	$table->string('name_acc', 255)->nullable();
        	$table->string('no_acc', 255)->nullable();
        	$table->string('banknote_image', 255)->nullable();
        	$table->text('work_notes')->nullable();
        	$table->text('delivery_notes')->nullable();
        	$table->text('reject_notes')->nullable();
        	$table->text('payment_data');
        	$table->timestamps();

        	$table->foreign('customer_id')
        		->references('id')
        		->on('customers');

        	$table->foreign('billing_addr')
        		->references('id')
        		->on('addresses');

        	$table->foreign('shipping_addr')
        		->references('id')
        		->on('addresses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('orders');
    }
}
