<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_items', function(Blueprint $table)
        {
        	$table->increments('id');
        	$table->integer('order_id')->unsigned();
        	$table->integer('product_id')->unsigned();
        	$table->string('color', 32)->nullable();
        	$table->string('size', 32)->nullable();
        	$table->decimal('price', 15, 2);
        	$table->integer('qty');
        	$table->decimal('disc_percent', 15, 2)->default(0);
        	$table->decimal('discount', 15, 2)->default(0);
        	$table->decimal('total', 15, 2);
        	$table->text('notes')->nullable();
        	$table->timestamps();

        	$table->foreign('order_id')
        		->references('id')
        		->on('orders');

        	$table->foreign('product_id')
        		->references('id')
        		->on('products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('order_items');
    }
}
