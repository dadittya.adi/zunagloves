<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Product;
use App\Models\ProductColorImage;

class CreateProductColorImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_color_images', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('product_color_id')->unsigned();
            $table->string('image', 127);
            $table->timestamps();

            $table->foreign('product_color_id')
                ->references('id')
                ->on('product_colors');
        });

        $products = Product::get();
        foreach ($products as $key => $product) {
            foreach ($product->colors as $color) {
                ProductColorImage::insert([
                    [
                        'product_color_id' => $color->id,
                        'image' => $color->product_image
                    ]
                ]);
            }
            
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('product_color_images');
    }
}
