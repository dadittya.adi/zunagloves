$(function() {
	var providers = JSON.parse($('#providerData').val());
	var shippings = JSON.parse($('#shippingData').val());
	var dest_selected = null;

	function renderProvider() 
	{
		var country = $('#ship_country').val();

		var shippings = providers[country];
		if (shippings == undefined) {
			$('#no_provider').removeClass('hidden');
			$('.shipping-method').addClass('hidden');
			$('.btn-order').attr('disabled', true);
		} else {
			$('#no_provider').addClass('hidden');
			$('.shipping-method').removeClass('hidden');
			$('.btn-order').removeAttr('disabled');
		}

		var tmpl = $('#providerTmpl').html();
		Mustache.parse(tmpl);
		var html = Mustache.render(tmpl, { 'data' : shippings });
		$('#provider').html(html);

		renderShipping();
	}

	function renderShipping()
	{
		var provider = $('#provider option:selected').val();
		var shipping = shippings[provider];
		
		$('#tags').val('');
		$('#dest_id').val(null);

		for (idx in shipping) {
			shipping[idx]['label'] = shipping[idx].destination;
			shipping[idx]['value'] = shipping[idx].destination;
		}

		var availableTags = shipping;
	    //console.log(availableTags);
	    $( "#tags" ).autocomplete({
	      source: availableTags,
	      minLength: 3,
	      select: function( event, ui ) {
	      	dest_selected = ui.item.id;
	      	$('#dest_id').val(ui.item.id);
	      	render();
	      }
	    });
		var tmpl = $('#destinationTmpl').html();
		Mustache.parse(tmpl);
		var html = Mustache.render(tmpl, { 'data' : shipping });
		$('#destination').html(html);

		render();
	}

	function render() 
	{
		var provider = $('#provider option:selected').val();
		var shipping = shippings[provider];
		///var destination = $('#destination option:selected').val();
		var destination = dest_selected;
		var subtotal = $('#calc_subtotal').val();
		var price = 0;

		var weight = parseInt($('#calc_weight').data('weight'), 10);
		if (isNaN(weight))
			weight = 1;
		else
			weight = Math.ceil(weight/1000);

		for (var i in shipping) {
			if (shipping[i].id == destination) {
				var price = shipping[i].price * weight;
				$('#shipping_time').text(shipping[i].time + ' day(s)');
				$('#shipping_price').text(currency(price));
				break;
			}
		}

		//calculating total
		var discount = $('#calc_total').data('discount');
		var total = parseFloat(subtotal) + price - discount;

		$('#calc_weight').html(weight);
		$('#calc_shipping').html(currency(price));
		$('#calc_total').html(currency(total));
	}

	function currency(obj)
	{
		var domain = $('#domain').val();
		var currency = 'IDR';

		if (domain == 'en')
			currency = 'SIG';

		return currency + ' ' + obj.formatMoney(domain, 2);
	}

	function sameAs() 
	{
		var use = $('#use_billing_address');
		if (use.prop('checked')){
			$('#shipping').addClass("hidden");
			$('#shipping').find(':input').removeAttr('required');
		} else {
			$('#shipping').removeClass("hidden");
			$('#shipping').find(':input').attr('required', true);
			$('#ship_organization').removeAttr('required');
		}

		renderProvider();
	}

	$('#use_billing_address').on('click', sameAs);
	$('#bill_country').on('click', renderProvider);
	$('#ship_country').on('click', renderProvider);
	$('#provider').on('click', renderShipping);
	$('#destination').on('click', render);

	renderProvider();
	sameAs();
});