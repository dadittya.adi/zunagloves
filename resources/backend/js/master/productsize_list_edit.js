$(function() {
	var sizes = JSON.parse($('#sizeData').val());
	var errors = JSON.parse($('#errorMsg').val());
	var error_id = parseInt($('#errorId').val(), 10);

	function render()
	{
		for (var i = 0; i < sizes.length; i++) {
			sizes[i]._no = i + 1;
			sizes[i]._row = i;
			sizes[i].dis_min = i == 0 ? 'disabled' : '';
			sizes[i].dis_max = i == sizes.length - 1 ? 'disabled' : '';
		}

		var data = {data : sizes}
		var tmpl = $('#sizeTmpl').html();
		var html = Mustache.render(tmpl, data);

		Mustache.parse(tmpl);
		$('#sizeTable').html(html);

		$('.btn-edit-item').on('click', edit);
		$('.btn-cancel-item').on('click', cancel);
		$('.btn-save-item').on('click', save);
	}

	function edit()
	{
		var row = parseInt($(this).data('row'), 10);
		sizes[row]._edit_state = 1;
		
		render();
		$('.btn-edit-item').addClass('hidden');
		$('.btn-delete-item').addClass('hidden');
	}

	function cancel()
	{
		var row = parseInt($(this).data('row'), 10);
		sizes[row]._edit_state = 0;
		
		render();
	}

	function save()
	{
		var row = $(this).data('row');
		var name = $('#name_' + row).val();
		var short_name = $('#shortName_' + row).val();

		$('#name_edit_' + row).val(name);
		$('#shortName_edit_' + row).val(short_name);

		$('#update_' + row).submit();
	}

	function checkError()
	{
		if (isNaN(error_id))
			return;

		$('#edit_' + error_id).click();

		for (var field in errors) {
			$('#col_' + field + '_' + error_id).addClass('has-error');
			$('#error_' + field + '_' + error_id).removeClass('hidden');
			$('#error_' + field + '_' + error_id).html(errors[field][0]);
		}
	}

	render();
	checkError();
});