$(function() {
	var data = JSON.parse($('#inputSize').val());
	var all_sizes = JSON.parse($('#sizeData').val());
	var sizes = [];

	function compileData()
	{
		sizes = [];
		for (var i = 0; i < all_sizes.length; i++) {
			var found = false;
			for (var j = 0; j < data.length; j++) {
				if (all_sizes[i].id === data[j].id) {
					found = true;
					break;
				}
			}
			if (!found) {
				sizes.push(all_sizes[i]);
			}
		}

		for (idx in data)
			data[idx]['_row'] = idx;

		for (idx in sizes)
			sizes[idx]['_row'] = idx;

		return {
			'data': data,
			'options': sizes
		};
	}

	function render() 
	{
		var data = compileData();
		$('#inputSize').val(JSON.stringify(data.data));

		var tmpl = $('#sizeTmpl').html();
		Mustache.parse(tmpl);
		var html = Mustache.render(tmpl, data);
		$('#sizeTable').html(html);

		$('#addButton').click(add);
		$('.btn-delete').click(remove);
	}

	function add() 
	{
		var i = $('#sizeSelect').val();
		
		for(var j in data)
			if (data[j].id == sizes[i].id) {
				var found = true;
				break;
			}

		if (!found)
			data.push(sizes[i]);

		render();
		$('#sizeSelect').focus();
	}

	function remove() 
	{
		var row = $(this).data('row');
		data.splice(row, 1);
		render();
	}

	render();
});