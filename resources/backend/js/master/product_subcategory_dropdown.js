$(function() {
	var subcategories = JSON.parse($('#subcategories').val());
	var selected = $('#selectedSub').val();

	function render()
	{
		var category = $('#inputCategory option:selected').val();
		var sub = subcategories[category];

		if (sub == undefined)
			$('#col_subcategory').addClass('hidden');
		else
			$('#col_subcategory').removeClass('hidden');

		var tmpl = $('#subcategoryTmpl').html();
		Mustache.parse(tmpl);
		var html = Mustache.render(tmpl, { data : sub });
		$('#subcategorySelect').html(html);
	}

	function checkSelected()
	{
		var category = $('#inputCategory option:selected').val();
		var list = subcategories[category];

		for (var i in list) {
			list[i].selected = '';

			if (list[i].id == selected)
				list[i].selected = 'selected';
		}
	}

	$('#inputCategory').on('change', render);
	$('#inputCategory').on('keypress', function(e) {
		if (e.keyCode == 39 || e.keyCode == 43)
			render();
	});

	checkSelected();
	render();
});