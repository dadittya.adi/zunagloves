$(function() {
	var subcategories = JSON.parse($('#subcategories').val());
	var errors = JSON.parse($('#errorMessage').val());
	var error_id = $('#errorId').val();

	function render()
	{
		for (var i in subcategories)
			subcategories[i]['_row'] = i;

		var tmpl = $('#subcategoryTmpl').html();
		Mustache.parse(tmpl);
		var html = Mustache.render(tmpl, { data : subcategories });
		$('#subcategoryTable').html(html);

		$('.btn-edit').on('click', edit);
		$('.btn-cancel').on('click', render);
		$('.btn-save').on('click', save);
	}

	function edit()
	{
		var row = $(this).data('row');

		$('.btn-delete').addClass('hidden');
		$('.btn-edit').addClass('hidden');
		$('#name_' + row).addClass('hidden');
		$('#cancel_' + row).removeClass('hidden');
		$('#save_' + row).removeClass('hidden');
		$('#inputSubname_' + row).removeClass('hidden');
		$('#inputIcon_' + row).removeClass('hidden');
	}

	function save()
	{
		var row = $(this).data('row');
		$('#formupdate_' + row).submit();
	}

	function checkError()
	{
		if (isNaN(error_id))
			return;

		$('#edit_' + error_id).click();

		for (var field in errors) {
			$('#col_' + field + '_' + error_id).addClass('has-error');
			$('#error_' + field + '_' + error_id).removeClass('hidden');
			$('#error_' + field + '_' + error_id).html(errors[field][0]);
		}
	}

	$('#addButton').click(function() {
		$('#form-addsub').submit();
	});

	render();
	checkError();
});