// set global moustache tags
Mustache.tags = ['{%', '%}'];

//set check for active radio button
$('.btn input:radio').on('change', function() {
	$('.btn input:radio').prop('checked', false);
	$(this).prop('checked', true);
});

//bootsrap datepicker
$('input.input-date').datepicker({
	format: "dd/mm/yyyy",
	autoclose: true,
	todayHighlight: true
});

//tumbnails
(function($){
	$.fn.extend({
		simpleGal: function (options) {
  			var defaults = {
    			mainImage: ".placeholder"
  			};
  		options = $.extend(defaults, options);
		return this.each(function () {
			var thumbnail = $(this).find("a"),
        	mainImage = $(this).siblings().find(options.mainImage);
			thumbnail.on("click", function (e) {
   	   			e.preventDefault();
      			var galleryImage = $(this).attr("href");
      			mainImage.attr("src", galleryImage);
    		});
  		});
		}
	});
})(jQuery);

$('.thumbnails').simpleGal({
  	mainImage: '.color-pic'
});

$('.thumbnails-sm').simpleGal({
  	mainImage: '.color-pica'
});

$('.thumbnails-sm .label-color a').on('click', function() {
	var id = $(this).data('id');
	$('#radio' + id).click();
});

//navbar
$('.navbar-collapse').on('show.bs.collapse', function() {
   $('.navbar-collapse.collapse.in').not(this).collapse('hide');
});

//cart number

$(".cart_number").keydown(function (e) {
    // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [8, 9, 27, 13, 110]) !== -1 ||
         // Allow: Ctrl+A, Command+A
        (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
         // Allow: home, end, left, right, down, up
        (e.keyCode >= 35 && e.keyCode <= 40)) {
             // let it happen, don't do anything
             return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
});