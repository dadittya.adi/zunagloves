$(function() {
	var items = JSON.parse($('#orderData').val());
	var shipping_price = $('#shippingPrice').val();
	var weight = $('#shippingPrice').data('weight');
	var voucher_discount = $('#voucherDisc').val();

	function parseInput(obj) {
		if (obj == undefined)
			return obj;

		var num = parseFloat(obj.replace(',', '.'));
		if (isNaN(num))
			num = 0;
		
		return num;
	}

	function render()
	{
		for(var i = 0; i < items.length; i++) {
			items[i]['no'] = i + 1;
			items[i]['_row'] = i;
		}

		var data = {
			items : items,
			shipping_price : shipping_price,
			weight : weight,
			voucher_discount : voucher_discount
		};

		var tmpl = $('#orderTmpl').html();
		var html = Mustache.render(tmpl, data);
		Mustache.parse(tmpl);
		$('#orderTable').html(html);

		$('#revisionItem').val(JSON.stringify(items));
		$('#revisionShipping').val($('#shipping').val());
		$('#revisionWeight').val($('#weight').val());
		$('#revisionDiscount').val($('#voucher').val());

		$('.btn-edit').on('click', edit);
		$('.btn-cancel').on('click', render);
		$('.btn-save').on('click', save);
		$('.calc').on('keyup', calculate);

		$('#buttonEditShipping').on('click', editShipping);
		$('#buttonSaveShipping').on('click', saveShipping);
		$('#buttonEditDisc').on('click', editDisc);
		$('#buttonSaveDisc').on('click', saveDisc);

		calculate();
	}

	function edit()
	{	
		var id = $(this).data('id');

		$('#price_' + id).removeAttr('readonly');
		$('#qty_' + id).removeAttr('readonly');
		$('#buttonSave_' + id).removeClass('hidden');
		$('#buttonCancel_' + id).removeClass('hidden');

		$('.btn-edit').addClass('hidden');
	}

	function save()
	{
		var id = $(this).data('id');
		var row = $(this).data('row');

		items[row]['price'] = parseInput($('#price_' + id).val());
		items[row]['qty'] = parseInput($('#qty_' + id).val());

		render();
	}

	function editShipping()
	{
		$('#shipping').removeAttr('readonly');
		$('#weight').removeAttr('readonly');
		$('#buttonSaveShipping').removeClass('hidden');
		$('#buttonCancelShipping').removeClass('hidden');
		$('.btn-edit').addClass('hidden');
	}

	function saveShipping()
	{
		shipping_price = parseInput($('#shipping').val());
		weight = parseInt($('#weight').val(), 10);
		render();
	}

	function editDisc()
	{
		$('#voucher').removeAttr('readonly');
		$('#buttonSaveDisc').removeClass('hidden');
		$('#buttonCancelDisc').removeClass('hidden');
		$('.btn-edit').addClass('hidden');
	}

	function saveDisc()
	{
		voucher_discount = parseInput($('#voucher').val());
		render();
	}

	function calculate()
	{
		var total = 0;

		$(".item").each(function () {
			var t = $(this);
			var id = t.data('id');
			var price = parseInput($('#price_' + id).val());
			var qty = parseInput($('#qty_' + id).val());
			var disc_percent = parseInput($('#disc_percent_' + id).val());

			var subtotal = (price * qty).toFixed(2);
			$("#subtotal_" + id).html(subtotal);

			var discount = 0;
			if (!isNaN(disc_percent)) {
				discount = (disc_percent * subtotal/100).toFixed(2);
				$("#discount_" + id).html(discount);
			}
			total = total + subtotal - discount;
		});

		var shipping_price = parseInput($('#shipping').val());
		var weight = parseInt($('#weight').val(), 10);
		if (isNaN(weight))
			weight = 1;

		shipping_price *= weight;
		total += shipping_price;

		var voucher_discount = parseInput($('#voucher').val());
		total -= voucher_discount;

		$('#total').html(total.toFixed(2));
		$('#weight').val(weight);
	}

	render();
});