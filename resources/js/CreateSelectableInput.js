function CreateSelectableInputGroups(name) {
	var data_id = '#' + name + 'Input';
	var options_id = '#' + name + 'Options';
	var select_id = '#' + name + 'Select';
	var template_id = '#' + name + 'Template';
	var table_id = '#' + name + 'Table';
	var addbutton_id = '#' + name + 'AddButton';
	var removebutton_id = '.' + name + 'RemoveButton';

	function cleanInput()
	{
		var all_options = JSON.parse($(options_id).val());
		changeInput(function (data) {
			var clean_data = [];
			for (var i = 0; i < data.length; i++) {
				for (var j = 0; j < all_options.length; j++) {
					if (data[i].id == all_options[j].id) {
						clean_data.push(data[i]);
						break;
					}
				}
			}

			return clean_data;
		});
	}

	function compileData()
	{
		var data = JSON.parse($(data_id).val());
		var all_options = JSON.parse($(options_id).val());

		var options = [];
		for (var i = 0; i < all_options.length; i++) {
			var found = false;
			for (var j = 0; j < data.length; j++) {
				if (all_options[i].id === data[j].id) {
					found = true;
					break;
				}
			}
			if (!found) {
				options.push(all_options[i]);
			}
		}

		return {
			'data': data,
			'options': options
		};
	}

	function render()
	{
		var data = compileData();

		var tmpl = $(template_id).html();
		Mustache.parse(tmpl);
		var html = Mustache.render(tmpl, data);
		$(table_id).html(html);

		$(addbutton_id).on('click', add);
		$(removebutton_id).on('click', remove);
	}

	function add()
	{
		var all_options = JSON.parse($(options_id).val());
		var selected_id = parseInt($(select_id).val(), 10);

		for (var i = 0; i < all_options.length; i++) {
			if (selected_id === all_options[i].id) {
				changeInput(function (input) {
					input.push(all_options[i]);
					return input;
				});
				render();
				break;
			}
		}
	}

	function remove()
	{
		var data = JSON.parse($(data_id).val());
		var selected_id = parseInt($(this).data('id'), 10);

		for (var i = 0; i < data.length; i++) {
			if (selected_id === data[i].id) {
				changeInput(function (input) {
					input.splice(i, 1);
					return input;
				})
				render();
				break;
			}
		}
	}

	function changeInput(callback)
	{
		var el = $(data_id);
		var current = JSON.parse(el.val());
		current = callback(current);
		el.val(JSON.stringify(current));
	}

	cleanInput();
	render();
}

function CreateDualBoxInputGroups(name) {
	var data_id = '#' + name + 'Input';
	var options_id = '#' + name + 'Options';
	var select_id = '#' + name + 'Select';
	var template_id = '#' + name + 'Template';
	var table_id = '#' + name + 'Table';
	var addbutton_id = '#' + name + 'AddButton';
	var removebutton_id = '#' + name + 'RemoveButton';

	function cleanInput()
	{
		var all_options = JSON.parse($(options_id).val());
		changeInput(function (data) {
			var clean_data = [];
			for (var i = 0; i < data.length; i++) {
				for (var j = 0; j < all_options.length; j++) {
					if (data[i].id == all_options[j].id) {
						clean_data.push(data[i]);
						break;
					}
				}
			}

			return clean_data;
		});
	}

	function compare(a, b)
	{
		return String(a).localeCompare(String(b));
	}

	function compileData()
	{
		var data = JSON.parse($(data_id).val());
		var all_options = JSON.parse($(options_id).val());

		data.sort(compare);
		all_options.sort(compare);

		var options = [];
		for (var i = 0; i < all_options.length; i++) {
			var found = false;
			for (var j = 0; j < data.length; j++) {
				if (all_options[i].id === data[j].id) {
					found = true;
					break;
				}
			}
			if (!found) {
				options.push(all_options[i]);
			}
		}

		return {
			'data': data,
			'options': options
		};
	}

	function render()
	{
		var html;
		var data = compileData();

		var tmpl = $(template_id).html();
		Mustache.parse(tmpl);
		html = Mustache.render(tmpl, { 'data': data.data });
		$(table_id).html(html);
		html = Mustache.render(tmpl, { 'data': data.options });
		$(select_id).html(html);
	}

	function bindButton()
	{
		$(addbutton_id).on('click', add);
		$(removebutton_id).on('click', remove);
	}

	function add()
	{
		var val = $(select_id).val();
		if (val === null)
			return;

		var all_options = JSON.parse($(options_id).val());

		changeInput(function (data) {
			for (var i = 0; i < val.length; i++) {
				var id = parseInt(val[i], 10);
				for (var j = 0; j < all_options.length; j++) {
					if (id === all_options[j].id) {
						data.push(all_options[j]);
						break;
					}
				}
			}

			return data;
		});

		render();
	}

	function remove()
	{
		var val = $(table_id).val();
		if (val === null)
			return;

		changeInput(function (data) {
			for (var i = 0; i < val.length; i++) {
				var id = parseInt(val[i], 10);
				for (var j = 0; j < data.length; j++) {
					if (id === data[j].id) {
						data.splice(j, 1);
						break;
					}
				}
			}

			return data;
		});

		render();
	}

	function changeInput(callback)
	{
		var el = $(data_id);
		var current = JSON.parse(el.val());
		current = callback(current);
		el.val(JSON.stringify(current));
	}

	cleanInput();
	render();
	bindButton();
}