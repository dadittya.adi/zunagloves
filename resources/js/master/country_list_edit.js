$(function() {
	var countries = JSON.parse($('#countryData').val());
	var errors = JSON.parse($('#errorMsg').val());
	var error_id = parseInt($('#errorId').val(), 10);

	function render()
	{
		for (var i = 0; i < countries.length; i++) {
			countries[i]._no = i + 1;
			countries[i]._row = i;
		}

		var data = {data : countries}
		var tmpl = $('#countryTmpl').html();
		var html = Mustache.render(tmpl, data);

		Mustache.parse(tmpl);
		$('#countryTable').html(html);

		$('.btn-edit').on('click', edit);
		$('.btn-cancel').on('click', cancel);
		$('.btn-save').on('click', save);
	}

	function edit()
	{
		var row = parseInt($(this).data('row'), 10);
		countries[row]._edit_state = 1;
		
		render();
		$('.btn-edit').addClass('hidden');
		$('.btn-delete').addClass('hidden');
	}

	function cancel()
	{
		var row = parseInt($(this).data('row'), 10);
		countries[row]._edit_state = 0;
		
		render();
	}

	function save()
	{
		var row = $(this).data('row');
		var name = $('#name_' + row).val();

		$('#name_edit_' + row).val(name);
		$('#update_' + row).submit();
	}

	function checkError()
	{
		if (isNaN(error_id))
			return;

		$('#edit_' + error_id).click();

		for (var field in errors) {
			$('#col_' + field + '_' + error_id).addClass('has-error');
			$('#error_' + field + '_' + error_id).removeClass('hidden');
			$('#error_' + field + '_' + error_id).html(errors[field][0]);
		}
	}

	render();
	checkError();
});