/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

import Axios from 'axios';
import { get, parseInt, set, split } from 'lodash';
import VueTheMask from 'vue-the-mask';
import noUiSlider from 'nouislider';
import 'lazysizes';


window.Vue = require('vue');

Vue.use(VueTheMask)

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('timer-pre-order', require('./components/TimerPreOrder.vue').default);
Vue.component('loading-component', require('./components/LoadingComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    data: {
        quantity: 1,
        qitems: [],
        cart: [],
        isAddQuantityDisabled: false,
        isMinQuantityDisabled: true,
        variant: {
            color: {
                status: false,
                message: 'Please select color'
            },
            size: {
                status: false,
                message: 'Please select size'
            },
            qty: {
                status: false,
                message: 'Please fill quantity'
            }
        },
        province: null,
        cities: [],
        city: null,
        districts: [],
        district: null,
        courier: null,
        selectedCity: null,
        courierCode: ['jne','jnt'],
        courierData: [],
        shippingCost: 0,
        showPaymentButton: false,
        showSearchBox: {
            topdesktop: false,
            topmobile: false,
            left: false,
        },
        showSidebar: false,
        showLoading: false,
        showGuestCheckoutBtn: false,
        voucher: null,
        errors: [],
        checkoutResponse: {
            status: null,
            message: null
        },
        selectedProduct: {
            productId: null,
            productColorId: null,
            productSizeId: null,
            stock: null
        }
    },
    computed: {
        discountAmount() {
            let nominal = 0

            if (this.voucher && this.voucher.status == 'available') {
                nominal = parseFloat(this.voucher.data.discount)   
            }

            return nominal
        },
        subtotal() {
            let subtotal = 0
            this.cart.forEach( obj => {
                subtotal += parseFloat(obj.price_total)
            });

            return subtotal
        },
        total() {
            let total = this.subtotal
            if (this.voucher && this.voucher.status == 'available') {
                total = total - this.discountAmount
            }

            total += this.shippingCost

            return total
        },
        totalWeight() {
            let weight = 0
            this.cart.forEach( obj => {
                weight += parseFloat(obj.weight)
            })

            return weight
        },
        currentAddress() {
            if (window.currentAddress) {
                return JSON.parse(window.currentAddress)
            }
            else {
                return {
                    province: null,
                    city: null,
                    district: null,
                }
            }
        }
    },
    mounted() {
        this.$nextTick(() => {
            this.cart = localStorage.getItem('cart') 
                    ? JSON.parse(localStorage.getItem('cart')) 
                    : []

            this.voucher = localStorage.getItem('applied_voucher') 
                    ? JSON.parse(localStorage.getItem('applied_voucher')) 
                    : null

            if (this.currentAddress.province) {
                this.province = this.currentAddress.province
                this.selectCity()
            }
            if (this.currentAddress.city) {
                this.city = this.currentAddress.city
                this.selectDistrict()
            }
            if (this.currentAddress.district) {
                this.district = this.currentAddress.district
                this.getCost()
            }

            // noUiSlider
            var slider = document.getElementById('price-range')
            
            if (slider) {
                var min = parseFloat(document.getElementById('min').value)
                var max = parseFloat(document.getElementById('max').value)
                var selectedMinPrice = document.getElementById('selectedMinPrice')
                var selectedMaxPrice = document.getElementById('selectedMaxPrice')
                var minPriceText = document.getElementById('minPriceText')
                var maxPriceText = document.getElementById('maxPriceText')
                
                noUiSlider.create(slider, {
                    start: [min, max],
                    connect: true,
                    range: {
                        'min': min,
                        'max': max
                    }
                })

                slider.noUiSlider.on('change', (values, handle) => {
                    var value = Math.round(values[handle]);

                    if (handle) {
                        selectedMaxPrice.value = value
                        maxPriceText.innerHTML = this.formatCurrency(value)
                    } else {
                        selectedMinPrice.value = value
                        minPriceText.innerHTML = this.formatCurrency(value)
                    }
                })
            }
        });
    },
    methods: {
        addToCart(event) {

            // Validation

            if (!this.variant.color.status) {
                $('#variantColor-' + event.target.slug.value).removeClass('d-none')
            } else {
                $('#variantColor-' + event.target.slug.value).addClass('d-none')
            }

            if (!this.variant.size.status) {
                $('#variantSize-' + event.target.slug.value).removeClass('d-none')
            } else {
                $('#variantSize-' + event.target.slug.value).addClass('d-none')
            }

            if (event.target.qty.value <= 0) {
                $('#variantQty-' + event.target.slug.value).removeClass('d-none')
            } else {
                $('#variantQty-' + event.target.slug.value).addClass('d-none')
            }

            if (this.selectedProduct.stock && this.selectedProduct.stock > 0 && event.target.qty.value > 0) {
                let formData = new FormData(event.target)
                let item = {}

                formData.forEach((value, key) => item[key] = value)

                let isKeyExists = this.findKey(item.slug,item.size,item.color)
                
                item.price_total = parseFloat(item.price) * item.qty

                if (isKeyExists != undefined) {
                    // Update Item in Cart
                    this.cart[isKeyExists].qty = item.qty
                    this.cart[isKeyExists].price_total = item.price_total
                } else {
                    // Store New Item to Cart
                    this.cart.push(item)
                }

                localStorage.setItem('cart', JSON.stringify(this.cart))

                $('#quick-view-' + item.slug).modal('hide');
                $('#shopping-cart').modal('show');
            }
        },
        addShippingCost(param) {
            let data = this.courierData[param]
            this.shippingCost = data.cost[0].value
            this.showPaymentButton = true
        },
        addQuantity(param, action) {
            if (action == 'update') {
                let qty = parseInt(this.cart[param].qty) + 1
                let price_total = parseFloat(this.cart[param].price) * qty

                this.cart[param].qty = qty
                this.cart[param].price_total = price_total

                localStorage.setItem('cart', JSON.stringify(this.cart))
            } else {
                let qty = parseInt(this.$refs[param].value)
                let max = parseInt(this.$refs[param].max)

                if (qty < max) {
                    let newQty = qty + 1
                    this.$refs[param].value = newQty
                    this.isMinQuantityDisabled = false

                    if (newQty == max) {
                        this.isAddQuantityDisabled = true
                    }
                }
            }
        },
        checkout(event) {
            Axios.post(event.target.action, this.cart).then((res) => {
                location.href = res.data
            })
        },
        checkVoucher(event) {
            let formData = new FormData(event.target)
            
            if (formData.get('code')) {
                this.voucher = null
                Axios.get(event.target.action, { params: { code: formData.get('code'), amount: formData.get('amount') } }).then((res) => {
                    if (res.data.status == 'available') {
                        localStorage.setItem('applied_voucher', JSON.stringify(res.data))
                    }
    
                    this.voucher = res.data
                })
            }
        },
        clearCart() {
            localStorage.removeItem('cart')
            this.removeAppliedVoucher()
        },
        deleteFromCart(id) {
            this.cart.splice(id,1)
            localStorage.setItem('cart', JSON.stringify(this.cart))
            this.removeAppliedVoucher()
        },
        findKey(slug, size, color) {
            let getKey
            this.cart.forEach((item, key) => {
                if (item.slug === slug && item.size === size && item.color === color) {
                    getKey = key
                }
            })

            return getKey
        },
        formatCurrency(price) {
            let result = new Intl.NumberFormat('en', { style: 'currency', currency: 'IDR' }).format(price).replace(/(\.|,)00$/g, '')

            result = result.replace('IDR','Rp')

            return result
        },
        async getCost() {
            this.toggleShowLoading()

            this.courierCode.forEach((courier) => {
                let formData = new FormData();

                formData.append('origin', 398)
                formData.append('city', this.city)
                formData.append('subdistrict', this.district)
                formData.append('courier', courier)
                formData.append('weight', this.totalWeight)

                Axios.post('/get-regions/cost', formData).then((res) => {
                    let result = res.data.rajaongkir.results[0]
                    
                    result.costs.forEach((service) => {
                        service.courier = result.code
                        service.courier_name = result.name

                        this.courierData.push(service)
                    })

                    
                })
            })
            this.toggleShowLoading()
        },
        hideAnotherModal() {
            $('#shopping-cart, #filter').modal('hide')
        },
        minQuantity(param, action) {
            this.removeAppliedVoucher()
            if (action == 'update') {
                let qty = parseInt(this.cart[param].qty) - 1
                let price_total = parseFloat(this.cart[param].price) * qty

                this.cart[param].qty = qty
                this.cart[param].price_total = price_total

                localStorage.setItem('cart', JSON.stringify(this.cart))
            } else {
                let qty = parseInt(this.$refs[param].value)
                let min = parseInt(this.$refs[param].min)
                
                if (qty > min) {
                    let count = qty - 1
                    this.$refs[param].value = count
                    this.isAddQuantityDisabled = false

                    if (count == min) {
                        this.isMinQuantityDisabled = true
                    }
                } 
            }
        },
        removeAppliedVoucher() {
            localStorage.removeItem('applied_voucher');
            this.voucher = null
        },
        reset(target) {
            this.$refs[target].value = 1
            this.isAddQuantityDisabled = false
            this.isMinQuantityDisabled = true

            // Reset Option
            let colorsRadio = document.getElementsByName('color')
            let sizesRadio = document.getElementsByName('size')

            colorsRadio.forEach( node => { node.checked = false })
            sizesRadio.forEach( node => { node.checked = false })
        },
        async selectCity() {
            this.toggleShowLoading()

            let formData = new FormData();

            formData.append('province', this.province)

            Axios.post('/get-regions/city/external', formData).then((res) => {
                this.cities = res.data
                this.toggleShowLoading()
            })
            
        },
        async selectDistrict() {
            this.toggleShowLoading()
            let formData = new FormData();

            formData.append('city', this.city)

            Axios.post('/get-regions/subdistrict/external', formData).then((res) => {
                this.districts = res.data
                this.toggleShowLoading()
            })
        },
        selectColorText(event) {
            let textId = event.target.id + '_text'
            let el = document.getElementById(textId)
            el.checked = true
        },
        selectSizeText(event) {
            let textId = event.target.id + '_text'
            let el = document.getElementById(textId)
            el.checked = true
        },
        setInputFilter(value) {
            return /^\d*$/.test(value);
        },
        setSubscribe(value) {
            sessionStorage.setItem('subscribe', value);
        },
        showProductColorImage(productColorId, carouselId, position) {
            this.variant.color.status = true
            $('#' + carouselId).carousel(position)
            this.selectedProduct.productColorId = productColorId;

            if (this.selectedProduct.productId && this.selectedProduct.productSizeId) {
                this.selectStock()
            }
        },
        selectProductSize(productId, productSizeId) {
            this.variant.size.status = true
            this.selectedProduct.productId = productId;
            this.selectedProduct.productSizeId = productSizeId;

            if (this.selectedProduct.productColorId) {
                this.selectStock()
            }
        },
        async selectStock() {
            let formData = new FormData();

            formData.append('productId', this.selectedProduct.productId)
            formData.append('productColorId', this.selectedProduct.productColorId)
            formData.append('productProductSizeId', this.selectedProduct.productSizeId)

            await Axios.post('/checkstock', formData).then((res) => {
                this.selectedProduct.stock = res.data
            })
        },
        shipToDifferentAddress() {
            this.province = null
            this.city = null
            this.district = null
            this.courierData = []

            $(`[name="full_name"],
               [name="email"],
               [name="phone"],
               [name="post_code"],
               [name="address"],
               [name="notes"]
            `).val('')
        },
        showSearchBoxToggle(param) {
            if (this.showSearchBox[param]) {
                this.showSearchBox[param] = false
            } else {
                this.showSearchBox[param] = true
                this.$nextTick(() => {
                    this.$refs['searchBox' + param].focus()
                })
            }
        },
        async submitFormCheckout() {
            this.errors = []

            let form = document.getElementById('form-checkout')
            let action = form.action 
            let formData = new FormData(form)
            let required = ['full_name', 'email', 'phone', 'province', 'city', 'district', 'post_code', 'address', 'dob']
            let errors = {}

            required.forEach((field) => {
                if (formData.get(field) == null || formData.get(field) == '') {
                    errors[field] = `${field} is required`
                }
            })

            if (Object.keys(errors).length > 0) {
                this.errors.push(errors)
            }

            if (this.errors.length > 0) {
                return false;
            } else {
                this.toggleShowLoading()
                formData.append('total_weight', this.totalWeight)
                formData.append('ongkir', this.shippingCost)
                if (this.voucher !== null) {
                    formData.append('voucher', this.voucher.data.code)
                }
                else {
                    formData.append('voucher', '')
                }
                await Axios.post(action, formData).then((res) => {
                    this.showPayment(res.data)
                    this.toggleShowLoading()

                    localStorage.removeItem('cart')
                    localStorage.removeItem('applied_voucher')

                    this.cart = []
                    if (this.voucher !== null) {
                        this.voucher = null
                    }
                })
            }
        },
        showPayment(snap_token) {
            snap.pay(snap_token, {
                // Optional
                onSuccess: (result) => {
                    this.checkoutResponse.status = 'success'
                    this.checkoutResponse.message = 'Payment success, We will send this item to your address as soon as.'
                    $('#checkoutResponse').modal('show')
                },
                // Optional
                onPending: (result) => {
                    this.checkoutResponse.status = 'pending'
                    this.checkoutResponse.message = 'Please proceed to payment through our website within 24 hours. For payment information you can check in your email inbox under the Promotion / Spam tab'
                    $('#checkoutResponse').modal('show')
                },
                // Optional
                onError: (result) => {
                    this.checkoutResponse.status = 'error'
                    this.checkoutResponse.message = 'Hi, we are unable to process your order.'
                    $('#checkoutResponse').modal('show')
                }
            });
        },
        toggleFilter(id) {
            var el = document.getElementById(id)
            var toggleBtn = document.getElementById('toggle-' + id)

            if (el.children[5].className == 'container d-none') {
                Array.from(el.children).forEach((element, key) => {
                    if (key >=4 && element.className == 'container d-none') {
                        element.className = 'container d-block'
                    }
                })

                toggleBtn.innerHTML = "Show Less"
            }
            else if (el.children[5].className == 'container d-block') {
                Array.from(el.children).forEach((element, key) => {
                    if (key >=4 && element.className == 'container d-block') {
                        element.className = 'container d-none'
                    }
                })

                toggleBtn.innerHTML = "Show More"
            }
        },
        showFilterToggle(event) {
            $('#filter').on('hidden.bs.modal', function () {
                // Tawk_API.showWidget();
                $(event.target).removeClass('text-warning');
                $(event.target).text('+ Filter');
            })

            $('#filter').on('shown.bs.modal', function () {
                // Tawk_API.hideWidget();
                $(event.target).addClass('text-warning');
                $(event.target).text('- Filter');
                $('.modal-backdrop').remove();
            })
        },
        toggleShowLoading() {
            if (this.showLoading) {
                this.showLoading = false
            } else {
                this.showLoading = true
            }
        },
        whatMenuToggle(event) {
            $(event.target).toggleClass('shown')
        },
        guestCheckoutToggle() {
            this.hideAnotherModal()
            this.showGuestCheckoutBtn = true
        }
    }
});
