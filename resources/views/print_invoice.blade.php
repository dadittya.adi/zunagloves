<html>
	<head>
		<title>Invoice Order #{{ $order->order_number }}</title>
		<link rel="stylesheet" href="{{ asset(elixir("css/app.css")) }}">			
		<link rel="stylesheet" href="{{ asset(elixir("css/custom.css")) }}">
	</head>
	<body>
		<div class="container-inv">
			<div class="row">
				<div class="title-inv row">
					<div class="col-xs-4 logo">
						<img src="/images/logo-xs.png" width="100%">
					</div>
					<div class="col-xs-3"></div>
					<div class="col-xs-5 pull-right">
						<h4 class="pull-right">Invoice Order #{{ $order->order_number }}</h4>
					</div>
				</div>
				<div class="info-inv row">
					<span class="col-xs-6">Customer : {{ $order->full_name }}</span>
					<span class="col-xs-5 pull-right">Date : {{ i18l_date($order->created_at) }}</span>
				</div>
				<div class="info-inv row">
					<div class="col-xs-6">
						<span>Shipping Address :</span><br>
						<span>
							{{ $order->shipping->full_name }}
							{{ $order->shipping->organization ? '(' . $order->shipping->organization . ')' : '' }}
						</span><br>
						<span>{{ nl2br(e($order->shipping->address)) }}</span><br>
						<span>
							{{ $order->shipping->cityb->full_name }}, {{ $order->shipping->provinceb->province }}, 
							{{-- {{ $order->shipping->postal_code }} {{ $order->shipping->country }} --}}
						</span><br>
						<span>Phone {{ $order->phone }}</span>
					</div>
					<div class="col-xs-5 pull-right">
						<span>Shipping Method :</span><br>
						<span>{{ $order->provider }}</span><br>
						<span>{{ $order->destination }}</span><br>
						@if ($order->shipping_time != '0')
					    	<span>{{ $order->shipping_time }} hari</span>
					    @endif
					</div>
				</div>
				<div class="content-inv">
					<table class="table table-inv">
					 	<thead>
	                        <tr>
	                            <th style="border-bottom: 1px solid #000;">
	                                <span>Product</span>
	                            </th>
	                            <th class="to-center" style="border-bottom: 1px solid #000;">
	                                <span>Price</span>
	                            </th>
	                            <th class="to-center" style="border-bottom: 1px solid #000;">Qty</th>
	                            <th class="to-right" style="border-bottom: 1px solid #000;">Subtotal</th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                        <?php $total = 0 ?>
	                        @foreach ($order->items as $item)
	                        <tr>
	                            <td>
                            		<span class="product-name">{{ $item->product->name }}</span>
                                    <p>
                                    @if ($item->color)
                                    	Color : {{ $item->color }}<br>
                                    @endif
                                    @if ($item->size)
                                    	Size : {{ $item->size }}<br>
                                    @endif
	                                @if ($item->notes)
	                                	Notes : <br>
										<span class="notes"><i>{{ nl2br(e($item->notes)) }}</i></span>
									@endif
	                            </td>
	                            <td class="to-center">
	                                <span>
	                                    <span>{{ i18l_currency($item->price, $order->domain) }}</span>
	                                </span>
	                            </td>
	                            <td class="to-center" style="white-space: nowrap;">
	                                {{ (float)$item->qty }}
	                            </td>
	                            <td class="to-right">
                                    <span>{{ i18l_currency($item->total, $order->domain) }}</span>
	                                @if ($item->discount > 0)
	                                	<br><span style="color: red">
	                                		Disc: ({{ i18l_currency($item->discount, $order->domain) }})
	                                	</span>
	                                @endif
	                            </td>
	                        </tr>
	                        <?php $total = $total + $item->total - $item->discount ?>
	                        @endforeach
	                    </tbody>
	                    <tfoot>
	                        <tr>
	                            <td colspan="3"> 
	                                Subtotal
	                            </td>
	                            <td>
	                                <span class="price">{{ i18l_currency($total, $order->domain) }}</span>
	                            </td>
	                        </tr>
	                        <tr>
	                            <td colspan="3"> 
	                                Delivery Cost ({{ $order->weight }}kg x {{ i18l_currency($order->shipping_price) }})
	                            </td>
	                            <td>
	                                <span>
	                                	@if ($order->shipping_price > 0)
						            		{{ i18l_currency($order->shipping_price * $order->weight, $order->domain) }}
						            	@else
						            		-
						            	@endif
	                                </span>
	                            </td>
	                        </tr>
	                        @if ($order->voucher_discount > 0)
	                        <tr>
	                            <td colspan="3"> 
	                                Discount ({{ $order->voucher_code }})
	                            </td>
	                            <td> 
	                                <span>({{ i18l_currency($order->voucher_discount, $order->domain) }})</span>
	                            </td>
	                        </tr>
	                        @endif
	                        <tr>
	                            <td colspan="3"> 
	                                <strong>Grand Total</strong>
	                            </td>
	                            <td> 
	                                <strong><span>{{ i18l_currency($order->total, $order->domain) }}</span></strong> 
	                            </td>
	                        </tr>
	                    </tfoot>
					</table>
				</div>
			</div>
		</div>
	</body>
</html>