@extends('admin.master_layout', ['active' => 'product'])

@section('sidebar')
	@include('admin.product.sidebar', ['active' => 'index'])
@endsection

@section('content')
	<div class="container-fluid">
		<div class="row">
			{!!
				Form::open([
					'role' => 'form',
					'url' => action('Admin\ProductColorController@store'),
					'method' => 'post',
					'enctype' => 'multipart/form-data'
				])
			!!}

			@include('form.text',[
				'field' => 'product_name',
				'label' => 'Product',
				'default' => ($product) ? $product->name : '',
				'placeholder' => 'Type Product Name',
				'attributes' => [
					'id' => 'auto_product',
					($product) ? 'readonly' : '' 
				]
			])
			
			@if ($errors->has('pro'))
				<div class="control-input {{ $errors->has('pro') ? 'has-error' : '' }}">
					<span class="help-block text-danger">{{ $errors->first('pro') }}</span>
				</div>
			@endif


			{!! Form::hidden('product', $selected_product, ['id' => 'product_id']) !!}

			@include('form.text', [
				'field' => 'name',
				'label' => 'Color Name',
				'placeholder' => 'Color Name'
			])

			@include('form.select', [
				'field' => 'type',
				'label' => 'Type',
				'options' => [
					'COLOR' => 'Color',
					'TEXTURE' => 'Texture'
				]
			])

			@include('form.text', [
				'field' => 'sku',
				'label' => 'SKU',
				'placeholder' => 'Stock Keeping Unit'
			])

			@include('form.select', [
				'field' => 'stock_out',
				'label' => 'Stock State',
				'options' => [
					'0' => 'In Stock',
					'1' => 'Out of Stock'
				]
			])

			@include('form.file', [
				'field' => 'color_image',
				'label' => 'Color Image Icon',
				'help' => 'Recomended resolution 60 x 60 px (JPG / PNG).'
			])
			<div class="form-group">

			<label for="productImage" class="control-label">
				Product Image
			</label>

				<div class="control-input {{ $errors->has('product_image') ? 'has-error' : '' }}">
				
					@if ($errors->has('product_image'))
						<span class="help-block text-danger">{{ $errors->first('product_image') }}</span>
					@endif

					@include('form.file', [
						'field' => 'product_image[]',
						'help' => 'Recomended resolution 540 x 684 px (JPG / PNG).'
					])
					@include('form.file', [
						'field' => 'product_image[]',
						'help' => 'Recomended resolution 540 x 684 px (JPG / PNG).'
					])
					@include('form.file', [
						'field' => 'product_image[]',
						'help' => 'Recomended resolution 540 x 684 px (JPG / PNG).'
					])
					@include('form.file', [
						'field' => 'product_image[]',
						'help' => 'Recomended resolution 540 x 684 px (JPG / PNG).'
					])
					@include('form.file', [
						'field' => 'product_image[]',
						'help' => 'Recomended resolution 540 x 684 px (JPG / PNG).'
					])
				</div>
			</div>

			<div class="form-group">
				<button type="submit" class="btn btn-primary">Save</button>
			</div>

			{!! Form::close() !!}
		</div>
	</div>
@endsection

@section('content-js')
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/smoothness/jquery-ui.css">
	<script src="//code.jquery.com/ui/1.12.0/jquery-ui.min.js"></script>
	<script type="text/javascript">
		var products = {!! json_encode($auto_product) !!};
		
		console.log(products);
		for (idx in products) {
			products[idx]['label'] = products[idx].name;
			products[idx]['value'] = products[idx].name;
		}

		var availableTags = products;

	    //console.log(availableTags);
	    $( "#auto_product" ).autocomplete({
	      source: availableTags,
	      minLength: 3,
	      select: function( event, ui ) {
	      	var id = ui.item.id;
	      	$('#product_id').val(id);
	      }
	    });
	</script>
@endsection