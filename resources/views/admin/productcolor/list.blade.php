@extends('admin.master_layout', ['active' => 'product'])

@section('sidebar')
	@include('admin.product.sidebar', ['active' => 'index'])
@endsection

@section('content')
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-9">
				@include('form.text',[
					'field' => 'product',
					'default' => ($product) ? $product->name : '',
					'placeholder' => 'Type Product Name',
					'attributes' => [
						'id' => 'auto_product'
					]
				])
			</div>
			<div class="col-md-3 text-right">
				<a class="btn btn-primary" 
					href="{{ action('Admin\ProductColorController@create', [
						$product_id == 'all' ? null : $product_id
					]) }}"
				>
					Create New
				</a>
			</div>
		</div>	

		<div class="row">
			<div class="table-responsive">
				<table class="table">
					<thead>
						<tr>
							<th>#</th>
							<th>SKU</th>
							<th>Name</th>
							<th>Type</th>
							<th>Product</th>
							<th>Color Image</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
					@foreach ($colors as $key => $color)
						<tr>
							<td>{{ $key + 1 }}</td>
							<td>
								@if($color->sku)
									{{ $color->sku }}
								@else
									<i>Kosong</i>
								@endif
							</td>
							<td>{{ $color->name }}</td>
							<td>{{ ucwords(strtolower($color->type)) }}</td>
							<td>{{ $color->product->name }}</td>
							<td>
								<img src="{{ action('LandingController@showColorImage', [$color->id]) }}" width="30" height="30">
							</td>
							<td>
								<a href="{{ action('Admin\ProductColorController@edit', [$color->id]) }}" class="btn btn-primary btn-xs">
									Edit
								</a>
							</td>
						</tr>
					@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
@endsection

@section('content-js')
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	<script type="text/javascript">
		var url = "{{ action('Admin\ProductColorController@index') }}";
		function load($id)
		{
			var product_id = parseInt($id, 10);
			if (isNaN(product_id))
				product_id = '';

			location.href = url + '/' + product_id;
		}

		var products = {!! json_encode($auto_product) !!};
		
		console.log(products);
		for (idx in products) {
			products[idx]['label'] = products[idx].name;
			products[idx]['value'] = products[idx].name;
		}

		var availableTags = products;

	    //console.log(availableTags);
	    $( "#auto_product" ).autocomplete({
	      source: availableTags,
	      minLength: 3,
	      select: function( event, ui ) {
	      	var id = ui.item.id;
	      	load(id);

	      }
	    });
	</script>
	<script>
		$(function () {
			var url = "{{ action('Admin\ProductColorController@index') }}";

			function load()
			{
				var product_id = parseInt($(this).val(), 10);
				if (isNaN(product_id))
					product_id = '';

				location.href = url + '/' + product_id;
			}

			$('#product').on('change', load);
		});
	</script>
@endsection