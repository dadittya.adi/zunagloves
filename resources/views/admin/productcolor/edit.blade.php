@extends('admin.master_layout', ['active' => 'product'])

@section('sidebar')
	@include('admin.product.sidebar', ['active' => 'index'])
@endsection

@section('content')
	<div class="container-fluid">
		<div class="row">
			<div class="content-header">
				<h2>
					Edit Product Color
					{!!
						Form::open(array(
							'role' => 'form',
							'url' => action('Admin\ProductColorController@delete', [$color->id]),
							'method' => 'post',
						))
					!!}
					<button type="submit" class="btn btn-danger"
						onclick="return confirm('Are you sure want to delete this product color?');"
					>
						Delete
					</button>
					{!! Form::close() !!}
				</h2>
			</div>
		</div>

		<div class="row">
			{!!
				Form::open([
					'role' => 'form',
					'url' => action('Admin\ProductColorController@update', [$color->id]),
					'method' => 'post',
					'enctype' => 'multipart/form-data'
				])
			!!}

			@include('form.text', [
				'field' => 'product',
				'label' => 'Product',
				'default' => $color->product->name,
				'attributes' => [
					'readonly' => 'true'
				]
			])

			@include('form.text', [
				'field' => 'name',
				'label' => 'Color Name',
				'placeholder' => 'Color Name',
				'default' => $color->name
			])

			
			@include('form.select', [
				'field' => 'type',
				'label' => 'Type',
				'options' => [
					'COLOR' => 'Color',
					'TEXTURE' => 'Texture'
				],
				'default' => $color->type
			])

			@include('form.text', [
				'field' => 'sku',
				'label' => 'SKU',
				'placeholder' => 'Stock Keeping Unit',
				'default' => $color->sku
			])

			@include('form.select', [
				'field' => 'stock_out',
				'label' => 'Stock State',
				'options' => [
					'0' => 'In Stock',
					'1' => 'Out of Stock'
				],
				'default' => $color->stock_out
			])

			<div class="form-group">
				<label for="colorImage" class="control-label">
					Color Image Icon
				</label>
				<div class="control-input {{ $errors->has('color_image') ? 'has-error' : '' }}">
					@if ($color->color_image)
					<img class="thumbnail" src="{{ action('LandingController@showColorImage', [$color->id]) }}" height="60">
					@endif

					{!! Form::file('color_image') !!}
					<span class="help-block">Recomended resolution 60 x 60 px (JPG / PNG).</span>

					@if ($errors->has('color_image'))
					<span class="help-block text-danger">{{ $errors->first('color_image') }}</span>
					@endif
				</div>
			</div>

			<div class="form-group">
				<label for="productImage" class="control-label">
					Product Image
				</label>
				<div class="control-input {{ $errors->has('product_image') ? 'has-error' : '' }}">
					@if ($color->colorImages()->count() > 0)
						@foreach($color->colorImages as $i)
							<img class="thumbnail" src="{{ action('LandingController@showProductColorImage', [$i->id]) }}" height="342" style="display:inline">
						@endforeach
					
					@endif
					@if ($errors->has('product_image'))
					<span class="help-block text-danger">{{ $errors->first('product_image') }}</span>
					@endif
					
					{!! Form::file('product_image[]') !!}
					<span class="help-block">Recomended resolution 540 x 684 px (JPG / PNG).</span>

					{!! Form::file('product_image[]') !!}
					<span class="help-block">Recomended resolution 540 x 684 px (JPG / PNG).</span>

					{!! Form::file('product_image[]') !!}
					<span class="help-block">Recomended resolution 540 x 684 px (JPG / PNG).</span>

					{!! Form::file('product_image[]') !!}
					<span class="help-block">Recomended resolution 540 x 684 px (JPG / PNG).</span>

					{!! Form::file('product_image[]') !!}
					<span class="help-block">Recomended resolution 540 x 684 px (JPG / PNG).</span>

					
				</div>
			</div>

			<div class="form-group">
				<button type="submit" class="btn btn-primary">Save</button>
			</div>

			{!! Form::close() !!}
		</div>
	</div>
@endsection