@extends('admin.master_layout', ['active' => 'product'])

@section('sidebar')
	@include('admin.product.sidebar', ['active' => 'media'])
@endsection

@section('content')
	<div class="container-fluid">
		<div class="row">
			{!!
				Form::open([
					'role' => 'form',
					'url' => action('Admin\MediaController@store'),
					'method' => 'post',
					'enctype' => 'multipart/form-data'
				])
			!!}

			@include('form.file', [
				'field' => 'media',
				'label' => 'Media'
			])
			
			<div class="form-group" style="margin-top: 18px">
				<button type="submit" class="btn btn-primary">Save</button>
			</div>

			{!! Form::close() !!}
		</div>
		@if ($media->count() > 0)
		<div class="row">
			<div class="panel panel-default" style="margin-top: 10px">
	        <div class="panel-heading"></div>
			<div class="panel-body" style="padding-bottom: 0">
                 @foreach($media as $key => $med)
                    <div class="form-group col-md-4" style="{{ $key % 3 == 0 ? 'clear: both;' : '' }}" style="padding-bottom: 20px">
                        <div style="border: 1px solid #E9E9E9;">
                            <span class="img-main">
                                <img src="{{ $med->getMediaFullPath() }}" width="100%">
                            </span>
                        </div>
	                    <center>
	                    	<a href="#" data-toggle="modal" data-target="#mediaModal{{ $med->id }}">
	                    		<span style="word-wrap: break-word">{{ $med->name }}</span>
	                    	</a>
	                    </center>
                    </div>
                @endforeach
	        </div>
	        <div class="panel-heading" style="padding-top: 12px"></div>
		</div>
		@endif
	</div>
@endsection

@section('content-modal')
	@foreach ($media as $med)
		@include('admin.media._edit_modal')
	@endforeach
@endsection