@extends('admin.master_layout', ['active' => 'admin'])

@section('sidebar')
	@include('admin.admin.sidebar', ['active' => 'index'])
@endsection

@section('content')
	<div class="container-fluid">
		<div class="row">
			<div class="content-header">
				<h2>
					Edit Admin
				</h2>
			</div>
		</div>

		<div class="row">
			{!!
				Form::open([
					'role' => 'form',
					'url' => action('Admin\AdminController@update', [$admin->id]),
					'method' => 'post',
				])
			!!}

			@include('form.text', [
				'field' => 'full_name',
				'label' => 'Full Name',
				'placeholder' => 'Full Name',
				'default' => $admin->full_name
			])

			@include('form.text', [
				'field' => 'email',
				'label' => 'Email',
				'placeholder' => 'Email',
				'default' => $admin->email
			])

			@include('form.password', [
				'field' => 'password',
				'label' => 'Password',
				'placeholder' => 'Password'
			])

			@include('form.password', [
				'field' => 'confirm_password',
				'label' => 'Password Confirmation',
				'placeholder' => 'Password Confirmation'
			])

			<div class="form-group">
				<label for="input_group" class="control-label">
					Group
				</label>
				<div class="control-input {{ $errors->has('groups') ? 'has-error' : '' }}">
					<table class="table table-condensed table-border" id="admingroupsTable" style="margin-bottom: 0px"></table>
					{!! 
						Form::hidden(
							'groups',
							$admin->groups->map(function($i) {
								return [
									'id' => intval($i->id),
									'name' => $i->name
								];
							}),
							['id' => 'admingroupsInput']
						)
					!!}
					<input type="hidden" value="{{
						$groups->map(function($i) {
							return [
								'id' => intval($i->id),
								'name' => $i->name
							];
						})
					}}" id="admingroupsOptions">

					@if ($errors->has('groups'))
					<span class="help-block text-danger">{{ $errors->first('groups') }}</span>
					@endif
				</div>
			</div>

			@include('form.select', [
				'field' => 'is_active',
				'label' => 'State',
				'options' => [
					'1' => 'Active',
					'0' => 'Inactive'
				],
				'default' => $admin->is_active
			])

			<div class="form-group">
				<button type="submit" class="btn btn-primary">Save</button>
			</div>

			{!! Form::close() !!}
		</div>
	</div>
@endsection
@section('content-js')
	<script src="{{ asset(elixir("js/CreateSelectableInput.js")) }}"></script>
	<script type="x-tmpl-mustache" id="admingroupsTemplate">
		<tbody>
			{% #data %}
			<tr>
				<td width="400px">
					<span>{% name %}</span>
				</td>
				<td>
					<button type="button" class="btn btn-xs btn-danger admingroupsRemoveButton" data-id="{% id %}">
						Delete
					</button>
				</td>
			</tr>
			{% /data %}
			<tr>
				<td width="400px">
					<select class="form-control" name="option_id" id="admingroupsSelect">
						{% #options %}
							<option value="{% id %}">{% name %}</option>
						{% /options %}
					</select>
				</td>
				<td>
					<button type="button" class="btn btn-sm btn-success" id="admingroupsAddButton">
						<span class="glyphicon glyphicon-plus"></span>
					</button>
				</td>
			</tr>
		</tbody>
	</script>
	<script>
		$(function() {
			CreateSelectableInputGroups('admingroups');
		});
	</script>
@endsection