@extends('admin.master_layout', ['active' => 'admin'])

@section('sidebar')
	@include('admin.admin.sidebar', ['active' => 'index'])
@endsection

@section('content')
	<div class="container-fluid">
		<div class="row">
			<div class="table-responsive">
				<table class="table table-banner">
					<thead>
						<tr>
							<th>#</th>
							<th>Full Name</th>
							<th>Email</th>
							<th>State</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($admins as $key => $admin)
							<tr>
								<td>{{ $key + 1 }}</td>
								<td>{{ $admin->full_name }}</td>
								<td>{{ $admin->email }}</td>
								<td>
									@if ($admin->is_active)
										Active
									@else
										Inactive
									@endif
								</td>
								<td>
									<a href="{{ action('Admin\AdminController@edit', [$admin->id]) }}" class="btn btn-primary btn-xs">
										Edit
									</a>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			<div class="pull-right">
				{!! $admins->render() !!}
			</div>
		</div>
	</div>
@endsection