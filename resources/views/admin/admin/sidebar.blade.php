<a class="list-group-item {{ isset($active) && $active == 'index' ? 'active' : '' }}" href="{{ action('Admin\AdminController@index') }}">
	List Admin
</a>
<a class="list-group-item {{ isset($active) && $active == 'create' ? 'active' : '' }}" href="{{ action('Admin\AdminController@create') }}">	
	Create Admin
</a>