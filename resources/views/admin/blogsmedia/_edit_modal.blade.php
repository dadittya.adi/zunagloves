<div class="modal fade" id="mediaModal{{ $med->id }}" role="dialog">
    <div class="modal-dialog">
      	<div class="modal-content">
	        <div class="modal-header">
	        	{!!
					Form::open(array(
						'action' => array('Admin\BlogsMediaController@delete', $med->id),
						'role' => 'form',
						'method' => 'post',
						'class' => 'form-inline'
					))
				!!}
	          	<h4 class="modal-title">
	          		Edit Blog Media
					<button type="submit" class="btn btn-danger pull-right">Delete</button>
				</h4>
				{!! Form::close() !!}
	        </div>
	        {!!
				Form::open(array(
					'action' => array('Admin\BlogsMediaController@update', $med->id),
					'role' => 'form',
					'method' => 'post'
				))
			!!}
	        <div class="modal-body container-fluid">
				@include('form.text', [
					'field' => 'name',
					'label' => 'Name',
					'default' => $med->name
				])

				@include('form.text', [
					'field' => 'filename',
					'label' => 'File Name',
					'default' => $med->filename,
					'attributes' => [
						'readonly' => 'true'
					]
				])

				@include('form.text', [
					'field' => 'url',
					'label' => 'File Url',
					'default' => $med->getMediaFullPath(),
					'attributes' => [
						'readonly' => 'true'
					]
				])
			</div>
	        <div class="modal-footer">
	        	<button type="submit" class="btn btn-success">Save</button>
	        	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        </div>
	        {!! Form::close() !!}
	    </div>
    </div>
</div>