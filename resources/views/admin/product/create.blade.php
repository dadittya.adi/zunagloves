@extends('admin.master_layout', ['active' => 'product'])

@section('sidebar')
	@include('admin.product.sidebar', ['active' => 'create'])
@endsection

@section('content')
	<div class="container-fluid">
		<div class="row">
			{!!
				Form::open([
					'role' => 'form',
					'url' => action('Admin\ProductController@store'),
					'method' => 'post',
					'enctype' => 'multipart/form-data'
				])
			!!}

			@include('form.text', [
				'field' => 'name',
				'label' => 'Name',
				'placeholder' => 'Product Name'
			])

			@include('form.select', [
				'field' => 'is_active',
				'label' => 'State',
				'options' => [
					'1' => 'Active',
					'0' => 'Inactive',
					'2' => 'Coming Soon',
					'3' => 'Pre Order'
				]
			])

			@include('form.date', [
				'field' => 'launch_date',
				'label' => 'Launch Date (Optional)',
				'placeholder' => 'dd/mm/yyyy',
				'default' => $launch_date->format('d/m/Y')
			])

			@include('form.select', [
				'field' => 'category',
				'label' => 'Category',
				'options' => $categories,
				'attributes' => [
					'id' => 'inputCategory'
				]
			])

			@include('form.select', [
				'field' => 'genre',
				'label' => 'Genre',
				'options' => [
					'MEN' => 'Men',
					'WMN' => 'Women',
					'UNI' => 'Unisex',
					'KID' => 'Kid'
				]
			])

			<div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}" id="des">
				<label>Description</label>
			</div>
			{{-- <div class="row">
				<div class="col-lg-6">
					<div class="input-group" style="margin-bottom: 10px;">
						<button id="buttonMediaDes" type="button" class="btn btn-primary btn-md" data-toggle="modal" data-target="#desModal">
							Use Media
						</button>
					</div>
				</div>
			</div> --}}
			<ul class="nav nav-tabs">
				<li role="presentation" id="navEditorDes"><a href="#des">Editor</a></li>
				<li role="presentation" id="navPreviewDes"><a href="#des">Preview</a></li>
			</ul>
			<div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
				{!! Form::textarea('description', $example, array(
					'class' => 'form-control ckeditor',
					'id' => 'description'
				)) !!}
				@if ($errors->has('description'))
				<span class="help-block text-danger">{{{ $errors->first('description') }}}</span>
				@endif
			</div>

			<div class="form-group">
				<div class="control-input" id="descriptionPreview">
				</div>
			</div>

			{{-- <input type="hidden" class="form-controll" name="mediaLinkDes" id="mediaLinkDes" value="">

			<div class="row">
				<div class="col-lg-6">
					<div class="input-group" style="margin-bottom: 10px;">
						<button id="buttonMediaFea" type="button" class="btn btn-primary btn-md" data-toggle="modal" data-target="#feaModal">
							Use Media
						</button>
					</div>
				</div>
			</div>  --}}
			
			<div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}" id="feature">
				<label>Additional Description (Optional)</label>
			</div>
			<ul class="nav nav-tabs">
				<li role="presentation" id="navEditorFea"><a href="#feature">Editor</a></li>
				<li role="presentation" id="navPreviewFea"><a href="#feature">Preview</a></li>
			</ul>

			<div class="form-group {{ $errors->has('feature_material') ? 'has-error' : '' }}">
				{!! Form::textarea('feature_material', '', array(
					'class' => 'form-control ckeditor',
					'id' => 'feature_material'
				)) !!}
				@if ($errors->has('feature_material'))
				<span class="help-block text-danger">{{{ $errors->first('feature_material') }}}</span>
				@endif
			</div>

			<div class="form-group">
				<div class="control-input" id="featurePreview">
				</div>
			</div>

			{{--<input type="hidden" class="form-controll" name="mediaLinkFea" id="mediaLinkFea" value="">

			@include('form.file', [
				'field' => 'guide_image',
				'label' => 'Guide Image',
				'help' => 'Recomended resolution 660 x 1100 px (JPG / PNG).'
			]) --}}

			<label for="productImage" class="control-label">
				Product Image (General)
			</label>

				<div class="control-input {{ $errors->has('product_image') ? 'has-error' : '' }}">
				
					@if ($errors->has('product_image'))
						<span class="help-block text-danger">{{ $errors->first('product_image') }}</span>
					@endif

					@include('form.file', [
						'field' => 'product_image[]',
						'help' => 'Recomended resolution 540 x 684 px (JPG / PNG).'
					])
					@include('form.file', [
						'field' => 'product_image[]',
						'help' => 'Recomended resolution 540 x 684 px (JPG / PNG).'
					])
					@include('form.file', [
						'field' => 'product_image[]',
						'help' => 'Recomended resolution 540 x 684 px (JPG / PNG).'
					])
					@include('form.file', [
						'field' => 'product_image[]',
						'help' => 'Recomended resolution 540 x 684 px (JPG / PNG).'
					])
					@include('form.file', [
						'field' => 'product_image[]',
						'help' => 'Recomended resolution 540 x 684 px (JPG / PNG).'
					])
				</div>
			</div>


			<div class="form-group">
				<label class="control-label">
					Size
				</label>
				<div class="control-input {{ $errors->has('product_size') ? 'has-error' : '' }}">
					<table class="table" id="sizeTable"></table>
					{!! Form::hidden('sizes', json_encode([]), ['id' => 'inputSize']) !!}
					<input type="hidden" id="sizeData" value="{{ json_encode($sizes, JSON_NUMERIC_CHECK) }}">
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-6">
					@include('form.text', [
						'field' => 'price_id',
						'label' => 'Price (IDR)',
						'placeholder' => 'Price (IDR)',
					])
				</div>
				{{-- <div class="col-md-6">
					@include('form.text', [
						'field' => 'price_en',
						'label' => 'Price (SGD)',
						'placeholder' => 'Price (SGD)'
					])
				</div> --}}
			</div>

			<div class="row">
				<div class="col-md-4">
					@include('form.text', [
						'field' => 'disc_percent',
						'label' => 'Discount (% / Nominal) (Optional)',
						'placeholder' => 'Discount (% / Nominal)'
					])
				</div>
				<div class="col-md-4">
					@include('form.date', [
						'field' => 'start_discount',
						'label' => 'Start Date (00:00) (Optional)',
						'placeholder' => 'dd/mm/yyyy'
					])
				</div>
				<div class="col-md-4">
					@include('form.date', [
						'field' => 'end_discount',
						'label' => 'End Date (23:59) (Optional)',
						'placeholder' => 'dd/mm/yyyy'
					])
				</div>
			</div>

			@include('form.text', [
				'field' => 'weight',
				'label' => 'Weight (gram)',
				'placeholder' => 'Weight (gram)',
				'help' => 'The weight will be used in calculating shipping price (round up 1 kg)'
			])

			<input type="hidden" id="subcategories" value="{{ json_encode($subcategories) }}">
			<div class="form-group">
				<button type="submit" class="btn btn-primary">Save</button>
			</div>

			{!! Form::close() !!}
		</div>
	</div>
@endsection

@section('content-js')
	@include('admin.product._productsize_mustache')
	
	<script type="x-tmpl-mustache" id="subcategoryTmpl">
		{% #data %}
			<option value="{% id %}">{% name %}</option>
		{% /data %}
	</script>
	
	<script src="{{ asset(elixir("js/admin/master/product.js")) }}"></script>
	<script>
	$(function()
	{
		var commonmark = window.commonmark;
		var parser = new commonmark.Parser();
		var renderer = new commonmark.HtmlRenderer();

		//bootsrap datepicker
		var date_input=$('input[name="launch_date"]');
		$('input-date').datepicker({
			format: "dd/mm/yyyy",
			autoclose: true,
			todayHighlight: true
		});

		$("#description").keyup(function(){
			$('#descriptionPreview').html(renderer.render(parser.parse($("#description").val())));
		});

		$("#feature_material").keyup(function(){
			$('#featurePreview').html(renderer.render(parser.parse($("#feature_material").val())));
		});

		$("#navEditorDes").click(function(){
			$('#descriptionPreview').hide();
			$('#description').show();
			$("#navEditorDes").addClass("active");
			$("#navPreviewDes").removeClass("active");
		});

		$("#navPreviewDes").click(function(){
			$('#descriptionPreview').show();
			$('#description').hide();
			$("#navEditorDes").removeClass("active");
			$("#navPreviewDes").addClass("active");
			$('#descriptionPreview').html(renderer.render(parser.parse($("#description").val())));
		});

		$("#navEditorFea").click(function(){
			$('#featurePreview').hide();
			$('#feature_material').show();
			$("#navEditorFea").addClass("active");
			$("#navPreviewFea").removeClass("active");
		});

		$("#navPreviewFea").click(function(){
			$('#featurePreview').show();
			$('#feature_material').hide();
			$("#navEditorFea").removeClass("active");
			$("#navPreviewFea").addClass("active");
			$('#featurePreview').html(renderer.render(parser.parse($("#feature_material").val())));
		});

		$("#description").focus(function(){
			$('#buttonMediaDes').show();
			$('#buttonMediaFea').hide();
		});

		$("#feature_material").focus(function(){
			$('#buttonMediaDes').hide();
			$('#buttonMediaFea').show();
		});

		$("#cancelselectDes").click(function(){
			$("#mediaLinkDes").val('');
		});

		$("#cancelselectFea").click(function(){
			$("#mediaLinkFea").val('');
		});

		$("#selectDes").click(function(){
			var cursorPosition = $('#description').prop("selectionStart");
			console.log(cursorPosition);
		    var v = $("#description").val();
		    var textBefore = v.substring(0,  cursorPosition);
		    var textAfter  = v.substring(cursorPosition, v.length);
		    if($("#mediaLinkDes").val() == ''){
		    	$("#description").val();
		    }else{
		    	$('#description').val(textBefore + "\n![Image]("+ $("#mediaLinkDes").val()+")\n" + textAfter);
		    }
		    $('#descriptionPreview').html(renderer.render(parser.parse($("#description").val())));
		});

		$("#selectFea").click(function(){
			var cursorPosition = $('#feature_material').prop("selectionStart");
			console.log(cursorPosition);
		    var v = $("#feature_material").val();
		    var textBefore = v.substring(0,  cursorPosition);
		    var textAfter  = v.substring(cursorPosition, v.length);
		    if($("#mediaLinkFea").val() == ''){
		    	$("#feature_material").val();
		    }else{
		    	$('#feature_material').val(textBefore + "\n![Image]("+ $("#mediaLinkFea").val()+")\n" + textAfter);
		    }
		    $('#featurePreview').html(renderer.render(parser.parse($("#feature_material").val())));
		});

		$(document).ready(function(){
			$('#descriptionPreview').hide();
			$('#featurePreview').hide();
			$('#description').show();
			$('#feature_material').show();
			$("#navEditorDes").addClass("active");
			$("#navEditorFea").addClass("active");
			$('#descriptionPreview').html(renderer.render(parser.parse($("#description").val())));
			$('#featurePreview').html(renderer.render(parser.parse($("#feature_material").val())));
			$('#buttonMediaDes').hide();
			$('#buttonMediaFea').hide();
		});
	});
	function setLinkD(link){
		$("#mediaLinkDes").val('');
		$("#mediaLinkDes").val(link);
	}

	function setLinkF(link){
		$("#mediaLinkFea").val('');
		$("#mediaLinkFea").val(link);
	}
	</script>
@endsection

@section('content-modal')
	@include('admin.product._media_modal')
@endsection