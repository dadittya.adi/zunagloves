<a class="list-group-item {{ isset($active) && $active == 'index' ? 'active' : '' }}" href="{{ action('Admin\ProductController@index') }}">
	List Product
</a>
<a class="list-group-item {{ isset($active) && $active == 'create' ? 'active' : '' }}" href="{{ action('Admin\ProductController@create') }}">	
	Create Product
</a>

<div class="separate-sidebar"></div>

<a class="list-group-item {{ isset($active) && $active == 'size' ? 'active' : '' }}" href="{{ action('Admin\ProductsizeController@index') }}">	
	Product Size
</a>
{{-- <a class="list-group-item {{ isset($active) && $active == 'color' ? 'active' : '' }}" href="{{ action('Admin\ProductColorController@index') }}">	
	Product Color
</a> --}}
{{-- <a class="list-group-item {{ isset($active) && $active == 'media' ? 'active' : '' }}" href="{{ action('Admin\MediaController@index') }}">	
	Media
</a> --}}