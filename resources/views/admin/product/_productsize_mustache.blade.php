<script type="x-tmpl-mustache" id="sizeTmpl">
	<tbody>
		{% #data %}
		<tr>
			<td>{% name %} ({% short_name %})</td>
			<td>
				<button type="button" class="btn btn-xs btn-danger btn-delete" data-row="{% _row %}">
					Delete
				</button>
			</td>
		</tr>
		{% /data %}
		<tr>
			<td>
				<select class="form-control" name="size_id" id="sizeSelect">
					{% #options %}
						<option value="{% _row %}">{% name %} ({% short_name %})</option>
					{% /options %}
				</select>
			</td>
			<td>
				<button type="button" class="btn btn-sm btn-success" id="addButton">
					<span class="glyphicon glyphicon-plus"></span>
				</button>
			</td>
		</tr>
	</tbody>
</script>