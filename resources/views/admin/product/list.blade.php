@extends('admin.master_layout', ['active' => 'product'])

@section('sidebar')
	@include('admin.product.sidebar', ['active' => 'index'])
@endsection

@section('content')
	<div class="container-fluid">
		{!!
			Form::open(array(
				'class' => 'form-signin',
				'role' => 'form',
				'url' => action('Admin\ProductController@index'),
				'method' => 'get',
			))
		!!}
		<div class="row" style="margin-left: -30px">
			<div class="col-md-3">
				@include('form.select', [
				'field' => 'category',
				'label' => 'Category',
				'options' => ['ALL' => 'All'] + ($category->all()),
				'default' => (request()->input('category') ? request()->input('c	ategory') : 'ALL')
				])
			</div>
			<div class="col-md-3">
				@include('form.select', [
				'field' => 'genre',
				'label' => 'Genre',
				'options' => ['ALL' => 'All', 'MEN' => 'MEN', 'WMN' => 'WOMEN', 'UNI' => 'UNISEX'],
				'default' => (request()->input('genre') ? request()->input('genre') : 'ALL')
				])
			</div>
			<div class="col-md-3">
				@include('form.select', [
				'field' => 'stock',
				'label' => 'Stock',
				'options' => ['ALL' => 'All', 0 => 'In Stock', 1 => 'Out of Stock'],
				'default' => (request()->input('stock') ? request()->input('stock') : 'ALL')
				])
			</div>
			<div class="col-md-3">
				@include('form.select', [
				'field' => 'state',
				'label' => 'Status',
				'options' => ['ALL' => 'All', '1' => 'Active', '0' => 'Inactive', '2' => 'Coming Soon', '3' => 'Pre Order'],
				'default' => (request()->input('state') ? request()->input('state') : 'ALL')
				])
			</div>
		</div>
		<div class="row" style="margin-left: -30px">
			<div class="col-md-9">
				@include('form.text', [
					'label' => 'Pencarian Detail',
					'field' => 'search',
					'placeholder' => "Search with name or sku",
					'default' => (request()->input('search') ? request()->input('search') : '')
				])
			</div>
			<div class="col-md-3" style="margin-top : 25px">
				<button type="submit" class="btn btn-primary">Search</button>
				{!! Form::close() !!}
			</div>
		</div>
		<div class="row">
			<div class="table-responsive">
				<table class="table table-banner">
					<thead>
						<tr>
							<th>#</th>
							<th>Name</th>
							<th>Genre</th>
							<th>Category</th>
							<th>Stock</th>
							<th>Price (IDR)</th>
							<th>State</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($products as $key => $product)
							<tr>
								<td>{{ $key + 1 }}</td>
								<td>{{ $product->name }}</td>
								<td>
									@if ($product->genre == 'MEN')
									Men
									@elseif ($product->genre == 'WMN')
									Women
									@elseif ($product->genre == 'UNI')
									Unisex
									@else
									{{ $product->genre }}
									@endif
								</td>
								<td>
									{{-- {{ $product->subcategory->category->name }}
									<span class="glyphicon glyphicon-chevron-right"></span>
									{{ $product->subcategory->name }} --}}
									{{ $product->category->name }}
								</td>
								<td>
									@if ($product->stock_out)
										<span style="color: red">Out of Stock</span>
									@else
										In Stock
									@endif
								</td>
								<td>{{ i18l_currency($product->price_id, 'id') }}</td>
								<td>
									@if ($product->is_active == 1)
										Active
									@elseif ($product->is_active == 2)
										Coming Soon
									@elseif ($product->is_active == 3)
										Pre Order
									@else
										Inactive
									@endif
								</td>
								<td>
									<a href="{{ action('Admin\ProductController@edit', [$product->id]) }}" class="btn btn-primary btn-xs">
										Edit
									</a>

									<a href="{{action('Admin\ProductColorController@index', [$product->id])}}" class="btn btn-warning btn-xs">
										Color
									</a>

									<a href="{{action('Admin\ProductStockController@index', [$product->id])}}" class="btn btn-success btn-xs">
										Stock
									</a>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			<div class="pull-right">
				{!! $products->render() !!}
			</div>
		</div>
	</div>
@endsection