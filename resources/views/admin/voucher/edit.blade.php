@extends('admin.master_layout', ['active' => 'voucher'])

@section('sidebar')
	@include('admin.voucher.sidebar', ['active' => 'create'])
@endsection

@section('content')
	<div class="container-fluid">
		<div class="row">
			<div class="content-header">
				<h2>
					Edit Voucher
					{!!
						Form::open(array(
							'role' => 'form',
							'url' => action('Admin\VoucherController@delete', [$voucher->id]),
							'method' => 'post',
						))
					!!}
					<button type="submit" class="btn btn-danger"
						onclick="return confirm('Are you sure want to delete this voucher?');"
					>
						Delete
					</button>
					{!! Form::close() !!}
				</h2>
			</div>
		</div>
		<div class="row">
			{!!
				Form::open([
					'role' => 'form',
					'url' => action('Admin\VoucherController@update', $voucher->id),
					'method' => 'post',
				])
			!!}

			@include('form.text', [
				'field' => 'code',
				'label' => 'Code',
				'placeholder' => 'Voucher Code',
				'default' => $voucher->code,
				'attributes' => [
					'maxlength' => 25
				]
			])

			@include('form.text', [
				'field' => 'promo',
				'label' => 'Promo',
				'placeholder' => 'Promo Name',
				'default' => $voucher->promo
			])

			@include('form.select', [
							'field' => 'type',
							'label' => 'Type',
							'options' => [
								'NOMINAL' => 'NOMINAL',
								'PERCENT' => 'PERCENT'
			],
			'default' => $voucher->type
			])
			
			<div class="row">
				<div class="col-md-4">
					@include('form.text', [
						'field' => 'discount',
						'label' => 'Discount',
						'placeholder' => 'Discount',
						'default' => (float)$voucher->discount
					])
				</div>
				<div class="col-md-4">
					@include('form.date', [
						'field' => 'start_date',
						'label' => 'Start Date (00:00)',
						'placeholder' => 'dd/mm/yyyy',
						'default' => $voucher->start_date->format('d/m/Y')
					])
				</div>
				<div class="col-md-4">
					@include('form.date', [
						'field' => 'end_date',
						'label' => 'End Date (23:59)',
						'placeholder' => 'dd/mm/yyyy',
						'default' => $voucher->end_date->format('d/m/Y')
					])
				</div>
			</div>

			@include('form.text', [
				'field' => 'minimum',
				'label' => 'Minimum Transaksi (Rupiah)',
				'placeholder' => 'Minimum Transaksi',
				'default' => (float)$voucher->minimum
			])

			<div class="form-group">
				<button type="submit" class="btn btn-primary">Save</button>
			</div>

			{!! Form::close() !!}
		</div>
	</div>
@endsection