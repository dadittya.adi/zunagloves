@extends('admin.master_layout', ['active' => 'voucher'])

@section('sidebar')
	@include('admin.voucher.sidebar', ['active' => 'create'])
@endsection

@section('content')
	<div class="container-fluid">
		<div class="row">
			{!!
				Form::open([
					'role' => 'form',
					'url' => action('Admin\VoucherController@store'),
					'method' => 'post',
				])
			!!}

			@include('form.text', [
				'field' => 'code',
				'label' => 'Code',
				'placeholder' => 'Voucher Code',
				'attributes' => [
					'maxlength' => 25
				]
			])

			@include('form.text', [
				'field' => 'promo',
				'label' => 'Promo',
				'placeholder' => 'Promo Name'
			])

			@include('form.select', [
				'field' => 'type',
				'label' => 'Type',
				'options' => [
					'NOMINAL' => 'NOMINAL',
					'PERCENT' => 'PERCENT'
				]
			])

			<div class="row">
				<div class="col-md-4">
					@include('form.text', [
						'field' => 'discount',
						'label' => 'Discount',
						'placeholder' => 'Discount'
					])
				</div>
				<div class="col-md-4">
					@include('form.date', [
						'field' => 'start_date',
						'label' => 'Start Date (00:00)',
						'placeholder' => 'dd/mm/yyyy'
					])
				</div>
				<div class="col-md-4">
					@include('form.date', [
						'field' => 'end_date',
						'label' => 'End Date (23:59)',
						'placeholder' => 'dd/mm/yyyy'
					])
				</div>
			</div>

			@include('form.text', [
				'field' => 'minimum',
				'label' => 'Minimum Transaksi (Rupiah)',
				'placeholder' => 'Minimum Transaksi'
			])

			<div class="form-group">
				<button type="submit" class="btn btn-primary">Save</button>
			</div>

			{!! Form::close() !!}
		</div>
	</div>
@endsection