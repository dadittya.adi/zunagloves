@extends('admin.master_layout', ['active' => 'voucher'])

@section('sidebar')
	@include('admin.voucher.sidebar', ['active' => 'index'])
@endsection

@section('content')
	<div class="container-fluid">
		<div class="row">
			<div class="table-responsive">
				<table class="table">
					<thead>
						<tr>
							<th>#</th>
							<th>Code</th>
							<th>Promo</th>
							<th>Type</th>
							<th>Amount</th>
							<th>Period</th>
							<th>State</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($vouchers as $key => $voucher)
							<tr>
								<td>{{ $key + 1 }}</td>
								<td>{{ $voucher->code }}</td>
								<td>{{ $voucher->promo }}</td>
								<td>{{ $voucher->type }}</td>
								<td>{{ $voucher->discount }}</td>
								<td>{{ $voucher->start_date->format('d F Y') }}
									- {{ $voucher->end_date->format('d F Y') }}
								</td>
								<td>
									{{ $voucher->state }}
								</td>
								<td>
									<a href="{{ action('Admin\VoucherController@edit', [$voucher->id]) }}" class="btn btn-primary btn-xs">
										Edit
									</a>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			<div class="pull-right">
				{!! $vouchers->render() !!}
			</div>
		</div>
	</div>
@endsection