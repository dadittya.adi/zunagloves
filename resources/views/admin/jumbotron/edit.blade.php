@extends('admin.master_layout', ['active' => 'jumbotron'])

@section('sidebar')
	@include('admin.jumbotron.sidebar', ['active' => 'index'])
@endsection

@section('content')
	<div class="container-fluid">
		<div class="row">
			<div class="content-header">
				<h2>
					Edit Jumbotron
					{!!
						Form::open(array(
							'role' => 'form',
							'url' => action('Admin\JumbotronController@delete', [$jumbotron->id]),
							'method' => 'post',
						))
					!!}
					<button type="submit" class="btn btn-danger"
						onclick="return confirm('Are you sure want to delete this showcase image?');"
					>
						Delete
					</button>
					{!! Form::close() !!}
				</h2>
			</div>
		</div>
		<div class="row">
			{!!
				Form::open([
					'role' => 'form',
					'url' => action('Admin\JumbotronController@update', [$jumbotron->id]),
					'method' => 'post',
					'enctype' => 'multipart/form-data'
				])
			!!}

			@include('form.text', [
				'field' => 'title',
				'label' => 'Title',
				'placeholder' => 'Title',
				'default' => $jumbotron->title
			])

			@include('form.text', [
				'field' => 'description',
				'label' => 'Description',
				'placeholder' => 'Description',
				'default' => $jumbotron->description
			])

			{{-- @include('form.select', [
				'field' => 'category',
				'label' => 'Category',
				'options' => $categories,
				'attributes' => [
					'id' => 'inputCategory'
				]
			]) --}}

			
			@include('form.text', [
				'field' => 'link',
				'label' => 'Link',
				'placeholder' => 'Link',
				'default' => $jumbotron->link
			])

			@include('form.text', [
				'field' => 'color',
				'label' => 'Color',
				'placeholder' => 'Color',
				'default' => $jumbotron->color
			])

			@include('form.text', [
				'field' => 'font_color',
				'label' => 'Font Color',
				'placeholder' => 'Font Color',
				'default' => $jumbotron->font_color
			])

			<div class="form-group">
				<label for="image" class="control-label">
					Image
				</label>
				<div class="control-input {{ $errors->has('image') ? 'has-error' : '' }}">
					@if ($jumbotron->image)
					<img class="thumbnail" src="{{ action('LandingController@showShowcaseImage', [$jumbotron->id]) }}" height="200">
					@endif

					{!! Form::file('image') !!}
					<span class="help-block">(JPG / PNG).</span>

					@if ($errors->has('image'))
					<span class="help-block text-danger">{{ $errors->first('image') }}</span>
					@endif
				</div>
			</div>

			<div class="form-group">
				<label for="image_detail" class="control-label">
					Image
				</label>
				<div class="control-input {{ $errors->has('image_detail') ? 'has-error' : '' }}">
					@if ($jumbotron->image)
					<img class="thumbnail" src="{{ action('LandingController@showShowcaseDetailImage', [$jumbotron->id]) }}" height="200">
					@endif

					{!! Form::file('image_detail') !!}
					<span class="help-block">(JPG / PNG).</span>

					@if ($errors->has('image_detail'))
					<span class="help-block text-danger">{{ $errors->first('image_detail') }}</span>
					@endif
				</div>
			</div>

			{{-- @include('form.select', [
				'field' => 'state',
				'label' => 'State',
				'options' => [
					'DRF' => 'Draft',
					'PUB' => 'Publish'
				],
				'default' => $jumbotron->state
			]) --}}

			<div class="form-group">
				<button type="submit" class="btn btn-primary">Save</button>
			</div>

			{!! Form::close() !!}
		</div>
	</div>
@endsection