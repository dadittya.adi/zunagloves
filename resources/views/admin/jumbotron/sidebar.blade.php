<a class="list-group-item {{ isset($active) && $active == 'desktop' ? 'active' : '' }}" href="{{ route('admin.showcase.index', 'desktop') }}">
	List Showcase Image Desktop Order
</a>

<a class="list-group-item {{ isset($active) && $active == 'mobile' ? 'active' : '' }}" href="{{ route('admin.showcase.index', 'mobile') }}">
	List Showcase Image Mobile Order
</a>

<a class="list-group-item {{ isset($active) && $active == 'create' ? 'active' : '' }}" href="{{ action('Admin\JumbotronController@create') }}">	
	Create Showcase Image
</a>