@extends('admin.master_layout', ['active' => 'jumbotron'])

@section('sidebar')
	@include('admin.jumbotron.sidebar', ['active' => 'create'])
@endsection

@section('content')
	<div class="container-fluid">
		<div class="row">
			{!!
				Form::open([
					'role' => 'form',
					'url' => action('Admin\JumbotronController@store'),
					'method' => 'post',
					'enctype' => 'multipart/form-data'
				])
			!!}

			@include('form.text', [
				'field' => 'title',
				'label' => 'Title',
				'placeholder' => 'Title'
			])

			@include('form.textarea', [
				'field' => 'description',
				'label' => 'Description',
				'placeholder' => 'Description'
			])

			{{-- @include('form.select', [
				'field' => 'category',
				'label' => 'Category',
				'options' => $categories,
				'attributes' => [
					'id' => 'inputCategory'
				]
			]) --}}

			 @include('form.text', [
				'field' => 'link',
				'label' => 'Link',
				'placeholder' => 'Link'
			])

			@include('form.text', [
				'field' => 'color',
				'label' => 'Color',
				'placeholder' => 'rgba(0,1,1,0.8)'
			])

			@include('form.text', [
				'field' => 'font_color',
				'label' => 'Font Color',
				'placeholder' => '#a83832'
			])



			@include('form.file', [
				'field' => 'image',
				'label' => 'Image',
				'help' => '(JPG / PNG).'
			])

			@include('form.file', [
				'field' => 'image_detail',
				'label' => 'Image_detail',
				'help' => '(JPG / PNG).'
			])

			{{-- @include('form.select', [
				'field' => 'state',
				'label' => 'State',
				'options' => [
					'DRF' => 'Draft',
					'PUB' => 'Publish'
				]
			]) --}}

			<div class="form-group">
				<button type="submit" class="btn btn-primary">Save</button>
			</div>

			{!! Form::close() !!}
		</div>
	</div>
@endsection