@extends('admin.master_layout', ['active' => 'showcase'])

@section('sidebar')
	@include('admin.jumbotron.sidebar', ['active' => 'desktop'])
@endsection

@section('content')
	<div class="container-fluid">
		<div class="row">
			<div class="table-responsive">
				<table class="table table-banner">
					<thead>
						<tr>
							<th>Show Order Desktop</th>
							<th>Title</th>
							<th>Description</th>
							<th>thumbnail</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($jumbotrons as $key => $jumbotron)
							<tr>
								<td>
									{!!
										Form::open(array(
											'action' => array('Admin\JumbotronController@move', $jumbotron->id, -1),
											'method' => 'post',
											'style' => 'display: inline-block;',
										))
									!!}

									<button type="submit" class="btn btn-xs" {{ $key > 0 ? '' : 'disabled' }}>
										<span class="glyphicon glyphicon-chevron-up"></span>
									</button>
									
									{!! Form::close() !!}
									
									{{ $key + 1 }}

									{!!
										Form::open(array(
											'action' => array('Admin\JumbotronController@move', $jumbotron->id, 1),
											'method' => 'post',
											'style' => 'display: inline-block;',
										))
									!!}

									<button type="submit" class="btn btn-xs" {{ ($key + 1) < $jumbotrons->count() ? '' : 'disabled' }}>
										<span class="glyphicon glyphicon-chevron-down"></span>
									</button>
									
									{!! Form::close() !!}
								</td>

								
								<td>{{ $jumbotron->title }}</td>
								<td>{{ $jumbotron->description ? $jumbotron->description : '-' }}</td>
								<td>
									<img class="thumbnail" src="{{ action('LandingController@showShowcaseImage', [$jumbotron->id]) }}" height="150" style="display:inline">
								</td>
								<td>
									<a href="{{ action('Admin\JumbotronController@edit', [$jumbotron->id]) }}" class="btn btn-primary btn-xs">
										Edit
									</a>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
@endsection