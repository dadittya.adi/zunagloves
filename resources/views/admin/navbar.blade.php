<ul class="nav navbar-nav">
	@if (Auth::guard('admins')->user()->hasGroup('ADM'))
	{{-- <li class="{{ isset($active) && $active == 'admin' ? 'active' : '' }}">
		<a href="{{ action('Admin\AdminController@index') }}">Admin</a>
	</li>
	<li class="{{ isset($active) && $active == 'activity' ? 'active' : '' }}">
		<a href="{{ action('Admin\ActivityController@index') }}">Activity</a>
	</li> --}}
	<li class="dropdown {{ isset($active) && ($active == 'shwocase' || $active == 'promo') ? 'active' : '' }}">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Promo <span class="caret"></span></a>
        <ul class="dropdown-menu">
         	<li class="{{ isset($active) && $active == 'shwocase' ? 'active' : '' }}">
				<a href="{{ action('Admin\JumbotronController@index', 'desktop') }}">Showcase Image</a>
			</li>
			<li class="{{ isset($active) && $active == 'voucher' ? 'active' : '' }}">
				<a href="{{ action('Admin\VoucherController@index') }}">Voucher</a>
			</li>
			<li class="{{ isset($active) && $active == 'subscribe' ? 'active' : '' }}">
				<a href="{{ action('Admin\SubscribedEmailController@index') }}">Subscribed Email</a>
			</li>
			<li class="{{ isset($active) && $active == 'citystore' ? 'active' : '' }}">
				<a href="{{ action('Admin\CityStoreController@index') }}">City Store</a>
			</li>
			{{-- <li class="{{ isset($active) && $active == 'promo' ? 'active' : '' }}">
				<a href="{{ action('Admin\PromoController@index') }}">Promo</a>
			</li> --}}
        </ul>
	</li>
	<li class="dropdown {{ isset($active) && ($active == 'category' || $active == 'product' || $active == 'voucher' || $active == 'shipping') ? 'active' : '' }}">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Product <span class="caret"></span></a>
        <ul class="dropdown-menu">
			<li class="{{ isset($active) && $active == 'category' ? 'active' : '' }}">
				<a href="{{ action('Admin\CategoryController@index') }}">Category</a>
			</li>
			<li class="{{ isset($active) && $active == 'product' ? 'active' : '' }}">
				<a href="{{ action('Admin\ProductController@index') }}">Product</a>
			</li>
			{{-- <li class="{{ isset($active) && $active == 'voucher' ? 'active' : '' }}">
				<a href="{{ action('Admin\VoucherController@index') }}">Voucher</a>
			</li>
			<li class="{{ isset($active) && $active == 'shipping' ? 'active' : '' }}">
				<a href="{{ action('Admin\ShippingController@index') }}">Shipping</a>
			</li> --}}
		</ul>
    </li>
	<li class="dropdown {{ isset($active) && ($active == 'category' || $active == 'product' || $active == 'voucher' || $active == 'shipping') ? 'active' : '' }}">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Customer <span class="caret"></span></a>
        <ul class="dropdown-menu">
			<li class="{{ isset($active) && $active == 'customer' ? 'active' : '' }}">
				<a href="{{ action('Admin\CustomerController@index') }}">Customer</a>
			</li>
			<li class="{{ isset($active) && $active == 'discount_request' ? 'active' : '' }}">
				<a href="{{ action('Admin\DiscountRequestController@getImage') }}">Pop Up Setting</a>
			</li>
		</ul>
    </li>

	<li class="{{ isset($active) && $active == 'order' ? 'active' : '' }}">
		<a href="{{ action('Admin\OrderController@index') }}">Order</a>
	</li>
	<li class="{{ isset($active) && $active == 'report' ? 'active' : '' }}">
		<a href="{{ action('Admin\OrderController@report') }}">Report</a>
	</li>
	@endif
	
	{{-- @if (Auth::guard('admins')->user()->hasGroup('PRD') || Auth::guard('admins')->user()->hasGroup('ADM'))
	<li class="dropdown {{ isset($active) && ($active == 'category' || $active == 'product' || $active == 'voucher' || $active == 'shipping') ? 'active' : '' }}">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Product <span class="caret"></span></a>
        <ul class="dropdown-menu">
			<li class="{{ isset($active) && $active == 'category' ? 'active' : '' }}">
				<a href="{{ action('Admin\CategoryController@index') }}">Category</a>
			</li>
			<li class="{{ isset($active) && $active == 'product' ? 'active' : '' }}">
				<a href="{{ action('Admin\ProductController@index') }}">Product</a>
			</li>
			<li class="{{ isset($active) && $active == 'voucher' ? 'active' : '' }}">
				<a href="{{ action('Admin\VoucherController@index') }}">Voucher</a>
			</li>
			<li class="{{ isset($active) && $active == 'shipping' ? 'active' : '' }}">
				<a href="{{ action('Admin\ShippingController@index') }}">Shipping</a>
			</li>
		</ul>
    </li>
	@endif

	@if (Auth::guard('admins')->user()->hasGroup('ORD') || AAuth::guard('admins')->user()->hasGroup('ADM'))
	<li class="dropdown {{ isset($active) && ($active == 'customer' || $active == 'subscribe') ? 'active' : '' }}">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Customer <span class="caret"></span></a>
        <ul class="dropdown-menu">
			<li class="{{ isset($active) && $active == 'customer' ? 'active' : '' }}">
				<a href="{{ action('Admin\CustomerController@index') }}">Customer</a>
			</li>
			<li class="{{ isset($active) && $active == 'subscribe' ? 'active' : '' }}">
				<a href="{{ action('Admin\SubscribedEmailController@index') }}">Subscribed Email</a>
			</li>
		</ul>
	</li>
	<li class="{{ isset($active) && $active == 'order' ? 'active' : '' }}">
		<a href="{{ action('Admin\OrderController@index') }}">Order</a>
	</li>
	<li class="{{ isset($active) && $active == 'report' ? 'active' : '' }}">
		<a href="{{ action('Admin\OrderController@report') }}">Report</a>
	</li>
	@endif
	
	@if (Auth::guard('admins')->user()->hasGroup('BLO') || Auth::guard('admins')->user()->hasGroup('ADM'))
	<li class="{{ isset($active) && $active == 'blog' ? 'active' : '' }}">
		<a href="{{ action('Admin\BlogController@index') }}">Blog</a>
	</li>
	@endif --}}
	
</ul>

