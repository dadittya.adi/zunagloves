@extends('admin.master_layout')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Change Password</div>
				<div class="panel-body">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Failed to change password!</strong><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif
					@if (Session::has('PASSWORD_CHANGED.OK'))
						<div class="alert alert-success">
							<strong>Password changed.</strong>
						</div>
					@elseif (Session::has('PASSWORD_CHANGED.FAIL'))
						<div class="alert alert-danger">
							<strong>Wrong current password.</strong>
						</div>
					@endif

					{!! Form::open([
						'role' => 'form',
						'method' => 'post',
						'class' => 'form-horizontal',
						'url' => action('Admin\AuthController@postChangePassword')
					]) !!}

					<div class="form-group">
						<label class="col-md-4 control-label">Current Password</label>
						<div class="col-md-6">
							<input class="form-control" type="password" name="password_current">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label">New Password</label>
						<div class="col-md-6">
							<input class="form-control" type="password" name="password_new">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label">Password Confirmation</label>
						<div class="col-md-6">
							<input class="form-control" type="password" name="password_confirm">
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-md-6 col-md-offset-4">
							<button type="submit" class="btn btn-primary">Submit</button>
						</div>
					</div>

					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
