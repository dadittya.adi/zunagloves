<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h3>Payment Order #{{ $order->order_number }}</h3>

		<div>
			<p>
				Payment order has been received.
				This order will be proccessed.
			</p>

			@include('admin.email._info_invoice')

			@if ($order->payment == 'ATM')
				<p>Payment by <b>ATM</b></p>

				<p>
					Date:
					@if($order->paid_at)
						{{ $order->paid_at->format('d-m-Y H:i') }}
					@else
						<i>[Empty]</i>
					@endif
					<br>

          Pay Amount:
					@if($order->paid_amount)
						{{ i18l_currency($order->paid_amount) }}
					@else
						<i>[Empty]</i>
					@endif
					<br>

          Bank:
					@if($order->bank_acc)
						{{ $order->bank_acc }}
					@else
						<i>[Empty]</i>
					@endif
					<br>

          In Behalf of:
					@if($order->name_acc)
						{{ $order->name_acc }}
					@else
						<i>[Empty]</i>
					@endif
					<br>

          Account Number:
					@if($order->no_acc)
						{{ $order->no_acc }}
					@else
						<i>[Empty]</i>
					@endif
					<br>
				</p>
			@endif

			@if ($order->paid_notes)
				<p>Notes : <br>
					<i>{{ nl2br(e($order->paid_notes)) }}</i>
				</p>
			@endif

			<p>You can get further information through:</p>
			<p style="margin-left: 15px">
				<a href="{{ action('Admin\OrderController@show', [$order->id]) }}" target="_blank">
					{{ action('Admin\OrderController@show', [$order->id]) }}
				</a>
			</p>
		</div>

		<div style="font-size: 11px; margin-top: 10px">
			This email was sent automatically by Zuna Sport.
		</div>
	</body>
</html>
