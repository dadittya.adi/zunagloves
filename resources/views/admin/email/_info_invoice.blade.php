<p>
	<span>Shipping Address:</span><br>
  	<span>
    	{{ $order->shipping->full_name }}
    	{{ $order->shipping->organization ? '(' . $order->shipping->organization . ')' : '' }}
  	</span><br>
  	<span>{{ nl2br(e($order->shipping->address)) }}</span><br>
  	<span>
        {{ $order->shipping->city }}, {{ $order->shipping->province }}, {{ $order->shipping->country }}
        {{ $order->shipping->postal_code }}
    </span><br>
  	<span>Telp. {{ $order->shipping->phone }}</span>
</p>

<p>
    <span>Shipping Method:</span><br>
    <span>{{ $order->provider }}</span><br>
    <span>{{ $order->destination }}</span><br>
    @if ($order->shipping_time != '0')
    	<span>{{ $order->shipping_time }} day(s)</span>
    @endif
</p>


<p>Order Items:</p>

<table border="1" cellpadding="5" style="border-spacing: 1px; border-color: #333; border-collapse: collapse">
	<thead>
	    <tr>
	        <th rowspan="1">
	            <span>Product</span>
	        </th>
	        <th colspan="1">
	            <span>Price</span>
	        </th>
	        <th rowspan="1">Qty</th>
	        <th colspan="1">Subtotal</th>
	    </tr>
	</thead>
	<tbody>
	    <?php $total = 0 ?>
	    @foreach ($order->items as $item)
	    <tr>
	        <td>
        		<strong>{{ $item->product->name }}</strong>
   				@if ($item->color)
					<br>Color : {{ $item->color }}
				@endif
				@if ($item->size)
					<br>Size : {{ $item->size }}
				@endif
                @if ($item->notes)
					<br>Keterangan : <br>
					<span style="padding-left: 10px">
						<i>{!! nl2br(e($item->notes)) !!}</i>
					</span>
				@endif
	        </td>
	        <td align="right">
	            {{ i18l_currency($item->price, $order->domain) }}
	        </td>
	        <td align="center">
				{{ (float)$item->qty }}
	        </td>
	        <td align="right">
	            {{ i18l_currency($subtotal = $item->total, $order->domain) }}
				@if ($item->discount > 0)
				<br><span style="color:red">
					Disc : ({{ i18l_currency($item->discount, $order->domain) }})
				</span>
				@endif
	        </td>
	    </tr>
	    <?php $total = $total + $subtotal - $item->discount ?>
	    @endforeach
	</tbody>
	<tfoot>
	    <tr>
	        <td colspan="3" align="right"> 
	            Subtotal
	        </td>
	        <td align="right">
	            <span>{{ i18l_currency($total, $order->domain)}}</span>
	        </td>
	    </tr>
	    <tr>
	        <td colspan="3" align="right"> 
	            Delivery Cost
	        </td>
	        <td align="right">
	            <span>
	            	@if ($order->shipping_price > 0)
	            		{{ i18l_currency($order->shipping_price, $order->domain) }}
	            	@else
	            		-
	            	@endif
	            </span>
	        </td>
	    </tr>
	    @if ($order->voucher_discount > 0)
	    <tr>
	        <td colspan="3" align="right"> 
	            Discount ({{ $order->voucher_code }})
	        </td>
	        <td align="right">
	            <span>({{ i18l_currency($order->voucher_discount, $order->domain) }})</span>
	        </td>
	    </tr>
	    @endif
	    <tr>
	        <td colspan="3" align="right"> 
	            <strong>Grand Total</strong>
	        </td>
	        <td align="right"> 
	            <strong><span>{{ i18l_currency($order->total, $order->domain) }}</span></strong> 
	        </td>
	    </tr>
	</tfoot>
</table>