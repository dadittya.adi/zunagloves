<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h3>Revision {{{ $order->revision }}} Invoice Order #{{{ $order->order_number }}}</h3>

		<div>
			@include('admin.email._info_invoice')

			@if ($notes)
				<p>Notes : <br>
					<i>{{ nl2br(e($notes)) }}</i>
				</p>
			@endif

			<p>You can get further information through:</p>
			<p style="margin-left: 15px">
				<a href="{{ action('Admin\OrderController@show', [$order->id]) }}" target="_blank">
					{{ action('Admin\OrderController@show', [$order->id]) }}
				</a>
			</p>
		</div>

		<div style="font-size: 11px; margin-top: 10px">
			This email was sent automatically by Zuna Sport.
		</div>
	</body>
</html>
