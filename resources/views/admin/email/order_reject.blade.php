<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h3>Order #{{ $order->order_number }} Rejected</h3>

		<div>
			@if (isset($admin))
				<p>{{ $admin->full_name }} decided that order #{{ $order->order_number }} can not be processed.</p>
			@else
				<p>Order #{{ $order->order_number }} is canceled / rejected by System.</p>
			@endif

			@if ($order->reject_notes)
				<p>Notes : <br>
					<i>{{ nl2br(e($order->reject_notes)) }}</i>
				</p>
			@endif
			<p>You can get further information through:</p>
			<p style="margin-left: 15px">
				<a href="{{ action('Admin\OrderController@show', [$order->id]) }}" target="_blank">
					{{ action('Admin\OrderController@show', [$order->id]) }}
				</a>
			</p>
		</div>

		<div style="font-size: 11px; margin-top: 10px">
			This email was sent automatically by Zuna Sport.
		</div>
	</body>
</html>
