<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h3>Order #{{{ $order->order_number }}} Delivered</h3>

		<div>
			<p>Order #{{ $order->order_number }} was declared completed by {{{ $admin->full_name }}}.</p>
			@if ($order->delivery_notes)
				<p>Notes : <br>
					<i>{{ nl2br(e($order->delivery_notes)) }}</i>
				</p>
			@endif
			<p>You can get further information through:</p>
			<p style="margin-left: 15px">
				<a href="{{ action('Admin\OrderController@show', [$order->id]) }}" target="_blank">
					{{ action('Admin\OrderController@show', [$order->id]) }}
				</a>
			</p>
		</div>

		<div style="font-size: 11px; margin-top: 10px">
			This email was sent automatically by Zuna Sport.
		</div>
	</body>
</html>
