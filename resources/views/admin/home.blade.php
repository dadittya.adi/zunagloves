@extends('admin.master_layout')

@section('sidebar')
	
@endsection

@section('content')
	<div class="container-fluid">
		<div class="row">
			<h5>Welcome, {{ Auth::guard('admins')->user()->full_name }}</h5>
		</div>
		@if (Auth::guard('admins')->user()->hasGroup('ORD') || Auth::guard('admins')->user()->hasGroup('ADM'))
		<div class="row">
			<h5>OUTSTANDING ORDER</h5>
			<table class="table table-banner">
				<thead>
					<tr>
						<th>#</th>
						<th>Status</th>
						<th>Total</th>
						<th>Detail</th>
					</tr>
				</thead>
				<tbody>
					@foreach($orders as $key => $order)
						<tr>
							<td>{{ $key + 1 }}</td>
							<td>
								@if($order->state == 'INV')
									{{ 'Invoice' }}
								@elseif($order->state == 'CHK')
									{{ 'Payment Check' }}
								@elseif($order->state == 'PAI')
									{{ 'Paid' }}
								@elseif($order->state == 'WRK')
									{{ 'Work' }}
								@endif
							</td>
							<td>{{ $order->count }}</td>
							<td>
								<a href="{{ action('Admin\OrderController@index', ['filter_state' => $order->state]) }}" class="btn btn-primary btn-xs">
										Show
									</a>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
		@endif
	</div>
@endsection