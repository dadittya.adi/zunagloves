<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Zuna Gloves Admin</title>

	<link rel="stylesheet" href="{{ asset("css/admin/app.css") }}">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/mustache.js/3.0.0/mustache.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/commonmark/0.29.3/commonmark.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/locales/bootstrap-datepicker-en-CA.min.js"></script>
</head>
<body>
	<div class="navbar navbar-default navbar-fixed-top">
		<div class="container-fluid">
			<div class="navbar-header logo-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle Navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="{{ action('Admin\HomeController@index') }}">
					<!--img class="pull-left" src="{{ asset("images/logo.jpg") }}" height="34" style="margin-top: -7px; padding-right: 7px"-->
					Zuna Gloves
				</a>
			</div>

			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				@if (Auth::guard('admins')->check())
				@include('admin.navbar', isset($active) ? ['active' => $active] : [] )
				<ul class="nav navbar-nav navbar-right">
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ Auth::guard('admins')->user()->full_name }} <span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							<li class="dropdown-header">{{ Auth::guard('admins')->user()->full_name }}</li>
							<li><a href="{{{ action('Admin\AuthController@getChangePassword') }}}">Change Password</a></li>
							<li><a href="{{{ action('Admin\AuthController@getLogout') }}}">Logout</a></li>
						</ul>
					</li>
				</ul>
				@endif
			</div>
		</div>
	</div>

	<div class="page-wrapper">
		@if (isset(View::getSections()['sidebar']))
		<div class="sidebar-wrapper">
			<div class="sidebar">
				<!-- TODO: REMAKE THIS MENU TO UL -->
				<div class="list-group">
					@yield('sidebar')
				</div>
			</div>
		</div>
		<div class="content-wrapper">
			<div class="content">
				@include('admin.top_notif')
				@yield('content')
			</div>
		</div>
		@else
		<div class="content-wrapper content-wrapper-no-sidebar">
			<div class="content">
				@include('admin.top_notif')
				@yield('content')
			</div>
		</div>
		@endif

	</div>
	
	<footer>
		<div class="copyright">
			Zunagloves by Lokavor Studio &copy;
		</div>
	</footer>

	<!-- Modal -->
	@yield('content-modal')

	<!-- Script -->
	<script src="{{ asset("js/admin/app.js") }}"></script>
	@yield('content-js')
</body>
</html>
