@extends('admin.master_layout', ['active' => 'jumbotron'])

@section('sidebar')
	@include('admin.discount_request.sidebar', ['active' => 'index'])
@endsection

@section('content')
	<div class="container-fluid">
		<div class="row">
			<div class="content-header">
				<h2>
					Pop Up Setting
					{!!
						Form::open(array(
							'role' => 'form',
							'url' => action('Admin\JumbotronController@delete', [$jumbotron->id]),
							'method' => 'post',
						))
					!!}
					{!! Form::close() !!}
				</h2>
			</div>
		</div>
		<div class="row">
			{!!
				Form::open([
					'role' => 'form',
					'url' => action('Admin\DiscountRequestController@updateImage', [$jumbotron->id]),
					'method' => 'post',
					'enctype' => 'multipart/form-data'
				])
			!!}

			<div class="form-group">
				<label for="image" class="control-label">
					Image
				</label>
				<div class="control-input {{ $errors->has('image') ? 'has-error' : '' }}">
					@if ($jumbotron->image)
					<img class="thumbnail" src="{{ action('LandingController@showPopupImage', [$jumbotron->id]) }}" height="200">
					@endif

					{!! Form::file('image') !!}
					<span class="help-block">(JPG / PNG).</span>

					@if ($errors->has('image'))
					<span class="help-block text-danger">{{ $errors->first('image') }}</span>
					@endif
				</div>

				@include('form.text', [
					'field' => 'promo',
					'label' => 'Promo',
					'placeholder' => 'Promo Name',
					'default' => $jumbotron->promo
				])

				@include('form.text', [
					'field' => 'code_length',
					'label' => 'Panjang Karakter Kode Voucher',
					'placeholder' => 'Panjang Karakter Kode Voucher',
					'default' => $jumbotron->code_length
				])

				@include('form.text', [
					'field' => 'valid_in',
					'label' => 'Masa Berlaku',
					'placeholder' => 'Hari',
					'default' => $jumbotron->valid_in
				])

				@include('form.select', [
					'field' => 'type',
					'label' => 'Type',
					'options' => [
						'NOMINAL' => 'NOMINAL',
						'PERCENT' => 'PERCENT'
					],
					'default' => $jumbotron->type
				])

				@include('form.text', [
					'field' => 'discount',
					'label' => 'Discount',
					'placeholder' => 'Discount',
					'default' => (float)$jumbotron->discount
				])

				@include('form.text', [
					'field' => 'minimum',
					'label' => 'Minimum Transaksi (Rupiah)',
					'placeholder' => 'Minimum Transaksi',
					'default' => (float)$jumbotron->minimum
				])
			</div>

			<div class="form-group">
				<button type="submit" class="btn btn-primary">Save</button>
			</div>

			{!! Form::close() !!}
		</div>
	</div>
@endsection