@extends('admin.master_layout', ['active' => 'subscribe'])

@section('sidebar')
	@include('admin.discount_request.sidebar', ['active' => 'index'])
@endsection

@section('content')
	<div class="container-fluid">
		<div class="row">
			<a href="{{ action('Admin\DiscountRequestController@export', ['search' => request()->input('search')]) }}" class="btn btn-success btn-md">
			  <span class="glyphicon glyphicon-file" aria-hidden="true"></span> Export To Excel
			</a>
			<div class="table-responsive">
				<table class="table table-banner">
					<thead>
						<tr>
							<th>#</th>
							<th>Email</th>

						</tr>
					</thead>
					<tbody>
						@foreach ($emails as $key => $email)
							<tr>
								<td>{{ $key + 1 }}</td>
								<td>{{ $email->email }}</td>
								
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
@endsection