@extends('admin.master_layout', ['active' => 'order'])

@section('sidebar')
	@include('admin.order.sidebar', ['active' => 'index'])
@endsection

@section('content')
	<div class="container-fluid">
		<div class="row">
			<div class="form-group col-md-4 pull-right">
				{!!
					Form::open([
						'action' => 'Admin\OrderController@index',
						'method' => 'get'
					])
				!!}
				{!! Form::select('filter_state', [
					'ALL' => 'All',
					'INV' => 'Invoice',
					'CHK' => 'Payment Check',
					'PAI' => 'Paid',
					'WRK' => 'Working',
					'DLV' => 'Delivered',
					'REJ' => 'Rejected',
					], $filter_state, [
					'class' => 'form-control',
					'onchange' => "this.form.submit()",
				]) !!}
				{!! Form::close() !!}
			</div>
		</div>
		<div class="row">
			<div class="table-responsive">
				<table class="table table-banner">
					<thead>
						<tr>
							<th>Order Number #</th>
							<th>Date</th>
							<th>Customer</th>
							<th>State</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($orders as $key => $order)
							<tr>
								<td>{{ $order->order_number }}</td>
								<td>{{ $order->created_at->format('d M Y H:i:s') }}</td>
								@if (isset($order->customer))
									<td>{{ $order->full_name }}</td>
								@else 
									<td>{{ $order->billing->full_name }}</td>
								@endif
								<td>
									@if ($order->state == 'INV')
										Invoice
									@elseif ($order->state == 'CHK')
										Payment Check
									@elseif ($order->state == 'PAI')
										Paid
									@elseif ($order->state == 'WRK')
										On Work
									@elseif ($order->state == 'DLV')
										Finished / Delivered
									@elseif ($order->state == 'REJ')
										Rejected / Canceled
									@else
										{{{ $order->state }}}
									@endif
								</td>
								<td>
									<a class="btn btn-primary btn-xs"
										href="{{ action('Admin\OrderController@show', [$order->id]) }}"
									>
										Show
									</a>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			<div class="pull-right">
				{!! $orders->render() !!}
			</div>
		</div>
	</div>
@endsection