@extends('admin.master_layout', ['active' => 'order'])

@section('sidebar')
	@include('admin.order.sidebar', ['active' => 'index'])
@endsection

@section('content')
	<div class="container-fluid">
		<div class="row form-group">
			<div class="content-header col-md-12">
				<h2>
					Order #{{{ $order->order_number }}}
					@if ($order->state == 'INV')
						[INVOICE] Revision {{{ $order->revision }}}
					@elseif ($order->state == 'CHK')
						[PAYMENT CHECK]
					@elseif ($order->state == 'PAI')
						[PAID]
					@elseif ($order->state == 'WRK')
						[ON WORK]
					@elseif ($order->state == 'DLV')
						[DELIVERED]
					@elseif ($order->state == 'REJ')
						[REJECTED]
					@else
						[{{ $order->state }}]
					@endif
				</h2>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12 table-responsive">
				<table class="table">
					<thead>
						<tr>
							<th colspan="2">BUYER ({{ strtoupper($order->domain) }})</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td width="30%">Name</td>
							<td>
								{{ $order->full_name }}
								{{-- <a href="{{ action('Admin\CustomerController@detail', [$order->customer->id]) }}"
									 class="btn btn-xs btn-info"
								>
									More Info
								</a> --}}
							</td>
						</tr>
						<tr>
							<td>Revision Number</td>
							<td>{{ $order->revision }}</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>

		<div class="row">
			<div class="col-md-6 table-responsive">
				<table class="table">
					<thead>
						<tr>
							<th colspan="2">SHIPPING ADDRESS</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>Full Name</td>
							<td>{{ $order->shipping->full_name }}</td>
						</tr>
						<tr>
							<td>Phone</td>
							<td>{{ $order->shipping->phone }}</td>
						</tr>
						{{-- <tr>
							<td>Organization / Company</td>
							<td>{{ $order->shipping->organization ? $order->shipping->organization : '-' }}</td>
						</tr> --}}
						<tr>
							<td>Address</td>
							<td>{!! nl2br(e($order->shipping->address)) !!}</td>
						</tr>
						<tr>
							<td>City / Regency</td>
							<td>{{ $order->shipping->cityb->full_name }}</td>
						</tr>
						<tr>
							<td>Province</td>
							<td>{{ $order->shipping->provinceb->province }}</td>
						</tr>
						{{-- <tr>
							<td>Country</td>
							<td>{{ $order->shipping->country }}</td>
						</tr> --}}
						<tr>
							<td>Postal Code</td>
							<td>{{ $order->shipping->postal_code }}</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="col-md-6 table-responsive">
				<table class="table">
					<thead>
						<tr>
							<th colspan="2">SHIPPING METHOD</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>Provider</td>
							<td>{{ $order->provider }}</td>
						</tr>
						<tr>
							<td>Destination</td>
							<td>{{ $order->destination }}</td>
						</tr>
						<tr>
							<td>Tariff</td>
							<td>{{ $order->shipping_price > 0 ? i18l_currency($order->shipping_price, $order->domain) : '-' }}</td>
						</tr>
						<tr>
							<td>Shipping Time</td>
							<td>{{ $order->shipping_time != '0' ? $order->shipping_time . ' day(s)' : '-' }}</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12 table-responsive">
				<table class="table">
					<thead>
						<tr>
							<th colspan="2">PAYMENT INFO</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td width="30%">Method</td>
							<td>
								@if ($order->payment_method == 'ATM')
									Direct Bank Transfer
								@else
									{{ $order->payment }}
								@endif
							</td>
						</tr>
						@if ($order->state != 'INV')
						<tr>
							<td>Date</td>
							@if ($order->paid_at != null)
							<td>{{ $order->paid_at->format('d/m/Y H:i') }}</td>	
							@endif
						</tr>
						<tr>
							<td>Amount</td>
							<td>{{ i18l_currency($order->paid_amount, $order->domain) }}</td>
						</tr>
						<tr>
							<td>Bank</td>
							<td>{{ $order->bank_acc ? $order->bank_acc : '-' }}
						</tr>
						<tr>
							<td>Name of Account</td>
							<td>{{ $order->name_acc ? $order->name_acc : '-' }}
						</tr>
						<tr>
							<td>Account Number</td>
							<td>{{ $order->no_acc ? $order->no_acc : '-' }}
						</tr>
						@if($order->banknote_image)
						<tr>
							<td>Transfer Receipt</td>
							<td>
								<a href="{{{ action('Admin\OrderController@downloadBanknote', [$order->id]) }}}" target="_blank">
									Transfer Receipt
								</a>
							</td>
						</tr>
						@endif
						@if ($order->paid_notes)
						<tr>
							<td>Notes</td>
							<td>{!! nl2br($order->paid_notes) !!}</td>
						</tr>
						@endif
						@endif
					</tbody>
				</table>
			</div>
		</div>

		@if ($order->voucher_discount > 0)
		<div class="row">
			<div class="col-md-12 table-responsive">
				<table class="table">
					<thead>
						<tr>
							<th>VOUCHER</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td width="30%">Code</td>
							<td>{{ $order->voucher_code }}</td>
						</tr>
						<tr>
							<td>Promo</td>
							<td>{{ $order->voucher_promo }}</td>
						</tr>
						@if ($order->voucher_minimum > 0)
						<tr>
							<td>Minimum Order</td>
							<td>{{ i18l_currency($order->voucher_minimum, $order->domain) }}</td>
						</tr>
						@endif
					</tbody>
				</table>
			</div>
		</div>
		@endif

		@if ($order->work_notes)
		<div class="row">
			<div class="col-md-12 table-responsive">
				<table class="table">
					<thead>
						<tr>
							<th>WORK NOTES</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>{!! nl2br(e($order->work_notes)) !!}</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		@endif

		@if ($order->delivery_notes)
		<div class="row">
			<div class="col-md-12 table-responsive">
				<table class="table">
					<thead>
						<tr>
							<th>DELIVERY NOTES</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>{!! nl2br(e($order->delivery_notes)) !!}</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		@endif

		@if ($order->reject_notes)
		<div class="row">
			<div class="col-md-12 table-responsive">
				<table class="table">
					<thead>
						<tr>
							<th>REJECT NOTES</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>{!! nl2br(e($order->reject_notes)) !!}</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		@endif

		@include('admin.order._detail_item')
		
		<div class="row">
			<div class="col-md-12">
				@if ($order->state == 'INV')
				<button type="button" class="btn btn-success" data-toggle="modal" data-target="#manualPaidModal">
					Manual Payment Confirmation
				</button>
				{{-- <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#revisionModal">
					Save &amp; Send Invoice Revision
				</button> --}}
				@elseif ($order->state == 'CHK')
				<button type="button" class="btn btn-success" data-toggle="modal" data-target="#paidModal">
					Set Paid
				</button>
				@elseif ($order->state == 'PAI')
				<button type="button" class="btn btn-success" data-toggle="modal" data-target="#workModal">
					Process Order
				</button>
				<button type="button" class="btn btn-warning" data-toggle="modal" data-target="#unpaidModal">
					Unpaid Order
				</button>
				@elseif ($order->state == 'WRK')
				<button type="button" class="btn btn-success" data-toggle="modal" data-target="#finishModal">
					Finish Order
				</button>
				@endif

				@if ($order->state != 'DLV' && $order->state != 'REJ')
				<button type="button" class="btn btn-danger pull-right" data-toggle="modal" data-target="#rejectModal">
					Reject Order
				</button>
				@endif
			</div>
		</div>
	</div>

	<input type="hidden" id="orderData" value="{{ $order->getItem() }}">
	<input type="hidden" id="shippingPrice" value="{{ (float)$order->shipping_price }}" data-weight="{{ $order->weight }}">
	<input type="hidden" id="voucherDisc" value="{{ (float)$order->voucher_discount }}">
@endsection

@section('content-modal')
	@include('admin.order._manual_paid_modal')
	@include('admin.order._paid_modal')
	@include('admin.order._work_modal')
	@include('admin.order._unpaid_modal')
	@include('admin.order._finish_modal')
	@include('admin.order._reject_modal')
	@include('admin.order._revision_modal')
@endsection

@section('content-js')
	@if ($order->state == "INV")
	@include('admin.order._detail_item_mustache')
	{{-- <script src="{{ asset(elixir("js/order.js")) }}"></script> --}}
	@endif
@endsection