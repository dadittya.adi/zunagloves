<div class="modal fade" id="finishModal" tabindex="-1" role="dialog" aria-labelledby="Finish Order" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<form action="{{ action('Admin\OrderController@finish', [$order->id]) }}" method="post">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Finish Order #{{ $order->order_number }}</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label for="inputDeliveryNotes">Delivery Notes</label>
						{!! Form::textarea('delivery_notes', "Order anda telah kami kirim menggunakan jasa pengiriman " . strtoupper($order->provider) . " dengan nomor resi ", [
							'class' => 'form-control',
							'id' => 'inputDeliveryNotes',
						]) !!}
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					<button type="submit" class="btn btn-success">Finish Order</button>
				</div>
			</form>
		</div>
	</div>
</div>