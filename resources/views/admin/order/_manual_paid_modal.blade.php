<div class="modal fade" id="manualPaidModal" tabindex="-1" role="dialog" aria-labelledby="Set as Paid" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<form action="{{ action('Admin\OrderController@manualPaid', [$order->id]) }}" method="post">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Set Paid Order #{{ $order->order_number }}</h4>
				</div>
				<div class="modal-body">
					<?php $now = \Carbon\Carbon::now(); ?>
					<div class="form-group">
						<label for="inputDay">Date</label>
						<div class="row">
							<div class="col-xs-2">
							{!! Form::select('paid_at_day', [
									'01' => '01',
									'02' => '02',
									'03' => '03',
									'04' => '04',
									'05' => '05',
									'06' => '06',
									'07' => '07',
									'08' => '08',
									'09' => '09',
									'10' => '10',
									'11' => '11',
									'12' => '12',
									'13' => '13',
									'14' => '14',
									'15' => '15',
									'16' => '16',
									'17' => '17',
									'18' => '18',
									'19' => '19',
									'20' => '20',
									'21' => '21',
									'22' => '22',
									'23' => '23',
									'24' => '24',
									'25' => '25',
									'26' => '26',
									'27' => '27',
									'28' => '28',
									'29' => '29',
									'30' => '30',
									'31' => '31'
									], $now->format('d'), ['class' => 'form-control']) !!}
							</div>
							<div class="col-xs-4 col-md-3">
								{!! Form::select('paid_at_month', [
									'01' => 'Januari',
									'02' => 'Februari',
									'03' => 'Maret',
									'04' => 'April',
									'05' => 'Mei',
									'06' => 'Juni',
									'07' => 'Juli',
									'08' => 'Agustus',
									'09' => 'September',
									'10' => 'Oktober',
									'11' => 'November',
									'12' => 'Desember'
									], $now->format('m'), ['class' => 'form-control']) !!}
							</div>
							<div class="col-xs-3">
								{!! Form::select('paid_at_year', [
									$now->year-1 => $now->year-1,
									$now->year => $now->year,
									$now->year+1 => $now->year+1,
									], $now->format('Y'), ['class' => 'form-control']) !!}
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="inputDay">Time</label>
						<div class="row">
							<div class="col-xs-3 col-md-2">
								{!!	Form::text(
										'paid_at_hour',
										$now->format('H'),
										['class' => 'form-control']
									)
								!!}			
							</div>
							<div class="col-xs-1" style="padding-left:5px; padding-right: 5px; width: inherit; padding-top: 6px">:</div>
							<div class="col-xs-3 col-md-2" style="padding-left: 0px">
								{!! Form::text(
										'paid_at_minute',
										$now->format('i'),
										['class' => 'form-control']
									)
								!!}
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="inputDay">Amount</label>
						{!!
							Form::text('paid_amount', '', ['class' => 'form-control'])
						!!}								
					</div>
					<div class="form-group">
						<label for="inputPaidNotes">Paid Notes</label>
						{!! Form::textarea('paid_notes', '', [
							'class' => 'form-control',
							'rows' => 5
						]) !!}
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					<button type="submit" class="btn btn-success">Set as Paid</button>
				</div>
			</form>
		</div>
	</div>
</div>