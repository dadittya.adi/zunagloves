<div class="modal fade" id="paidModal" tabindex="-1" role="dialog" aria-labelledby="Paid Order" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<form action="{{ action('Admin\OrderController@paid', [$order->id]) }}" method="post">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Set Paid Order</h4>
				</div>
				<div class="modal-body">
					<p>Set order #{{ $order->order_number }} as paid order?</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					<button type="submit" class="btn btn-danger">Set Paid</button>
				</div>
			</form>
		</div>
	</div>
</div>