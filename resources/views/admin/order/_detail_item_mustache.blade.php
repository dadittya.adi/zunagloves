<script type="x-tmpl-mustache" id="orderTmpl">
	{% #items %}
		<tr class="item" data-id="{% id %}">
			<td>{% no %}</td>
			<td>
        		<b>{% product_name %}</b>
    			{% #disc_percent %}
    			<span style="color: red">
    				(Disc {% disc_percent %}%)
    			</span>
    			{% /disc_percent %}

				{% #color %}
				<br>Color : {% color %}
				{% /color %}

				{% #size %}
				<br>Size : {% size %}
				{% /size %}

				{% #notes %}
				<br>Notes : <pre>{% notes %}</pre>
				{% /notes %}
			</td>
			<td class="text-right">
				<input type="text" class="form-control input-price calc" 
					value="{% price %}" id="price_{% id %}" data-id="{% id %}" 
					readonly
				>
			</td>
			<td class="text-right">
				<input type="text" class="form-control input-qty calc" 
					value="{% qty %}" id="qty_{% id %}" data-id="{% id %}" 
					readonly
				>
			</td>
			<td class="text-right">
				<span id="subtotal_{% id %}">{% total %}</span>
				<input type="hidden" id="disc_percent_{% id %}" value="{% disc_percent %}">
				{% #discount %}
				<br><span style="color:red">
					Disc : (<span id="discount_{% id %}">{% discount %}</span>)
				</span>
				{% /discount %}
			</td>
			<td>
				<button class="btn btn-primary btn-xs btn-edit" data-id="{% id %}">
					Edit
				</button>
				<button class="btn btn-success btn-xs btn-save hidden" id="buttonSave_{% id %}" 
					data-id="{% id %}" data-row="{% _row %}"
				>
					Save
				</button>
				<button class="btn btn-warning btn-xs btn-cancel hidden" id="buttonCancel_{% id %}">
					Cancel
				</button>
			</td>
		</tr>
	{% /items %}
	<tr>
		<th colspan="4" class="text-right">Delivery Cost</th>
		<td class="text-right">
			<input type="text" class="form-control calc ship" data-id="{% id %}"
				value="{% weight %}" id="weight" readonly style="width: 50px; display: inline-block"
			>
			kg x 
			<input type="text" class="form-control calc ship" data-id="{% id %}"
				value="{% shipping_price %}" id="shipping" readonly style="width: 150px; display: inline-block"
			>
		</td>
		<td style="min-width: 110px">
			<button class="btn btn-primary btn-xs btn-edit" id="buttonEditShipping">
				Edit
			</button>
			<button class="btn btn-success btn-xs hidden" id="buttonSaveShipping">
				Save
			</button>
			<button class="btn btn-warning btn-xs btn-cancel hidden" id="buttonCancelShipping">
				Cancel
			</button>
		</td>
	</tr>
	<tr>
		<th colspan="4" class="text-right">Discount</th>
		<td class="text-right">
			<input type="text" class="form-control calc" data-id="{% id %}"
				value="{% voucher_discount %}" id="voucher" readonly
			>
		</td>
		<td style="min-width: 110px">
			<button class="btn btn-primary btn-xs btn-edit" id="buttonEditDisc">
				Edit
			</button>
			<button class="btn btn-success btn-xs hidden" id="buttonSaveDisc">
				Save
			</button>
			<button class="btn btn-warning btn-xs btn-cancel hidden" id="buttonCancelDisc">
				Cancel
			</button>
		</td>
	</tr>
	<tr>
		<th colspan="4" class="text-right">Total</th>
		<th class="text-right">
			<span id="total"></span>
		</th>
	</tr>
</script>