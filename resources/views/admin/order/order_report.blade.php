@extends('admin.master_layout', ['active' => 'report'])

@section('content')
	<div class="container-fluid">
		{!!
			Form::open(array(
				'class' => 'form-signin',
				'role' => 'form',
				'url' => action('Admin\OrderController@report'),
				'method' => 'get',
			))
		!!}
		
		<div class="row form-group">		
			<div class="col-md-2">
				@include('form.date', [
					'field' => 'from_date',
					'label' => 'Start Date',
					'placeholder' => 'dd/mm/yyyy',
					'default' => $from_date->format('d/m/Y')
				])
			</div>
			<div class="col-md-2">
				@include('form.date', [
					'field' => 'to_date',
					'label' => 'End Date',
					'placeholder' => 'dd/mm/yyyy',
					'default' => $to_date->format('d/m/Y')
				])
			</div>
			<div class="col-md-6">
				<label>Search</label>
				<div class="input-group input-search">
			      	{!! Form::text('q', $q, [
						'class' => 'form-control',
						'placeholder' => 'Search by order number, name or email customer'
					]) !!}
			      	<div class="input-group-addon">
			      		<span class="glyphicon glyphicon-search"></span>
			      	</div>
			    </div>
			</div>
			<div class="col-md-2" style="margin-top : 25px">
				<div class="pull-right">
					<button type="submit" class="btn btn-primary">Filter</button>
					<a class="btn btn-success"
						href="{{ action('Admin\OrderController@export', [
							'from_date' => $from_date->format('d/m/Y'), 
							'to_date' => $to_date->format('d/m/Y'),
							'q' => $q
						]) }}" 						
					>
						Print to Excel
					</a>
				</div>
			</div>
		</div>
		{!! Form::close() !!}

		<div class="row">
			<div class="title text-center">
				<h2>
					Order Report<br>
					<small>
						Date: {{ i18l_date($from_date, ['date' => IntlDateFormatter::LONG, 'time' => IntlDateFormatter::NONE]) }} 
						@if ($to_date > $from_date->addDay())
							to
							{{ i18l_date($to_date, ['date' => IntlDateFormatter::LONG, 'time' => IntlDateFormatter::NONE]) }} 
						@endif
					</small>
				</h2>
			</div>
			<div class="table-responsive">
				<table class="table table-bordered">
					<thead>
						<tr>
							<th>#</th>
							<th>Date</th>
							<th>Order Number</th>
							<th>Customer</th>
							<th>Product</th>
							<th>Price</th>
							<th>Qty</th>
							<th>Subtotal</th>
						</tr>
					</thead>
					<?php $total = 0?>
					@foreach ($orders as $key => $order)
						<tbody>
							<tr>
								<td>{{ $key + 1 }}</td>
								<td>{{ $order->created_at->format('d/m/Y') }}</td>
								<td>{{ $order->order_number }}</td>
								<td>
									{{ $order->full_name }}<br>
									<small>{{ $order->email  }}</small>
								</td>
								<td colspan="4"></td>
							</tr>
							@foreach ($order->items as $item)
								<tr>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>
										{{ $item->product->name }}
										@if ($item->disc_percent > 0)
											(Disc: {{ (float)$item->disc_percent }}%)
										@endif
									</td>
									<td align="right">{{ i18l_number($item->price) }}</td>
									<td align="right">{{ i18l_number($item->qty) }}</td>
									<td align="right">
										{{ i18l_number($item->total - $item->discount) }}
									</td>
								</tr>
							@endforeach
							@if ($order->voucher_discount > 0)
							<tr>
								<th colspan="7" style="text-align: right">
									Discount
								</th>
								<th style="text-align: right;">
									{{ i18l_number($order->voucher_discount) }}
								</th>
							</tr>
							@endif
							<tr>
								<th colspan="7" style="text-align: right">Subtotal</th>
								<th style="text-align: right">{{ i18l_number($subtotal = $order->total - ($order->weight * $order->shipping_price)) }}</th>
							</tr>
						</tbody>
						<?php $total += $subtotal ?>
					@endforeach
					<tbody>
						<tr>
							<td colspan="7" align="right">
								<h4><b>TOTAL</b></h4>
							</td>
							<td style="text-align: right">
								<h4><b>{{ i18l_number($total) }}</b></h4>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
@endsection