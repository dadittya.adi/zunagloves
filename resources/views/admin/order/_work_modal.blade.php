<div class="modal fade" id="workModal" tabindex="-1" role="dialog" aria-labelledby="On Progress Order" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<form action="{{ action('Admin\OrderController@work', [$order->id]) }}" method="post">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Confirm Order</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label for="inputNotes">Notes</label>
						{!! Form::textarea('work_notes', "Order segera akan diproses.", [
							'class' => 'form-control'
						]) !!}
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					<button type="submit" class="btn btn-success">Confirm Order</button>
				</div>
			</form>
		</div>
	</div>
</div>