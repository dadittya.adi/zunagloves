<div class="modal fade" id="rejectModal" tabindex="-1" role="dialog" aria-labelledby="Reject Order" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<form action="{{ action('Admin\OrderController@reject', [$order->id]) }}" method="post">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Reject Order #{{ $order->order_number }}</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label for="inputReason">Reason</label>
						{!! Form::textarea('reject_notes', '', [
							'class' => 'form-control'
						]) !!}
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					<button type="submit" class="btn btn-danger">Reject Order</button>
				</div>
			</form>
		</div>
	</div>
</div>