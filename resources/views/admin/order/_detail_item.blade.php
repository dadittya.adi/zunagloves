<div class="row">
	<div class="col-md-12 table-responsive">
		<div class="button text-right">
		<a href="{{ action('Admin\OrderController@printInvoice', [$order->id]) }}"
				class="btn btn-info" target="_blank"
			>
				Print Invoice
			</a>
		</div>
		<h5><b>ORDER ITEM</b></h5>
		<table class="table table-bordered">
			<thead>
				<tr>
					<th>#</th>
					<th>Item</th>
					<th class="text-right">Price</th>
					<th class="text-right">Qty</th>
					<th class="text-right">Subtotal</th>
				</tr>
			</thead>
			<tbody id="orderTable">
				@foreach ($order->items as $key => $item)
					<tr>
						<td>{{ $key + 1 }}</td>
						<td>
			        		<b>{{ $item->product->name }}</b>
							@if ($item->color)
								<br>Color : {{ $item->color }}
							@endif
							@if ($item->size)
								<br>Size : {{ $item->size }}
							@endif
							@if ($item->notes)
								<br>Notes : <pre>{{ nl2br(e($item->notes)) }}</pre>
							@endif
						</td>
						<td class="text-right">
							{{ i18l_currency($item->price, $order->domain) }}
						</td>
						<td class="text-right">
							{{ (float)$item->qty }}
						</td>
						<td class="text-right">
							{{ i18l_currency($item->total) }}
							@if ($item->discount > 0)
							<br><span style="color:red">
								Disc : ({{ i18l_currency($item->discount, $order->domain) }})
							</span>
							@endif
						</td>
					</tr>
				@endforeach
				<tr>
					<th colspan="4" class="text-right">
						Delivery Cost ({{ $order->weight }}kg x {{ i18l_currency($order->shipping_price) }})
					</th>
					<th class="text-right">
						@if ($order->shipping_price > 0)
		            		{{ i18l_currency($order->weight * $order->shipping_price, $order->domain) }}
		            	@else
		            		-
		            	@endif
					</th>
				</tr>
				<tr>
					<th colspan="4" class="text-right">Total</th>
					<th class="text-right">{{ i18l_currency($order->total, $order->domain) }}</th>
				</tr>
			</tbody>
		</table>
	</div>
</div>