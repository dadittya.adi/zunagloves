<!-- Invoice Modal -->
<div class="modal fade" id="revisionModal" tabindex="-1" role="dialog" aria-labelledby="Send Invoice" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<form action="{{ action('Admin\OrderController@revision', [$order->id]) }}" id="formInvoice" method="post">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Revision Order #{{ $order->order_number }}</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label for="inputMailNotes">Mail Notes</label>
						{!! Form::textarea('mail_notes', 
"Payment can be transfered to Bank :
	BCA XXXX XXXX XXX a/n Lorem Ipsum
at least 1 x 24 hours after this invoice has been sent.
We have no responsibility for over due payment order.", [
							'class' => 'form-control'
						])!!}
					</div>
					{!! Form::hidden('items', '', ['id' => 'revisionItem']) !!}
					{!! Form::hidden('shipping', '', ['id' => 'revisionShipping']) !!}
					{!! Form::hidden('weight', '', ['id' => 'revisionWeight']) !!}
					{!! Form::hidden('discount', '', ['id' => 'revisionDiscount']) !!}
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					<button type="submit" class="btn btn-primary">Send Invoice</button>
				</div>
			</form>
		</div>
	</div>
</div>