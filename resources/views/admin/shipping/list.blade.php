@extends('admin.master_layout', ['active' => 'shipping'])

@section('sidebar')
	@include('admin.shipping.sidebar', ['active' => 'index'])
@endsection

@section('content')
	<div class="container-fluid">
	@if (Session::has('Wrong_File'))
		<div class="alert alert-danger">
			<strong>Uploaded File didn't CSV format</strong>
		</div>
	@endif
	@if (Session::has('Success'))
		<div class="alert alert-success">
			<strong>Your Upload Success</strong>
		</div>
	@elseif (Session::has('No_File'))
		<div class="alert alert-warning">
			<strong>No Uploaded File</strong>
		</div>
	@endif
		
		<div class="row">
			<div class="table-responsive">
				<table class="table table-banner">
					<thead>
						<tr>
							<th>Show Order</th>
							<th>Name</th>
							<th>Display Name</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($shippings as $key => $shipping)
							<tr>
								<td>
									{!!
										Form::open(array(
											'action' => array('Admin\ShippingController@move', $shipping->id, -1),
											'method' => 'post',
											'style' => 'display: inline-block;',
										))
									!!}

									<button type="submit" class="btn btn-xs" {{ $key > 0 ? '' : 'disabled' }}>
										<span class="glyphicon glyphicon-chevron-up"></span>
									</button>
									
									{!! Form::close() !!}
									
									{{ $key + 1 }}

									{!!
										Form::open(array(
											'action' => array('Admin\ShippingController@move', $shipping->id, 1),
											'method' => 'post',
											'style' => 'display: inline-block;',
										))
									!!}

									<button type="submit" class="btn btn-xs" {{ ($key + 1) < $shippings->count() ? '' : 'disabled' }}>
										<span class="glyphicon glyphicon-chevron-down"></span>
									</button>
									
									{!! Form::close() !!}
								</td>
								<td>{{ $shipping->name }}</td>
								<td>{{ $shipping->display_name }}</td>
								<td>
									<a href="{{ action('Admin\ShippingController@edit', [$shipping->id]) }}" class="btn btn-primary btn-xs">
										Edit
									</a>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			<div class="pull-right">
				{!! $shippings->render() !!}
			</div>
		</div>
	</div>
@endsection