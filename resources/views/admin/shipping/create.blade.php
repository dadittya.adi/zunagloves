@extends('admin.master_layout', ['active' => 'shipping'])

@section('sidebar')
	@include('admin.shipping.sidebar', ['active' => 'create'])
@endsection

@section('content')
	<div class="container-fluid">
		<div class="row">
			{!!
				Form::open([
					'role' => 'form',
					'url' => action('Admin\ShippingController@store'),
					'method' => 'post',
				])
			!!}

			@include('form.select', [
				'field' => 'country',
				'label' => 'Country',
				'options' => $countries
			])

			@include('form.text', [
				'field' => 'name',
				'label' => 'Name',
				'placeholder' => 'Shipping Name'
			])

			@include('form.text', [
				'field' => 'display_name',
				'label' => 'Display Name',
				'placeholder' => 'Shipping Display Name',
				'help' => 'This name which will be shown on website.'
			])

			<div class="form-group">
				<button type="submit" class="btn btn-primary">Save</button>
			</div>

			{!! Form::close() !!}
		</div>
	</div>
@endsection