@extends('admin.master_layout', ['active' => 'shipping'])

@section('sidebar')
	@include('admin.shipping.sidebar', ['active' => 'index'])
@endsection

@section('content')
	<div class="container-fluid">
		<div class="row">
			<div class="content-header">
				<h2>
					Edit Shipping
					{!!
						Form::open(array(
							'role' => 'form',
							'url' => action('Admin\ShippingController@delete', [$shipping->id]),
							'method' => 'post',
						))
					!!}
					<button type="submit" class="btn btn-danger"
						onclick="return confirm('Are you sure want to delete this shipping?');"
					>
						Delete
					</button>
					{!! Form::close() !!}
				</h2>
			</div>
		</div>
		<div class="row">
			{!!
				Form::open([
					'role' => 'form',
					'url' => action('Admin\ShippingController@update', [$shipping->id]),
					'method' => 'post',
				])
			!!}

			@include('form.select', [
				'field' => 'country',
				'label' => 'Country',
				'options' => $countries,
				'default' => $shipping->country_id
			])

			@include('form.text', [
				'field' => 'name',
				'label' => 'Name',
				'placeholder' => 'Shipping Name',
				'default' => $shipping->name
			])

			@include('form.text', [
				'field' => 'display_name',
				'label' => 'Display Name',
				'placeholder' => 'Shipping Display Name',
				'help' => 'This name which will be shown on website.',
				'default' => $shipping->display_name
			])

			<div class="form-group">
				<button type="submit" class="btn btn-primary">Save</button>
			</div>

			{!! Form::close() !!}

			<input type="hidden" id="priceData" value="{{ json_encode($prices) }}">
			<input type="hidden" id="errorId" value="{{ Session::get('error_id') }}">
			<input type="hidden" id="errorMessage" value="{{ json_encode($errors->getMessages()) }}">		

			<div class="form-group">
				<label>Shipping Price</label>
				<table class="table">
					<thead>
						<tr>
							<th width="30%">Destination</th>
							<th width="25%">Price</th>
							<th width="20%">Shipping Time</th>
							<th></th>
						</tr>
					</thead>
					<tbody id="priceTable"></tbody>
					<tfoot>
						<tr>
							<td class="{{ $errors->has('destination') ? 'has-error' : '' }}">
								{!! Form::text('destination', '', [
										'class' => 'form-control',
										'placeholder' => 'Destination',
										'id' => 'inputDestination'
									]) 
								!!}
								@if ($errors->has('destination'))
								<span class="help-block text-danger">{{ $errors->first('destination') }}</span>
								@endif
							</td>
							<td class="{{ $errors->has('price') ? 'has-error' : '' }}">
								{!! Form::text('price', '', [
										'class' => 'form-control',
										'placeholder' => 'Price',
										'id' => 'inputPrice'
									]) 
								!!}
								@if ($errors->has('price'))
								<span class="help-block text-danger">{{ $errors->first('price') }}</span>
								@endif
							</td>
							<td class="{{ $errors->has('time') ? 'has-error' : '' }}">
								<div class="input-group">
									{!! Form::text('time', '', [
											'class' => 'form-control',
											'placeholder' => 'Shipping Time',
											'default' => '1',
											'id' => 'inputTime'
										]) 
									!!}
									<span class="input-group-addon">day(s)</span>
								</div>
								@if ($errors->has('time'))
								<span class="help-block text-danger">{{ $errors->first('time') }}</span>
								@endif
							</td>
							<td>
								{!! 
									Form::open([
										'role' => 'form',
										'url' => action('Admin\ShippingController@addPrice', [$shipping->id]),
										'method' => 'post',
										'id' => 'form-addprice'
									]) 
								!!}

								<input type="hidden" name="destination" id="sentDestination">
								<input type="hidden" name="price" id="sentPrice">
								<input type="hidden" name="time" id="sentTime">

								<button class="btn btn-success" id="addButton">
									Add Price
								</button>
								{!! Form::close() !!}
							</td>
						</tr>
					</tfoot>
				</table>
			</div>
		</div>
	</div>
@endsection

@section('content-js')
	<script type="x-tmpl-mustache" id="priceTmpl">
		{% #data %}
		<tr>
			<td id="col_destination_edit_{% id %}">
				<span id="destination_text_{% _row %}">{% destination %}</span>
				<input type="text" class="form-control input-text{% _row %} hidden" 
					id="destination_{% _row %}" value="{% destination %}"
				>
				<span class="help-block text-danger hidden" id="error_destination_edit_{% id %}"></span>
			</td>
			<td id="col_price_edit_{% id %}">
				<span id="price_text_{% _row %}">{% price %}</span>
				<input type="text" class="form-control input-text{% _row %} hidden" 
					id="price_{% _row %}" value="{% price %}"
				>
				<span class="help-block text-danger hidden" id="error_price_edit_{% id %}"></span>
			</td>
			<td id="col_time_edit_{% id %}">
				<span id="time_text_{% _row %}">{% time %} day(s)</span>
				<div class="input-group input-text{% _row %} hidden">
					<input type="text" class="form-control" id="time_{% _row %}" 
						value="{% time %}"
					>
					<span class="input-group-addon">day(s)</span>
				</div>
				<span class="help-block text-danger hidden" id="error_time_edit_{% id %}"></span>
			</td>
			<td>
				<button type="button" class="btn btn-xs btn-primary btn-edit-price" 
					data-row="{% _row %}" id="edit_{% id %}"
				>
					Edit
				</button>
				{!! 
					Form::open([
						'role' => 'form',
						'url' => "/admin/shipping/$shipping->id/delete/{% id %}",
						'method' => 'post',
						'style' => 'display: inline-block',
					]) 
				!!}
				<button class="btn btn-danger btn-xs btn-delete-price"
					onclick="return confirm('Are you sure want to delete shipping price `{% destination %}`?');"
				>
					Delete
				</button>
				{!! Form::close()  !!}

				{!! 
					Form::open([
						'role' => 'form',
						'url' => "/admin/shipping/$shipping->id/update/{% id %}",
						'method' => 'post',
						'style' => 'display: inline-block',
						'class' => 'hidden form-updatesub',
						'id' => 'formupdate_{% _row %}'
					]) 
				!!}

				<input type="hidden" name="destination_edit" id="destination_edit_{% _row %}" value="{% destination %}">
				<input type="hidden" name="price_edit" id="price_edit_{% _row %}" value="{% price %}">
				<input type="hidden" name="time_edit" id="time_edit_{% _row %}" value="{% time %}">

				<button type="button" class="btn btn-xs btn-success btn-save-price hidden" 
					id="save_{% _row %}" data-row="{% _row %}"
				>
					Save
				</button>
				{!! Form::close()  !!}

				<button type="button" class="btn btn-xs btn-warning btn-cancel-price hidden" 
					id="cancel_{% _row %}"
				>
					Cancel
				</button>				
			</td>
		</tr>
		{% /data %}
	</script>

	<script>
		$(function() {
			var prices = JSON.parse($('#priceData').val());
			var errors = JSON.parse($('#errorMessage').val());
			var error_id = parseInt($('#errorId').val(), 10);

			function render()
			{
				for (var i = 0; i < prices.length; i++)
					prices[i]._row = i;

				var tmpl = $('#priceTmpl').html();
				var html = Mustache.render(tmpl, { data : prices });
				Mustache.parse(tmpl);
				$('#priceTable').html(html);

				$('.btn-edit-price').on('click', edit);
				$('.btn-cancel-price').on('click', render);
				$('.btn-save-price').on('click', save);
			}

			function edit()
			{
				var row = $(this).data('row');

				$('.btn-delete-price').addClass('hidden');
				$('.btn-edit-price').addClass('hidden');

				$('#destination_text_' + row).addClass('hidden');
				$('#price_text_' + row).addClass('hidden');
				$('#time_text_' + row).addClass('hidden');

				$('.input-text' + row).removeClass('hidden');
				$('#cancel_' + row).removeClass('hidden');
				$('#save_' + row).removeClass('hidden');
				$('#formupdate_' + row).removeClass('hidden');
			}

			function save()
			{
				var row = $(this).data('row');

				var dest = $('#destination_' + row).val();
				var price = parseInt($('#price_' + row).val(), 10);
				var time = parseInt($('#time_' + row).val(), 10);

				if (isNaN(time))
					time = 1;

				$('#destination_edit_' + row).val(dest);
				$('#price_edit_' + row).val(price);
				$('#time_edit_' + row).val(time);

				$('#formupdate_' + row).submit();
			}

			function checkError()
			{
				if (isNaN(error_id))
					return;

				$('#edit_' + error_id).click();

				for (var field in errors) {
					$('#col_' + field + '_' + error_id).addClass('has-error');
					$('#error_' + field + '_' + error_id).removeClass('hidden');
					$('#error_' + field + '_' + error_id).html(errors[field][0]);
				}
			}

			$('#addButton').on('click', function() {
				var dest = $('#inputDestination').val();
				var price = parseInt($('#inputPrice').val(), 10);
				var time = parseInt($('#inputTime').val(), 10);

				if (isNaN(time))
					time = 1;

				$('#sentDestination').val(dest);
				$('#sentPrice').val(price);
				$('#sentTime').val(time);

				$('#form-addprice').submit();
			});

			render();
			checkError();
		});
	</script>
@endsection