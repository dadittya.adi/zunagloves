@extends('admin.master_layout', ['active' => 'blog'])

@section('sidebar')
	@include('admin.blog.sidebar', ['active' => 'indexTag'])
@endsection

@section('content')
	<div class="container-fluid">
		<div class="row">
			<div class="table-responsive">
				<table class="table table-banner">
					<thead>
						<tr>
							<th>#</th>
							<th>Name</th>
							<th>State</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($tags as $key => $tag)
							<tr>
								<td>{{ $key + 1 }}</td>
								<td>{{ $tag->name }}</td>
								<td>
									@if ($tag->state)
										Active
									@else
										Inactive
									@endif
								</td>
								<td>
									<a href="{{ action('Admin\TagController@edit', [$tag->id]) }}" class="btn btn-primary btn-xs">
										Edit
									</a>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			<div class="pull-right">
				{!! $tags->render() !!}
			</div>
		</div>
	</div>
@endsection