@extends('admin.master_layout', ['active' => 'blog'])

@section('sidebar')
	@include('admin.blog.sidebar', ['active' => 'createTag'])
@endsection

@section('content')
	<div class="container-fluid">
		<div class="row">
			<div class="content-header">
				<h2>
					Edit Tag
				</h2>
			</div>
		</div>
		<div class="row">
			{!!
				Form::open([
					'role' => 'form',
					'url' => action('Admin\TagController@update', [$tag->id]),
					'method' => 'post'
				])
			!!}
			@include('form.text', [
				'field' => 'name',
				'label' => 'Name',
				'placeholder' => 'Tag Name',
				'default' => $tag->name
			])

			@include('form.select', [
				'field' => 'state',
				'label' => 'State',
				'options' => [
					'1' => 'Active',
					'0' => 'Inactive'
				],
				'default' => $tag->state
			])

			<div class="form-group">
				<button type="submit" class="btn btn-primary">Save</button>
			</div>

			{!! Form::close() !!}				
		</div>
	</div>
@endsection

@section('content-js')
@endsection