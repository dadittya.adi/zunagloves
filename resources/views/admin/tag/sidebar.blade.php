@if(isset($active) && ($active == 'index' || $active == 'create'))
<a class="list-group-item {{ isset($active) && $active == 'index' ? 'active' : '' }}" href="{{ action('Admin\TagController@index') }}">
	List Tag
</a>
<a class="list-group-item {{ isset($active) && $active == 'create' ? 'active' : '' }}" href="{{ action('Admin\TagController@create') }}">
	Create Tag
</a>
@elseif(isset($active) && $active == 'edit')
<a class="list-group-item {{ isset($active) && $active == 'edit' ? 'active' : '' }}" href="{{ action('Admin\TagController@edit', [$tag->id]) }}">
	Edit Tag
</a>
@endif
