@extends('admin.master_layout', ['active' => 'promo'])

@section('sidebar')
	@include('admin.promo.sidebar', ['active' => 'index'])
@endsection

@section('content')
	<div class="container-fluid">
		<div class="row">
			<div class="content-header">
				<h2>
					Edit Promo
				</h2>
			</div>
		</div>

		<div class="row">
			{!!
				Form::open([
					'role' => 'form',
					'url' => action('Admin\PromoController@update', [$promo->id]),
					'method' => 'post',
					'enctype' => 'multipart/form-data'
				])
			!!}

			@include('form.text', [
				'field' => 'title',
				'label' => 'Title',
				'placeholder' => 'Title',
				'default' => $promo->title
			])

			@include('form.text', [
				'field' => 'link',
				'label' => 'Link',
				'placeholder' => 'Link',
				'default' => $promo->link
			])

			<div class="form-group">
				<label for="image" class="control-label">
					Image
				</label>
				<div class="control-input {{ $errors->has('image') ? 'has-error' : '' }}">
					@if ($promo->image)
					<img class="thumbnail" src="{{ action('HomeController@showPromoImage', [$promo->id]) }}" height="224">
					@endif

					{!! Form::file('image') !!}
					
					<span class="help-block">Recomended resolution 1140 x 448 px (JPG / PNG).</span>
					
					@if ($errors->has('image'))
					<span class="help-block text-danger">{{ $errors->first('image') }}</span>
					@endif
				</div>
			</div>

			<div class="form-group">
				<button type="submit" class="btn btn-primary">Save</button>
			</div>

			{!! Form::close() !!}
		</div>
	</div>
@endsection