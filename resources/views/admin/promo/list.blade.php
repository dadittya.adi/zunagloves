@extends('admin.master_layout', ['active' => 'promo'])

@section('sidebar')
	@include('admin.promo.sidebar', ['active' => 'index'])
@endsection

@section('content')
	<div class="container-fluid">
		<div class="row">
			<div class="table-responsive">
				<table class="table table-banner">
					<thead>
						<tr>
							<th>#</th>
							<th>Title</th>
							<th>Link</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($promos as $key => $promo)
							<tr>
								<td>{{ $key + 1 }}</td>
								<td>{{ $promo->title }}</td>
								<td>{{ $promo->link ? $promo->link : '-' }}</td>
								<td>
									<a href="{{ action('Admin\PromoController@edit', [$promo->id]) }}" class="btn btn-primary btn-xs">
										Edit
									</a>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
@endsection