@extends('admin.master_layout', ['active' => 'customer'])

@section('sidebar')
	@include('admin.customer.sidebar', ['active' => 'index'])
@endsection

@section('content')
	<div class="container-fluid">
		<div class="row">
			<div class="content-header">
				<h2>
					Detail Customer
				</h2>
			</div>
		</div>

		<div class="row">
			{!!
				Form::open([
					'role' => 'form',
					'url' => action('Admin\CustomerController@update', [$customer->id]),
					'method' => 'post',
				])
			!!}

			@include('form.text', [
				'field' => 'full_name',
				'label' => 'Full Name',
				'placeholder' => 'Full Name',
				'default' => $customer->full_name,
				'attributes' => [
						'readonly' => 'true'
					]
			])

			@include('form.text', [
				'field' => 'phone',
				'label' => 'Phone',
				'placeholder' => 'Phone',
				'default' => $customer->phone,
				'attributes' => [
						'readonly' => 'true'
					]
			])

			@include('form.text', [
				'field' => 'email',
				'label' => 'Email',
				'placeholder' => 'Email',
				'default' => $customer->email,
				'attributes' => [
						'readonly' => 'true'
					]
			])

			@include('form.textarea', [
				'field' => 'address',
				'label' => 'Address',
				'placeholder' => 'Address',
				'default' => $customer->address,
				'attributes' => [
						'readonly' => 'true'
					]
			])

			@include('form.text', [
				'field' => 'city',
				'label' => 'City',
				'placeholder' => 'City',
				'default' => $customer->city != null ? $customer->cityb->city_name : "",
				'attributes' => [
						'readonly' => 'true'
					]
			])

			@include('form.text', [
				'field' => 'province',
				'label' => 'Province',
				'placeholder' => 'Province',
				'default' => $customer->province != null ? $customer->provinceb->province : "",
				'attributes' => [
						'readonly' => 'true'
					]
			])

			@include('form.text', [
				'field' => 'postal_code',
				'label' => 'Postal Code',
				'placeholder' => 'Postal Code',
				'default' => $customer->postal_code,
				'attributes' => [
						'readonly' => 'true'
					]
			])

			@include('form.select', [
				'field' => 'state',
				'label' => 'State',
				'options' => [
					'NEW' => 'New',
					'ACT' => 'Active',
					'BAN' => 'Banned'
				],
				'default' => $customer->state,
				'attributes' => [
						'readonly' => 'true'
					]
			])

			{{-- <div class="form-group">
				<button type="submit" class="btn btn-primary">Close</button>
			</div> --}}

			{!! Form::close() !!}
		</div>
	</div>
@endsection