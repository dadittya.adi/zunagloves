@extends('admin.master_layout', ['active' => 'customer'])

@section('sidebar')
	@include('admin.customer.sidebar', ['active' => 'create'])
@endsection

@section('content')
	<div class="container-fluid">
		<div class="row">
			{!!
				Form::open([
					'role' => 'form',
					'url' => action('Admin\CustomerController@store'),
					'method' => 'post',
				])
			!!}

			@include('form.text', [
				'field' => 'full_name',
				'label' => 'Full Name',
				'placeholder' => 'Full Name'
			])

			@include('form.text', [
				'field' => 'phone',
				'label' => 'Phone',
				'placeholder' => 'Phone'
			])

			@include('form.text', [
				'field' => 'email',
				'label' => 'Email',
				'placeholder' => 'Email'
			])

			@include('form.text', [
				'field' => 'organization',
				'label' => 'Organization',
				'placeholder' => 'Organization / Company Name'
			])

			@include('form.textarea', [
				'field' => 'address',
				'label' => 'Address',
				'placeholder' => 'Address'
			])

			@include('form.text', [
				'field' => 'city',
				'label' => 'City',
				'placeholder' => 'City'
			])

			@include('form.text', [
				'field' => 'province',
				'label' => 'Province',
				'placeholder' => 'Province'
			])

			@include('form.select', [
				'field' => 'country',
				'label' => 'Country',
				'options' => $countries
			])

			@include('form.text', [
				'field' => 'postal_code',
				'label' => 'Postal Code',
				'placeholder' => 'Postal Code'
			])

			@include('form.select', [
				'field' => 'state',
				'label' => 'State',
				'options' => [
					'NEW' => 'New',
					'ACT' => 'Active',
					'BAN' => 'Banned'
				]
			])

			<div class="form-group">
				<button type="submit" class="btn btn-primary">Save</button>
			</div>

			{!! Form::close() !!}
		</div>
	</div>
@endsection