@extends('admin.master_layout', ['active' => 'customer'])

@section('sidebar')
	@include('admin.customer.sidebar', ['active' => 'index'])
@endsection

@section('content')
	<div class="container-fluid">
		<div class="row" style="margin-left: -30px">
			{!!
				Form::open(array(
					'class' => 'form-signin',
					'role' => 'form',
					'url' => action('Admin\CustomerController@index'),
					'method' => 'get',
				))
			!!}
			
			
			{{-- <div class="col-md-3">
				@include('form.select', [
				'field' => 'state',
				'label' => 'Status',
				'options' => ['ALL' => 'All', 'NEW' => 'New', 'ACT' => 'Active', 'BAN' => 'Banned'],
				'default' => (request()->input('state') ? request()->input('state') : 'ALL')
				])
			</div> --}}
			<div class="col-md-4">
				@include('form.text', [
					'label' => 'Pencarian Detail',
					'field' => 'search',
					'placeholder' => "Search with name, email, phone",
					'default' => (request()->input('search') ? request()->input('search') : '')
				])
			</div>
			<div class="col-md-2" style="margin-top : 25px">
				<button type="submit" class="btn btn-primary">Search</button>
				{!! Form::close() !!}
			</div>
		</div>
		<div class="row">
			<a href="{{ action('Admin\CustomerController@export', ['country'=>request()->input('country'), 'state'=>request()->input('state'), 'search'=>request()->input('search')]) }}" class="btn btn-success btn-md">
			  <span class="glyphicon glyphicon-file" aria-hidden="true"></span> Export
			</a>
			<div class="table-responsive">
				<table class="table table-banner">
					<thead>
						<tr>
							<th>#</th>
							<th>Full Name</th>
							<th>Email</th>
							<th>Phone</th>
							{{-- <th>State</th> --}}
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($customers as $key => $customer)
							<tr>
								<td>{{ $key + 1 }}</td>
								<td>{{ $customer->full_name }}</td>
								<td>{{ $customer->email }}</td>
								<td>{{ $customer->phone }}</td>
								{{-- <td>
									@if ($customer->state == 'NEW')
										New
									@elseif ($customer->state == 'ACT')
										Active
									@elseif ($customer->state == 'BAN')
										Banned
									@endif
								</td> --}}
								<td>
									{{-- <a href="{{ action('Admin\CustomerController@edit', [$customer->id]) }}" class="btn btn-primary btn-xs">
										Edit
									</a> --}}
									<a href="{{ action('Admin\CustomerController@detail', [$customer->id]) }}" class="btn btn-success btn-xs">
										Show Detail
									</a>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			<div class="pull-right">
				{!! $customers->render() !!}
			</div>
		</div>
	</div>
@endsection