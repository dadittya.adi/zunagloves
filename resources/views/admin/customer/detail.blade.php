@extends('admin.master_layout', ['active' => 'customer'])

@section('sidebar')
	@include('admin.customer.sidebar', ['active' => 'index'])
@endsection

@section('content')
	<div class="container-fluid">
		<div class="row">
			<div class="content-header">
				<div class="col-md-3">
				<h2>
					Info Customer
				</h2>
				</div>
				{{-- <div class="col-md-9 text-right">

					<a href="{{ action('Admin\CustomerController@edit', [$customer->id]) }}" class="btn btn-primary">
						Edit
					</a>
				</div> --}}
			</div>
		</div>
		<div class="row">
			<div class="col-md-3"><label>Full Name</label></div>
			<div class="col-md-9">{{ $customer->full_name }}</div>
		</div>
		<div class="row">
			<div class="col-md-3"><label>Email</label></div>
			<div class="col-md-9">{{ $customer->email }}</div>
		</div>
		<div class="row">
			<div class="col-md-3"><label>Phone</label></div>
			<div class="col-md-9">{{ $customer->phone }}</div>
		</div>
		{{-- <div class="row">
			<div class="col-md-3"><label>Organization</label></div>
			<div class="col-md-9">{{ $customer->organization ? $customer->organization : '-' }}</div>
		</div> --}}
		<div class="row">
			<div class="col-md-3"><label>Address</label></div>
			<div class="col-md-9">
				@if ($customer->address || $customer->city || $customer->province || $customer->city)
				<ul class="comma-list">
					@if ($customer->address)
					<li>{{ $customer->address }}</li>
					@endif
					@if ($customer->city)
					<li>{{ $customer->cityb->city_name }}</li>
					@endif
					@if ($customer->province)
					<li>{{ $customer->provinceb->province }}</li>
					@endif
					@if ($customer->postal_code)
					<li>{{ $customer->postal_code }}</li>
					@endif
				</ul>
				@else
				-
				@endif
			</div>
		</div>
		{{-- <div class="row">
			<div class="col-md-3"><label>Country</label></div>
			<div class="col-md-9">{{ $customer->country ? $customer->country : '-' }}</div>
		</div> --}}
		{{-- <div class="row form-group">
			<div class="col-md-3"><label>State</label></div>
			<div class="col-md-9">
				@if ($customer->state == 'NEW')
					New
				@elseif ($customer->state == 'ACT')
					Active
				@elseif ($customer->state == 'BAN')
					Banned
				@endif
			</div>
		</div> --}}
		<div class="row">
			<div class="col-md-12"><label>Order List</label></div>
			<div class="col-md-12 table-responsive">
				<table class="table table-bordered">
					<thead>
						<th>#Order</th>
						<th>Date</th>
						<th>Total</th>
						<th>State</th>
						<th>Action</th>
					</thead>
					<tbody>
					@forelse ($customer->orders as $order)
						<tr>
							<td>{{ $order->order_number }}</td>
							<td>{{ i18l_date($order->created_at) }}</td>
							<td>{{ i18l_currency($order->total, $order->domain) }}</td>
							<td>
								@if ($order->state == 'INV')
									Invoice
								@elseif ($order->state == 'CHK')
									Payment Check
								@elseif ($order->state == 'PAI')
									Paid
								@elseif ($order->state == 'WRK')
									On Work
								@elseif ($order->state == 'DLV')
									Delivered
								@elseif ($order->state == 'REJ')
									Rejected
								@else
									{{{ $order->state }}}
								@endif
							</td>
							<td style="min-width: 57px;">
								<a href="{{ action('Admin\OrderController@show', [$order->id]) }}" 
									class="btn btn-primary btn-xs"
								>
									View
								</a>
								<a href="{{ action('Admin\OrderController@printInvoice', [$order->id]) }}"
									class="btn btn-success btn-xs" target="_blank"
								>
									Print Invoice
								</a>
							</td>
						</tr>
					@empty
						<tr>
							<td colspan="5" align="center">No order record.</td>
						</tr>
					@endforelse
					</tbody>
				</table>
			</div>
		</div>
	</div>
@endsection