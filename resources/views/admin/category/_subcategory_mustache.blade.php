<script type="x-tmpl-mustache" id="subcategoryTmpl">
	{% #data %}
		<tr>
			<td>
				{!! 
					Form::open([
						'role' => 'form',
						'url' => "/admin/category/$category->id/update/{% id %}",
						'method' => 'post',
						'class' => 'form-updatesub',
						'id' => 'formupdate_{% _row %}',
						'enctype' => 'multipart/form-data'
					]) 
				!!}

				<div class="col-xs-5" id="col_subcategory_name_edit_{% id %}"
					style="padding-left:0"
				>
					<span id="name_{% _row %}">{% name %}</span>
					<input type="text" class="form-control hidden" name="subcategory_name_edit" 
						value="{% name %}" id="inputSubname_{% _row %}"
					>
					<span class="help-block text-danger hidden" id="error_subcategory_name_edit_{% id %}"></span>
				</div>
				<div class="col-xs-7" id="col_icon_edit_{% id %}">
					<img src="/content/subcategory/icon/{% id %}" id="icon_{% _row %}" height="90">
					<input type="file" class="form-control hidden" name="icon_edit"
						id="inputIcon_{% _row %}" style="margin-top:10px"
					>
					<span class="help-block text-danger hidden" id="error_icon_edit_{% id %}"></span>
				</div>
				{!! Form::close()  !!}
			</td>
			<td>
				<button class="btn btn-primary btn-xs btn-edit" id="edit_{% id %}" data-row="{% _row %}">
					Edit
				</button>
				<button class="btn btn-success btn-xs btn-save hidden" id="save_{% _row %}" data-row="{% _row %}">
					Save
				</button>
				<button class="btn btn-warning btn-xs btn-cancel hidden" id="cancel_{% _row %}">
					Cancel
				</button>

				{!! 
					Form::open([
						'role' => 'form',
						'url' => "/admin/category/$category->id/delete/{% id %}",
						'method' => 'post',
						'style' => 'display: inline-block',
					]) 
				!!}
				<button class="btn btn-danger btn-xs btn-delete"
					onclick="return confirm('Are you sure want to delete subcategory `{% name %}`?');"
				>
					Delete
				</button>
				{!! Form::close()  !!}
			</td>
		</tr>
	{% /data %}
</script>