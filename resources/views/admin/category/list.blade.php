@extends('admin.master_layout', ['active' => 'category'])

@section('sidebar')
	@include('admin.category.sidebar', ['active' => 'index'])
@endsection

@section('content')
	<div class="container-fluid">
		<div class="row">
			<div class="table-responsive">
				<table class="table table-banner">
					<thead>
						<tr>
							<th>Show Order</th>
							<th>Category</th>
							<th>Subcategory</th>
							<th>State</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($categories as $key => $category)
							<tr>
								<td>
									{!!
										Form::open(array(
											'action' => array('Admin\CategoryController@move', $category->id, -1),
											'method' => 'post',
											'style' => 'display: inline-block;',
										))
									!!}

									<button type="submit" class="btn btn-xs" {{ $key > 0 ? '' : 'disabled' }}>
										<span class="glyphicon glyphicon-chevron-up"></span>
									</button>
									
									{!! Form::close() !!}
									
									{{ $key + 1 }}

									{!!
										Form::open(array(
											'action' => array('Admin\CategoryController@move', $category->id, 1),
											'method' => 'post',
											'style' => 'display: inline-block;',
										))
									!!}

									<button type="submit" class="btn btn-xs" {{ ($key + 1) < $categories->count() ? '' : 'disabled' }}>
										<span class="glyphicon glyphicon-chevron-down"></span>
									</button>
									
									{!! Form::close() !!}
								</td>
								<td>{{ $category->name }}</td>
								<td style="width: 40%; word-wrap: normal">
									<ul class="comma-list">
										@foreach ($category->subcategories as $sub)
											<li>{{ $sub->name }}</li>
										@endforeach
									</ul>
								</td>
								<td>
									@if ($category->is_active)
										Active
									@else
										Inactive
									@endif
								</td>
								<td>
									<a href="{{ action('Admin\CategoryController@edit', [$category->id]) }}" class="btn btn-primary btn-xs">
										Edit
									</a>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			<div class="pull-right">
				{!! $categories->render() !!}
			</div>
		</div>
	</div>
@endsection