@extends('admin.master_layout', ['active' => 'category'])

@section('sidebar')
	@include('admin.category.sidebar', ['active' => 'index'])
@endsection

@section('content')
	<div class="container-fluid">
		<div class="row">
			<div class="content-header">
				<h2>
					Edit Category
				</h2>
			</div>
		</div>
		<div class="row">
			{!!
				Form::open([
					'role' => 'form',
					'url' => action('Admin\CategoryController@update', [$category->id]),
					'method' => 'post'
				])
			!!}

			@include('form.text', [
				'field' => 'name',
				'label' => 'Name',
				'placeholder' => 'Category Name',
				'default' => $category->name
			])

			@include('form.select', [
				'field' => 'is_active',
				'label' => 'State',
				'options' => [
					'1' => 'Active',
					'0' => 'Inactive'
				],
				'default' => $category->is_active
			])

			<div class="form-group">
				<button type="submit" class="btn btn-primary">Save</button>
			</div>

			{!! Form::close() !!}

			<input type="hidden" id="errorId" value="{{ Session::get('error_id') }}">
			<input type="hidden" id="errorMessage" value="{{ json_encode($errors->getMessages()) }}">		
		
		</div>
	</div>
@endsection
