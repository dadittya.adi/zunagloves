@extends('admin.master_layout', ['active' => 'citystore'])

@section('sidebar')
	@include('admin.citystore.sidebar', ['active' => 'index'])
@endsection

@section('content')
	<div class="container-fluid">
		<div class="row">
			<div class="table-responsive">
				<table class="table table-banner">
					<thead>
						<tr>
							<th>Show Order</th>
							<th>City</th>
							<th>State</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($citystore as $key => $city)
							<tr>
								<td>
									{!!
										Form::open(array(
											'action' => array('Admin\CityStoreController@move', $city->id, -1),
											'method' => 'post',
											'style' => 'display: inline-block;',
										))
									!!}

									{{-- <button type="submit" class="btn btn-xs" {{ $key > 0 ? '' : 'disabled' }}>
										<span class="glyphicon glyphicon-chevron-up"></span>
									</button>
									 --}}
									{!! Form::close() !!}
									
									{{ $key + 1 }}

									{!!
										Form::open(array(
											'action' => array('Admin\CityStoreController@move', $city->id, 1),
											'method' => 'post',
											'style' => 'display: inline-block;',
										))
									!!}

									{{-- <button type="submit" class="btn btn-xs" {{ ($key + 1) < $city->count() ? '' : 'disabled' }}>
										<span class="glyphicon glyphicon-chevron-down"></span>
									</button> --}}
									
									{!! Form::close() !!}
								</td>
								<td>{{ $city->name }}</td>
								
								<td>
									@if ($city->is_active)
										Active
									@else
										Inactive
									@endif
								</td>
								<td>
									<a href="{{ action('Admin\CityStoreController@edit', [$city->id]) }}" class="btn btn-primary btn-xs">
										Edit
									</a>
									<a href="{{ action('Admin\StoreController@index', [$city->id]) }}" class="btn btn-primary btn-xs">
										Store
									</a>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			<div class="pull-right">
				{!! $citystore->render() !!}
			</div>
		</div>
	</div>
@endsection