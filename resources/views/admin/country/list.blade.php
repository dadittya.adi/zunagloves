@extends('admin.master_layout', ['active' => 'shipping'])

@section('sidebar')
	@include('admin.shipping.sidebar', ['active' => 'country'])
@endsection

@section('content')
	<div class="container-fluid">
		{!!
			Form::open([
				'role' => 'form',
				'url' => action('Admin\CountryController@store'),
				'method' => 'post'
			])
		!!}

		<div class="row form-group">
			<div class="col-md-8">
				@include('form.text', [
					'field' => 'name',
					'label' => 'Name',
					'placeholder' => 'Name'
				])
			</div>
			<div class="col-md-3" style="padding-top: 25px;">
				<button type="submit" class="btn btn-primary">
					Add
				</button>
			</div>	
		</div>

		{!! Form::close() !!}		

		<div class="row">
			<div class="table-responsive">
				<table class="table">
					<thead>
						<tr>
							<th>#</th>
							<th width="60%">Name</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody id="countryTable"></tbody>
				</table>
			</div>
		</div>
		
		<input type="hidden" id="countryData" value="{{ json_encode($countries, JSON_NUMERIC_CHECK) }}">
		<input type="hidden" id="errorId" value="{{ Session::get('error_id') }}">
		<input type="hidden" id="errorMsg" value="{{ json_encode($errors->getMessages()) }}">
	</div>
@endsection

@section('content-js')
	@include('admin.country._mustache')
	<script src="{{ asset(elixir("js/master/country.js")) }}"></script>
@endsection

