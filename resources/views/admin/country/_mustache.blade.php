<script type="x-tmpl-mustache" id="countryTmpl">
	{% #data %}
		{% #_edit_state %}
		<tr>
			<td>{% _no %}</td>
			<td id="col_name_edit_{% id %}">
				<input type="text" class="form-control" id="name_{% _row %}" value="{% name %}">
				<span class="help-block text-danger hidden" id="error_name_edit_{% id %}"></span>
			</td>
			<td>
				<form method="post" action="/admin/country/{% id %}" id="update_{% _row %}">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<input type="hidden" name="name_edit" id="name_edit_{% _row %}" value="{% name %}">
					
					<button type="button" class="btn btn-success btn-xs btn-save" 
						data-row="{% _row %}"
					>
						Save
					</button>
					<button type="button" class="btn btn-warning btn-xs btn-cancel" 
						data-row="{% _row %}"
					>
						Cancel
					</button>
				</form>
			</td>
		</tr>
		{% /_edit_state %}
		{% ^_edit_state %}
		<tr>
			<td>{% _no %}</td>
			<td>{% name %}</td>
			<td>
				<form method="post" action="/admin/country/{% id %}/delete">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<button type="button" class="btn btn-primary btn-xs btn-edit" 
						id="edit_{% id %}" data-row="{% _row %}"
					>
						Edit
					</button>			
					<button type="submit" class="btn btn-danger btn-xs btn-delete" 
						onclick="return confirm('Are you sure want to delete country `{% name %}`?');"
					>
						Delete
					</button>
				</form>
			</td>
		</tr>
		{% /_edit_state %}
	{% /data %}
</script>