@extends('admin.master_layout', ['active' => 'subscribe'])

@section('sidebar')
	@include('admin.subscribed_email.sidebar', ['active' => 'index'])
@endsection

@section('content')
	<div class="container-fluid">
		<div class="row">
			<a href="{{ action('Admin\SubscribedEmailController@export', ['search' => request()->input('search')]) }}" class="btn btn-success btn-md">
			  <span class="glyphicon glyphicon-file" aria-hidden="true"></span> Export To Excel
			</a>
			<div class="table-responsive">
				<table class="table table-banner">
					<thead>
						<tr>
							<th>#</th>
							<th>Email</th>
							<th>Delete</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($emails as $key => $email)
							<tr>
								<td>{{ $key + 1 }}</td>
								<td>{{ $email->email }}</td>
								<td>
									{!!
										Form::open(array(
											'role' => 'form',
											'url' => action('Admin\SubscribedEmailController@delete', [$email->id]),
											'method' => 'post',
										))
									!!}
									<button type="submit" class="btn btn-sm btn-danger"
										onclick="return confirm('Are you sure want to delete this subscribed email?');"
									>
										Delete
									</button>
									{!! Form::close() !!}
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
@endsection