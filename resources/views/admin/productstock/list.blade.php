@extends('admin.master_layout', ['active' => 'product'])

@section('sidebar')
	@include('admin.product.sidebar', ['active' => 'index'])
@endsection

@section('content')
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-9">
				{{ $productName }}
			</div>
		</div>	

		<div class="row">
			<div class="table-responsive">
				{!!
					Form::open([
						'role' => 'form',
						'url' => action('Admin\ProductStockController@update', [$productId]),
						'method' => 'post',
						'enctype' => 'multipart/form-data'
					])
				!!}
				<table class="table">
					<thead>
						<tr>
							<th style="width: 1%">#</th>
							<th style="width: 10%">SKU</th>
							<th style="width: 15%">Varian</th>
							<th style="width: 5%">Size</th>
							<th style="width: 2%">Stock</th>
						</tr>
					</thead>
					<tbody>
					@foreach ($productStock as $key => $stock)
						<tr>
							<td style="vertical-align:middle">{{ $key + 1 }}</td>
							<td>
								
								@include('form.text',[
									'field' => 'sku[]',
									'default' => $stock->sku,
									'placeholder' => 'SKU',
									'attributes' => []
								])
							</td>
							<td style="vertical-align:middle">{{ ucwords(strtolower($stock->productColor->name)) }}</td>
							<td style="vertical-align:middle">{{ $stock->productProductsize->size->short_name }}</td>
							<td>
								

								@include('form.text',[
									'field' => 'stock[]',
									'default' => $stock->stock,
									'placeholder' => 'Stock',
									'attributes' => []
								])
							</td>
							<input name="id[]" type="hidden" value="{{ $stock->id }}" />
						</tr>
					@endforeach
					</tbody>
				</table>
				
			</div>
		</div>
		<div class="form-group">
			<button type="submit" class="btn btn-primary">Save</button>
			{!! Form::close() !!}
		</div>
	</div>
@endsection

@section('content-js')
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	
	<script>
		$(function () {
			var url = "{{ action('Admin\ProductColorController@index') }}";

			function load()
			{
				var product_id = parseInt($(this).val(), 10);
				if (isNaN(product_id))
					product_id = '';

				location.href = url + '/' + product_id;
			}

			$('#product').on('change', load);
		});
	</script>
@endsection