@extends('admin.master_layout', ['active' => 'activity'])

@section('sidebar')
	@include('admin.activity.sidebar', ['active' => 'index'])
@endsection

@section('content')
	<div class="container-fluid">
		<div class="row">
			<div class="table-responsive">
				<table class="table table-banner">
					<thead>
						<tr>
							<th>Date</th>
							<th>IP Address</th>
							<th>Message</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($activities as $key => $activity)
							<tr class="{{ $activity->status == 1 ? 'success' : 'danger' }}">
								<td>[{{ $activity->created_at->format('d M Y H:i:s') }}]</td>
								<td>{{ $activity->ip_address }}</td>
								<td>{{ $activity->user_name }} {{ $activity->short_message }}.</td>
								<td>
									<a href="{{ action('Admin\ActivityController@detail', [$activity->id]) }}" class="btn btn-primary btn-xs">
										Detail
									</a>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			<div class="pull-right">
				{!! $activities->render() !!}
			</div>
		</div>
	</div>
@endsection