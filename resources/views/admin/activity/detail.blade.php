@extends('admin.master_layout', ['active' => 'activity'])

@section('sidebar')
	@include('admin.activity.sidebar', ['active' => 'index'])
@endsection

@section('content')
	<div class="container-fluid">
		<div class="row">
			<div class="content-header">
				<div class="col-md-3">
				<h2>
					Activity Detail
				</h2>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3"><label>Date</label></div>
			<div class="col-md-9">{{ $activity->created_at->format('d M Y H:i:s') }}</div>
		</div>
		<div class="row">
			<div class="col-md-3"><label>IP Address</label></div>
			<div class="col-md-9">{{ $activity->ip_address }}</div>
		</div>
		<div class="row">
			<div class="col-md-3"><label>User Agent</label></div>
			<div class="col-md-9">{{ $activity->user_agent }}</div>
		</div>
		<div class="row">
			<div class="col-md-3"><label>By</label></div>
			<div class="col-md-9">{{ $activity->user_name }}</div>
		</div>
		<div class="row">
			<div class="col-md-3"><label>Action</label></div>
			<div class="col-md-9">{{ $activity->action }}</div>
		</div>
		<div class="row">
			<div class="col-md-3"><label>Section</label></div>
			<div class="col-md-9">{{ $activity->type }}</div>
		</div>
		<div class="row">
			<div class="col-md-3"><label>Short Message</label></div>
			<div class="col-md-9">{{ $activity->user_name }} {{ $activity->short_message }}.</div>
		</div>
		<div class="row">
			<div class="col-md-3"><label>Detail Message</label></div>
			<div class="col-md-9">{{ $activity->message }}</div>
		</div>
	</div>
@endsection