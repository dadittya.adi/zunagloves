@extends('admin.master_layout', ['active' => 'product'])

@section('sidebar')
	@include('admin.product.sidebar', ['active' => 'size'])
@endsection

@section('content')
	<div class="container-fluid">
		{!!
			Form::open([
				'role' => 'form',
				'url' => action('Admin\ProductsizeController@store'),
				'method' => 'post'
			])
		!!}

		<div class="row form-group">
			<div class="col-md-4">
				@include('form.text', [
					'field' => 'name',
					'label' => 'Name',
					'placeholder' => 'Name'
				])
			</div>
			<div class="col-md-4">
				@include('form.text', [
					'field' => 'short_name',
					'label' => 'Short Name',
					'placeholder' => 'Short Name'
				])
			</div>	
			<div class="col-md-3" style="padding-top: 25px;">
				<button type="submit" class="btn btn-primary">
					Add
				</button>
			</div>	
		</div>

		{!! Form::close() !!}		

		<div class="row">
			<div class="table-responsive">
				<table class="table">
					<thead>
						<tr>
							<th>Show Order</th>
							<th width="40%">Name</th>
							<th width="40%">Short Name</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody id="sizeTable"></tbody>
				</table>
			</div>
		</div>
		
		<input type="hidden" id="sizeData" value="{{ json_encode($sizes, JSON_NUMERIC_CHECK) }}">
		<input type="hidden" id="errorId" value="{{ Session::get('error_id') }}">
		<input type="hidden" id="errorMsg" value="{{ json_encode($errors->getMessages()) }}">
	</div>
@endsection

@section('content-js')
	@include('admin.productsize._mustache')
	<script src="{{ asset(elixir("js/admin/master/productsize.js")) }}"></script>
@endsection

