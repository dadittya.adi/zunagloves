<script type="x-tmpl-mustache" id="sizeTmpl">
	{% #data %}
		{% #_edit_state %}
		<tr>
			<td>
				<form method="post" action="/admin/productsize/{% id %}/move/-1" style="display: inline-block">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<button type="submit" class="btn btn-xs " {% dis_min %}>
						<span class="glyphicon glyphicon-chevron-up"></span>
					</button>
				</form>
					{% _no %}
				
				<form method="post" action="/admin/productsize/{% id %}/move/1" style="display: inline-block">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<button type="submit" class="btn btn-xs " {% dis_max %}>
						<span class="glyphicon glyphicon-chevron-down"></span>
					</button>
				</form>

			</td>
			<td id="col_name_edit_{% id %}">
				<input type="text" class="form-control" id="name_{% _row %}" value="{% name %}">
				<span class="help-block text-danger hidden" id="error_name_edit_{% id %}"></span>
			</td>
			<td id="col_short_name_edit_{% id %}">
				<input type="text" class="form-control" id="shortName_{% _row %}" value="{% short_name %}">
				<span class="help-block text-danger hidden" id="error_short_name_edit_{% id %}"></span>
			</td>
			<td>
				<form method="post" action="/admin/productsize/{% id %}" id="update_{% _row %}">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<input type="hidden" name="name_edit" id="name_edit_{% _row %}" value="{% name %}">
					<input type="hidden" name="short_name_edit" id="shortName_edit_{% _row %}" value="{% short_name %}">
					
					<button type="button" class="btn btn-success btn-xs btn-save-item" 
						data-row="{% _row %}"
					>
						Save
					</button>
					<button type="button" class="btn btn-warning btn-xs btn-cancel-item" 
						data-row="{% _row %}"
					>
						Cancel
					</button>
				</form>
			</td>
		</tr>
		{% /_edit_state %}
		{% ^_edit_state %}
		<tr>
			<td>
				<form method="post" action="/admin/productsize/{% id %}/move/-1" style="display: inline-block">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<button type="submit" class="btn btn-xs " {% dis_min %}>
						<span class="glyphicon glyphicon-chevron-up"></span>
					</button>
				</form>
					{% _no %}
				
				<form method="post" action="/admin/productsize/{% id %}/move/1" style="display: inline-block">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<button type="submit" class="btn btn-xs " {% dis_max %}>
						<span class="glyphicon glyphicon-chevron-down"></span>
					</button>
				</form>
				
			</td>
			<td>{% name %}</td>
			<td>{% short_name %}</td>
			<td>
				<form method="post" action="/admin/productsize/{% id %}/delete">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<button type="button" class="btn btn-primary btn-xs btn-edit-item" 
						id="edit_{% id %}" data-row="{% _row %}"
					>
						Edit
					</button>			
					<button type="submit" class="btn btn-danger btn-xs btn-delete-item" 
						onclick="return confirm('Are you sure want to delete product size `{% name %}`?');"
					>
						Delete
					</button>
				</form>
			</td>
		</tr>
		{% /_edit_state %}
	{% /data %}
</script>