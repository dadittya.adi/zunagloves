<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="z-index: 9999;">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Media</h4>
      </div>
      <div class="modal-body">
        <div class="row">
        @if ($media->count() > 0)
          @foreach($media as $key => $med)
		  <div class="col-xs-6 col-md-3">
		    <a href="#" class="thumbnail">
		      <img id="useMedia" src="{{ $med->getMediaFullPath() }}" alt="Beautifull" onclick="setLink('{{ $med->getMediaFullPath() }}')">
		    </a>
		  </div>
		  @endforeach
		@endif
		</div>
      </div>
      <div class="modal-footer">
      	<div class="row">
      		<div class="col-lg-6">
      			
      		</div>
      	</div>
      	<div class="row">
		  <!--<div class="col-lg-6">
		    <div class="input-group">
		      <input type="file" class="form-control">
		      <span class="input-group-btn">
		      	{!!
				Form::open([
					'role' => 'form',
					'url' => '#',
					'method' => 'post',
					'enctype' => 'multipart/form-data'
				])
			!!}
		        <button class="btn btn-success" type="button">Import Media</button>
		        {!! Form::close() !!}
		      </span>
		    </div>
		  </div>-->
		  <div class="col-lg-6" style="float: right;">
		    <div class="input-group">
		      <span class="input-group-btn">
		        <button type="button" class="btn btn-default" id="cancelselect" data-dismiss="modal">Close</button>
        		<button type="button" class="btn btn-primary" id="select" data-dismiss="modal">Select</button>
		      </span>
		    </div><!-- /input-group -->
		  </div><!-- /.col-lg-6 -->
		</div><!-- /.row -->
      </div>
    </div>
  </div>
</div>