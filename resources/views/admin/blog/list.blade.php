@extends('admin.master_layout', ['active' => 'blog'])

@section('sidebar')
	@include('admin.blog.sidebar', ['active' => 'index'])
@endsection

@section('content')
	<div class="row">
		<div class="col-xs-10">
			<h1>Blogs</h1>
		</div>
		<div class="col-xs-2">
			<h1>
				<a href="{{ action('Admin\BlogController@create') }}" class="btn btn-primary">
					<span class="glyphicon glyphicon-plus"></span> Create New
				</a>
			</h1>
		</div>
	</div>
	<div class="row">
		<table class="table table-hover">
			<thead>
				<tr>
					<th>#</th>
					<th>Title</th>
					<th>Date</th>
					<th>State</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				@foreach($blogs as $key => $article)
				<tr>
					<td>
						{{{ $key + 1 }}}
					</td>
					<td>
						{{{ $article->title }}}
					</td>
					<td>
						<?php 
							$pub =explode(" ", $article->publish_at);
							$time = strtotime($pub[0]);
							$newformat = date('d M Y',$time);
						?>
						{{{ $newformat }}} {{{$pub[1]}}}
					</td>
					<td>
						@if ($article->publish_state == 'PUB')
							Published
						@elseif ($article->publish_state == 'DRF')
							Draft
						@else
							{{{ $article->publish_state }}}
						@endif
					</td>
					<td>
						<a href="{{ action('Admin\BlogController@edit', array($article->id)) }}" class="btn btn-xs btn-primary">Edit</a>
						<a href="{{ action('Admin\BlogController@delete', array($article->id)) }}" class="btn btn-xs btn-danger" onclick="return confirm('are you sure?')">Delete</a>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
@endsection