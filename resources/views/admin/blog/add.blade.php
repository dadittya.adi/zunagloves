@extends('admin.master_layout', ['active' => 'blog'])

@section('sidebar')
	@include('admin.blog.sidebar', ['active' => 'create'])
@endsection

@section('content')
	<div class="container-fluid">
		<div class="row">
			{!!
				Form::open([
					'role' => 'form',
					'url' => action('Admin\BlogController@store'),
					'method' => 'post',
				])
			!!}

			@include('form.text', [
				'field' => 'title',
				'label' => 'Title',
				'placeholder' => 'Title',
				'attributes' => [
					'id' => 'inputTitle'
				]
			])

			@include('form.text', [
				'field' => 'permalink',
				'label' => 'Permalink',
				'placeholder' => 'Permalink',
				'attributes' => [
					'id' => 'permalink'
				]
			])

			<div class="row">
				<div class="col-lg-6">
					<label for="tags[]" class="control-label">
						Tags
					</label>
					@foreach($tags as $key=>$tag)
				    <div class="input-group" style="margin-bottom: 10px;">
				    	<span class="input-group-addon">
				    		<input type="checkbox" name="tags[]" value="{{$tag->id}}">
				    	</span>
				    	<input type="text" disabled="disabled" class="form-control" value="{{$tag->name}}">
				    </div>
				    @endforeach
				</div>
			</div>

			@include('form.select', [
				'field' => 'publish_state',
				'label' => 'Publish State',
				'options' => [
					'PUB' => 'Published',
					'DRF' => 'Draft'
				]
			])

			<div class="row">
				<div class="col-lg-6">
					<div class="input-group" style="margin-bottom: 10px;">
						<button id="buttonMedia" type="button" class="btn btn-primary btn-md" data-toggle="modal" data-target="#myModal">
							Use Media
						</button>
					</div>
				</div>
			</div>

			@include('form.textarea', [
				'field' => 'content',
				'label' => 'Content',
				'placeholder' => 'Content',
				'default' => $example,
				'attributes' => [
					'id' => 'content'
				]
			])

			<div class="form-group">
				<label>Preview</label>
				<div class="control-input" id="preview">
				</div>
			</div>
			<input type="hidden" class="form-controll" name="mediaLink" id="mediaLink" value="">

			<div class="form-group">
				<button type="submit" class="btn btn-primary">Save</button>
			</div>

			{!! Form::close() !!}
		</div>
	</div>
@endsection

@section('content-js')
	<script>
		$(function() {
			var commonmark = window.commonmark;
			var parser = new commonmark.Parser();
			var renderer = new commonmark.HtmlRenderer();

			$("#inputTitle").keyup(function(){
				var title = $('#inputTitle').val();
				var permalink;

				permalink = title.replace(/[^a-zA-Z0-9\-_]+/g, '-').toLowerCase();
				permalink = permalink.replace(/^-+|-+$/g, '');

				if (permalink.length > 64)
					permalink = permalink.substring(0, 64);

				$('#permalink').val(permalink);
			});

			$("#content").keyup(function(){
				$('#preview').html(renderer.render(parser.parse($("#content").val())));
			});

			$(document).ready(function(){
				$('#preview').html(renderer.render(parser.parse($("#content").val())));
			});

			$("#cancelselect").click(function(){
				$("#mediaLink").val('');
			});

			$("#select").click(function(){
				var cursorPosition = $('#content').prop("selectionStart");
				//console.log(cursorPosition);
			    var v = $("#content").val();
			    var textBefore = v.substring(0,  cursorPosition);
			    var textAfter  = v.substring(cursorPosition, v.length);
			    if($("#mediaLink").val() == ''){
			    	$("#content").val();
			    }else{
			    	$('#content').val(textBefore + "\n![Image]("+ $("#mediaLink").val()+")\n" + textAfter);
			    }
			    $('#preview').html(renderer.render(parser.parse($("#content").val())));
			});

			$("#buttonMedia").click(function(){
				$("#mediaLink").val('');
			});

		});

		function setLink(link){
			$("#mediaLink").val();
			$("#mediaLink").val(link);
		}
		
	</script>
@endsection

@section('content-modal')
	@include('admin.blog._media_modal')
@endsection