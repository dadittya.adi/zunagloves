@if(isset($active) && ($active == 'index' || $active == 'create' || $active == 'indexTag' || $active == 'createTag'))
<a class="list-group-item {{ isset($active) && $active == 'index' ? 'active' : '' }}" href="{{ action('Admin\BlogController@index') }}">
	List Articles
</a>
<a class="list-group-item {{ isset($active) && $active == 'create' ? 'active' : '' }}" href="{{ action('Admin\BlogController@create') }}">	
	Create Article
</a>
<div class="separate-sidebar"></div>
<a class="list-group-item {{ isset($active) && $active == 'indexTag' ? 'active' : '' }}" href="{{ action('Admin\TagController@index') }}">
	List Tags
</a>
<a class="list-group-item {{ isset($active) && $active == 'createTag' ? 'active' : '' }}" href="{{ action('Admin\TagController@create') }}">	
	Create Tag
</a>
<div class="separate-sidebar"></div>
<a class="list-group-item {{ isset($active) && $active == 'indexMedia' ? 'active' : '' }}" href="{{ action('Admin\BlogsMediaController@index') }}">
	List Media
</a>
@elseif(isset($active) && ($active == 'edit'))
<a class="list-group-item {{ isset($active) && $active == 'index' ? 'active' : '' }}" href="{{ action('Admin\BlogController@index') }}">
	List Articles
</a>
<a class="list-group-item {{ isset($active) && $active == 'edit' ? 'active' : '' }}" href="{{ action('Admin\BlogController@edit', [$blogs[0]->id]) }}">	
	Edit Article
</a>
<div class="separate-sidebar"></div>
<a class="list-group-item {{ isset($active) && $active == 'indexTag' ? 'active' : '' }}" href="{{ action('Admin\TagController@index') }}">
	List Tags
</a>
<a class="list-group-item {{ isset($active) && $active == 'createTag' ? 'active' : '' }}" href="{{ action('Admin\TagController@create') }}">	
	Create Tag
</a>
<div class="separate-sidebar"></div>
<a class="list-group-item {{ isset($active) && $active == 'indexMedia' ? 'active' : '' }}" href="{{ action('Admin\BlogsMediaController@index') }}">
	List Media
</a>
@elseif(isset($active) && ($active == 'editTag'))
<a class="list-group-item {{ isset($active) && $active == 'index' ? 'active' : '' }}" href="{{ action('Admin\BlogController@index') }}">
	List Articles
</a>
<a class="list-group-item {{ isset($active) && $active == 'create' ? 'active' : '' }}" href="{{ action('Admin\BlogController@create') }}">	
	Create Article
</a>
<div class="separate-sidebar"></div>
<a class="list-group-item {{ isset($active) && $active == 'indexTag' ? 'active' : '' }}" href="{{ action('Admin\TagController@index') }}">
	List Tags
</a>
<a class="list-group-item {{ isset($active) && $active == 'editTag' ? 'active' : '' }}" href="{{ action('Admin\TagController@edit', [$tag->id]) }}">	
	Edit Tag
</a>
<div class="separate-sidebar"></div>
<a class="list-group-item {{ isset($active) && $active == 'indexMedia' ? 'active' : '' }}" href="{{ action('Admin\BlogsMediaController@index') }}">
	List Media
</a>
@elseif(isset($active) && ($active == 'indexMedia'))
<a class="list-group-item {{ isset($active) && $active == 'index' ? 'active' : '' }}" href="{{ action('Admin\BlogController@index') }}">
	List Articles
</a>
<a class="list-group-item {{ isset($active) && $active == 'create' ? 'active' : '' }}" href="{{ action('Admin\BlogController@create') }}">	
	Create Article
</a>
<div class="separate-sidebar"></div>
<a class="list-group-item {{ isset($active) && $active == 'indexTag' ? 'active' : '' }}" href="{{ action('Admin\TagController@index') }}">
	List Tags
</a>
<a class="list-group-item {{ isset($active) && $active == 'createTag' ? 'active' : '' }}" href="{{ action('Admin\TagController@create') }}">	
	Create Tag
</a>
<div class="separate-sidebar"></div>
<a class="list-group-item {{ isset($active) && $active == 'indexMedia' ? 'active' : '' }}" href="{{ action('Admin\BlogsMediaController@index') }}">
	List Media
</a>
@endif