<a class="list-group-item {{ isset($active) && $active == 'index' ? 'active' : '' }}" href="{{ action('Admin\StoreController@index', \Route::current()->parameter('id')) }}">
	List Store
</a>
<a class="list-group-item {{ isset($active) && $active == 'create' ? 'active' : '' }}" href="{{ action('Admin\StoreController@create', \Route::current()->parameter('id')) }}">	
	Create Store
</a>