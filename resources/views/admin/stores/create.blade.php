@extends('admin.master_layout', ['active' => 'store'])

@section('sidebar')
	@include('admin.stores.sidebar', ['active' => 'index'])
@endsection

@section('content')
	<div class="container-fluid">
		<div class="row">
			<div class="content-header">
				<h2>
					Create Store
				</h2>
			</div>
		</div>
		<div class="row">
			{!!
				Form::open([
					'role' => 'form',
					'url' => action('Admin\StoreController@store'),
					'method' => 'post'
				])
			!!}

			{!! Form::hidden('city', $city->id, ['id' => 'city_id']) !!}

			@include('form.text', [
				'field' => 'name',
				'label' => 'Name',
				'placeholder' => 'Name'
			])

			@include('form.text', [
				'field' => 'url',
				'label' => 'URL',
				'placeholder' => 'URL'
			])

			@include('form.select', [
				'field' => 'is_active',
				'label' => 'State',
				'options' => [
					'1' => 'Active',
					'0' => 'Inactive'
				],
				'default' => '1'
			])

			<div class="form-group">
				<button type="submit" class="btn btn-primary">Save</button>
			</div>

			{!! Form::close() !!}

			<input type="hidden" id="errorId" value="{{ Session::get('error_id') }}">
			<input type="hidden" id="errorMessage" value="{{ json_encode($errors->getMessages()) }}">		
		
		</div>
	</div>
@endsection