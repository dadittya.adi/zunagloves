@extends('admin.master_layout', ['active' => 'store'])

@section('sidebar')
	@include('admin.stores.sidebar', ['active' => 'index'])
@endsection

@section('content')
	<div class="container-fluid">
		<div class="row">
			<div class="table-responsive">
				<table class="table table-banner">
					<thead>
						<tr>
							<th>Show Order</th>
							<th>Name</th>
							<th>URL</th>
							<th>State</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($stores as $key => $store)
							<tr>
								<td>
									{!!
										Form::open(array(
											'action' => array('Admin\StoreController@move', $store->id, -1),
											'method' => 'post',
											'style' => 'display: inline-block;',
										))
									!!}

									<button type="submit" class="btn btn-xs" {{ $key > 0 ? '' : 'disabled' }}>
										<span class="glyphicon glyphicon-chevron-up"></span>
									</button>
									
									{!! Form::close() !!}
									
									{{ $key + 1 }}

									{!!
										Form::open(array(
											'action' => array('Admin\StoreController@move', $store->id, 1),
											'method' => 'post',
											'style' => 'display: inline-block;',
										))
									!!}

									<button type="submit" class="btn btn-xs" {{ ($key + 1) < $stores->count() ? '' : 'disabled' }}>
										<span class="glyphicon glyphicon-chevron-down"></span>
									</button>
									
									{!! Form::close() !!}
								</td>
								<td>{{ $store->name }}</td>
								<td>{{ $store->url }}</td>
								
								<td>
									@if ($store->is_active)
										Active
									@else
										Inactive
									@endif
								</td>
								<td>
									<a href="{{ action('Admin\StoreController@edit', [$store->id]) }}" class="btn btn-primary btn-xs">
										Edit
									</a>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
@endsection