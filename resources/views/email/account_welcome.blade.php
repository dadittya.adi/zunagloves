<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h3>Your account in Zunagloves has been actived !</h3>

		<div>
			<p style="margin: 1em 0;">Welcome to Zunagloves!</p>
			<p style="margin: 1em 0;">
				Thank you for confirming your account. <br>
				Now that it has been activated, you have opened the doors to many of our products to suit your needs in sports. 
			</p>

			<p style="margin: 1em 0;">
				Thank you for your purchases in our store. <br> 
				Happy shopping and Perform in Style!
			</p>
			
			<p style="margin: 1em 0;">
				Cheers and Love<br>
				Zunagloves			</p>
			
			<p style="margin: 1em 0;">If you have any inquiries, please kindly email us at support@zunasport.com or call our HOTLINE at +62 882 1515 1818.</p>
			<p style="margin: 1em 0;">Our working hours is :<br>
				Monday – Friday	: 08:00 – 16:00 WIB<br>
				Saturday		: 08:00 – 13:00 WIB
			</p>
			<p style="margin: 1em 0;">We will reply you as soon as we could and please do not reply to this email.</p>

		</div>

		<div style="font-size: 11px; margin-top: 10px">
			This email was sent automatically by Zunagloves.
		</div>
		<br>
		<hr>

		<div>
			<p style="margin: 1em 0;">Selamat datang di Zunagloves!</p>
			<p style="margin: 1em 0;">
				Terima kasih telah mengkonfirmasi akun anda <br>
				Akun anda sekarang telah aktif, anda telah membuka pintu untuk semua kebutuhan olah raga anda.
			</p>

			<p style="margin: 1em 0;">
				Terima kasih untuk pembelian anda di Zunagloves<br> 
				Selamat berbelanja dan Perform in Style!
			</p>
			
			<p style="margin: 1em 0;">
				Salam Hangat<br>
				Zunagloves
			</p>
			
			<p style="margin: 1em 0;">Jika anda mempunyai pertanyaan atau keluhan, silahkan email kami di support@zunasport.com atau telepon ke +62 882 1515 1818.</p>
			<p style="margin: 1em 0;">Jam kerja :<br>
				Senin – Jumat	: 08:00 – 16:00 WIB<br>
				Sabtu			: 08:00 – 13.00 WIB
			</p>
			<p style="margin: 1em 0;">Kami akan membalas segera dan harap jangan membalas email ini.</p>

		</div>

		<div style="font-size: 11px; margin-top: 10px">
			This email was sent automatically by Zunagloves.
		</div>

	</body>
</html>









