<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h3>Confirmation of your account with Zunagloves</h3>

		<div>
			<p style="margin: 1em 0;">Thank you for signing up with Zunagloves!<br>Please kindly confirm your account by clicking below URL and for further transaction:</p>
			<p style="margin: 1em 0 1em 15px;">
				<a href="{{ action('AccountController@activate', [$customer->id, $customer->generateConfirmHash()]) }}" target="_blank">
					{{ action('AccountController@activate', [$customer->id, $customer->generateConfirmHash()]) }}
				</a>
			</p>
			<p style="margin: 1em 0;">Thank you for choosing Zunagloves and hope our products suit your needs.<br>Happy Shopping and Perform in Style!</p>
			
			<p style="margin: 1em 0;">Cheers and Love<br>Zunagloves</p>
			
			<p style="margin: 1em 0;">If you have any inquiries, please kindly email us at support@zunagloves.com or call our HOTLINE at +62 882 1515 1818.</p>
			<p style="margin: 1em 0;">Our working hours is :<br>
				Monday – Friday	: 08:00 – 16:00 WIB<br>
				Saturday		: 08:00 – 13:00 WIB
			</p>
			<p style="margin: 1em 0;">We will reply you as soon as we could and please do not reply to this email.</p>
		</div>

		<div style="font-size: 11px; margin-top: 10px">
			This email was sent automatically by Zunagloves.
		</div>
		<br>
		<hr>
		<div>
			<p style="margin: 1em 0;">
				Terima kasih telah mendaftar di Zunagloves!<br>
				Mohon klik link di bawah ini untuk transaksi selanjutnya:
			</p>
			<p style="margin: 1em 0 1em 15px">
				<a href="{{ action('AccountController@activate', [$customer->id, $customer->generateConfirmHash()]) }}" target="_blank">
					{{ action('AccountController@activate', [$customer->id, $customer->generateConfirmHash()]) }}
				</a>
			</p>
			<p style="margin: 1em 0;">
				Terima kasih telah memilih Zunagloves dan kami berharap produk-produk kami sesuai dengan kebutuhan anda.<br> 
				Selamat berbelanja dan Perform in Style!
			</p>
			
			<p style="margin: 1em 0;">
				Salam Hangat<br>
				Zunagloves
			</p>
			
			<p style="margin: 1em 0;">Jika anda mempunyai pertanyaan atau keluhan, silahkan email kami di support@zunagloves.com atau telepon ke +62 882 1515 1818.</p>
			<p style="margin: 1em 0;">Jam kerja :<br>
				Senin – Jumat	: 08:00 – 16:00 WIB<br>
				Sabtu			: 08:00 – 13.00 WIB
			</p>
			<p style="margin: 1em 0;">Kami akan membalas segera dan harap jangan membalas email ini.</p>
		</div>

		<div style="font-size: 11px; margin-top: 10px">
			This email was sent automatically by Zunagloves.
		</div>

	</body>
</html>