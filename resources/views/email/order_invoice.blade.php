<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h3>
			Invoice Order #{{{ $order->order_number }}}
		</h3>
		@if ($order->poFinish != null)
			THANK YOU for your pre order with Zuna Gloves!
			<p>
				Your order will be sent on {{  Carbon\Carbon::parse($order->poFinish)->format('d M Y')}}
			</p>
		@else
			THANK YOU for your purchase with Zuna Gloves!
		@endif
		

		<div>
			@include('email._info_invoice')

			@if ($order->payment == 'ATM')
				<p style="margin: 1em 0;">Payment method you chose is Direct Payment Bank.</p>
				<p style="margin: 1em 0;">You can make direct transfer to Bank:</p>
					<ul>
						<li>BCA 0098555512 a/n CV Anugerah Jaya</li>
					</ul>
			@endif

			<p style="margin: 1em 0;">Please proceed to payment through our website within 24 hours.<br>
			You will be guided to Midtrans, Zuna Gloves official online payment gateway for secure payment.<br> 
			{{-- You can get further information through:</p>
			<p style="margin: 1em 0 1em 15px">
				<a href="{{ action('DashboardController@home') }}" target="_blank">
				</a>
			</p> --}}
			<p style="margin: 1em 0;">
				Thank you for your purchases in our store.
			</p>
			
			<p style="margin: 1em 0;">
				Zuna #GlovesForEveryone
			</p>
			
			<p style="margin: 1em 0;">If you have any inquiries, please kindly email us at support@zunagloves.com oror whatsapp at +62 81 1278 1818.</p>
			<p style="margin: 1em 0;">Our working hours is :<br>
				Monday – Friday : 08:00 – 17:00 WIB
			</p>
			<p style="margin: 1em 0;">We will reply you as soon as we could and please do not reply to this email.</p>
		</div>

		<div style="font-size: 11px; margin-top: 10px">
			This email was sent automatically by Zuna Gloves.
		</div>
	</body>
</html>
