<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h3>Revision {{{ $order->revision }}} Invoice Order #{{{ $order->order_number }}}</h3>

		<div>
			@include('email._info_invoice')

			@if ($notes)
				<p style="margin: 1em 0;">{{ nl2br(e($notes)) }}</p>
			@endif

			{{-- <p style="margin: 1em 0;">You can get further information through:</p>
			<p style="margin: 1em 0 1em 15px">
				<a href="{{ action('AccountController@detailOrder', [$order->order_number]) }}" target="_blank">
					{{ action('AccountController@detailOrder', [$order->order_number]) }}
				</a>
			</p> --}}
			<p style="margin: 1em 0;">
				Thank you for your purchases in our store. <br> 
				Happy shopping and Perform in Style!
			</p>
			
			<p style="margin: 1em 0;">
				Cheers and Love<br>
				Zunagloves
			</p>
			
			<p style="margin: 1em 0;">If you have any inquiries, please kindly email us at support@zunasport.com or call our HOTLINE at +62 882 1515 1818.</p>
			<p style="margin: 1em 0;">Our working hours is :<br>
				Monday – Friday	: 08:00 – 16:00 WIB<br>
				Saturday		: 08:00 – 13:00 WIB
			</p>
			<p style="margin: 1em 0;">We will reply you as soon as we could and please do not reply to this email.</p>
		</div>

		<div style="font-size: 11px; margin-top: 10px">
			This email was sent automatically by Zunagloves.
		</div>
	</body>
</html>
