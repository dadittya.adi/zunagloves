<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h3>On Process Order #{{{ $order->order_number }}}</h3>

		@if ($order->poFinish != null)
			<p>
				Your order will be sent on {{  Carbon\Carbon::parse($order->poFinish)->format('d M Y')}}
			</p>
		@endif

		<div>
			<p style="margin: 1em 0;">Thank you for your payment order #{{ $order->order_number }}.</p>
			<p style="margin: 1em 0;">{{ nl2br(e($order->work_notes)) }}</p>

			{{-- <p style="margin: 1em 0;">You can get further information through:</p>
			<p style="margin: 1em 0 1em 15px">
				<a href="{{ action('AccountController@detailOrder', [$order->order_number]) }}" target="_blank">
					{{ action('AccountController@detailOrder', [$order->order_number]) }}
				</a>
			</p> --}}
			<p style="margin: 1em 0;">
				Thank you for your purchases in our store.
			</p>
			
			<p style="margin: 1em 0;">
				Zuna #GlovesForEveryone
			</p>
			
			<p style="margin: 1em 0;">If you have any inquiries, please kindly email us at support@zunagloves.com  or whatsapp at +62 81 1278 1818.</p>
			<p style="margin: 1em 0;">Our working hours is :<br>
				Monday – Friday	: 08:00 – 17:00 WIB
			</p>
			<p style="margin: 1em 0;">We will reply you as soon as we could and please do not reply to this email.</p>
		</div>

		<div style="font-size: 11px; margin-top: 10px">
			This email was sent automatically by Zuna Gloves.
		</div>
	</body>
</html>
