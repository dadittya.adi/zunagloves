<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h3>Forget Password</h3>

		<div>
			<p style="margin: 1em 0;">We have received your request for resetting your password account.</p>
			<p style="margin: 1em 0;">To reset your password, visit link below :</p>
			<p style="margin-left:1em 0 1em 15px">
				<a href="{{ action('AccountController@showResetPassword', [$customer->id, $datetime, $token]) }}" target="_blank">
					{{ action('AccountController@showResetPassword', [$customer->id, $datetime, $token]) }}
				</a>
			</p>
			<p style="margin: 1em 0;">If you did not reset the password, ignore this email.</p>
			<p style="margin: 1em 0;">This link will be expired on {{ Config::get('auth.reminder.expire', 60) }} minutes.</p>
			<p style="margin: 1em 0;">
				Thank you for your purchases in our store. <br> 
				Happy shopping and Perform in Style!
			</p>
			
			<p style="margin: 1em 0;">
				Cheers and Love<br>
				Zunagloves
			</p>
			
			<p style="margin: 1em 0;">If you have any inquiries, please kindly email us at support@zunagloves.com or call our HOTLINE at +62 882 1515 1818.</p>
			<p style="margin: 1em 0;">Our working hours is :<br>
				Monday – Friday	: 08:00 – 16:00 WIB<br>
				Saturday		: 08:00 – 13:00 WIB
			</p>
			<p style="margin: 1em 0;">We will reply you as soon as we could and please do not reply to this email.</p>
		</div>

		<div style="font-size: 11px; margin-top: 10px">
			This email was sent automatically by Zunagloves.
		</div>
	</body>
</html>

