<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
		<style type="text/css">
			tr{
				height:25px;
			}
		</style>
	</head>
	<body>
		Thank you, your order has successfully received. Following table shows your detail order with invoice number #20160407134016.
		<br><br>
		<div>
			<table width="100%" style="border-top: 1px solid black; border-bottom: 1px solid black; max-width: 940px">
				<tr>
					<th align="left">Id</th>
					<th align="left">Name</th>
					<th align="right">Price</th>
					<th align="right">Quantity</th>
					<th align="right">Subtotal</th>
				</tr>
				<tr>
					<td>1234567899</td>
					<td>Billiard Basic (Warna, Ukuran)</td>
					<td align="right">Rp. 35,500 </td>
					<td align="right">10</td>
					<td align="right">Rp. 355,000</td>
				</tr>
				<tr>
					<td>DELIVERY</td>
					<td>Semarang</td>
					<td align="right">Rp. 35,000</td>
					<td align="right">1</td>
					<td align="right">Rp. 35,000</td>
				</tr>
				<tr >
					<td colspan="4" style="border-top: 1px solid black;">Total</td>
					<td align="right" style="border-top: 1px solid black;"><b>Rp.390,000 </b></td>
				</tr>
			</table>

			<p style="margin: 1em 0;">
				To continue proceeding your order, please do bank transfer process with Zunagloves secure payment by this following steps.
			</p>

			<b>ATM Mandiri/ATM Bersama Network</b>
			<ol>
				<li>In the main menu, choose Other Transaction menu.</li>
				<li>Choose Transfer menu.</li>
				<li>Choose Other Bank menu.</li>
				<li>Enter this transaction number 013 8778 002266247539 (code 013 and 16 digits Virtual account).</li>
				<li>Enter your bill amount Rp.390,000 correctly and completely. Payment with inappropiate amount will be automatically rejected.</li>
				<li>In transfer confirmation page will be shown your payment amount & destination bank account. If shown information is matching to your order, press Correct.</li>
			</ol>

			<b>ATM BCA/ATM PRIMA Network</b>
			<ol>
				<li>In the main menu, choose Other Transaction menu.</li>
				<li>Choose Transfer menu.</li>
				<li>Choose Other Bank menu.</li>
				<li>Enter code 013 to refer Bank Permata, after that press Correct.</li>
				<li>Enter your bill amount Rp.390,000 correctly and completely. Payment with inappropiate amount will be automatically rejected.</li>
				<li>Enter 16 digits virtual account payment number 8778 002266247539, after that press Correct.</li>
				<li>In transfer confirmation page will be show your payment amount & destination bank account. If shown information is matching to your order, press Correct.</li>
			</ol>

			<b>ATM Bank Permata/ATM Alto</b>
			<ol>
				<li>In the main menu, choose Other Transaction menu.</li>
				<li>Choose Payment Transaction menu.</li>
				<li>Choose Other menu.</li>
				<li>Choose Virtual Account menu.</li>
				<li>Enter 16 digits virtual account payment number 8778 002266247539.</li>
				<li>In transfer confirmation page will be show your virtual account number & your bill, after that press Correct.</li>
				<li>Choose your payment account and press Correct.</li>
			</ol>
			<div style="padding-left: 7%; padding-right: 7%;">
			<div style="width: 100%;border: 1px solid black; max-width: 500px; ">
				<div style="padding:0 15px 0 15px; text-align: left">
				<p><b>Notice:</b></p>
				<p>Your payment deadline is 1x24 hours after finishing order process: <b>14 April 2016 - 16:39:29 Asia/Jakarta. </b></p>
				<p>If there is no payment until deadline time, your order will automatically rejected</p>
				</div>
			</div>
			</div>
			<p style="margin: 1em 0;">
				Thank you for trusting Zunagloves to fill your needs.<br> 
				Happy shopping and Perform in Style!
			</p>
			
			<p style="margin: 1em 0;">
				Cheers and Love<br>
				Zunagloves
			</p>
			
			<p style="margin: 1em 0;">If you have any inquiries, please kindly email us at support@zunagloves.com  or whatsapp at +62 81 1278 1818.</p>
			<p style="margin: 1em 0;">Our working hours is :<br>
				Monday – Friday	: 08:00 – 17:00 WIB
			</p>
			<p style="margin: 1em 0;">We will reply you as soon as we could and please do not reply to this email.</p>
		</div>

		<div style="font-size: 11px; margin-top: 10px">
			This email was sent automatically by Zuna Gloves.
		</div>
	</body>
</html>
