<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h3>Terimakasih</h3>
		<div>
			<p style="margin: 1em 0;">
				Terima kasih telah Mensubscribe di Zuna Gloves!<br>
				Berikut kode voucher yang bisa anda gunakan <br>
				{{ $voucherCode}} <br>
				Masa berlaku voucher tersebut adalah {{ $validIn }} hari
			</p>
			
			<p style="margin: 1em 0;">
				Terima kasih telah memilih Zunagloves dan kami berharap produk-produk kami sesuai dengan kebutuhan anda.<br> 
				Selamat berbelanja dan Perform in Style!
			</p>
			
			<p style="margin: 1em 0;">
				Salam Hangat<br>
				Zunagloves
			</p>
			
			<p style="margin: 1em 0;">Jika anda mempunyai pertanyaan atau keluhan, silahkan email kami di support@zunagloves.com atau telepon ke +62 882 1515 1818.</p>
			<p style="margin: 1em 0;">Jam kerja :<br>
				Senin – Jumat	: 08:00 – 17:00 WIB
			</p>
			<p style="margin: 1em 0;">Kami akan membalas segera dan harap jangan membalas email ini.</p>
		</div>

		<div style="font-size: 11px; margin-top: 10px">
			This email was sent automatically by Zunagloves.
		</div>

	</body>
</html>