<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h3>Payment Pending/Check Order #{{{ $order->order_number }}}</h3>
		We are sorry to inform that payment failed. <br>
		Please click below link to reprocess the payment within 24 hours:
		<p style="margin: 1em 0 1em 15px">
			<a href="{{ action('AccountController@detailOrder', [$order->order_number]) }}" target="_blank">
				{{ action('AccountController@detailOrder', [$order->order_number]) }}
			</a>
		</p>
		<div>
			@if ($order->payment_method == 'ATM')
			<p style="margin: 1em 0;">You just confirmed your payment [Transfer ATM].</p>
			@elseif ($order->payment_method == 'VTW')
			<p style="margin: 1em 0;">You just confirmed your payment method. Please process as instructed.</p>
			@endif

			@include('email._info_invoice')

			
			<p style="margin: 1em 0;">
				Zuna #GlovesForEveryone
			</p>
			
			<p style="margin: 1em 0;">If you have any inquiries, please kindly email us at support@zunagloves.com  or whatsapp at +62 81 1278 1818.</p>
			<p style="margin: 1em 0;">Our working hours is :<br>
				Monday – Friday	: 08:00 – 17:00 WIB
			</p>
			<p style="margin: 1em 0;">We will reply you as soon as we could and please do not reply to this email.</p>
		</div>

		<div style="font-size: 11px; margin-top: 10px">
			This email was sent automatically by Zuna Gloves.
		</div>
	</body>
</html>
