<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h3>Message from {{ $name }}</h3>
		<div>
			<img src="{{ asset("images/logo.png") }}" height="150px">
			<p style="margin: 1em 0;">
				<a href="{{ action('ProductController@showProduct', [$product->id]) }}">
					{{ action('ProductController@showProduct', [$product->id]) }}
				</a>
			</p>
			<p style="margin: 1em 0;">
				{!! nl2br($msg) !!}
			</p>
			<img src="{{ action('HomeController@showProductImage', [$product->id]) }}" height="100px"><br>
			<span><b>{{ $product->name }}</b></span>
			<p style="margin: 1em 0;">{{ $name }}</p>
			<br><br>
			<div style="font-size:11px;color:gray">
				Zunagloves has not saved your email address.<br>
				If you would like to receive emails from Zunagloves in the future, register on
				<a href="{{ action('HomeController@index') }}" target="_blank">Zunagloves</a>
			</div>
		</div>
	</body>
</html>