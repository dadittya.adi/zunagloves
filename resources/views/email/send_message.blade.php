<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h3>New message from website</h3>

		<div>
			<p style="margin: 1em 0;">
				<b>Name</b> : {{{ ucfirst($input['first_name']) . ' ' . ucfirst($input['last_name']) }}}
			</p>
			<p style="margin: 1em 0;">
				<b>Email</b> : {{{ $input['email'] }}}
			</p>
			<p style="margin: 1em 0;">
				<b>Phone</b> : {{{ $input['phone'] }}}
			</p>
			<p style="margin: 1em 0;">
				<b>Message</b> : <br>
				<i>{!! nl2br($input['message']) !!}</i>
			</p>
		</div>
	</body>
</html>
