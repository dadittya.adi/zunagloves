@extends('layouts.web')

@section('content')
<div class="container-fluid">
    <div class="d-flex mt-4 mb-2">
        <a href="#" class="ml-auto">Clear All</a>
    </div>
    <div class="table-zn table-zn-cart table-zn-bordered">
        <div class="table-zn__header">
            <div class="col-4" id="product-column">
                <span class="text-muted">Product</span>
            </div>
            <div class="d-none d-md-block col-2 text-right" id="price-column">
                <span class="text-muted">Price</span>
            </div>
            <div class="d-none d-md-block col-2 offset-1" id="qty-column">
                <span class="text-muted">Quantity</span>
            </div>
            <div class="d-none d-md-block col-2 text-right" id="subtotal-column">
                <span class="text-muted">Subtotal</span>
            </div>
            <div class="d-none d-md-block col-2" id="action-column">
                <span class="text-muted">Action</span>
            </div>
        </div>
        <div class="table-zn__body" v-if="cart.length > 0">
            <div class="table-zn__row" v-for="(item, key) in cart" :key="key">
                <div class="col-12 col-lg-4">
                    <div class="product-info">
                        <img :src="item.image" class="product-info__img" alt="">
                        <div class="detail-info">
                            <p class="pb-1 mb-1 font-weight-bold" v-cloak>@{{ item.name }}</p>
                            <div class="text-xs">
                                <span class="d-block" v-if="item.size">Size: <span class="font-weight-bold">@{{ item.size }}</span></span>
                                <span class="d-block" v-if="item.color">Color: <span class="font-weight-bold">@{{ item.color }}</span></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="d-none d-md-block col-lg-1 text-right font-weight-bold" v-cloak>@{{ formatCurrency(parseFloat(item.price)) }}</div>
                <div class="col-5 col-lg-2 offset-md-1">
                    <div class="input-group qty mx-auto">
                        <div class="input-group-prepend">
                            <button type="button" v-cloak :class="{ 'btn px-0': true, 'text-warning': item.qty > 1 }" :disabled="item.qty == 1" @click="minQuantity(key, 'update')">
                                <i class="fas fa-2x fa-minus-circle"></i>
                            </button>
                        </div>
                        <input type="text" v-cloak name="qty" class="form-control qty text-center mx-3 bg-white border-top-0 border-right-0 border-left-0" min="1" max="12" :value="item.qty" v-mask="['#','##']" :ref="`qty-${item.slug}`" readonly>
                        <div class="input-group-append">
                            <button type="button" v-cloak :class="{ 'btn px-0': true, 'text-warning': item.qty < 12 }" :disabled="item.qty == 12" @click="addQuantity(key, 'update')">
                                <i class="fas fa-2x fa-plus-circle"></i>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="col-5 col-lg-2 text-right font-weight-bold" v-cloak>@{{ formatCurrency(parseFloat(item.price) * parseInt(item.qty)) }}</div>
                <div class="col-2 col-lg-2 text-center">
                    <button class="btn btn-sm text-muted" title="Remove" @click="deleteFromCart(key)"><i class="fas fa-trash"></i></button>
                </div>
            </div>
        </div>
        <div class="table-zn_body py-5" v-cloak v-else>
            <div class="d-flex flex-column justify-content-center align-items-center">
                <div class="col-lg-2 mb-3">
                    <img src="{{ asset('images/empty-cart.png') }}" class="rounded-circle img-fluid" alt="">
                </div>
                <p class="text-muted text-center h2 font-weight-bold">Your cart is empty</p>
            </div>
        </div>
        <div class="table-zn__footer bg-white" v-if="cart.length > 0">
            <div class="table-zn__row">
                <div class="col-lg-6">
                    <p class="text-muted mb-2">Have a promo code? <br> Fill in this field and apply.</p>
                    <form action="{{ route('voucher.check') }}" @submit.prevent="checkVoucher" method="POST">
                        <div class="form-group px-3 row">
                            <input type="hidden" :value="subtotal" name="amount">
                            <input type="text" name="code" class="form-control col-6" :value="voucher && voucher.status == 'available' ? voucher.data.code : ''" placeholder="Add promo code">
                            <div class="col-6">
                                <button type="button" class="btn btn-danger" @click="removeAppliedVoucher" v-if="voucher && voucher.status == 'available'">Remove</button>
                                <button type="submit" class="btn btn-warning">Apply</button>
                            </div>
                            <small v-if="voucher" :class="{'form-text font-italic': true, 'text-danger': voucher.status == 'fail', 'text-success': voucher.status == 'available'}">@{{ voucher.message }}</small>
                        </div>
                    </form>
                </div>
                <div class="col-lg-7">
                    <div class="row">
                        <div class="col-4">
                            <p class="font-weight-bold text-right">Subtotal</p>
                        </div>
                        <div class="col-4">
                            <p class="font-weight-bold text-right" v-cloak>@{{ formatCurrency(subtotal) }}</p>
                        </div>
                    </div>
                    <div class="row" v-if="voucher && voucher.status == 'available'">
                        <div class="col-4">
                            <p class="font-weight-bold text-right">Discount</p>
                        </div>
                        <div class="col-4">
                            <p class="font-weight-bold text-success text-right" v-cloak>- @{{ formatCurrency(discountAmount) }}</p>
                        </div>
                    </div>
                    <div class="row" v-if="shippingCost > 0">
                        <div class="col-4">
                            <p class="font-weight-bold text-right">Shipping Cost</p>
                        </div>
                        <div class="col-4">
                            <p class="font-weight-bold text-right" v-cloak>@{{ formatCurrency(subtotal) }}</p>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-4">
                            <p class="font-weight-bold text-right">Total</p>
                        </div>
                        <div class="col-4">
                            <p class="font-weight-bold text-warning text-right" v-cloak>@{{ formatCurrency(total) }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row my-5 justify-content-end" v-if="cart.length > 0">
        <div class="col-6 col-lg-3">
            <a href="{{ route('web.category', 'accessories') }}" class="btn btn-block btn-lg btn-outline-secondary">Continue Shopping</a>
        </div>
        <div class="col-6 col-lg-3" v-if="cart.length > 0">
            @auth
            <form action="{{ route('web.cart.store') }}" class="w-100" id="form_cart" @submit.prevent="checkout" method="post">
                @csrf
                <button class="btn btn-block btn-lg btn-warning">Process to Checkout</button>
            </form>
            @endauth

            @guest
            <button class="btn btn-block btn-lg btn-warning" data-toggle="modal" data-target="#login-form" @click="guestCheckoutToggle">Process to Checkout</button>
            @endguest
        </div>
    </div>
</div>
@endsection