@extends('layouts.web')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 py-3">
                <loading-component v-if="showLoading"></loading-component>
                <h5 class="font-weight-bold">Shipping Info</h5>
                <form action="{{ route('checkout.store') }}" id="form-checkout" @submit.prevent="submitFormCheckout" method="POST">
                    @csrf
                    @auth 
                    <div class="form-group form-check">
                        <label class="form-check-label">
                            <input type="checkbox" class="form-check-input" @change="shipToDifferentAddress">
                            Ship to a different address?
                        </label>
                    </div>
                    @endauth
                    <div class="form-group row required">
                        <div class="col-md-6">
                            <label for="">Full Name</label>
                            <input type="text" class="form-control form-control-lg" name="full_name" 
                                @auth value="{{ Auth::user()->full_name }}" @endauth
                                @guest value="" @endguest
                                aria-describedby="helpId" placeholder="Type Full Name">
                                <small class="form-text text-danger" v-if="errors.length > 0 && errors[0]['full_name']">@{{ errors[0]['full_name'] }}</small>
                        </div>
                        <div class="col-md-6">
                            <label for="">Date of Birth</label>
                            <input type="date" class="form-control form-control-lg" name="dob" 
                                @auth value="{{ Auth::user()->dob }}" @endauth
                                @guest value="" @endguest>
                                <small class="form-text text-danger" v-if="errors.length > 0 && errors[0]['dob']">@{{ errors[0]['dob'] }}</small>
                        </div>
                    </div>
                    <div class="form-group row required">
                        <div class="col-md-6">
                            <label for="">Email</label>
                            <input type="text" class="form-control form-control-lg" name="email" 
                                @auth value="{{ Auth::user()->email }}" @endauth
                                aria-describedby="helpId" placeholder="sample@gmail.com">
                                <small class="form-text text-danger" v-if="errors.length > 0 && errors[0]['email']">@{{ errors[0]['email'] }}</small>
                        </div>
                        <div class="col-md-6">
                            <label for="">Phone Number</label>
                            <input type="text" class="form-control form-control-lg" name="phone" 
                                @auth value="{{ Auth::user()->phone }}" @endauth
                                aria-describedby="helpId" placeholder="08xxxxxxxxx">
                                <small class="form-text text-danger" v-if="errors.length > 0 && errors[0]['phone']">@{{ errors[0]['phone'] }}</small>
                        </div>
                    </div>
                    <div class="form-group row required">
                        <div class="col-md-6">
                            <label for="">Country / Region</label>
                            <select name="province" class="form-control form-control-lg" v-model="province" @change="selectCity" id="province">
                                @foreach( getProvinceForInsert() as $id => $province)
                                <option value="{{ $id }}" 
                                    @auth @if($id == Auth::user()->province) selected @endif @endauth>{{ $province }} 
                                </option>
                                @endforeach
                            </select>
                            <small class="form-text text-danger" v-if="errors.length > 0 && errors[0]['province']">@{{ errors[0]['province'] }}</small>
                        </div>
                        <div class="col-md-6">
                            <label for="">Town / City</label>
                            <select name="city" class="form-control form-control-lg" v-model="city" id="city" @change="selectDistrict">
                                <option v-cloak v-if="cities.length > 0" :value="city.city_id" v-for="city in cities" :key="city.city_id">@{{ city.type + ' ' +city.city_name }}</option>
                                <option v-cloak v-else value="">-- Select City --</option>
                            </select>
                            <small class="form-text text-danger" v-if="errors.length > 0 && errors[0]['city']">@{{ errors[0]['city'] }}</small>
                        </div>
                    </div>
                    <div class="form-group row required">
                        <div class="col-md-6">
                            <label for="">Sub-district</label>
                            <select name="district" class="form-control form-control-lg" v-model="district" id="district" @change="getCost">
                                <option v-cloak v-if="districts.length > 0" :value="district.subdistrict_id" v-for="(district, key) in districts" :key="district.subdistrict_id">@{{ district.subdistrict_name }}</option>
                                <option v-cloak v-else value="">-- Select District --</option>
                            </select>
                            <small class="form-text text-danger" v-if="errors.length > 0 && errors[0]['district']">@{{ errors[0]['district'] }}</small>
                        </div>
                        <div class="col-md-6">
                            <label for="">Post Code / ZIP</label>
                            <input type="text" name="post_code" class="form-control form-control-lg" value="@auth {{ Auth::user()->postal_code }} @endauth" placeholder="Post Code">
                            <small class="form-text text-danger" v-if="errors.length > 0 && errors[0]['post_code']">@{{ errors[0]['post_code'] }}</small>
                        </div>
                    </div>
                    <div class="form-group required">
                        <label for="">Street Address</label>
                        <textarea name="address" class="form-control" cols="30" rows="10" placeholder="Type full of street address">@auth {{ Auth::user()->address }} @endauth</textarea>
                        <small class="form-text text-danger" v-if="errors.length > 0 && errors[0]['address']">@{{ errors[0]['address'] }}</small>
                    </div>
                    <hr>
                    <div class="form-group">
                        <label for="">Order Notes (Optional)</label>
                        <textarea name="notes" class="form-control" cols="30" rows="10" placeholder="Note about your order"></textarea>
                    </div>
                    <hr>
                    <div class="form-group">
                        <label>Select courier</label>
                        <p class="text-muted text-xs" v-if="courierData.length == 0">Please fill out the address before select courier service</p>
                        <div v-cloak class="form-check" v-if="courierData.length > 0" v-for="(data, key) in courierData" :key="key">
                            <input type="radio" class="form-check-input" :id="`${data.courier}_${key}`" name="courier" v-model="courier" :value="data.courier" @change="addShippingCost(key)">
                            <label :for="`${data.courier}_${key}`" class="form-check-label">
                                @{{ data.courier_name + ' (' + data.service + ') - ' + formatCurrency(data.cost[0].value) }}
                            </label>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-4 py-3 bg-light" style="z-index: 0">
                <div class="sticky-top">
                    <h5 class="font-weight-bold">Order Summary ({{ count($cart_data) }})</h5>
                    <div class="table-zn">
                        <div class="table-zn__body">
                            @php 

                            @endphp 
                            @foreach ($cart_data as $item)
                            <div class="table-zn__row">
                                <div class="product-info px-0">
                                    <img src="{{ asset($item->image) }}" class="product-info__img" alt="">
                                    <div class="detail-info">
                                        <p class="pb-1 mb-1">{{ $item->name }}</p>
                                        <p class="pb-1 mb-3">Size: {{ $item->size }} | Color: {{ $item->color }}</p>
                                        <span class="font-weight-bold">{{ $item->qty }} x {{ i18l_currency($item->price) }}</span>
                                    </div>
                                </div>
                            </div>    
                            @endforeach
                        </div>
                        <div class="table-zn__footer bg-light pt-3">
                            <div class="table-zn__row">
                                <div class="col-6 py-0">Subtotal</div>
                                <div class="col-6 py-0 text-right font-weight-bold">@{{ formatCurrency(subtotal) }}</div>
                            </div>
                            <div class="table-zn__row" v-if="voucher && voucher.status == 'available'">
                                <div class="col-6">
                                    <p class="font-weight-bold mb-0 text-muted">Discount</p>
                                </div>
                                <div class="col-6">
                                    <p class="font-weight-bold mb-0 text-success text-right" v-cloak>- @{{ formatCurrency(discountAmount) }}</p>
                                </div>
                            </div>
                            <div class="table-zn__row">
                                <div class="col-6 py-0">
                                    Shipping
                                    <span v-if="shippingCost > 0" class="italic font-weight-bold"><small>(@{{ totalWeight / 1000 }}kg)</small></span>
                                </div>
                                <div class="col-6 py-0 text-right font-weight-bold">@{{ formatCurrency(shippingCost) }}</div>
                            </div>
                            <hr>
                            <div class="table-zn__row">
                                <div class="col-6 py-0 h5 font-weight-bold">Total</div>
                                <div class="col-6 py-0 text-right font-weight-bold h5">@{{ formatCurrency(total) }}</div>
                            </div>
                        </div>
                    </div>
                    <div class="mt-5">
                        <button v-cloak type="submit" form="form-checkout" v-if="showPaymentButton" class="btn btn-lg btn-block btn-warning" id="payButton">Select Payment</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('modal')
<!-- Modal -->
<div class="modal fade" id="checkoutResponse" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog modal-sm modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="py-5 text-center">
                    <p>
                        <i class="fas fa-check-circle h1 text-success" v-if="checkoutResponse.status == 'success'"></i>
                        <i class="fas fa-exclamation-circle h1 text-danger" v-if="checkoutResponse.status == 'pending'"></i>
                        <i class="fas fa-times-circle h1 text-danger" v-if="checkoutResponse.status == 'error'"></i>
                    </p>
                    <h4 class="font-weight-bold mb-4 text-secondary">@{{ checkoutResponse.message ? checkoutResponse.message : 'Terima Kasih!' }}</h4>
                    <a href="{{ isset(Auth::user()->id) ? route('dashboard.home') : route('web.home')}}" class="btn rounded-pill font-weight-bold btn-warning">{{ isset(Auth::user()->id) ? "Go To Dashboard" : "Go To Home Page" }}</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endpush

@auth
<script type="text/javascript">
    window.currentAddress = JSON.stringify({
        province: '{{ Auth::user()->province }}',
        city: '{{ Auth::user()->city }}',
        district: '{{ Auth::user()->sub_district }}'
    });
</script>
@endauth
@push('scripts')
<script
  src="{{ Config::get('payment.veritrans.url') }}"
  data-client-key="{{ Config::get('payment.veritrans.client_key') }}" async></script>
@endpush