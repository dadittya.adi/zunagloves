@extends('layouts.web')

{!! NoCaptcha::renderJs() !!}
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-6 py-4">
                <h3 class="mb-4">Live Chat | <small>Weekday, 08.00-17.00 WIB</small> </h3>
                <div class="row">
                    <div class="col d-flex align-items-center">
                        <a href="https://api.whatsapp.com/send?phone=+628112781818" target="_blank">
                        <i class="fab fa-3x mr-2 fa-whatsapp text-success "></i>
                        </a>
                        <span>0811-2781-818</span>
                        
                    </div>
                    <div class="col d-flex align-items-center">
                        <i class="fab fa-3x mr-2 fa-line text-success"></i>
                        <span>@ZunaGloves</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 py-4">
                <h3 class="mb-4">Social Media</h3>
                <div class="row">
                    <div class="col d-flex align-items-center">
                        <a href="https://www.instagram.com/zunagloves/" target="_blank">
                        <i class="fab fa-3x mr-2 fa-instagram text-danger"></i>
                        </a>
                        <span>@ZunaGloves</span>
                    </div>
                    <div class="col d-flex align-items-center">
                        <a href="https://www.facebook.com/ZunaGloves/" target="_blank">
                        <i class="fab fa-3x mr-2 fa-facebook text-primary"></i>
                        </a>
                        <span>Zuna Gloves</span>
                        
                    </div>
                </div>
            </div>
            <div class="col-lg-12 py-4">
                @if (Session::has('_SENT_SUCCESS'))
                <div class="alert alert-success">
                    Successfully sent message!
                </div>
                @endif
               
                <h3 class="mb-4">Write us and we'll get back to you</h3>
                <form action="{{ route('web.contact_us') }}" method="POST">
                    @csrf
                    <div class="form-group form-row required">
                        <div class="col-12">
                            <label for="">Name</label>
                        </div>
                        <div class="col">
                            <input type="text" name="first_name" id="" class="form-control form-control-lg" placeholder="Type your first name">
                            @error('first_name')
                            <div class="error">{{ $message }}</div>
                          @enderror
                        </div>
                        <div class="col">
                            <input type="text" name="last_name" id="" class="form-control form-control-lg" placeholder="Type your last name">
                            @error('last_name')
                            <div class="error">{{ $message }}</div>
                          @enderror
                        </div>
                    </div>
                    <div class="form-group required">
                      <label for="">Email</label>
                      <input type="text" name="email" id="" class="form-control form-control-lg" placeholder="Type your Email Address">
                      @error('email')
                        <div class="error">{{ $message }}</div>
                      @enderror
                    </div>
                    <div class="form-group required">
                      <label for="">Phone Number</label>
                      <input type="text" name="phone" id="" class="form-control form-control-lg" placeholder="Type your Phone Number">
                      {{-- <small id="helpId" class="text-muted"></small> --}}
                      @error('phone')
                        <div class="error">{{ $message }}</div>
                      @enderror
                    </div>
                    <div class="form-group required">
                      <label for="">Comment or Message</label>
                      <textarea class="form-control form-control-lg" name="message" id="" rows="3" placeholder="Type Comment or Message here"></textarea>
                      @error('message')
                      <div class="error">{{ $message }}</div>
                    @enderror
                    </div>
                    <div class="form-group required">
                        {!! NoCaptcha::display() !!}
                        @error('g-recaptcha-response')
                            <div class="error">Captcha Failed</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-lg btn-warning">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection