@extends('layouts.web')

@section('content')
    <div class="container">
        <div class="py-4">
            <h3 class="mb-4">Online Store</h3>
            <div class="row row-cols-2 row-cols-lg-5 d-flex align-items-center">
                @foreach($online_store as $key => $store)
                    <div class="col py-3 py-md-0">
                        <a href="{{ $store['link'] }}" target="_blank">
                            <img src="{{ asset($store['img']) }}" class="img-fluid" alt="">
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="py-4">
            <h3 class="mb-4">Offline Store</h3>
            <div class="accordion" id="offlineStore">
                @foreach($CityStore as $key => $city) 
                <div class="card border-top-0 border-left-0 border-right-0 border-bottom">
                    <div class="card-header bg-transparent" id="heading{{ $city->id }}">
                        <h2 class="mb-0">
                            <button class="btn btn-block d-flex align-items-center p-0" type="button" data-toggle="collapse" data-target="#collapse{{$city->id }}" aria-expanded="false" aria-controls="collapse{{ $city->id }}">
                                <small><i class="fas fa-circle mr-3"></i></small>
                                {{$city->name }}
                                <i class="fas fa-plus ml-auto"></i>
                                <i class="fas fa-minus ml-auto"></i>
                            </button>
                        </h2>
                    </div>
                
                    <div id="collapse{{ $city->id }}" class="collapse" aria-labelledby="heading{{$city->name }}" data-parent="#offlineStore">
                      <div class="card-body">
                        <ul class="mb-0">
                            @foreach($city->stores as $store)
                                @if($store->is_active)
                                    <li><a href="{{ $store->url}}" style="color: black" target="_blank"> {{ $store->name }} </a></li>
                                @endif
                            @endforeach
                        </ul>
                      </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection