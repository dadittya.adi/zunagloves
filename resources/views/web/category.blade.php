@extends('layouts.web')

@section('content')
<div class="container-fluid">
    <div class="nav-filter py-3">
        <div class="row row-cols-2 row-cols-lg-4 d-flex align-items-center">
            <div class="col">Showing {{ $products->firstItem() . ' - ' . $products->lastItem() }} of {{ $products->total() }} result</div>
            <div class="col d-flex align-items-center">
                <span class="mr-2">Show:</span>
                <div>
                    @php
                        $separator = count($query_string) > 0 ? '&' : '';
                        $filtered_query_string = $query_string;
                        unset($filtered_query_string['show']);
                    @endphp
                    <a href="{{ route('web.category', 'all?'. http_build_query($filtered_query_string) . $separator . 'show=12') }}" class="px-1">12</a>
                    <a href="{{ route('web.category', 'all?'. http_build_query($filtered_query_string) . $separator . 'show=24') }}" class="px-1">24</a>
                    <a href="{{ route('web.category', 'all?'. http_build_query($filtered_query_string) . $separator . 'show='.$products->total()) }}" class="px-1">All</a>
                </div>
            </div>
            <div class="col d-flex align-items-center">
                <span class="mr-2">Sort by:</span>
                <div class="dropdown">
                    <button class="btn btn-warning dropdown-toggle d-flex align-items-center" data-toggle="dropdown">{{  \Request::has('sortby') ? $sort_by[\Request::get('sortby')]['title'] : 'Default' }}</button>
                    <div class="dropdown-menu">
                        @foreach($sort_by as $key => $by)
                        <form action="{{ route('web.category', 'all') }}" id="{{ $key }}" method="get">
                            <input type="hidden" name="sortby" value="{{ $key }}">
                        </form>
                        <button type="submit" form="{{ $key }}" class="dropdown-item">{{ $by['title'] }}</button>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="col d-flex align-items-center">
                <a href="javascript:void(0)" data-toggle="modal" data-target="#filter" @click="showFilterToggle">+ Filter</a>
            </div>
        </div>
    </div>
    @if($products->count() > 0)
    <div class="row row-cols-1 row-cols-2 row-cols-lg-4 px-md-3 products position-relative">
        @foreach($products as $key => $product)
        @php 
            $category_slug = Illuminate\Support\Str::slug($product->category->name);
        @endphp
        <div class="col product-item">
            @include('web.components.product_label', $product)
            <div class="product-image">
                @if($product->images->count() > 0)
                <img class="lazyload" src="{{ action('LandingController@showProductImage', [$product->images->first()->id]) }}" alt="">
                @else
                <img class="lazyload" src="" alt="">
                @endif
                
                <a href="{{ route('web.product', [ $category_slug, $product->slug ]) }}">
                    <i class="fas fa-plus-square"></i>
                    <p class="mb-0">View Detail</p>
                </a>
            </div>
            <div class="product-info">
                <p class="product-title mb-0">{{ $product->name }}</p>
                @if($product->discount > 0)
                <p class="mb-0 text-muted text-right"><del>{{ i18l_currency($product->price_id) }}</del></p>
                @php 
                    if($product->disc_percent <= 100) {
                        $price_after_disc = $product->price_id - ($product->price_id * $product->disc_percent / 100);
                        $discView = $product->disc_percent . '%';
                    }
                    else {
                        $price_after_disc = $product->price_id -$product->disc_percent;
                        $discView = $product->disc_percent / 1000;
                        $discView = $discView . "rb";
                        //$discView = number_format($product->disc_percent, 0, ',', '.');
                    }
                @endphp 
                <p class="price-tag text-right text-warning font-weight-bold d-flex justify-content-end">{{ i18l_currency($price_after_disc) }} <small><span class="badge badge-pill badge-danger ml-2">-{{ $discView }}</span></small></p>
                @else
                <p class="price-tag text-right text-warning font-weight-bold">{{ i18l_currency($product->price_id, 'id') }}</p>
                @endif
                <div class="action-button">
                    <button class="btn btn-block btn-warning" data-toggle="modal" data-target="#quick-view-{{ $product->slug }}" @click="reset('{{ 'qty-' . $product->slug }}')">Quick View</button>
                </div>
            </div>
        </div>
        @endforeach
    </div>
    @else 
        <div class="d-flex align-items-center justify-content-center pt-5 mt-5">
            <h5 class="text-muted mt-5">Pencarian Tidak Ditemukan</h5>
        </div>
    @endif

    {{ $products->appends($query_string)->links() }}
</div>
@endsection

@push('modal')
    @foreach($products as $key => $product)
    @php 
        $product_slug = $product->slug;
        $category_slug = Illuminate\Support\Str::slug($product->category->name);
    @endphp
    @include('web.components.quick_view', compact('product', 'product_slug'))
    @endforeach

    @include('web.components.filter')
@endpush