@extends('layouts.web')

@section('content')
    @include('web.components.nav_pill_about')
    <div class="p-4">
        <h5 class="font-weight-bold">SHIPMENT INFORMATION</h5>
        <p></p>
        <ul>
            <li>Semua pesanan yang kami terima melalui website <a href="http://www.zunagloves.com">zunagloves.com</a> akan dikirim melalui ekspedisi JNE Express atau J&amp;T Express</li>
            <li>Pesanan dan pengiriman akan di proses saat jam operasional yaitu Senin-Jumat pukul 00-16.00WIB</li>
            <li>Sabtu-Minggu dan Tanggal Merah tidak ada pengiriman</li>
            <li>Pesanan akan dikirimkan dalam 1-2 hari setelah pembayaran di konfirmasi.</li>
            </ul>
            
            <ul>
            <li>All orders we receive through the website www.zunagloves.com will be sent via JNE Express or J&amp;T Express</li>
            <li>Orders and shipments will be processed during operational hours, Monday-Friday 08.00-16.00 WIB</li>
            <li>We do not process the orders and shipments on Saturday-Sunday and national holidays</li>
            <li>Orders will be shipped within 1-2 days after we receive the payment confirmation</li>
            </ul>
    </div>
@endsection