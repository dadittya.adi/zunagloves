@extends('layouts.web')

@section('content')
<div class="grid">
    @foreach ($gallery as $key => $photo)
    <div class="grid-item">
        <a href="javascript:void(0)" class="gallery-item" data-toggle="modal" data-target="#home-gallery-{{ $key }}">
            <img src="{{ route('web.showcase.image', $photo->id) }}" class="img-fluid" alt="">
            <div class="gallery-hover flex-column align-items-center justify-content-center">
                <i class="fas fa-2x fa-plus-square"></i>
                <p class="font-weight-bold">{{ $photo['title'] }}</p>
            </div>
        </a>
        
        <!-- Modal -->
        <div class="modal fade" id="home-gallery-{{ $key }}" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg black-modal" role="document">
                <div class="modal-content">
                    <div class="modal-body" {{ $photo->color != "" ? "style=background-color:".$photo->color.";" : ""}}>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-lg-6 py-3">
                                    <img src="{{ $photo->image_detail != '' ? route('web.showcase.detail.image', $photo->id) : route('web.showcase.image', $photo->id) }}" class="modal-photo" alt="">
                                </div>
                                <div class="col-lg-6 py-3">
                                    <h3 class="text-warning">{{ $photo->title }}</h3>
                                    <p class="photo-desc"  {{ $photo->font_color != "" ? "style=color:".$photo->font_color.";" : ""}}>{{ $photo->description }}</p>
                                    <a href="{{ $photo->link = null ? '#' : $photo->link}}" class="btn btn-block btn-warning">SHOP NOW!</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endforeach
</div>
@endsection

@push('modal')
<!-- Modal -->
<div class="modal fade" id="emailSubscribe" tabindex="-1" aria-labelledby="emailSubscribeLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body py-0">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="z-index: 1024">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="row">
                    <div class="col-lg-4 px-0">
                        <img src="{{ route('web.popup.image', $popup->id) }}}" class="img-popup-supersale" alt="">
                    </div>
                    <div class="col-lg-8">
                        <div class="px-lg-4 py-4 py-lg-5 mt-lg-5">
                            <p class="text-center">ENTER EMAIL BELOW TO UNLOCK</p>
                            <form action="{{ route('discount.request')}}" method="POST" class="w-full">
                                @csrf
                                <div class="form-group">
                                    <input name="email" type="text" class="form-control form-control-lg" placeholder="Enter email address">
                                </div>
                                <div class="form-group form-check">
                                    <input name="subscribe" type="checkbox" class="form-check-input" id="exampleCheck1" value="subscribe">
                                    <label class="form-check-label" for="exampleCheck1">Yes, i’d like to receive news and special offers.</label>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-block btn-warning rounded-pill" @click="setSubscribe(true)">Submit</button>
                                    <button type="button" class="btn btn-block btn-default rounded-pill" data-dismiss="modal" @click="setSubscribe(false)">No Thanks</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endpush

@push('scripts')
<script type="text/javascript">
    let subscribe = sessionStorage.getItem('subscribe')

    if (!subscribe) {
        $('#emailSubscribe').modal('show')
        sessionStorage.setItem('subscribe', false)
    }
</script>
@endpush