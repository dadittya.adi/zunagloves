@extends('layouts.web')

@section('content')
    @include('web.components.nav_pill_about')
    <div class="p-4">
        <p><strong>ZUNA Gloves</strong>&nbsp;merupakan brand sarung tangan yang dimulai&nbsp;pada tahun 2013 oleh Tania Hermawan.&nbsp;Fokus ZUNA Gloves adalah untuk merancang berbagai&nbsp;produk sarung tangan yang inovatif dan energetik untuk segala kegiatan&nbsp;konsumen.&nbsp;</p>
<p>Walaupun baru startup 8 tahun, ZUNA Gloves didukung oleh tim profesional yang telah berkiprah di bidang sarung tangan selama lebih dari 30 tahun, oleh karena itu produk sarung tangan ZUNA diproduksi dengan pilihan bahan yang berkualitas, teknologi yang terbaru dan fitting yang ter-ergonomis.</p>
<p><em>Revamp</em> di tahun 2020&nbsp;dengan tagline&nbsp;<em>&ldquo;Gloves for Everyone&rdquo;</em>, ZUNA Gloves&nbsp;lahir kembali dengan berbagai&nbsp;varian sarung tangan mulai dari kategori<em>&nbsp;cycling, golf, fitness,&nbsp;</em><em>horse-</em><em>riding, fishing, motorbike,</em><em>&nbsp;outdoor&nbsp;</em>dan<em>&nbsp;traveling&nbsp;</em>dengan berbagai ukuran mulai dari XS, S, M, L dan XL dan terdiri dari&nbsp;<em>ladies, men, unisex</em>&nbsp;dan&nbsp;<em>kids.</em>&nbsp;Dengan harapan&nbsp;sarung tangan ZUNA dapat digunakan oleh semua orang tidak hanya di bidang olahraga namun juga untuk&nbsp;<em>everyday lifestyle</em>.</p>
<p>ZUNA Gloves sangat fokus&nbsp;pada fungsi untuk setiap kategori sarung tangan. Kami bekerja sama dengan para professional di bidang kegiatannya, memastikan bahwa fungsi bahan, fitur tekologi dan fitting sarung tangan adalah yang paling tepat untuk kegiatan tersebut.</p>
<p>Menginjak usianya yang ke 8 ditahun 2021 ini, ZUNA Gloves semakin berkomitmen terhadap kepuasan pelanggan dengan terus berinovasi untuk menciptakan sarung tangan yang berkualitas dan sebagai produk lokal yang mampu bersaing dengan produk import lainnya.&nbsp;Kami ingin menjadi kebanggaan warga Indonesia terhadap produk lokal sendiri!&nbsp;</p>
<p>Visi kami adalah untuk menjadi satu-satunya brand terlengkap untuk pilihan sarung tangan bagi segala kegiatan konsumen. Dengan&nbsp;selalu mengutamakan kualitas dan inovasi, merancang produk sarung tangan terbaik di berbagai bidang aktifitas konsumen.</p>
<p>Happy shopping Zunaers...</p>
    </div>
@endsection