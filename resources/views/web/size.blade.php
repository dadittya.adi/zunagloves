@extends('layouts.web')

@section('content')
    @include('web.components.nav_pill_about')
    <div class="p-4">
        <p>
            Find your perfect size with our size chart below. <br>
            Need more fit advice? <br>
            Contact our Online Customer Service between 8AM - 11PM, 7 days a week.
        </p>
        <div class="row">
            <div class="col-4">
                <img src="{{ asset('images/mensize.jpg') }}" class="img-fluid" alt="">
            </div>
            <div class="col-4">
                <img src="{{ asset('images/womensize.jpg') }}" class="img-fluid" alt="">
            </div>
        </div>
    </div>
@endsection