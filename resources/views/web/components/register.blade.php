<!-- Register Form -->
<div class="modal fade" id="register-form" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content p-5">
            <div class="modal-header p-0 pb-4">
                <div class="d-flex w-100 align-items-end">
                    <div class="px-0 col-md-6 mr-auto">
                        <h2 class="modal-title font-weight-bold text-warning">Register</h2>
                    </div>
                    <div class="px-0 text-right">
                        <p class="mb-0"><small>Have an account ?</small></p>
                        <a href="#" class="text-warning" data-toggle="modal" data-target="#login-form" data-dismiss="modal">Login Here</a>
                    </div>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="fas fa-times"></i>
                </button>
            </div>
            <div class="modal-body p-0 pt-4">
                <form action="{{ route('register') }}" method="POST" id="register_form">
                    @csrf
                    <input type="hidden" name="register_form" value="register_form">
                    <input type="hidden" name="cart" :value="JSON.stringify(cart)" v-if="cart.length > 0">
                    <div class="form-group">
                        <input type="text" name="name" class="form-control form-control-lg" value="{{ old('name') }}" placeholder="Full Name">
                        @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input type="tel" name="phone" class="form-control form-control-lg @error('phone') is-invalid @enderror" value="{{ old('phone') }}" placeholder="Phone Number">
                        @error('phone')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input type="email" name="email" class="form-control form-control-lg @error('email') is-invalid @enderror" value="{{ old('email') }}" placeholder="Email Address">
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input type="password" name="password" class="form-control form-control-lg @error('password') is-invalid @enderror" placeholder="Password">
                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input type="password" name="password_confirmation" class="form-control form-control-lg" placeholder="Password Confirmation">
                    </div>
                    <div class="form-group">
                        <p>By submitting the form you agree with our <a href="#" class="text-warning">Terms Of Service</a></p>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-lg btn-block btn-warning font-weight-bold">REGISTER</button>
                    </div>
                </form>
                {{-- <p class="text-center py-4 mb-0">or you can also sign in with:</p>
                <div class="row">
                    <div class="col border-right">
                        <a href="#" class="d-flex align-items-center text-body justify-content-center">
                            <i class="fab fa-facebook text-muted fa-2x mr-2"></i>
                            <span>Facebook</span>
                        </a>
                    </div>
                    <div class="col border-left">
                        <a href="#" class="d-flex align-items-center text-body justify-content-center">
                            <i class="fab fa-google text-muted fa-2x mr-2"></i>
                            <span>Google</span>
                        </a>
                    </div>
                </div> --}}
            </div>
        </div>
    </div>
</div>