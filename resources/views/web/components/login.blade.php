<!-- Login Form -->
<div class="modal fade" id="login-form" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content p-5">
            <div class="modal-header p-0 pb-4">
                <div class="d-flex w-100 align-items-end">
                    <div class="px-0 col-md-6 mr-auto">
                        <h2 class="modal-title font-weight-bold text-warning">Login</h2>
                    </div>
                    <div class="px-0 text-right">
                        <p class="mb-0"><small>Don't have any account yet.</small></p>
                        <a href="#" class="text-warning" data-toggle="modal" data-target="#register-form" data-dismiss="modal">Create One</a>
                    </div>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="fas fa-times"></i>
                </button>
            </div>
            <div class="modal-body p-0 pt-4">
                <form action="{{ route('login') }}" method="POST">
                    @csrf
                    <input type="hidden" name="login_form" value="login_form">
                    <input type="hidden" name="cart" :value="JSON.stringify(cart)" v-if="cart.length > 0">
                    <div class="form-group">
                        <input type="text" name="email" class="form-control form-control-lg @error('email') is-invalid @enderror" placeholder="Email">
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input type="password" name="password" class="form-control form-control-lg @error('password') is-invalid @enderror" placeholder="Password">
                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group form-row">
                        <div class="col">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" id="gridCheck">
                                <label class="form-check-label" for="gridCheck">
                                    Remember me
                                </label>
                            </div>
                        </div>
                        <div class="col text-right">
                            <a href="{{ route('password.request') }}" class="text-warning">Forgot password?</a>
                        </div>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-lg btn-block btn-warning font-weight-bold">LOG IN</button>
                    </div>
                </form>
                <p class="text-center py-4 mb-0">or you can also sign in with:</p>
                <div class="row">
                    <div class="col border-right">
                        <a href="{{ route('login.social', 'facebook') }}" class="d-flex align-items-center text-body justify-content-center">
                            <i class="fab fa-facebook text-muted fa-2x mr-2"></i>
                            <span>Facebook</span>
                        </a>
                    </div>
                    <div class="col border-left">
                        <a href="{{ route('login.social', 'google') }}" class="d-flex align-items-center text-body justify-content-center">
                            <i class="fab fa-google text-muted fa-2x mr-2"></i>
                            <span>Google</span>
                        </a>
                    </div>
                </div>
                <div v-if="showGuestCheckoutBtn && cart.length > 0">
                    <hr>
                    <p>Or continue your purchase without creating an account</p>
                    <form action="{{ route('web.cart.store') }}" class="w-100" id="form_cart" @submit.prevent="checkout" method="post">
                        @csrf
                        <button class="btn btn-block btn-lg btn-outline-warning">Guest Checkout</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>