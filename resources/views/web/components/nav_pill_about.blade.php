<ul class="nav nav-fill">
    <li class="nav-item">
        <a class="nav-link {{ Route::is('web.size') ? 'active' : '' }}" href="{{ route('web.size') }}">Size Chart</a>
    </li>
    <li class="nav-item">
        <a class="nav-link {{ Route::is('web.about') ? 'active' : '' }}" href="{{ route('web.about') }}">About Us</a>
    </li>
    <li class="nav-item">
        <a class="nav-link {{ Route::is('web.shipping') ? 'active' : '' }}" href="{{ route('web.shipping') }}">Shipping & Handling</a>
    </li>
    <li class="nav-item">
        <a class="nav-link {{ Route::is('web.policy') ? 'active' : '' }}" href="{{ route('web.policy') }}">Return and Claim Policy</a>
    </li>
    <li class="nav-item">
        <a class="nav-link {{ Route::is('web.tos') ? 'active' : '' }}" href="{{ route('web.tos') }}">Terms of Use</a>
    </li>
</ul>