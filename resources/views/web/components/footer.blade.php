<form action="{{ route('subscribe.store') }}" class="d-flex justify-content-center w-100 form-inline mb-4" method="post">
    <div class="input-group w-100">
        @csrf
        <input type="text" name="sub_email" class="form-control" style="height: 30px !important">
        <div class="input-group-append">
            <button class="btn btn-outline-secondary" style="height: 30px !important; font-size: 0.7rem !important">Subscribe</button>
        </div>
    </div>
</form>
<div class="icon-menu d-flex justify-content-center mb-4 mb-lg-2">
    <a href="https://www.facebook.com/ZunaGloves/" class="flex-fill px-lg-3 text-center text-muted"  target="_blank"><i class="fab fa-facebook"></i></a>
    <a href="https://www.youtube.com/channel/UCOfuv6pX07sh1y2PuwqKkEw" class="flex-fill px-lg-3 text-center text-muted"  target="_blank"><i class="fab fa-youtube"></i></a>
    <a href="https://www.instagram.com/zunagloves/" class="flex-fill px-lg-3 text-center text-muted"  target="_blank"><i class="fab fa-instagram"></i></a>
    <a href="http://blog.zunagloves.com/" class="flex-fill px-lg-3 text-center text-muted"  target="_blank">Blog</a>
</div>
<div class="footer-link w-100 text-right mb-md-2 mb-4">
    <a href="{{ route('web.about') }}" class="d-lg-block py-1 text-muted">About Us</a>
    <a href="{{ route('web.policy') }}" class="d-lg-block py-1 text-muted">Return & Claim Policy</a>
</div>
<div class="text-right mt-auto copyright">
    <small class="text-muted"  style="padding-left: 0px !important; font-size: 8.3px !important">©2021 ZUNA GLOVES, All Rights Reserved</small>
</div>