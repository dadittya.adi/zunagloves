<!-- Quick View -->
<div class="modal quickview" id="quick-view-{{ $product->slug }}" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content p-4">
            <div class="modal-header p-0 pb-4">
                <div class="w-100">
                    <div class="row">
                        <div class="col-9 mr-auto">
                            <h3 class="modal-title font-weight-bold">{{ $product->name }}</h3>
                        </div>
                        <div class="col-2 text-right">
                            <button type="button" class="close py-2" data-dismiss="modal" aria-label="Close">
                                <i class="fas fa-times"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-body p-0 py-4">
                <div class="row">
                    <div class="col-lg-6 col-12">
                        @include('web.components.product_label', $product)
                        <div id="carousel_{{ $product->slug }}" class="carousel carousel-product-images slide" data-ride="carousel" data-interval="false">
                            <div class="carousel-inner" role="listbox">
                                @foreach($product->images as $key => $image)
                                <div class="carousel-item @if ($key == 0) active @endif">
                                    <img src="{{ action('LandingController@showProductImage', [$image->id]) }}" class="img-fluid w-100 lazyload">
                                </div>
                                @endforeach

                                @foreach($product->colors as $key => $color)
                                <div class="carousel-item">
                                    <img src="{{ action('LandingController@showProductColorImage', [$color->colorImages->first()->id]) }}" class="img-fluid w-100 lazyload">
                                </div>
                                @endforeach
                            </div>
                            <ol class="carousel-indicators">
                                @foreach($product->images as $key => $image)
                                <li data-target="#carousel_{{ $product->slug}}" data-slide-to="{{ $key }}" @if ($key == 0) class="active" @endif>
                                    <img src="{{ action('LandingController@showProductImage', [$image->id]) }}" class="img-fluid w-100 lazyload">
                                </li>
                                @endforeach

                                @foreach($product->colors as $key => $color)
                                <li data-target="#carousel_{{ $product->slug }}" data-slide-to="{{ $key + $product->images->count() }}">
                                    <img src="{{ action('LandingController@showProductColorImage', [$color->colorImages->first()->id]) }}" class="img-fluid w-100 lazyload">
                                </li>
                                @endforeach
                            </ol>
                        </div>
                    </div>
                    <div class="col-lg-6 col-12">
                        @php
                            $price_after_disc = $product->price_id 
                        @endphp
                        @if($product->discount > 0)
                        <p class="mb-0 text-muted"><del>{{ i18l_currency($product->price_id) }}</del></p>

                        @php 
                            if($product->discount <= 100) {
                                $price_after_disc = $product->price_id - ($product->price_id * $product->disc_percent / 100);
                                $discView = $product->disc_percent . '%';
                            }
                            else {
                                $price_after_disc = $product->price_id -$product->disc_percent;
                                //$discView = number_format($product->disc_percent, 0, ',', '.');
                                $discView = $product->disc_percent / 1000;
                                $discView = $discView . "rb";
                            }
                        @endphp 
                        <h2 class="price-tag text-warning font-weight-bold d-flex items-center">{{ i18l_currency($price_after_disc) }} <small><span class="badge badge-pill badge-danger ml-2">-{{ $discView }}</span></small></h2>
                        @else
                        <h2 class="price-tag text-warning font-weight-bold">{{ i18l_currency($product->price_id, 'id') }}</h2>
                        @endif
                        <hr>
                        <form action="#" id="modal_{{ $product->slug }}" @submit.prevent="addToCart">
                            <input type="hidden" name="id" value="{{ $product->id }}">
                            <input type="hidden" name="name" value="{{ $product->name }}">
                            <input type="hidden" name="price_tag" value="{{ i18l_currency($product->price_id, 'id') }}">
                            <input type="hidden" name="price" value="{{ $price_after_disc }}">
                            @if($product->images->count() > 0)
                            <input type="hidden" name="image" value="{{ action('LandingController@showProductImage', [$product->images->first()->id]) }}">
                            @else
                            <input type="hidden" name="image" value="">
                            @endif
                            <input type="hidden" name="slug" value="{{ $product->slug }}">
                            <input type="hidden" name="weight" value="{{ $product->weight }}">
                            <div class="form-group form-variant row">
                                <label for="" class="col-sm-2 control-label font-weight-bold">Color</label>
                                <div class="col-sm-10">
                                    <div class="mb-3">Choose a variant</div>
                                    <div class="d-flex items-center mx-n1">
                                        @foreach ($product->colors as $key => $color)
                                        @php 
                                            $id_variant = $product->slug . '_' . Illuminate\Support\Str::slug($color->name) . '_' . rand(1,9);
                                            $img = action('LandingController@showProductColorImage', $color->colorImages->first()->id);
                                        @endphp
                                        <div class="px-1">
                                            <input type="radio" name="color" id="color_{{ $id_variant }}" value="{{ $color->name }}">
                                            <label for="color_{{ $id_variant }}" class="color" style="background-image: url('{{ action('LandingController@showColorImage', $color->id) }}')" title="{{ $color->name }}" @click="showProductColorImage('{{ $color->id }}', 'carousel_{{ $product->slug }}',{{ $key + $product->images->count() }})"></label>
                                        </div>
                                        @endforeach
                                    </div>
                                    <small class="form-text font-italic text-danger d-none" id="variantColor-{{ $product->slug }}">@{{ variant.color.message }}</small>
                                </div>
                            </div>
                            @if ($product->sizes->count() > 0)
                            <div class="form-group form-variant row">
                                <label for="size" class="col-sm-2 control-label font-weight-bold">Size</label>
                                <div class="col-sm-10">
                                    <div class="mb-3">Choose a variant</div>
                                    <div class="d-flex items-center mx-n1">
                                        @foreach($product->sizes as $key => $size)
                                        @php 
                                            $size_slug = $product->slug . '_' . $size->short_name;
                                            $productSizeId = $product->productSizes[$key]->id;
                                        @endphp
                                        <div class="px-1">
                                            <input type="radio" name="size" id="size_{{ $size_slug }}" value="{{ $size->short_name }}">
                                            <label for="size_{{ $size_slug }}" class="size" @click="selectProductSize({{ $product->id }}, {{ $productSizeId }})">{{ $size->short_name }}</label>
                                        </div>
                                        @endforeach
                                    </div>
                                    <small class="form-text font-italic text-danger d-none" id="variantSize-{{ $product->slug }}">@{{ variant.size.message }}</small>
                                </div>
                            </div>
                            @endif
                            <div class="form-group row mb-0">
                                <label for="" class="col-sm-2 control-label font-weight-bold">Quantity</label>
                                <div class="col-sm-10">
                                    <div v-if="selectedProduct.productId && selectedProduct.productColorId && selectedProduct.productSizeId" :class="{'mb-3': true, 'text-success': selectedProduct.stock > 0 || !selectedProduct.stock, 'text-danger': selectedProduct.stock == 0}">@{{ selectedProduct.stock == null || selectedProduct.stock > 0 ? 'Ready Stock' : selectedProduct.stock == -2 ? 'Coming Soon' : 'Out Of Stock' }}</div>
                                    <div v-else class="mb-3 text-danger">Please Select Varian</div>
                                    <div class="row">
                                        <div class="input-group col-7">
                                            <div class="input-group-prepend">
                                                <button type="button" :class="{ 'btn px-0': true, 'text-warning': !isMinQuantityDisabled }" :disabled="isMinQuantityDisabled" @click="minQuantity('{{ 'qty-' . $product->slug }}')">
                                                    <i class="fas fa-2x fa-minus-circle"></i>
                                                </button>
                                            </div>
                                            <input type="text" name="qty" class="form-control qty text-center mx-3 border-top-0 border-right-0 border-left-0" min="1" max="12" v-mask="['#','##']" ref="qty-{{ $product->slug }}">
                                            <div class="input-group-append">
                                                <button type="button" :class="{ 'btn px-0': true, 'text-warning': !isAddQuantityDisabled }" :disabled="isAddQuantityDisabled" @click="addQuantity('{{ 'qty-' . $product->slug }}')">
                                                    <i class="fas fa-2x fa-plus-circle"></i>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <small class="form-text font-italic text-danger d-none" id="variantQty-{{ $product->slug }}">@{{ variant.qty.message }}</small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer pt-4 pb-0">
                <a href="{{ route('web.product', [ $category_slug, $product->slug ]) }}" class="btn btn-lg btn-outline-secondary">View Detail</a>
                <button type="submit" form="modal_{{ $product->slug }}" class="btn btn-lg btn-warning"><i class="fas fa-shopping-bag mr-2"></i> Add to Cart</button>
            </div>
        </div>
    </div>
</div>