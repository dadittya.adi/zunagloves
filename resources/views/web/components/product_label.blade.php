@if($product->discount > 0)
<div class="product-label">
    <div class="label-status">
        <p class="mb-0">SALE</p>
    </div>
</div>
@endif

@php 
    // Preorder
    $current_date = date('Y-m-d h:i:s');
    $current_date = date('Y-m-d h:i:s', strtotime($current_date));
    $launch_date = date('Y-m-d h:i:s', strtotime($product->launch_date));
@endphp

@if($product->status == 3)
<div class="product-label">
    <div class="px-2 w-100">
        <p class="mb-0">Getting Ready</p>
        <timer-pre-order :launch-date="'{{ $product->countdown }}'" v-cloak></timer-pre-order>
    </div>
    <div class="label-status pre-order">
        <p class="mb-0">PRE</p>
        <span>ORDER</span>
    </div>
</div>
@endif

@if($product->status == 2)
<div class="product-label">
    <div class="label-status coming-soon">
        <p class="mb-0">COMING</p>
        <span>SOON</span>
    </div>
</div>
@endif

@if($product->status == 4)
<div class="product-label">
    <div class="label-status new">
        <p class="mb-0">NEW</p>
    </div>
</div>
@endif

@if(!$product->in_stock && $product->is_active == 1)
<div class="product-label">
    <div class="label-status out-stock">
        <p class="mb-0">OUT OF</p>
        <span>STOCK</span>
    </div>
</div>
@endif