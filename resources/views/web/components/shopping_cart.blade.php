<!-- Shopping Cart -->
<div class="modal" id="shopping-cart" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header d-flex align-items-center">
                <h5 class="modal-title font-weight-bold">Shopping Cart (@{{ cart.length }})</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="fas fa-times-circle"></i>
                </button>
            </div>
            <div class="modal-body">
                <div class="table-zn" v-if="cart.length > 0">
                    <div class="table-zn__body">
                        <div class="table-zn__row" v-for="(item, key) in cart" :key="key">
                            <div class="product-info px-0">
                                <img :src="item.image" class="product-info__img" alt="">
                                <div class="detail-info w-100">
                                    <div class="d-flex align-items-top">
                                        <div class="w-100 pb-1 pr-1 mb-1">
                                            <p class="font-weight-bold mb-1">@{{ item.name }}</p>
                                            <p class="mb-1">
                                                <span v-if="item.size">Size: @{{ item.size }} | </span>
                                                <span v-if="item.color">Color: @{{ item.color }}</span>
                                            </p>
                                        </div>
                                        <span>
                                            <button class="btn btn-sm text-muted" title="Remove" @click="deleteFromCart(key)"><i class="fas fa-trash"></i></button>
                                        </span>
                                    </div>
                                    <span class="d-block font-weight-bold">@{{ item.qty + ' x ' + formatCurrency(item.price) }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="table-zn__footer border-top">
                        <div class="table-zn__row bg-light">
                            <div class="col">
                                <div class="row">
                                    <div class="col-6">
                                        <p class="font-weight-bold mb-0 text-muted">Subtotal</p>
                                    </div>
                                    <div class="col-6">
                                        <p class="font-weight-bold mb-0 text-right" v-cloak>@{{ formatCurrency(subtotal) }}</p>
                                    </div>
                                </div>
                                <div class="row" v-if="voucher && voucher.status == 'available'">
                                    <div class="col-6">
                                        <p class="font-weight-bold mb-0 text-muted">Discount</p>
                                    </div>
                                    <div class="col-6">
                                        <p class="font-weight-bold mb-0 text-success text-right" v-cloak>- @{{ formatCurrency(discountAmount) }}</p>
                                    </div>
                                </div>
                                <div class="row" v-if="shippingCost > 0">
                                    <div class="col-6">
                                        <p class="font-weight-bold mb-0 text-muted">Shipping Cost</p>
                                    </div>
                                    <div class="col-6">
                                        <p class="font-weight-bold mb-0 text-right" v-cloak>@{{ formatCurrency(shippingCost) }}</p>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-6">
                                        <p class="font-weight-bold mb-0 h5 text-muted">Total</p>
                                    </div>
                                    <div class="col-6">
                                        <p class="font-weight-bold mb-0 text-warning text-right" v-cloak>@{{ formatCurrency(total) }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div v-else>
                    <img src="{{ asset('images/empty-cart.png') }}" class="img-fluid" alt="">
                    <p class="text-muted text-center h2 font-weight-bold">Your cart is empty</p>
                </div>
            </div>
            <div class="modal-footer" v-if="cart.length > 0">
                <button type="button" class="btn btn-lg btn-block btn-outline-secondary" data-dismiss="modal" aria-label="Close">Other Shopping</button>
                <a href="{{ route('web.cart') }}" class="btn btn-lg btn-block btn-outline-secondary">View Cart</a>
                @auth
                <form action="{{ route('web.cart.store') }}" class="w-100" id="form_cart_sidebar" @submit.prevent="checkout" method="post">
                    @csrf
                    <button class="btn btn-block btn-lg btn-warning">Process to Checkout</button>
                </form>
                @endauth

                @guest
                <button class="btn btn-block btn-lg btn-warning" data-toggle="modal" data-target="#login-form" @click="guestCheckoutToggle">Process to Checkout</button>
                @endguest
            </div>
        </div>
    </div>
</div>