    <!-- Filter -->
<div class="modal" id="filter" tabindex="-1" role="dialog" data-backdrop="false" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <form action="{{ route('web.category','all') }}" id="form-filter" method="GET">
                    <h4 class="font-weight-bold">Categories</h4>
                    <div class="form-group" id="form-group-categories">
                    @php
                        $categories = \App\Models\Category::get()
                    @endphp

                    @foreach ($categories as $key => $item)
                        <label class="container{{ $key >= 4 ? ' d-none' : '' }}">
                            <span>{{ $item->name }}</span>
                            <input type="checkbox" name="category[]" id="category-{{ $key }}" value="{{ $item->id }}">
                            <span class="checkmark"></span>
                        </label>
                    @endforeach
                    @if($categories->count() > 5)
                    <a href="javascript:void(0)" class="text-warning" id="toggle-form-group-categories" @click.prevent="toggleFilter('form-group-categories')">Show More</a>
                    @endif
                    </div>

                    <h4 class="font-weight-bold">Size</h4>
                    <div class="form-group">
                    @foreach (\App\Models\ProductSize::get() as $key => $item)
                        <label class="container">
                            <span>{{ $item->short_name }}</span>
                            <input type="checkbox" name="size[]" id="size-{{ $key }}" value="{{ $item->id }}">
                            <span class="checkmark"></span>
                        </label>
                    @endforeach
                    </div>

                    <h4 class="font-weight-bold">Gender</h4>
                    <div class="form-group">
                        <label class="container">
                            <span>Men</span>
                            <input type="checkbox" name="genre[]" id="genre-MEN" value="MEN">
                            <span class="checkmark"></span>
                        </label>
                        <label class="container">
                            <span>Woman</span>
                            <input type="checkbox" name="genre[]" id="genre-WMN" value="WMN">
                            <span class="checkmark"></span>
                        </label>
                        <label class="container">
                            <span>Unisex</span>
                            <input type="checkbox" name="genre[]" id="genre-UNI" value="UNI">
                            <span class="checkmark"></span>
                        </label>
                        <label class="container">
                            <span>Kid</span>
                            <input type="checkbox" name="genre[]" id="genre-UNI" value="KID">
                            <span class="checkmark"></span>
                        </label>
                    </div>

                    <h4 class="font-weight-bold">Color</h4>
                    <div class="form-group" id="form-group-color">
                    @php 
                        $colors = \App\Models\ProductColor::distinct()->orderBy('name')->get('name');
                    @endphp 
                    @foreach ($colors as $key => $item)
                        <label class="container{{ $key >= 4 ? ' d-none' : '' }}">
                            <span>{{ $item->name }}</span>
                            <input type="checkbox" name="color-filter[]" id="color-{{ $key }}" value="{{ $item->name }}">
                            <span class="checkmark"></span>
                        </label>
                    @endforeach
                    @if($colors->count() > 5)
                    <a href="javascript:void(0)" class="text-warning" id="toggle-form-group-color" @click.prevent="toggleFilter('form-group-color')">Show More</a>
                    @endif
                    </div>

                    <h4 class="font-weight-bold">Price</h4>
                    <div class="form-group px-3">
                        <div class="range d-flex mb-2">
                            <div class="flex-fill" id="minPriceText">{{ i18l_currency(\App\Models\Product::min('price_id')) }}</div>
                            <div class="flex-fill text-right" id="maxPriceText">{{ i18l_currency(\App\Models\Product::max('price_id')) }}</div>
                        </div>
                        <div id="price-range"></div>
                        <input type="hidden" id="min" value="{{\App\Models\Product::min('price_id') }}">
                        <input type="hidden" id="max" value="{{\App\Models\Product::max('price_id') }}">
                        <input type="hidden" id="selectedMinPrice" name="min-price" value="{{\App\Models\Product::min('price_id') }}">
                        <input type="hidden" id="selectedMaxPrice" name="max-price" value="{{\App\Models\Product::max('price_id') }}">
                    </div>
                    
                </form>
            </div>
            <div class="modal-footer">
                <div class="d-flex w-100 align-items-center">
                    <form action="{{ route('web.category','all') }}" id="form-filter-reset" method="GET"></form>
                    <button type="submit" form="form-filter-reset" class="btn btn-block btn-lg mr-2 mt-0 btn-outline-secondary">Reset Filter</button>
                    <button type="submit" form="form-filter" class="btn btn-block btn-lg ml-2 mt-0 btn-warning">Apply</button>
                </div>
            </div>
        </div>
    </div>
</div>