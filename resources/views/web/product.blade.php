@extends('layouts.web', ['web_title'=> $product->name, 'image' =>  $product->images->first()->id])

@section('content')
<div class="container">
    <div class="row mt-3 show-product">
        <div class="col-12">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb bg-white px-0">
                    <li class="breadcrumb-item text-capitalize"><a href="{{ route('web.category', Illuminate\Support\Str::slug($product->category->name) ) }}">{{ $product->category->name }}</a></li>
                    <li class="breadcrumb-item active" aria-current="page">{{ $product->name }}</li>
                </ol>
            </nav>
        </div>
        <div class="col-12 col-lg-6 product-image">
            @include('web.components.product_label', $product)
            <div id="carousel_{{ $product->slug }}" class="carousel carousel-product-images slide" data-ride="carousel" data-interval="false">
                <div class="carousel-inner" role="listbox">
                    @foreach($product->images as $key => $image)
                    <div class="carousel-item @if ($key == 0) active @endif">
                        <img src="{{ action('LandingController@showProductImage', [$image->id]) }}" class="img-fluid w-100 lazyload">
                    </div>
                    @endforeach

                    @foreach($product->colors as $key => $color)
                    <div class="carousel-item">
                        <img src="{{ action('LandingController@showProductColorImage', [$color->colorImages->first()->id]) }}" class="img-fluid w-100 lazyload">
                    </div>
                    @endforeach
                </div>
                <ol class="carousel-indicators">
                    @foreach($product->images as $key => $image)
                    <li data-target="#carousel_{{ $product->slug }}" data-slide-to="{{ $key }}" @if ($key == 0) class="active" @endif>
                        <img src="{{ action('LandingController@showProductImage', [$image->id]) }}" class="img-fluid w-100 lazyload">
                    </li>
                    @endforeach

                    @foreach($product->colors as $key => $color)
                    <li data-target="#carousel_{{ $product->slug }}" data-slide-to="{{ $key + $product->images->count() }}">
                        <img src="{{ action('LandingController@showProductColorImage', [$color->colorImages->first()->id]) }}" class="img-fluid w-100 lazyload">
                    </li>
                    @endforeach
                </ol>
            </div>
        </div>
        <div class="col-12 col-lg-6">
            <h2 class="font-weight-bold">{{ $product->name }}</h2>
            @if($product->discount > 0)
            <p class="mb-0">Normal <del>{{ i18l_currency($product->price_id, 'id') }}</del></p>
            @endif
            @php
                $price_after_disc = $product->price_id
            @endphp
            @if($product->discount > 0)
                @php 
                    if($product->discount <= 100) {
                        $price_after_disc = $product->price_id - ($product->price_id * $product->disc_percent / 100);
                        $discView = $product->disc_percent . '%';
                        $discType = 'percent';
                    }
                    else {
                        $price_after_disc = $product->price_id -$product->disc_percent;
                        $discView = i18l_currency($product->disc_percent, 'id');
                        $discType = 'nominal';
                    }
                @endphp 
                <p class="h3 my-1 text-warning font-weight-bold">{{ i18l_currency($price_after_disc, 'id') }}</p>
                @if($discType == 'percent')
                    {{-- <p class="mb-0 text-primary"> Save {{ i18l_currency(($product->price_id * $product->disc_percent / 100), 'id') }} ({{ $discView}})</p> --}}
                    <p class="mb-0 text-primary"> Save {{ i18l_currency(($product->price_id * $product->disc_percent / 100), 'id') }} ({{ $discView}})</p>
                @else
                    <p class="mb-0 text-primary"> Save {{ i18l_currency($product->disc_percent, 'id') }} </p>
                @endif
            @else 
            <p class="h3 my-3 mt-1 text-warning font-weight-bold">{{ i18l_currency($product->price_id, 'id') }}</p>
            @endif
            <hr>
            <form action="#" id="modal_{{ $product->slug }}" @submit.prevent="addToCart">
                <input type="hidden" name="id" value="{{ $product->id }}">
                <input type="hidden" name="name" value="{{ $product->name }}">
                <input type="hidden" name="price_tag" value="{{ i18l_currency($product->price_id, 'id') }}">
                <input type="hidden" name="price" value="{{ $price_after_disc }}">
                <input type="hidden" name="image" value="{{ action('LandingController@showProductImage', [$product->images->first()->id]) }}">
                <input type="hidden" name="slug" value="{{ $product->slug }}">
                <input type="hidden" name="weight" value="{{ $product->weight }}">
                <div class="form-group form-variant row">
                    <label for="" class="col-sm-2 control-label font-weight-bold">Color</label>
                    <div class="col-sm-10">
                        <div class="mb-3">Choose a variant</div>
                        <div class="d-flex items-center mx-n1">
                            @foreach ($product->colors as $key => $color)
                            @php 
                                $id_variant = $product->slug . '_' . Illuminate\Support\Str::slug($color->name);
                                $img = action('LandingController@showProductColorImage', $color->colorImages->first()->id);
                            @endphp
                            <div class="px-1">
                                <input type="radio" name="color" id="color_{{ $id_variant }}" value="{{ $color->name }}">
                                <label for="color_{{ $id_variant }}" class="color" style="background-image: url('{{ action('LandingController@showColorImage', $color->id) }}')" title="{{ $color->name }}" @click="showProductColorImage('{{ $color->id }}', 'carousel_{{ $product->slug }}',{{ $key + $product->images->count() }})"></label>
                            </div>
                            @endforeach
                        </div>
                        <small class="form-text font-italic text-danger d-none" id="variantColor-{{ $product->slug }}">@{{ variant.color.message }}</small>
                    </div>
                </div>
                @if ($product->sizes->count() > 0)
                <div class="form-group form-variant row">
                    <label for="" class="col-sm-2 control-label font-weight-bold">Size</label>
                    <div class="col-sm-10">
                        <div class="mb-3">Choose a variant</div>
                        <div class="d-flex items-center mx-n1">
                            @foreach($product->sizes as $key => $size)
                            @php 
                                $size_slug = $product->slug . '_' . $size->short_name;
                                $productSizeId = $product->productSizes[$key]->id;
                            @endphp
                            <div class="px-1">
                                <input type="radio" name="size" id="size_{{ $size_slug }}" value="{{ $size->short_name }}">
                                <label for="size_{{ $size_slug }}" class="size" @click="selectProductSize({{ $product->id }}, {{ $productSizeId }})">{{ $size->short_name }}</label>
                            </div>
                            @endforeach
                        </div>
                        <small class="form-text font-italic text-danger d-none" id="variantSize-{{ $product->slug }}">@{{ variant.size.message }}</small>
                    </div>
                </div>
                @endif
                <div class="form-group row">
                    <label for="" class="col-sm-2 control-label font-weight-bold">Quantity</label>
                    <div class="col-sm-10">
                        <div v-if="selectedProduct.productId && selectedProduct.productColorId && selectedProduct.productSizeId" :class="{'mb-3': true, 'text-success': selectedProduct.stock > 0 || !selectedProduct.stock, 'text-danger': selectedProduct.stock == 0}">@{{ selectedProduct.stock == null || selectedProduct.stock > 0 ? 'Ready Stock' : selectedProduct.stock == -2 ? 'Coming Soon' : 'Out Of Stock'}}</div>
                        <div v-else class="mb-3 text-danger">Please Select Varian</div>
                        <div class="row">
                            <div class="input-group col-5">
                                <div class="input-group-prepend">
                                    <button type="button" :class="{ 'btn px-0': true, 'text-warning': !isMinQuantityDisabled }" :disabled="isMinQuantityDisabled"  @click="minQuantity('{{ 'qty-' . $product->slug }}')">
                                        <i class="fas fa-2x fa-minus-circle"></i>
                                    </button>
                                </div>
                                <input type="text" name="qty" class="form-control qty text-center mx-3 border-top-0 border-right-0 border-left-0" min="1" max="12" value="1" v-mask="['#','##']" ref="qty-{{ $product->slug }}">
                                <div class="input-group-append">
                                    <button type="button" :class="{ 'btn px-0': true, 'text-warning': !isAddQuantityDisabled }" :disabled="isAddQuantityDisabled"  @click="addQuantity('{{ 'qty-' . $product->slug }}')">
                                        <i class="fas fa-2x fa-plus-circle"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="col-12">
                                <small class="form-text font-italic text-danger d-none" id="variantQty-{{ $product->slug }}">@{{ variant.qty.message }}</small>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="" class="col-sm-2 control-label font-weight-bold">Note</label>
                    <div class="col-sm-10">
                        <textarea class="form-control" name="" id="" rows="3" placeholder="Type here if you have a note for us"></textarea>
                    </div>
                </div>
                <hr class="my-4">
                <div class="form-group row mb-4">
                    <div class="offset-sm-2 col-sm-10">
                        <button type="submit" class="btn btn-warning btn-lg"><i class="fas fa-shopping-bag mr-2"></i> Add to Cart</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-12">
            <ul class="nav border-top">
                <li class="nav-item" role="presentation ">
                    <a class="nav-link active" href="#product-desc" data-toggle="tab" role="tab" aria-controls="product-desc" aria-selected="true">Description</a>
                </li>
                <li class="nav-item" role="presentation ">
                    <a class="nav-link" href="#product-add-info" data-toggle="tab" role="tab" aria-controls="product-add-info" aria-selected="false">Additional Information</a>
                </li>
                <li class="nav-item  ml-auto" role="presentation">
                    <!-- ShareThis BEGIN --><div class="sharethis-inline-share-buttons nav-link" style="z-index: 0"></div><!-- ShareThis END -->
                    {{-- <a class="nav-link" href="#">Share it now <i class="fas fa-share-alt ml-2"></i></a> --}}
                </li>
            </ul>
            <div class="tab-content" id="product-detail">
                <div class="tab-pane fade show active p-3" id="product-desc">
                    {!! $product->description_html !!}
                </div>
                <div class="tab-pane p-3" id="product-add-info">
                    {!! $product->feature_html !!}
                </div>
            </div>
        </div>
    </div>

    
    <div class="d-flex border-top align-items-center py-4">
        <h4 class="font-weight-bold mb-0">You may also like:</h4>
        <a href="{{ route('web.category', Illuminate\Support\Str::slug($product->category->name)) }}" class="ml-auto">View All <i class="fas fa-arrow-right ml-2"></i></a>
    </div>
    <div class="row row-cols-1 row-cols-2 row-cols-lg-4 px-md-3 products position-relative">
        @foreach($products as $key => $value)
        @php 
            $category_slug = Illuminate\Support\Str::slug($value->category->name);
        @endphp
        <div class="col product-item">
            @include('web.components.product_label', $value)
            <div class="product-image">
                <img src="{{ action('LandingController@showProductImage', [$value->images->first()->id]) }}" alt="">
                <a href="{{ route('web.product', [ $category_slug, $value->slug ]) }}">
                    <i class="fas fa-plus-square"></i>
                    <p class="mb-0">View Detail</p>
                </a>
            </div>
            <div class="product-info">
                <p class="product-title mb-0">{{ $value->name }}</p>
                @if($value->discount > 0)
                
                <p class="mb-0 text-muted text-right"><del>{{ i18l_currency($value->price_id) }}</del></p>
                @php
                $discView = ''; 
                    if($value->disc_percent <= 100) {
                        $price_after_disc = $value->price_id - ($value->price_id * $value->disc_percent / 100);
                        $discView = $value->disc_percent . '%';
                        $discType = 'percent';
                    }
                    else {
                        $price_after_disc = $value->price_id - $value->disc_percent;
                        //$discView = number_format($product->disc_percent, 0, ',', '.');
                        $discView = $value->disc_percent / 1000;
                        $discView = $discView . "rb";
                        $discType = 'nominal';
                    }
                    
                @endphp 
                <p class="price-tag text-right text-warning font-weight-bold d-flex justify-content-end">{{ i18l_currency($price_after_disc) }} <small><span class="badge badge-pill badge-danger ml-2">-{{ $discView }}</span></small></p>
                @else
                <p class="price-tag text-right text-warning font-weight-bold">{{ i18l_currency($value->price_id, 'id') }}</p>
                @endif
                <div class="action-button">
                    <button class="btn btn-block mr-md-2 btn-warning" data-toggle="modal" data-target="#quick-view-{{ $value->slug }}" @click="reset('{{ 'qty-' . $value->slug }}')">Quick View</button>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>

<div class="product-spectator alert alert-info" role="alert">
    <i class="fas fa-bullhorn"></i>
    {{ rand(3,7) }} Visitors are currently viewing this product
</div>
@endsection

@push('scripts')
<script src="{{ asset('js/jquery.zoom.min.js') }}"></script>
<script>
    $(document).ready(function(){
        $('.carousel-item img')
            .wrap('<span style="display:inline-block"></span>')
            .css('display', 'block')
            .parent()
            .zoom();
    });
    
</script>
@endpush