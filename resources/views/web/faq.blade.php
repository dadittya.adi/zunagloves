@extends('layouts.web')

@section('content')
    @include('web.components.nav_pill_about')
    <div class="p-4">
        <h5>1. Why it is a must to register as a member at Zuna Sport?</h5>
        <p>
        By registering as a member, you will have many benefits such as simplifying the process of ordering goods every time you shop. Other advantage includes getting our newsletters, discounts information, special offers, gift vouchers and gifts.

        Mengapa Anda harus registrasi sebagai member Zuna Sport?
        Dengan mendaftarkan sebagai member, Anda akan mendapatkan manfaat seperti memudahkan Anda setiap kali berbelanja. Manfaat lainnya Anda akan menerima newsletter kami, informasi diskon, penawaran special, voucher dan hadiah.
        </p>

        <h5>2. Why it is a must to do a payment confirmation?</h5>
        <p>
        Zuna Sport are always connected to a data system that allows us to know all incoming transactions. However, we still need your help to make payment confirmation for double checking and quicken your shopping experience.

        Mengapa Anda harus melakukan konfirmasi pembayaran?
        Zuna Sport selalu terhubung dengan sistem yang menampilkan semua transaksi yang masuk. Tetapi, kami masih membutuhkan bantuan Anda untuk melakukan konfirmasi pembayaran untuk cek ulang dan mempercepat belanja Anda.
        </p>

        <h5>3. Is it safe to process payments in Zuna Sport?</h5>
        <p>
        Zuna Sport is committed to providing and maintaining TRUST, SAFETY and SECURITY shop for every customer of Zuna Sport.

        Apakah aman melakukan proses pembayaran di Zuna Sport?
        Zuna Sport berkomitmen untuk menyediakan dan menjaga KEPERCAYAAN, KEAMANAN dan JAMINAN toko kami kepada setiap customer Zuna Sport.
        </p>

        <h5>4. Is the ordered products can be replaced or canceled?</h5>
        <p>
        If you do not make a payment, you can cancel and replace your order product. After the cancellation or replament of goods is done, we will resend a new invoice to your email address. However, if you have made the payment, the product that has been ordered cannot be changed or canceled.

        Apakah produk yang sudah dipesan dapat diganti atau dibatalkan?
        Apabila Anda belum melakukan pembayaran, Anda dapat membatalkan dan mengganti produk pesanan Anda. Setelah pembatalan atau pergantian barang selesai, kami akan mengirimkan invoice baru ke alamat email Anda. Tetapi, apabila Anda sudah melakukan pembayaran, produk yang sudah dipesan tidak dapat diubah atau dibatalkan.
        </p>

        <h5>5. Is the product received may be returned?</h5>
        <p>
        We have our own standard to proceed your transaction to ensure we do not make any mistakes while executing your orders Please carefully check your orders in your wishlist before checking out such as size, colour and quantity. Unless in any rare occasion we have processed your order wrongly we will not accept any form of return.

        Apakah produk yang diterima boleh dikembalikan?
        Kami memiliki standar untuk melakukan proses terhadap transaksi untuk memastikan kami tidak melakukan kesalahan ketika eksekusi pesanan Anda. Silakan teliti pesanan Anda di Shopping Cart sebelum melakukan check out, seperti ukuran, warna dan jumlah. Selain kesalahan pesanan akibat kesalahan kami, kami tidak menerima segala bentuk pengembalian.
        </p>
    </div>
@endsection