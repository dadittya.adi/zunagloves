@extends('layouts.web')

@section('content')
    @include('web.components.nav_pill_about')
    <div class="p-4">
        <h5 class="font-weight-bold">RETURN AND CLAIM POLICY</h5>
        <p></p>
        <ol style="list-style-type: upper-alpha;">
            <li>Penukaran barang dapat dilakukan jika Anda menerima produk yang tidak sesuai pesanan.</li>
            <li>Produk yang kami kirim dalam kondisi yang kurang sesuai (Cacat, robek, tidak dapat digunakan)</li>
            <li>Produk dapat ditukarkan dalam jangka waktu 3 hari setelah barang diterima.</li>
            <li>Jika kesalahan Size yang dipilih oleh Anda, maka biaya ongkos kirim ditanggung oleh Anda.</li>
            <li>Jika kesalahan dari kami, (Size, produk cacat, tidak berfungsi dll) maka biaya ongkos kirim akan ditanggung oleh kami</li>
            <li>Pengembalian dana hanya dapat dilakukan ketika barang yang diterima dalam kondisi yang kurang baik dan tidak ada stock produk yang sesuai dalam gudang Zuna Gloves.</li>
            <li>Pengembalian dana akan diproses dengan selambat lambatnya 7 hari setelah barang kami terima.</li>
            <li>Semua Claim Penukaran dan pengembalian disertakan bukti autentik berupa Foto atau video.</li>
            </ol>
    </div>
@endsection