<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta property="og:title" content="{{ isset($web_title) ? $web_title : 'Zuna Gloves' }}" />
    {{-- <meta property="og:image" itemprop="image" content="{{ isset($image) ? action('LandingController@showProductImage', [$image]) : asset('images/logo.png')}}"> --}}
    <meta property="og:image" itemprop="image" content="{{ isset($image) ? action('LandingController@showProductImage', [$image]) : asset('images/logo.png')}}">

    <title>{{ isset($web_title) ? $web_title : 'Zuna Gloves' }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;700&display=swap" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ mix('css/web.css') }}" rel="stylesheet">
    <link rel="icon" href="{{ asset('images/favicon-new.png') }}" type="image/x-icon"/>
    <style type="text/css">
        [v-cloak] {
            display: none;
        }
    </style>

    <meta name="facebook-domain-verification" content="kscpr423fqqhz4wzmpo6wq7o4vvyzg" />

    <script type='text/javascript' src='https://platform-api.sharethis.com/js/sharethis.js#property=60409a46e898f6001887132c&product=sop' async='async'></script>
</head>
<body>
    <div id="app">
        <aside :class="{ 'sidebar col-lg-2 fixed-top': true, 'active': showSidebar }">
            <div class="sidebar-header">
                <a class="d-block text-center" href="{{ route('web.home') }}">
                    <img src="{{ asset('images/logo.png') }}" class="logo" alt="">
                </a>
            </div>
            <div class="sidebar-body">
                <ul class="sidebar-menu m-0 py-md-2">
                    <li class="menu-item dropdown">
                        <a href="{{ route('web.category', 'all') }}" class="item-link">What</a>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <li>
                                <div class="row">
                                    @php 
                                        $categories = App\Models\Category::where('is_active', 1)->get();
                                        $linkPerMenu = 8;
                                    @endphp
                                    @foreach($categories as $key => $category)
                                        @if ($key == 0 || $key == $linkPerMenu)
                                        <ul class="list-unstyled col-md-6">
                                        @endif
                                            <li><a href="{{ route('web.category', Illuminate\Support\Str::slug($category->name) ) }}" class="dropdown-item">{{ $category->name }}</a></li>
                                        @if ($key == ($linkPerMenu-1) || $key == 17)
                                        </ul>
                                        @endif
                                    @endforeach
                                </div>
                            </li>
                        </ul>
                    </li>
                    <li class="menu-item">
                        <a href="{{ route('web.find_us') }}" class="item-link">Where</a>
                    </li>
                    <li class="menu-item">
                        <a href="{{ route('web.contact') }}" class="item-link">How</a>
                    </li>
                </ul>

                @if (Route::is('web.home'))
                <ul class="navbar-nav" v-if="!showSearchBox.left">
                    <li class="nav-item">
                        <a class="nav-link" href="javascript:void(0)" @click="showSearchBoxToggle('left')"><i class="fas fa-search"></i></a>
                    </li>
                    <li class="nav-item @auth dropdown no-caret @endauth">
                        @auth
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownLeft" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-user"></i></a>    
                        <div class="dropdown-menu mx-auto" aria-labelledby="navbarDropdownLeft">
                            <a class="dropdown-item" href="{{ route('dashboard.profile') }}">My Profile</a>
                            <a class="dropdown-item" href="{{ route('dashboard.home') }}">Purchase History</a>
                            <a class="dropdown-item" href="{{ route('dashboard.address') }}">Manage Address</a>
                            <div class="dropdown-divider"></div>
                            <form action="{{ route('logout') }}" method="post">
                            @csrf
                            <button type="submit" class="dropdown-item" @click="clearCart">Logout</button>
                            </form>
                        </div>
                        @else
                        <a class="nav-link" href="#" data-toggle="modal" data-target="#login-form"><i class="fas fa-user"></i></a>    
                        @endauth
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" data-toggle="modal" data-target="#shopping-cart">
                            <i class="fas fa-shopping-bag"></i>
                            <span class="badge cart-count" v-if="cart.length > 0" v-cloak>@{{ cart.length }}</span>
                        </a>
                    </li>
                </ul>
                <div class="d-flex justify-content-center mb-5" style="padding-top: 10px; padding-start: 30px; padding-end: 30px" v-else>
                    <form action="{{ route('web.product.search') }}" method="GET">
                        @csrf
                        <input type="hidden" name="search_form" value="search_form">
                        <div class="input-group">
                            <input type="text" class="form-control form-input-lg" style="height: 30px !important" name="keyword" placeholder="Type Keyword" @blur="showSearchBoxToggle('left')" ref="searchBoxleft">
                            <div class="input-group-append">
                                <button class="btn btn-outline-secondary" style="height: 30px !important; font-size: 0.7rem !important"><i class="fas fa-search"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
                @endif
            </div>
            <div class="sidebar-footer d-flex flex-column p-3 p-lg-4">
                @include('web.components.footer')
            </div>
        </aside>

        <main class="offset-lg-2">
            @if(!Route::is('web.home'))
            <nav class="navbar navbar-expand-md navbar-light bg-white">
                @auth    
                <span class="navbar-brand mx-auto">{{ isset($title) ? $title : "Hi, " . ucwords(Auth::guard('web')->user()->full_name) }}</span>
                @endauth
                @guest
                <span class="navbar-brand mx-auto">{{ isset($title) ? $title : "" }}</span>
                @endguest
                <div class="collapse navbar-collapse" id="collapsibleNavId">
                    <ul class="navbar-nav ml-auto mt-2 mt-lg-0" v-if="!showSearchBox.topdesktop">
                        <li class="nav-item">
                            <a class="nav-link" href="javascript:void(0)" @click="showSearchBoxToggle('topdesktop')"><i class="fas fa-search"></i></a>
                        </li>
                        <li class="nav-item @auth dropdown no-caret @endauth">
                            @auth
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownTopDesktop" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-user"></i></a>    
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownTopDesktop">
                                <a class="dropdown-item" href="{{ route('dashboard.profile') }}">My Profile</a>
                                <a class="dropdown-item" href="{{ route('dashboard.home') }}">Purchase History</a>
                                <a class="dropdown-item" href="{{ route('dashboard.address') }}">Manage Address</a>
                                <div class="dropdown-divider"></div>
                                <form action="{{ route('logout') }}" method="post">
                                @csrf
                                <button type="submit" class="dropdown-item" @click="clearCart">Logout</button>
                                </form>
                            </div>
                            @else
                            <a class="nav-link" href="#" data-toggle="modal" data-target="#login-form"><i class="fas fa-user"></i></a>    
                            @endauth
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#" data-toggle="modal" data-target="#shopping-cart">
                                <i class="fas fa-shopping-bag"></i>
                                <span class="badge cart-count" v-if="cart.length > 0" v-cloak>@{{ cart.length }}</span>
                            </a>
                        </li>
                    </ul>

                    <div class="d-flex justify-content-center ml-auto" v-else>
                        <form action="{{ route('web.product.search') }}" method="GET">
                            @csrf
                            <input type="hidden" name="search_form" value="search_form">
                            <div class="input-group">
                                <input type="text" class="form-control form-input-lg" style="height: 30px !important" name="keyword" placeholder="Type Keyword" @blur="showSearchBoxToggle('topdesktop')" ref="searchBoxtopdesktop">
                                <div class="input-group-append">
                                    <button class="btn btn-outline-secondary" style="height: 30px !important; font-size: 0.7rem !important"><i class="fas fa-search"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </nav>
            @endif

            <nav class="navbar navbar-mobile navbar-light bg-white">
                <a href="{{ route('web.home') }}" class="navbar-brand">
                    <img src="{{ asset('images/logo.png') }}" class="logo" alt="">
                </a>
                <ul class="navbar-nav" v-if="!showSearchBox.topmobile">
                    <li class="nav-item">
                        <a class="nav-link" href="javascript:void(0)" @click="showSearchBoxToggle('topmobile')"><i class="fas fa-search"></i></a>
                    </li>
                    <li class="nav-item @auth dropdown no-caret @endauth">
                        @auth
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownTopMobile" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-user"></i></a>    
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownTopMobile">
                            <a class="dropdown-item" href="{{ route('dashboard.profile') }}">My Profile</a>
                            <a class="dropdown-item" href="{{ route('dashboard.home') }}">Purchase History</a>
                            <a class="dropdown-item" href="{{ route('dashboard.address') }}">Manage Address</a>
                            <div class="dropdown-divider"></div>
                            <form action="{{ route('logout') }}" method="post">
                            @csrf
                            <button type="submit" class="dropdown-item" @click="clearCart">Logout</button>
                            </form>
                        </div>
                        @else
                        <a class="nav-link" href="#" data-toggle="modal" data-target="#login-form"><i class="fas fa-user"></i></a>    
                        @endauth
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" data-toggle="modal" data-target="#shopping-cart">
                            <i class="fas fa-shopping-bag"></i>
                            <span class="badge cart-count" v-if="cart.length > 0" v-cloak>@{{ cart.length }}</span>
                        </a>
                    </li>
                </ul>
                <div class="d-flex justify-content-center" v-else>
                    <form action="{{ route('web.product.search') }}" method="GET">
                        <div class="input-group">
                            <input type="text" class="form-control form-input-lg" style="height: 30px !important" name="keyword" placeholder="Type Keyword" @blur="showSearchBoxToggle('topmobile')" ref="searchBoxtopmobile">
                            <div class="input-group-append">
                                <button class="btn btn-outline-secondary" style="height: 30px !important; font-size: 0.7rem !important"><i class="fas fa-search"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
                <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#navbarMobileMenu" aria-controls="navbarMobileMenu" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarMobileMenu">
                    <ul class="navbar-nav navbar-menu-link">
                        <li class="nav-item dropdown">
                            <a href="{{ route('web.category', 'all') }}" class="item-link w-100">What</a>
                            <a href="#" class="dropdown-toggle-btn" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                @foreach(App\Models\Category::where('is_active', 1)->get() as $category)
                                <a href="{{ route('web.category', Illuminate\Support\Str::slug($category->name) ) }}" class="dropdown-item">{{ $category->name }}</a>
                                @endforeach
                            </div>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('web.find_us') }}" class="item-link">Where</a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('web.contact') }}" class="item-link">How</a>
                        </li>
                    </ul>
                </div>
            </nav>

            @yield('content')

            <div class="footer">
                @include('web.components.footer')
            </div>
        </main>

        @include('web.components.shopping_cart')

        @include('web.components.login')
        @include('web.components.register')

        @stack('modal')

        <a href="https://wa.wizard.id/0b026e" class="float" target="_blank">
            <i class="fab fa-whatsapp my-float"></i>
        </a>        
    </div>

    <!-- Scripts -->
    <script src="{{ mix('js/app.js') }}"></script>

   <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window, document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '767042307323186');
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
        src="https://www.facebook.com/tr?id=767042307323186&ev=PageView&noscript=1"
    /></noscript>
  <!-- End Facebook Pixel Code -->
    @if (old('register_form'))
        <script type="text/javascript">
            $('#register-form').modal('show');
        </script>
    @elseif (old('login_form'))
        <script type="text/javascript">
            $('#login-form').modal('show');
        </script>
    @endif

    @stack('scripts')
</body>
</html>
