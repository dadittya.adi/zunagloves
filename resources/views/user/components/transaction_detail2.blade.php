<div class="modal fade" id="view-detail-{{ $order_number}}" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content p-4">
            <div class="modal-header p-0 pb-4">
                <div class="w-100">
                    <div class="row">
                        <div class="col-lg-9 mr-auto">
                            <h3 class="modal-title font-weight-bold">Detail Order</h3>
                        </div>
                        <div class="col-lg-2 text-right">
                            <button type="button" class="close py-2" data-dismiss="modal" aria-label="Close">
                                <i class="fas fa-times"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-body p-0">
                <div class="py-3 border-bottom">
                    <div class="row">
                        <div class="col-lg-9">
                            <div class="row">
                                <div class="col-12 mb-2">
                                    <span class="font-weight-bold">{{ $transaction->order_number}}</span>
                                </div>
                                <div class="col-lg-6">
                                    <span>Total : <span class="text-warning font-weight-bold">{{ i18l_currency($transaction->total, 'id') }}</span></span>
                                    <p class="mb-0">Tanggal Pembelian  {{ \Carbon\Carbon::parse($transaction->created_at)->format('d M Y')}}</p>
                                </div>
                                <div class="col-lg-6">
                                    <span>Status </span>
                                    {{-- <p class="mb-0 text-warning">Waiting Payment</p> --}}
                                    @if($transaction->state == 'INV')
                                        <p class="text-warning">Waiting Payment</p>
                                    @elseif($transaction->state == 'PAI')
                                        <p class="text-primary">Confirmed</p>
                                    @elseif($transaction->state == 'WRK')
                                        <p class="text-success">Procesed</p>
                                        @elseif($transaction->state == 'DLV')
                                        <p class="text-success">On Delivery</p>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            {{-- <button class="btn btn-block btn-outline-primary rounded-pill">Cancel Order</button>
                            <button class="btn btn-block btn-outline-warning rounded-pill">Download Invoice</button> --}}
                        </div>
                    </div>
                </div>
                <div class="py-3 border-bottom">
                    @foreach ($transaction->items as $item)
                    <div class="row">
                        <div class="col-lg-2">
                            <img src="{{ action('LandingController@showProductColorImage', $item->productColor->colorImages->first()->id) }}" class="img-fluid" alt="">
                        </div>
                        <div class="col-lg-6">
                            <p class="pb-1 mb-1 border-bottom">{{ $item->product->name}}</p>
                            <table class="text-xs">
                                <tbody>
                                    <tr>
                                        <td>Color</td>
                                        <td>:</td>
                                        <td>{{ $item->productColor->name}}</td>
                                    </tr>
                                    <tr>
                                        <td>Size</td>
                                        <td>:</td>
                                        {{-- <td>{{ $item->productProductsize->size->name}}</td> --}}
                                        {{-- <td>{{ $item->ProductProductsize->size->name}}</td> --}}
                                        <td>{{ $item->ProductProductsize->size->name}}</td>
                                    </tr>
                                    {{-- <tr>
                                        <td>Note</td>
                                        <td>:</td>
                                        <td>Text</td>
                                    </tr> --}}
                                    <tr>
                                        <td>Qty</td>
                                        <td>:</td>
                                        <td>{{ $item->qty }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-lg-4">
                            <span>Total</span>
                            <span class="font-weight-bold d-block">{{ i18l_currency($item->price) }}</span>
                        </div>
                    </div>
                    @endforeach
                    {{-- <div class="row">
                        <div class="col-lg-2">
                            <img src="{{ asset('images/tas.png') }}" class="img-fluid" alt="">
                        </div>
                        <div class="col-lg-6">
                            <p class="pb-1 mb-1 border-bottom">Masker Kain 3D 3Ply Anti Mikro Bakteri Anti Air 5 Pcs</p>
                            <table class="text-xs">
                                <tbody>
                                    <tr>
                                        <td>Color</td>
                                        <td>:</td>
                                        <td>Blue</td>
                                    </tr>
                                    <tr>
                                        <td>Size</td>
                                        <td>:</td>
                                        <td>All Size</td>
                                    </tr>
                                    <tr>
                                        <td>Note</td>
                                        <td>:</td>
                                        <td>Text</td>
                                    </tr>
                                    <tr>
                                        <td>Qty</td>
                                        <td>:</td>
                                        <td>Text</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-lg-4">
                            <span>Total</span>
                            <span class="font-weight-bold d-block">Rp 50.000</span>
                        </div>
                    </div> --}}
                </div>
                @if( $transaction->delivery_notes != '')
                    <div class="py-3 border-bottom">
                        <h5 class="font-weight-bold">Delivery Note</h5>
                        {{-- <span class="font-weight-bold">{{ strtoupper($transaction->provider)}}</span> --}}
                        <div class="d-flex">
                            {{-- <span>No. Resi : &nbsp;</span><span>{{ $order->}}</span> --}}
                            <span>{{ $transaction->delivery_notes}}</span>
                        </div>
                    </div>
                @endif
                <div class="py-3 border-bottom">
                    <h5 class="font-weight-bold">Your items will be shipped to</h5>
                    <span>{{ ucwords($transaction->shipping->full_name) }}</span>
                    <div class="d-flex">
                        <span>Telp : &nbsp;</span><span> {{ $transaction->shipping->phone }}</span>
                    </div>
                    <address class="mb-0">
                        {{  ucwords($transaction->shipping->address) }} <br>
                        {{  $transaction->shipping->sub_districtb->subdistrict_name }}, {{  $transaction->shipping->cityb->city_name }} <br>
                        {{  $transaction->shipping->provinceb->province}} {{  $transaction->shipping->postal_code}}
                    </address>
                </div>
                {{-- <div class="py-3 border-bottom">
                    <h5 class="font-weight-bold">Total and Payment Status</h5>
                </div>
                <div class="py-3 border-bottom">
                    <h5 class="font-weight-bold">Order Status Tracking</h5>
                </div> --}}
            </div>
        </div>
    </div>
</div>