<ul class="nav flex-nowrap">
    <li class="nav-item">
        <a class="nav-link {{ Route::is('dashboard.home') ? 'active' : '' }}" href="{{ route('dashboard.home') }}">Purchase History</a>
    </li>
    <li class="nav-item">
        <a class="nav-link {{ Route::is('dashboard.profile') ? 'active' : '' }}" href="{{ route('dashboard.profile') }}">My Profile Information</a>
    </li>
    <li class="nav-item">
        <a class="nav-link {{ Route::is('dashboard.address') ? 'active' : '' }}" href="{{ route('dashboard.address') }}">Manage Address</a>
    </li>
</ul>