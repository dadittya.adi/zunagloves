@extends('layouts.web')

@section('content')
    @include('user.components.nav_menu')
    <div class="p-4">
        @if (session('address-updated'))
        <div class="alert alert-primary alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                <span class="sr-only">Close</span>
            </button>
            {{ session('address-updated') }}
        </div>
        @endif
        <h5 class="font-weight-bold mb-3">Shipping Address</h5>
        <div>
            <span class="text-muted">Alamat</span>
            <p>{{ $customer->address != null ? $customer->address : '-'}}</p>
        </div>
        <div>
            <span class="text-muted">Town / City</span>
            <p>{{ $customer->city != null ? $customer->cityb->type . ' ' . $customer->cityb->city_name : '-'}}</p>
        </div>
        <div>
            <span class="text-muted">Sub-district</span>
            <p>{{ $customer->sub_district != null ? $customer->sub_districtb->subdistrict_name : '-'}}</p>
        </div>
        <div>
            <span class="text-muted">Sub-district</span>
            <p>{{  $customer->province != null ? $customer->provinceb->province : '-'}}</p>
        </div>
        <div>
            <span class="text-muted">Postcode / ZIP</span>
            <p>{{ $customer->postal_code != null ? $customer->postal_code : '-'}}</p>
        </div>
        <a href="#" class="btn btn-warning" data-toggle="modal" data-target="#edit-address"><i class="fas fa-edit"></i> Edit Address</a>
    </div>
@endsection

@push('modal')

<!-- View Detail -->
<div class="modal fade" id="edit-address" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content p-4">
            <div class="modal-header border-0 p-0 pb-4">
                <div class="w-100">
                    <div class="row">
                        <div class="col-lg-9 mr-auto">
                            <h3 class="modal-title font-weight-bold">Edit Address</h3>
                        </div>
                        <div class="col-lg-2 text-right">
                            <button type="button" class="close py-2" data-dismiss="modal" aria-label="Close">
                                <i class="fas fa-times"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-body p-0">
                <loading-component v-if="showLoading"></loading-component>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="alert alert-secondary">
                            Make sure the address field you enter is complete
                        </div>
                    </div>
                </div>
                <form action="{{ route('dashboard.address.update') }}" method="POST">
                    @csrf 
                    <input type="hidden" name="_method" value="PUT">
                    <div class="form-group">
                        <label for="" class="text-muted">Street Address</label>
                        <textarea name="address" class="form-control" cols="30" rows="3">{{ Auth::user()->address }}</textarea>
                    </div>
                    <div class="form-group row required">
                        <div class="col-md-6">
                            <label for="">Country / Region</label>
                            <select name="province" class="form-control form-control-lg" v-model="province" @change="selectCity" id="province">
                                @foreach( getProvinceForInsert() as $id => $province)
                                <option value="{{ $id }}" 
                                    @auth @if($id == Auth::user()->province) selected @endif @endauth>{{ $province }} 
                                </option>
                                @endforeach
                            </select>
                            {{-- <small id="helpId" class="form-text text-muted">Help text</small> --}}
                        </div>
                        <div class="col-md-6">
                            <label for="">Town / City</label>
                            <select name="city" class="form-control form-control-lg" v-model="city" id="city" @change="selectDistrict">
                                <option v-cloak v-if="cities.length > 0" :value="city.city_id" v-for="city in cities" :key="city.city_id">@{{ city.type + ' ' +city.city_name }}</option>
                                <option v-cloak v-else value="">-- Select City --</option>
                            </select>
                            {{-- <small id="helpId" class="form-text text-muted">Help text</small> --}}
                        </div>
                    </div>
                    <div class="form-group row required">
                        <div class="col-md-6">
                            <label for="">Sub-district</label>
                            <select name="district" class="form-control form-control-lg" v-model="district" id="district">
                                <option v-cloak v-if="districts.length > 0" :value="district.subdistrict_id" v-for="(district, key) in districts" :key="district.subdistrict_id">@{{ district.subdistrict_name }}</option>
                                <option v-cloak v-else value="">-- Select District --</option>
                            </select>
                            {{-- <small id="helpId" class="form-text text-muted">Help text</small> --}}
                        </div>
                        <div class="col-md-6">
                            <label for="">Post Code / ZIP</label>
                            <input type="text" name="postal_code" class="form-control form-control-lg" value="@auth {{ Auth::user()->postal_code }} @endauth" placeholder="Post Code">
                            {{-- <small id="helpId" class="form-text text-muted">Help text</small> --}}
                        </div>
                    </div>
                    <button class="btn btn-block btn-lg btn-warning">Save Change</button>
                </form>
            </div>
        </div>
    </div>
</div>

@endpush

<script type="text/javascript">
    window.currentAddress = JSON.stringify({
        province: '{{ Auth::user()->province }}',
        city: '{{ Auth::user()->city }}',
        district: '{{ Auth::user()->sub_district }}'
    });
</script>
@push('scripts')