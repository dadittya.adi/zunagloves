@extends('layouts.web')

@section('content')
    @include('user.components.nav_menu')
    <div class="p-4">
        <div class="row">
            <div class="col-lg-3">
                <div class="input-group mb-4">
                    <form action="{{ route('dashboard.home') }}" method="GET">
                        @csrf
                    <input type="text" name="keyword" id="keyword" class="form-control" placeholder="Find your invoice code" aria-describedby="prefixId">
                    {{-- <div class="input-group-append">
                        <button class="btn btn-secondary"><i class="fas fa-search"></i></button>
                    </div> --}}
                </form>
                </div>
            </div>
        </div>
        <div class="d-flex flex-nowrap align-items-center mb-4">
            <span class="font-weight-bold">Filter Status :</span>
            <div class="filter-cat">
                <a href="{{ route('dashboard.home', array_merge(request()->all(), ['status' =>  'all'])) }}" class="{{ $status == 'all' ? "active" : "" }}">All Purchase</a>
                <a href="{{ route('dashboard.home', array_merge(request()->all(), ['status' =>  'INV'])) }}" class="{{ $status == 'INV' ? "active" : "" }}">Waiting Payment</a>
                <a href="{{ route('dashboard.home', array_merge(request()->all(), ['status' =>  'PAI'])) }}" class="{{ $status == 'PAI' ? "active" : "" }}">Confirmed</a>
                <a href="{{ route('dashboard.home', array_merge(request()->all(), ['status' =>  'WRK'])) }}" class="{{ $status == 'WRK' ? "active" : "" }}">Processed</a>
                <a href="{{ route('dashboard.home', array_merge(request()->all(), ['status' =>  'DLV'])) }}" class="{{ $status == 'DLV' ? "active" : "" }}">On Delivery</a>
                {{-- <a href="#">Completed</a> --}}
            </div>
        </div>
        <div class="table-zn">
            <div class="table-zn__body">

                @foreach($transactions as $transaction)
                <div class="table-zn__row">
                    <div class="col-5 col-lg-1">
                        <img src="{{ action('LandingController@showProductImage', [$transaction->items->first()->product->images->first()->id]) }}" class="img-fluid" alt="">
                    </div>
                    <div class="col-7 col-lg-11">
                        <div class="row mb-2">
                            <div class="col-12 col-md-8">
                                <span>{{ $transaction->order_number }}</span>
                            </div>
                            {{-- <div class="col-4 text-right">
                                <a href="javascript:void(0)" class="text-muted">Batalkan Transaksi</a>
                            </div> --}}
                        </div>
                        <div class="row">
                            <div class="col-12 col-md-4">
                                <span class="text-muted">Total : <span class="text-warning font-weight-bold">{{ i18l_currency($transaction->total) }}</span></span>
                                <p><span class="text-muted">Tanggal Pembelian</span> {{ \Carbon\Carbon::parse($transaction->created_at)->format('d M Y')}}</p>
                            </div>
                            <div class="col-12 col-md-3">
                                <span class="text-muted">Status :</span>
                                @if($transaction->state == 'INV')
                                    <p class="text-warning">Waiting Payment</p>
                                @elseif($transaction->state == 'PAI')
                                    <p class="text-primary">Confirmed</p>
                                @elseif($transaction->state == 'WRK')
                                    <p class="text-success">Processed</p>
                                @elseif($transaction->state == 'DLV')
                                    <p class="text-success">On Delivery</p>
                                @elseif($transaction->state == 'EXP')
                                    <p class="text-danger">Expired</p>
                                @endif
                            </div>
                            <div class="col-12 col-md-2 text-right">
                                <a href="#" class="btn btn-block btn-outline-warning rounded-pill" data-toggle="modal" data-target="#view-detail-{{ $transaction->order_number }}">View Detail</a>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
               
            </div>
        </div>
    </div>
@endsection

@push('modal')

<!-- View Detail -->
    @foreach($transactions as $key => $transaction)
    @php 
        $order_number = $transaction->order_number;
    @endphp
         @include('user.components.transaction_detail2', compact('transaction', 'order_number'))
    @endforeach

@endpush