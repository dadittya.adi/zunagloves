@extends('layouts.web')

@section('content')
    @include('user.components.nav_menu')
    <div class="p-4">
        @if (session('status'))
        <div class="alert alert-primary alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                <span class="sr-only">Close</span>
            </button>
            {{ session('status') }}
        </div>
        @endif
        <h5 class="font-weight-bold mb-3">Basic Information</h5>
        <div>
            <span class="text-muted">Nama Lengkap</span>
            <p>{{ ucwords(Auth::guard('web')->user()->full_name) }}</p>
        </div>
        <div>
            <span class="text-muted">Email Address</span>
            <p>{{ Auth::guard('web')->user()->email }}</p>
        </div>
        <div>
            <span class="text-muted">Phone Number</span>
            <p>{{ Auth::guard('web')->user()->phone }}</p>
        </div>
        <a href="#" class="btn btn-warning" data-toggle="modal" data-target="#basic-info"><i class="fas fa-edit"></i> Edit Information</a>
        <hr>
        @if (session('password'))
        <div class="alert alert-primary alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                <span class="sr-only">Close</span>
            </button>
            {{ session('password') }}
        </div>
        @endif
        <h5 class="font-weight-bold mb-3">Change Password</h5>
        <div class="row">
            <div class="col-md-6">
                <div class="alert alert-secondary" role="alert">
                    <p class="mb-0">If you wish to change the password to access your account, Your current e-mail address is <b>{{ Auth::guard('web')->user()->email }}</b></p>
                </div>
            </div>
        </div>
        <form action="{{ route('dashboard.password.update') }}" method="POST">
            @csrf
            <input type="hidden" name="_method" value="PUT">
            <div class="form-group row">
                <label for="" class="text-muted col-sm-12">Current Password</label>
                <div class="col-sm-3">
                    <input type="password" name="password_current" class="form-control">
                </div>
            </div>
            <div class="form-group row">
                <label for="" class="text-muted col-sm-12">New Password</label>
                <div class="col-sm-3">
                    <input type="password" name="password_new" class="form-control">
                </div>
            </div>
            <div class="form-group row">
                <label for="" class="text-muted col-sm-12">Confirm New Password</label>
                <div class="col-sm-3">
                    <input type="password" name="password_confirm" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-warning">Change Password</button>
            </div>
        </form>
    </div>
@endsection

@push('modal')

<!-- View Detail -->
<div class="modal fade" id="basic-info" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content p-4">
            <div class="modal-header border-0 p-0 pb-4">
                <div class="w-100">
                    <div class="row">
                        <div class="col-lg-9 mr-auto">
                            <h3 class="modal-title font-weight-bold">Edit Basic Information</h3>
                        </div>
                        <div class="col-lg-2 text-right">
                            <button type="button" class="close py-2" data-dismiss="modal" aria-label="Close">
                                <i class="fas fa-times"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-body p-0">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="alert alert-secondary">
                            You can only change your phone number
                        </div>
                    </div>
                </div>
                <form action="{{ route('dashboard.profile.update') }}" method="POST">
                    @csrf
                    <input type="hidden" name="_method" value="PUT">
                    <div class="form-group">
                        <label for="" class="text-muted">Nama Lengkap</label>
                        <p>{{ ucwords(Auth::guard('web')->user()->full_name) }}</p>
                    </div>
                    <div class="form-group">
                        <label for="" class="text-muted">Email Address</label>
                        <p>{{ Auth::guard('web')->user()->email }}</p>
                    </div>
                    <div class="form-group">
                        <label for="" class="text-muted">Phone Number</label>
                        <input type="text" name="phone" class="form-control">
                    </div>
                    <button class="btn btn-block btn-lg btn-warning">Save Change</button>
                </form>
            </div>
        </div>
    </div>
</div>

@endpush