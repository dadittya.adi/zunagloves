<?php namespace App;

use Config;
use Veritrans_Config;
use Veritrans_VtWeb;
use Veritrans_Notification;
use Log;

class VeritransPaymentGateway implements PaymentGateway {
	protected function buildTransaction($order)
	{
		$transaction = [
    		'order_id' => $order->order_number,
    		'gross_amount' => floor($order->total),
    	];

    	$items = [];
    	$total = 0;
    	foreach($order->items as $item) {
    		$subtotal = ceil($item->price - $item->discount);
			if ($item->size) {
				$items[] = [
					'id' => $item->productColor->sku,
					'name' => substr($item->product->name . ' (' . $item->size . ')', 0, 100),
					'price' => $subtotal,
					'quantity' => $item->qty
				];
			} else {
				$items[] = [
					'id' => $item->productColor->sku,
					'name' => substr($item->product->name, 0, 100),
					'price' => $subtotal,
					'quantity' => $item->qty
				];
			}

    		$total += $subtotal * $item->qty;
    	}

    	$subtotal = ceil($order->shipping_price * $order->weight);
    	$items[] = [
    		'id' => 'DELIVERY',
    		'name' => substr($order->destination, 0, 50),
    		'price' => $subtotal,
    		'quantity' => 1
    	];

    	$total += $subtotal;

    	if ($total != floor($order->total)) {
    		$items[] = [
    			'id' => 'ROUNDING',
    			'name' => 'Rounding Error',
    			'price' => floor($order->total) - $total,
    			'quantity' => 1
    		];
    	};

    	$customer = [
    		'first_name' => substr($order->customer->full_name, 0, 20),
    		'last_name' => '',
    		'email' => $order->customer->email,
    		'phone' => $order->customer->phone,
    		'shipping_address' => [
	    		'first_name' => substr($order->shipping->full_name, 0, 20),
	    		'last_name' => '',
	    		'address' => substr($order->shipping->address, 0, 100),
	    		'city' => substr($order->shipping->city, 0, 100),
	    		'postal_code' => substr($order->shipping->postal_code, 0, 10),
	    		'phone' => $order->shipping->phone,
	    		'country_code' => 'IDN'
	    	]
    	];

    	$vtweb = [
    		'credit_card_3d_secure' => true,
    		'finish_redirect_url' => action('AccountController@detailOrder', $order->order_number),
    		'unfinish_redirect_url' => action('AccountController@detailOrder', $order->order_number),
    		'error_redirect_url' => action('AccountController@detailOrder', $order->order_number)
    	];

    	return [
			'transaction_details' => $transaction,
			'item_details' => $items,
			'customer_details' => $customer,
			'vtweb' => $vtweb
    	];
	}
	
	public function charge($order)
	{
		static::initConfig();

		$data = $this->buildTransaction($order);
		$vtweb = Veritrans_VtWeb::getResult($data);

		Log::info('VTW.CHARGE result', json_decode(json_encode($vtweb), true));

		return $vtweb;
	}

	public function getTransactionStatus($order)
	{
	}

	public static function handleNotif()
	{
        static::initConfig();

		$notif = new Veritrans_Notification();
		return $notif;
	}

    public static function initConfig()
    {
        Veritrans_Config::$serverKey = Config::get('payment.veritrans.server_key');
        Veritrans_Config::$isProduction = !Config::get('payment.veritrans.sandbox');
        Veritrans_Config::$is3ds = Config::get('payment.veritrans.3d_secure');
    }
}