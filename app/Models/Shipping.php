<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Shipping extends Model {

	protected $guarded = ['id'];

	public function prices()
	{
		return $this->hasMany(
            'App\Models\ShippingPrice',
            'shipping_id'            
        );
	}

	public function country()
	{
		return $this->belongsTo(
			'App\Models\Country',
			'country_id'
		);
	}

	public function cityb()
	{
		return $this->belongsTo(
			'App\Models\City',
			'cit'
		)
	}
}