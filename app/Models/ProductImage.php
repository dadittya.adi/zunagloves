<?php

namespace App\Models;

use Config;
use StdClass;
use Illuminate\Support\Str;

use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{
    protected $guarded = ['id'];
    protected $touches = ['product'];

    public function product()
    {
    	return $this->belongsTo(
    		'App\Models\Product',
			'product_id'
    	);
    }

    public function getProductImageFullPath()
	{
		$value = $this->image;
		if (!$value)
			return null;

		$path = Config::get('storage.product');		
		return "{$path}/{$value}";
    }

    static public function random($path, $extension = null) 
    {
		if (!$path)
			throw Exception("Config not found [{$key}]");
		$ret = new StdClass();
		$try = 250;
		do {
			if ($try <= 0)
				throw Exception("Storage::random fails to produce randomized filename");
			$hash = Str::random(32);
			if ($extension)
				$hash = $hash . '.' . $extension;
			$file = $path . '/' . $hash;
			$try -= 1;
		} while (file_exists($file));
		$ret = array($path, $hash, $file);
		return $ret;
    }
    
    public function uploadImage($value){
		if (!$value->isValid())
			return;
		
		$ext = $value->getClientOriginalExtension();
		$path = Config::get('storage.product');

		$file_path = static::random($path, $ext);
		//var_dump($value->getPathName()); var_dump($file_path);
		$value->move($file_path[0], $file_path[1]);
		return $file_path[1];
	}
}
