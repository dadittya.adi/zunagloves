<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model {

	protected $guarded = ['id'];

	public function product()
	{
		return $this->belongsTo(
            'App\Models\Product',
            'product_id'            
        );
	}

	public function productColor()
	{
		return $this->belongsTo(
            'App\Models\ProductColor',
            'product_color_id'            
        );
	}

	public function productProductSize()
	{
		return $this->belongsTo(
            'App\Models\ProductProductsize',
            'product_productsize_id'            
        );
	}

	public function getpriceDiscountAttribute()
	{
		$disc_percent = $this->disc_percent;

		$value = null;
		if ($disc_percent > 0 && $this->price > 0) {
			$disc_amount = $this->price * $disc_percent / 100;
			$value = $this->price - $disc_amount;
		}

		return $value;
	}
}