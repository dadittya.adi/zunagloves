<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class Customer extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    protected $guarded = ['id'];
	protected $hidden = ['password', 'remember_token'];
	protected $_cache = [];
	protected $fillable = ['full_name', 'email', 'password', 'phone', 'organization', 'address', 'city', 'province', 'country', 'posta;_code',
 			'state', 'resetpwd+at', 'remember_token', 'provider', 'provide_id', 'photo', 'sub_district'];

	public function orders()
	{
		return $this->hasMany(
            'App\Models\Order',
            'customer_id'            
        );
	}

	public function wishlists()
    {
    	return $this->belongsToMany(
    		'App\Models\Product',
			'wishlists',
			'customer_id',
			'product_id'
    	)->withTimestamps();
    }

	public function generateConfirmHash()
	{
		$s = hash_hmac('sha1', $this->email, $this->created_at, true);
		$s = base64_encode($s);
		$s = strtr($s, array('+' => '-', '/' => '_', '=' => ''));
		return $s;
	}

	public function checkConfirmHash($hash)
	{
		$s = hash_hmac('sha1', $this->email, $this->created_at, true);
		$s = base64_encode($s);
		$s = strtr($s, array('+' => '-', '/' => '_', '=' => ''));
		return $s == $hash;
	}

	public function cityb()
	{
		return $this->belongsTo(
            'App\Models\City',
            'city'            
        );
	}

	public function provinceb()
	{
		return $this->belongsTo(
            'App\Models\Province',
            'province'            
        );
	}

	public function sub_districtb()
	{
		return $this->belongsTo(
            'App\Models\SubDistrict',
            'sub_district'            
        );
	}
}