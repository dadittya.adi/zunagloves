<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $guarded = ['id'];
    public function admins()
	{
		return $this->belongsToMany(
			'App\Models\Admin',
			'admin_groups',
			'group_id',
			'admin_id'
		);
	}
}
