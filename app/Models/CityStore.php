<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CityStore extends Model
{
    protected $primaryKey = 'id';

    protected $fillable = [
        'name',
        'is_active',
        'show_order'
    ];

    public function stores() {
        return $this->hasMany('App\Models\Store','city_store_id');
    }

}
