<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DiscountRequest extends Model
{
    protected $guarded = ['id'];
    protected $table = 'customer_discounts';
    protected $fillable = ['email'];
}
