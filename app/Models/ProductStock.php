<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductStock extends Model
{
    protected $guarded = ['id'];

    public function product()
    {
    	return $this->belongsTo(
    		'App\Models\Product',
			'product_id'
    	);
    }

    public function productColor()
    {
    	return $this->belongsTo(
    		'App\Models\ProductColor',
			'product_color_id'
    	);
    }

    public function productProductsize()
    {
    	return $this->belongsTo(
    		'App\Models\ProductProductsize',
			'product_productsize_id'
    	);
    }
}
