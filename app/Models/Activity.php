<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Config;
use StdClass;
use DB;
use Auth;

class Activity extends Model {

	protected $guarded = ['id'];
	protected $dates = [
		'created_at',
		'updated_at',
	];

	public static function save_activity($action, $user = null, $before = null, $middle = null, $after = null, $table = null)
	{
		$activity = new Activity();
		$act = $action;
		$message = null;
		$short_message = null;
		$action = explode('|', $action);
		$intl_act = $action[0];

		switch($act) {
			//-----------------
			case 'CREATE.SUCCESS|ADMIN':
				$message = 'Data ' . $action[1] . ' with full_name: ' . $before->full_name . ' success added.';
				$short_message = 'success add ' . $action[1] . ' user';
				$stat = true;
				break;
			case 'UPDATE.SUCCESS|ADMIN':
				$message = 'Data ' . $action[1] . ' updated where full_name: "'. $after->getOriginal()['full_name'] . '" with data: ' . Activity::update_message($action[1], $after);
				$short_message = 'success update ' . $action[1] . ' user';
				$stat = Activity::cek_message($action[1], $after);
				break;
			case 'DELETE.SUCCESS|ADMIN':
				$message = 'Data ' . $action[1] . ' where full_name: "' . $before->full_name  . '" success deleted.';
				$short_message = 'success delete ' . $action[1] . ' user';
				$stat = true;
				break;
			//-------------------
			case 'LOGIN.SUCCESS|AUTHENTICATION':
				$message = 'USER where full_name: "' . $user->full_name  . '" and email: "' . $user->email  . '" success logged in.';
				$short_message = 'success login';
				$stat = true;
				break;
			case 'LOGOUT.SUCCESS|AUTHENTICATION':
				$message = 'USER where full_name: "' . $user->full_name  . '" and email: "' . $user->email  . '" success logged out.';
				$short_message = 'success logout';
				$stat = true;
				break;
			//-------------------
			case 'CREATE.SUCCESS|BLOG':
				$message = 'Data ' . $action[1] . ' with title: "' . $before->title . '" success added.';
				$short_message = 'success add ' . $action[1] . ' data';
				$stat = true;
				break;
			case 'UPDATE.SUCCESS|BLOG':
				$message = 'Data ' . $action[1] . ' updated where title: "'. $after->getOriginal()['title'] . '" with data: ' . Activity::update_message($action[1], $after);
				$short_message = 'success update ' . $action[1] . ' data';
				$stat = Activity::cek_message($action[1], $after);
				break;
			case 'DELETE.SUCCESS|BLOG':
				$message = 'Data ' . $action[1] . ' where title: "' . $before->title  . '" success deleted.';
				$short_message = 'success delete ' . $action[1] . ' data';
				$stat = true;
				break;
			//-------------------
			case 'CREATE.SUCCESS|BLOG MEDIA':
				$message = $action[1] . ' success uploaded.';
				$short_message = 'success add ' . $action[1] . ' data';
				$stat = true;
				break;
			case 'UPDATE.SUCCESS|BLOG MEDIA':
				$message = $action[1] . ' updated where name: "'. $after->getOriginal()['name'] . '" with data: ' . Activity::update_message($action[1], $after);
				$short_message = 'success update ' . $action[1] . ' data';
				$stat = Activity::cek_message($action[1], $after);
				break;
			case 'DELETE.SUCCESS|BLOG MEDIA':
				$message = $action[1] . ' where name: "' . $before->name  . '" success deleted.';
				$short_message = 'success delete ' . $action[1] . ' data';
				$stat = true;
				break;
			//------------------
			case 'UPDATE.CAT.SUCCESS|CATEGORY':
				$message = 'Data ' . $action[1] . ' updated where name: "'. $after->getOriginal()['name'] . '" with data: ' . Activity::update_message($action[1], $after);
				$short_message = 'success update ' . $action[1] . ' data';
				$stat = Activity::cek_message($action[1], $after);
				break;
			case 'MOVE.CAT.SUCCESS|CATEGORY':
				$message = 'CATEGORIES order has been changed.';
				$short_message = 'success change ' . $action[1] . ' data order';
				$stat = true;
				break;
			//-----------------
			case 'CREATE.SUBCAT.SUCCESS|SUB CATEGORY':
				$message = 'SUB CATEGORY with name: "' . $before->subcategory_name . '" in category: "' . $middle->name . '" success added.';
				$short_message = 'success add ' . $action[1] . ' data';
				$stat = true;
				break;
			case 'UPDATE.SUBCAT.SUCCESS|SUB CATEGORY':
				$message = 'Data ' . $action[1] . ' updated where name: "'. $after->getOriginal()['name'] . '" with data: ' . Activity::update_message($action[1], $after);
				$short_message = 'success update ' . $action[1] . ' data';
				$stat = Activity::cek_message($action[1], $after);
				break;
			case 'DELETE.SUBCAT.SUCCESS|SUB CATEGORY':
				$message = 'SUB CATEGORY with name: "' . $before->name . '" in category: "' . $middle->name . '" success deleted.';
				$short_message = 'success delete ' . $action[1] . ' data';
				$stat = true;
				break;
			//-----------------
			case 'CREATE.SUCCESS|COUNTRY':
				$message = $action[1] . ' with name: "' . $before->name . '" success added.';
				$short_message = 'success add ' . $action[1] . ' data';
				$stat = true;
				break;
			case 'UPDATE.SUCCESS|COUNTRY':
				$message = $action[1] . ' updated where name: "'. $after->getOriginal()['name'] . '" with data: ' . Activity::update_message($action[1], $after);
				$short_message = 'success update ' . $action[1] . ' data';
				$stat = Activity::cek_message($action[1], $after);
				break;
			case 'DELETE.SUCCESS|COUNTRY':
				$message = $action[1] . ' with name: "' . $before->name . '" success deleted.';
				$short_message = 'success delete ' . $action[1] . ' data';
				$stat = true;
				break;
			//-----------------
			case 'CREATE.SUCCESS|CUSTOMER':
				$message = $action[1] . ' with full_name: "' . $before->full_name . '" success added.';
				$short_message = 'success add ' . $action[1] . ' data';
				$stat = true;
				break;
			case 'UPDATE.SUCCESS|CUSTOMER':
				$message = $action[1] . ' updated where full_name: "'. $after->getOriginal()['full_name'] . '" with data: ' . Activity::update_message($action[1], $after);
				$short_message = 'success update ' . $action[1] . ' data';
				$stat = Activity::cek_message($action[1], $after);
				break;
			case 'EXPORT.SUCCESS|CUSTOMER':
				$message = $action[1] . ' data success exported.';
				$short_message = 'success export ' . $action[1] . ' data';
				$stat = true;
				break;
			//-----------------
			case 'CREATE.SUCCESS|JUMBOTRON':
				$message = $action[1] . ' with title: "' . $before->title . '" success added.';
				$short_message = 'success add ' . $action[1] . ' data';
				$stat = true;
				break;
			case 'UPDATE.SUCCESS|JUMBOTRON':
				$message = $action[1] . ' updated where title: "'. $after->getOriginal()['title'] . '" with data: ' . Activity::update_message($action[1], $after);
				$short_message = 'success update ' . $action[1] . ' data';
				$stat = Activity::cek_message($action[1], $after);
				break;
			case 'DELETE.SUCCESS|JUMBOTRON':
				$message = $action[1] . ' with title: "' . $before->title . '" success deleted.';
				$short_message = 'success delete ' . $action[1] . ' data';
				$stat = true;
				break;
			case 'MOVE.SUCCESS|JUMBOTRON':
				$message = $action[1] . ' order has been changed.';
				$short_message = 'success change ' . $action[1] . ' data order';
				$stat = true;
				break;
			//-----------------
			case 'CREATE.SUCCESS|MEDIA':
				$message = $action[1] . ' success uploaded.';
				$short_message = 'success upload ' . $action[1] . ' data';
				$stat = true;
				break;
			case 'UPDATE.SUCCESS|MEDIA':
				$message = $action[1] . ' updated where name: "'. $after->getOriginal()['name'] . '" with data: ' . Activity::update_message($action[1], $after);
				$short_message = 'success update ' . $action[1] . ' data';
				$stat = Activity::cek_message($action[1], $after);
				break;
			case 'DELETE.SUCCESS|MEDIA':
				$message = $action[1] . ' with name: "' . $before->name . '" success deleted.';
				$short_message = 'success delete ' . $action[1] . ' data';
				$stat = true;
				break;
			//-----------------
			case 'UPDATE.SUCCESS|ORDER':
				$message = $action[1] . ' updated where order_number: "'. $after->getOriginal()['order_number'] . '" with data: ' . Activity::update_message($action[1], $after);
				$short_message = 'success update ' . $action[1] . ' data where order number: '. $after->getOriginal()['order_number'];
				$stat = Activity::cek_message($action[1], $after);
				break;
			case 'DOWNLOAD.BANK.SUCCESS|ORDER':
				$message = $action[1] . ' with order_number: "' . $before->order_number . '" success downloaded.';
				$short_message = 'success download ' . $action[1] . ' data';
				$stat = true;
				break;
			case 'PRINT.SUCCESS|ORDER':
				$message = $action[1] . ' with order_number: "' . $before->order_number . '" success printed.';
				$short_message = 'success print ' . $action[1] . ' data';
				$stat = true;
				break;
			case 'EXPORT.SUCCESS|ORDER':
				$message = $action[1] . ' data success exported.';
				$short_message = 'success export ' . $action[1] . ' data';
				$stat = true;
				break;
			//-----------------
			case 'CREATE.SUCCESS|PRODUCT COLOR':
				$message = $action[1] . ' with name: "' . $before->name . '" for PRODUCT where name: "' . $middle->name . '" success added.';
				$short_message = 'success add ' . $action[1] . ' data';
				$stat = true;
				break;
			case 'UPDATE.SUCCESS|PRODUCT COLOR':
				$message = $action[1] . ' updated where name: "'. $after->getOriginal()['name'] . '" with data: ' . Activity::update_message($action[1], $after);
				$short_message = 'success update ' . $action[1] . ' data';
				$stat = Activity::cek_message($action[1], $after);
				break;
			case 'DELETE.SUCCESS|PRODUCT COLOR':
				$message = $action[1] . ' where name: "' . $before->name  . '" success deleted.';
				$short_message = 'success delete ' . $action[1] . ' data';
				$stat = true;
				break;
			//-----------------
			case 'CREATE.SUCCESS|PRODUCT':
				$message = $action[1] . ' with name: "' . $before->name . '" success added.';
				$short_message = 'success add ' . $action[1] . ' data';
				$stat = true;
				break;
			case 'UPDATE.SUCCESS|PRODUCT':
				$message = $action[1] . ' updated where name: "'. $after->getOriginal()['name'] . '" with data: ' . Activity::update_message($action[1], $after);
				$short_message = 'success update ' . $action[1] . ' data';
				$stat = Activity::cek_message($action[1], $after);
				break;
			//-----------------
			case 'CREATE.SUCCESS|PRODUCT SIZE':
				$message = $action[1] . ' with name: "' . $before->name . '"  success added.';
				$short_message = 'success add ' . $action[1] . ' data';
				$stat = true;
				break;
			case 'UPDATE.SUCCESS|PRODUCT SIZE':
				$message = $action[1] . ' updated where name: "'. $after->getOriginal()['name'] . '" with data: ' . Activity::update_message($action[1], $after);
				$short_message = 'success update ' . $action[1] . ' data';
				$stat = Activity::cek_message($action[1], $after);
				break;
			case 'DELETE.SUCCESS|PRODUCT SIZE':
				$message = $action[1] . ' where name: "' . $before->name  . '" success deleted.';
				$short_message = 'success delete ' . $action[1] . ' data';
				$stat = true;
				break;
			case 'MOVE.SUCCESS|PRODUCT SIZE':
				$message = $action[1] . ' order has been changed.';
				$short_message = 'success change ' . $action[1] . ' data order';
				$stat = true;
				break;
			//-----------------
			case 'UPDATE.SUCCESS|PROMO':
				$message = $action[1] . ' updated where title: "'. $after->getOriginal()['title'] . '" with data: ' . Activity::update_message($action[1], $after);
				$short_message = 'success update ' . $action[1] . ' data';
				$stat = Activity::cek_message($action[1], $after);
				break;
			//-----------------
			case 'CREATE.SUCCESS|SHIPPING':
				$message = $action[1] . ' with name: "' . $before->name . '" success added.';
				$short_message = 'success add ' . $action[1] . ' data';
				$stat = true;
				break;
			case 'UPDATE.SUCCESS|SHIPPING':
				$message = $action[1] . ' updated where name: "'. $after->getOriginal()['name'] . '" with data: ' . Activity::update_message($action[1], $after);
				$short_message = 'success update ' . $action[1] . ' data';
				$stat = Activity::cek_message($action[1], $after);
				break;
			case 'DELETE.SUCCESS|SHIPPING':
				$message = $action[1] . ' where name: "' . $before->name  . '" success deleted.';
				$short_message = 'success delete ' . $action[1] . ' data';
				$stat = true;
				break;
			case 'MOVE.SUCCESS|SHIPPING':
				$message = $action[1] . ' order has been changed.';
				$short_message = 'success change ' . $action[1] . ' data order';
				$stat = true;
				break;
			//-----------------
			case 'CREATE.SUCCESS|SHIPPING PRICE':
				$message = $action[1] . ' with name: "' . $before->name . '" for SHIPPING where name: "' . $middle->name . '" success added.';
				$short_message = 'success add ' . $action[1] . ' data';
				$stat = true;
				break;
			case 'UPDATE.SUCCESS|SHIPPING PRICE':
				$message = $action[1] . ' updated where name: "'. $after->getOriginal()['name'] . '" with data: ' . Activity::update_message($action[1], $after);
				$short_message = 'success update ' . $action[1] . ' data';
				$stat = Activity::cek_message($action[1], $after);
				break;
			case 'DELETE.SUCCESS|SHIPPING PRICE':
				$message = $action[1] . ' where name: "' . $before->name  . '" success deleted.';
				$short_message = 'success delete ' . $action[1] . ' data';
				$stat = true;
				break;
			case 'UPLOAD.SUCCESS|SHIPPING PRICE':
				$message = $action[1] . ' data has been uploaded.';
				$short_message = 'success add ' . $action[1] . ' data';
				$stat = true;
				break;
			//-----------------
			case 'CREATE.SUCCESS|VOUCHER':
				$message = $action[1] . ' with code: "' . $before->code . '" success added.';
				$short_message = 'success add ' . $action[1] . ' data';
				$stat = true;
				break;
			case 'UPDATE.SUCCESS|VOUCHER':
				$message = $action[1] . ' updated where code: "'. $after->getOriginal()['code'] . '" with data: ' . Activity::update_message($action[1], $after);
				$short_message = 'success update ' . $action[1] . ' data';
				$stat = Activity::cek_message($action[1], $after);
				break;
			case 'DELETE.SUCCESS|VOUCHER':
				$message = $action[1] . ' where code: "' . $before->code  . '" success deleted.';
				$short_message = 'success delete ' . $action[1] . ' data';
				$stat = true;
				break;
			//-----------------
			case 'DELETE.SUCCESS|SUBSCRIBED':
				$message = $action[1] . ' where email: "' . $before->email  . '" success deleted.';
				$short_message = 'success delete ' . $action[1] . ' data';
				$stat = true;
				break;
			case 'EXPORT.SUCCESS|SUBSCRIBED':
				$message = $action[1] . ' data success exported.';
				$short_message = 'success export ' . $action[1] . ' data';
				$stat = true;
				break;
			//-----------------
			case 'CREATE.SUCCESS|TAG':
				$message = 'Data ' . $action[1] . ' with name: "' . $before->name . '" success added.';
				$short_message = 'success add ' . $action[1] . ' data';
				$stat = true;
				break;
			case 'UPDATE.SUCCESS|TAG':
				$message = 'Data ' . $action[1] . ' updated where name: "'. $after->getOriginal()['name'] . '" with data: ' . Activity::update_message($action[1], $after);
				$short_message = 'success update ' . $action[1] . ' data';
				$stat = Activity::cek_message($action[1], $after);
				break;
			case 'DELETE.SUCCESS|TAG':
				$message = 'Data ' . $action[1] . ' where title: "' . $before->name  . '" success deleted.';
				$short_message = 'success delete ' . $action[1] . ' data';
				$stat = true;
				break;
			//-------------------------ooooooooo---------------------
			case 'UPDATE_ORDER.SUCCESS|ORDER':
				$message = 'Order data succes updated where order number: "'. $after->getOriginal()['order_number'] . '" with data: ' . Activity::update_message($action[1], $after);
				$short_message = 'success update ' . $action[1] . ' data where order_number: '. $after->getOriginal()['order_number'];
				$stat = Activity::cek_message($action[1], $after);
				break;
			case 'PRINT_INV.SUCCESS|ORDER':
				$message = 'Order data success printed  where order number: "' . $before->order_number  . '" .';
				$short_message = 'success print order with order_number: ' . $before->order_number . '';
				$stat = true;
				break;
			case 'PRINT_INV.SUCCESS|ORDER':
				$message = 'Order data success printed  where order number: "' . $before->order_number  . '" .';
				$short_message = 'success print order with order_number: ' . $before->order_number . '';
				$stat = true;
				break;
			case 'CHECKOUT.SUCCESS|ORDER':
				$message = 'Order success where order number: "' . $before->order_number  . '" .';
				$short_message = 'success order with order_number: ' . $before->order_number . '';
				$stat = true;
				break;
			case 'PAYMENT.SUCCESS|ORDER':
				$message = 'Order status change where order number: "' . $before->order_number  . '" from '. Activity::update_message($action[1], $after);
				$short_message = 'success order with order_number: ' . $before->order_number . '';
				$stat = true;
				break;
		}

		if($table == 'admins'){
			$user_id = Auth::check() ? $user->id : null;
			$user_name = Auth::check() ? $user->full_name : null;
		}else if($table == 'customers'){
			$user_id = $user['id'];
			$user_name = $user['full_name'];
		}
		$activity->fill([
			'user_id' => $user_id,
			'user_name' => $user_name,
			'ip_address' => $_SERVER['REMOTE_ADDR'],
			'user_agent' => $_SERVER['HTTP_USER_AGENT'],
			'message' => $message,
			'short_message' => $short_message,
			'table' => $table,
			'action' => $intl_act,
			'type' => $action[1],
			'status' => $stat,
			'created_at' => date('Y-m-d H:i:s'),
			'updated_at' => date('Y-m-d H:i:s')
		]);

		if($stat != false)
			$activity->save();

		return $activity;
	}

	public static function update_message($type = null, $data = null)
	{
		if($data != null){
			$new = $data->getDirty();
			$old = $data->getOriginal();
		}else{
			$new = null;
			$old = null;
		}
		//var_dump($new);die;
		$message = null;
		if($new != null || $new != '' || count($new) != 0){
			foreach ($new as $key => $value) {
				$message .= '"' . $key . '" from: "' . $old[$key] . '" to "' . $value . '" ';
			}
		}else{
			return null;
		}

		return $message;
	}

	public static function cek_message($type = null, $data = null)
	{
		if(Activity::update_message($type, $data) != null){
			return true;
		}else{
			return false;
		}
	}
}