<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    protected $primaryKey = 'id';

    protected $fillable = [
        'name',
        'url',
        'is_active',
        'city_store_id',
        'show_order'
    ];

    public function city() {
        return $this->belongsTo('App\Models\CityStore', 'city_store_id');
    }
}
