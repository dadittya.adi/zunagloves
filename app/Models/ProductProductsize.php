<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductProductsize extends Model
{
    protected $table = 'products_productsizes';
    protected $guarded = ['id'];

    public function product()
    {
    	return $this->belongsTo(
    		'App\Models\Product',
			'product_id'
        );
    }
        
    public function size()
    {
    	return $this->belongsTo(
    		'App\Models\ProductSize',
			'productsize_id'
    	);
    }
}
