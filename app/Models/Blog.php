<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Config;
use StdClass;

class Blog extends Model {

	protected $guarded = ['id'];
	protected $dates = [
		'publish_at',
		'created_at',
		'updated_at',
	];

	public function admin()
	{
		return $this->belongsTo(
            'App\Models\Admin',
            'writen_by'            
        );
	}

	public function tag()
	{
		return $this->belongsToMany('App\Models\Tag', 'blog_tag','blog_id','tag_id');
	}
}