<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Voucher extends Model {

	protected $guarded = ['id'];
	protected $dates = [
		'start_date',
		'end_date'
	];

	public function getStateAttribute()
	{
		$start = $this->start_date;
		$end = $this->end_date;
		$now = Carbon::now();

		$state = null;
		if ($now < $start)
			$state = 'Inactive';
		elseif ($now > $end)
			$state = 'Expired';
		else 
			$state = 'Active';

		return $state;
	}
}