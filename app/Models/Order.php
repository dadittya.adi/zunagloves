<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Mail;
use Auth;
use Carbon\Carbon;
use Config;
use StdClass;

class Order extends Model {

	protected $guarded = ['id'];
	protected $dates = [
		'created_at',
		'paid_at'
	];

	public function items()
	{
		return $this->hasMany(
            'App\Models\OrderItem',
            'order_id'            
        );
	}

	public function customer()
	{
		return $this->belongsTo(
            'App\Models\Customer',
            'customer_id'            
        );
	}

	public function billing()
	{
		return $this->belongsTo(
			'App\Models\Address', 
			'billing_addr'
		);
	}

	public function shipping()
	{
		return $this->belongsTo(
			'App\Models\Address',
			'shipping_addr'
		);
	}

	public function getTotalAttribute()
	{
		$total = 0;
		$proddisc = 0;
		foreach ($this->items as $item) {
			$total += $item->total;
			$proddisc += $item->discount;
		}
		
		$total += ($this->shipping_price * $this->weight);
		$total -= $proddisc;
		$total -= $this->voucher_discount;
		return $total;
	}

	public function getPaymentDataAttribute()
	{
		return json_decode($this->attributes['payment_data'], true);
	}

	public function setPaymentDataAttribute($value)
	{
		$this->attributes['payment_data'] = json_encode($value);
	}

	public function setPaymentData($key, $val)
	{
		$data = $this->payment_data;
		$data[$key] = $val;
		$this->payment_data = $data;
	}

	public function sendNotif($email = null, $name = null, $notes = null) 
	{
		//TODO: priviledge admin
		$admins = Admin::all();
		if (isset($email)) {
			$customer = new Customer([
				'email' => $email,
				'full_name' => $name
			]);
		}
		else {
			$customer = $this->customer;
		}
		$order = $this;

		switch ($order->state) {
			case 'INV':
				//cust
				if ($order->revision == 0) //new invoice
					Mail::send('email.order_invoice', ['customer' => $customer, 'order' => $order], function($message) use ($customer, $order)
					{
						$message->to($customer->email, $customer->full_name)->subject("[Zunagloves] Invoice Order #{$order->order_number}");
					});
				else //invoice revision
					Mail::send('email.order_invoice_revision', ['customer' => $customer, 'order' => $order, 'notes' => $notes], function($message) use ($customer, $order)
					{
						$message->to($customer->email, $customer->full_name)->subject("[Zunagloves] Invoice Order #{$order->order_number} Rev {$order->revision}");
					});

				//admin
				// foreach ($admins as $admin) {
				// 	if ($order->revision == 0) //new invoice
				// 		Mail::send('admin.email.order_invoice', ['customer' => $customer, 'order' => $order], function($message) use ($admin, $order)
				// 		{
				// 			$message->to($admin->email, $admin->name)->subject("[Zuna Sport][Admin] Invoice Order #" . $order->order_number);
				// 		});
				// 	else //invoice revision
				// 		Mail::send('admin.email.order_invoice_revision', ['customer' => $customer, 'order' => $order, 'notes' => $notes], function($message) use ($admin, $order)
				// 		{
				// 			$message->to($admin->email, $admin->name)->subject("[Zuna Sport][Admin] Invoice Order # Rev {$order->revision} " . $order->order_number);
				// 		});
				// }
				break;
			case 'CHK':
				//cust
				Mail::send('email.order_paymentcheck', ['customer' => $customer, 'order' => $order], function($message) use ($customer, $order)
				{
					$message->to($customer->email, $customer->full_name)->subject("[Zunagloves] Order #{$order->order_number} Payment Process/Check");
				});

				//admin
				// foreach ($admins as $admin) {
				// 	Mail::send('email.order_paymentcheck', ['customer' => $customer, 'order' => $order], function($message) use ($admin, $order)
				// 	{
				// 		$message->to($admin->email, $admin->name)->subject("[Zuna Sport][Admin] Order #{$order->order_number} Payment Process/Check");
				// 	});
				// }
				break;
			case 'PAI':
				//cust
				Mail::send('email.order_paid', ['customer' => $customer, 'order' => $order], function($message) use ($customer, $order)
				{
					$message->to($customer->email, $customer->full_name)->subject("[Zunagloves] Order #{$order->order_number} Paid");
				});

				//admin
				// foreach ($admins as $admin) {
				// 	Mail::send('admin.email.order_paid', ['customer' => $customer, 'order' => $order], function($message) use ($admin, $order)
				// 	{
				// 		$message->to($admin->email, $admin->name)->subject("[Zuna Sport][Admin] Order #{$order->order_number} Paid");
				// 	});
				// }
				break;
			case 'WRK':
				//cust
				Mail::send('email.order_work', ['customer' => $customer, 'order' => $order], function($message) use ($customer, $order)
				{
					$message->to($customer->email, $customer->full_name)->subject("[Zunagloves] Order #{$order->order_number} Process");
				});

				//admin
				// foreach ($admins as $admin) {
				// 	Mail::send('admin.email.order_work', ['order' => $order, 'admin' => Auth::admin()->get()], function($message) use ($admin, $order)
				// 	{
				// 		$message->to($admin->email, $admin->name)->subject("[Zuna Sport][Admin] Order #{$order->order_number} Process");
				// 	});
				// }
				break;
			case 'DLV':
				//cust
				Mail::send('email.order_deliver', ['customer' => $customer, 'order' => $order], function($message) use ($customer, $order)
				{
					$message->to($customer->email, $customer->full_name)->subject("[Zunagloves] Order #{$order->order_number} Delivered");
				});

				//admin
				// foreach ($admins as $admin) {
				// 	Mail::send('email.order_deliver', ['order' => $order, 'admin' => Auth::admin()->get()], function($message) use ($admin, $order)
				// 	{
				// 		$message->to($admin->email, $admin->name)->subject("[Zuna Sport][Admin] Order #{$order->order_number} Delivered");
				// 	});
				// }
				break;
			case 'REJ':
				//cust
				Mail::send('email.order_reject', ['customer' => $customer, 'order' => $order], function($message) use ($customer, $order)
				{
					$message->to($customer->email, $customer->full_name)->subject("[Zunagloves] Order #{$order->order_number} Rejected");
				});

				//admin
				// foreach ($admins as $admin) {
				// 	Mail::send('admin.email.order_reject', ['order' => $order, 'admin' => Auth::admin()->get()], function($message) use ($admin, $order)
				// 	{
				// 		$message->to($admin->email, $admin->name)->subject("[Zuna Sport][Admin] Order #{$order->order_number} Rejected");
				// 	});
				// }
				break;
		}
	}

	static public function random($extension = null) 
    {
		$path = Config::get('storage.banknote');
		if (!$path)
			throw Exception("Config not found [{$key}]");
		$ret = new StdClass();
		$try = 250;
		do {
			if ($try <= 0)
				throw Exception("Storage::random fails to produce randomized filename");
			$hash = Str::random(32);
			if ($extension)
				$hash = $hash . '.' . $extension;
			$file = $path . '/' . $hash;
			$try -= 1;
		} while (file_exists($file));
		$ret = array($path, $hash, $file);
		return $ret;
	}

	public function setBanknoteImageAttribute($value)
	{
		$this->attributes['banknote_image'] = null;
		if (!$value->isValid())
			return;

		$ext = $value->getClientOriginalExtension();

		$file_path = static::random($ext);
		$value->move($file_path[0], $file_path[1]);

		$this->attributes['banknote_image'] = $file_path[1];
	}

	public function getBanknoteImageFullPath()
	{
		$value = $this->banknote_image;
		if ($value == null)
			return null;

		$path = Config::get('storage.banknote');		
		return "{$path}/{$value}";
	}

	public function getItem()
	{
		$items = $this->items()->get()
			->map(function($i) {
				return [
					'id' => $i->id,
					'product_name' => $i->product->name,
					'color' => $i->color,
					'size' => $i->size,
					'notes' => $i->notes,
					'price' => (float)$i->price,
					'qty' => (float)$i->qty,
					'total' => $i->total,
					'disc_percent' => $i->disc_percent > 0 ? (float)$i->disc_percent : null,
					'discount' => $i->discount > 0 ? $i->discount : null
				];
			});

		return $items;
	}

	public function getPoFinishAttribute()
	{
		$launchDate = null;

		foreach ($this->items as $item) {
			if ($item->product->launch_date > Carbon::now()) {
				if ($launchDate != null) {
					$launchDate = $item->product->launch_date;
				}
				else {
					if ($launchDate < $item->product->launch_date) {
						$launchDate = $item->product->launch_date;
					}
				}
			}
		}
		
		return $launchDate;
		
	}

	public function scopeActive($query)
	{
		return $query->where('orders.created_at', '>=', '2021/04/20');
	}
}