<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Config;
use StdClass;

class Jumbotron extends Model {
   
    protected $guarded = ['id'];

    static public function random($extension = null) 
    {
		$path = Config::get('storage.jumbotron');
		if (!$path)
			throw Exception("Config not found [{$key}]");
		$ret = new StdClass();
		$try = 250;
		do {
			if ($try <= 0)
				throw Exception("Storage::random fails to produce randomized filename");
			$hash = Str::random(32);
			if ($extension)
				$hash = $hash . '.' . $extension;
			$file = $path . '/' . $hash;
			$try -= 1;
		} while (file_exists($file));
		$ret = array($path, $hash, $file);
		return $ret;
	}

    public function setImageAttribute($value)
	{
		$this->attributes['image'] = null;
		if (!$value->isValid())
			return;

		$mime = $value->getMimeType();
		$ext = $value->getClientOriginalExtension();

		$file_path = static::random($ext);
		$value->move($file_path[0], $file_path[1]);

		$this->attributes['image'] = $file_path[1];
		$this->attributes['mimetype'] = $mime;
	}
	
	public function setImageDetailAttribute($value)
	{
		$this->attributes['image_detail'] = null;
		if (!$value->isValid())
			return;

		$mime = $value->getMimeType();
		$ext = $value->getClientOriginalExtension();

		$file_path = static::random($ext);
		$value->move($file_path[0], $file_path[1]);

		$this->attributes['image_detail'] = $file_path[1];
		$this->attributes['image_detail_mimetype'] = $mime;
	}
	

	public function category()
	{
		return $this->belongsTo(
            'App\Models\Category',
            'link'            
        );
	}

	public function getImageFullPath()
	{
		$value = $this->image;
		if ($value == null)
			return null;

		$path = Config::get('storage.jumbotron');		
		return "{$path}/{$value}";
	}

	public function getImageDetailFullPath()
	{
		$value = $this->image_detail;
		if ($value == null)
			return null;

		$path = Config::get('storage.jumbotron');		
		return "{$path}/{$value}";
	}
}
