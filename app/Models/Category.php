<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Config;
use StdClass;

class Category extends Model {

	protected $guarded = ['id'];

	public function subcategories()
	{
		return $this->hasMany(
            'App\Models\CategorySubcategory',
            'category_id'            
        );
	}

	public function products()
	{
		return $this->hasMany('App\Models\Product');
	}

	public function scopeActive($query)
	{
		return $query->where('is_active', '=', 1);
	}
}