<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Config;
use StdClass;

class Tag extends Model {

	protected $guarded = ['id'];

	public function blog()
	{
		return $this->belongsToMany('App\Models\Blog', 'blog_tag','tag_id','blog_id');
	}
}