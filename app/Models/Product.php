<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

use Config;
use StdClass;
use Carbon\Carbon;

class Product extends Model {

	protected $guarded = ['id'];
	protected $dates = [
		'start_discount',
		'end_discount',
		'launch_date'
	];

	public function subcategory()
	{
		return $this->belongsTo(
            'App\Models\CategorySubcategory',
            'subcategory_id'            
        );
	}

	public function category()
	{
		return $this->belongsTo(
            'App\Models\Category',
            'category_id'            
        );
	}

	public function sizes()
    {
    	return $this->belongsToMany(
    		'App\Models\ProductSize',
			'products_productsizes',
			'product_id',
			'productsize_id'
    	)->withTimestamps();
    }

    public function colors()
    {
    	return $this->hasMany(
    		'App\Models\ProductColor',
			'product_id'
    	);
	}

	public function productSizes() 
	{
		return $this->hasMany(
			'App\Models\ProductProductsize',
			'product_id'
		);
	}
	
	public function images()
    {
    	return $this->hasMany(
    		'App\Models\ProductImage',
			'product_id'
    	);
    }

	public function productStok()
	{
		return $this->hasMany(
			'App\Models\ProductStock',
			'product_id'
		);
	}

	public function scopeActive($query)
	{
		return $query->where('products.is_active', '=', 1);
	}

	public function getpriceDiscountAttribute()
	{
		$disc_percent = $this->disc_percent;

		$value = null;
		if ($disc_percent && $disc_percent > 0 && $this->price > 0) {
			$disc_amount = $this->price * $disc_percent / 100;
			$value = $this->price - $disc_amount;
		}

		return $value;
	}

	public function getDiscountAttribute()
	{
		$value = $this->disc_percent;
		$now = Carbon::now();

		$disc = 0;

		if ($now >= $this->start_discount && $now <= $this->end_discount) {
			$disc = $value;
		}
		return $disc;
	}

	public function scopeFindCategory($query, $id)
	{
		return $query->select('products.*')
			->join('category_subcategories', 'category_subcategories.id', '=', 'products.subcategory_id')
			->join('categories', 'categories.id', '=', 'category_subcategories.category_id')
			->where('categories.id', '=', $id);
	}

	public function getPriceAttribute()
	{
		$domain = $locale = Config::get('app.domain');
		$price = 'price_' . $domain;

		return $this->$price;
	}

	public function getSizeListAttribute()
	{
		$sizes = $this->sizes()->orderBy('order')->get();
		$value = [];
		foreach ($sizes as $size) {
			$value[$size->id] = $size->short_name . ' - ' . $size->name;
		}

		return $value;
	}

	static public function random($extension = null) 
    {
		$path = Config::get('storage.guide');
		if (!$path)
			throw Exception("Config not found [{$key}]");
		$ret = new StdClass();
		$try = 250;
		do {
			if ($try <= 0)
				throw Exception("Storage::random fails to produce randomized filename");
			$hash = Str::random(32);
			if ($extension)
				$hash = $hash . '.' . $extension;
			$file = $path . '/' . $hash;
			$try -= 1;
		} while (file_exists($file));
		$ret = array($path, $hash, $file);
		return $ret;
	}

    public function setGuideImageAttribute($value)
	{
		$this->attributes['guide_image'] = null;
		if (!$value->isValid())
			return;

		$ext = $value->getClientOriginalExtension();

		$file_path = static::random($ext);
		$value->move($file_path[0], $file_path[1]);

		$this->attributes['guide_image'] = $file_path[1];
	}

	public function getGuideImageFullPath()
	{
		$value = $this->guide_image;
		if ($value == null)
			return null;

		$path = Config::get('storage.guide');		
		return "{$path}/{$value}";
	}

	public function getInStockAttribute()
	{
		$inStock = true;

		$stock = ProductStock::where('product_id', $this->id)
						->where('stock', '>', '0')
						->count();
		
		if($stock == 0)
		{
			$inStock = false;
		}

		return $inStock;
	}

	public function getCountdownAttribute()
	{
		$diff = 0;
		if ($this->launch_date != '') {
			$launchDate = strtotime($this->launch_date);
			$now = strtotime(now());
			if ($launchDate > $now) {
				$diff = $launchDate - $now;
			}
		}
		
		
		return $diff;
		
	}

	public function getStatusAttribute()
	{
		$status = $this->is_active;

		
		if ($this->is_active == 2 || $this->is_active == 3) {
			if ($this->launch_date < Carbon::now()) {
				$_prod = Product::find($this->id);
				$_prod->is_active = 1;
				$_prod->save();

				$status = 1;
			}
		}
		else {
			
			if($this->launch_date == '') {
				$launchDate = $this->created_at;
				$_prod = Product::find($this->id);
				$_prod->launch_date = $launchDate;
				$_prod->save();
			}
			else {
				$launchDate = $this->launch_date;
			}

			$launchDate =  Carbon::parse($launchDate);

			if(($launchDate->diffInDays(Carbon::now())) <= 30) {
				$status = 4;
			}
		}


		return $status;
		
	}
}