<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

use Config;
use StdClass;

class ProductColor extends Model {

	protected $guarded = ['id'];
	protected $touches = ['product'];

	public function product()
    {
    	return $this->belongsTo(
    		'App\Models\Product',
			'product_id'
    	);
    }

    public function colorImages()
    {
    	return $this->hasMany(
    		'App\Models\ProductColorImage',
			'product_color_id'
    	);
	}
	
	public function productStocks()
	{
		return $this->hasMany(
			'App\Models\ProductStock',
			'product_color_id'
		);
	}

    static public function random($path, $extension = null) 
    {
		if (!$path)
			throw Exception("Config not found [{$key}]");
		$ret = new StdClass();
		$try = 250;
		do {
			if ($try <= 0)
				throw Exception("Storage::random fails to produce randomized filename");
			$hash = Str::random(32);
			if ($extension)
				$hash = $hash . '.' . $extension;
			$file = $path . '/' . $hash;
			$try -= 1;
		} while (file_exists($file));
		$ret = array($path, $hash, $file);
		return $ret;
	}

    public function setColorImageAttribute($value)
	{
		$this->attributes['color_image'] = null;
		if (!$value->isValid())
			return;

		$ext = $value->getClientOriginalExtension();
		$path = Config::get('storage.color');

		$file_path = static::random($path, $ext);
		$value->move($file_path[0], $file_path[1]);

		$this->attributes['color_image'] = $file_path[1];
	}

	public function setProductImageAttribute($value)
	{
		$this->attributes['product_image'] = null;
		if (!$value->isValid())
			return;

		$ext = $value->getClientOriginalExtension();
		$path = Config::get('storage.product');

		$file_path = static::random($path, $ext);
		$value->move($file_path[0], $file_path[1]);

		$this->attributes['product_image'] = $file_path[1];
	}

	public function uploadImage($value){
		if (!$value->isValid())
			return;
		
		$ext = $value->getClientOriginalExtension();
		$path = Config::get('storage.product');

		$file_path = static::random($path, $ext);
		//var_dump($value->getPathName()); var_dump($file_path);
		$value->move($file_path[0], $file_path[1]);
		return $file_path[1];
	}

	public function getColorImageFullPath()
	{
		$value = $this->color_image;
		if (!$value)
			return null;

		$path = Config::get('storage.color');		
		return "{$path}/{$value}";
	}

	public function getProductImageFullPath()
	{
		$value = $this->product_image;
		if (!$value)
			return null;

		$path = Config::get('storage.product');		
		return "{$path}/{$value}";
	}
}