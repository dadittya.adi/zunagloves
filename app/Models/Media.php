<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Config;

class Media extends Model {

	protected $guarded = ['id'];
	protected $dates = ['upload_at'];

	public function getMediaFullPath()
	{
		$value = $this->filename;
		$upload_at = $this->attributes['upload_at'];
		$year = Carbon::createFromFormat('Y-m-d H:i:s', $upload_at)->year;

		if ($value == null)
			return null;

		$path = "media/{$year}/{$value}";
		return asset($path);
	}
}