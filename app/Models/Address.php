<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Address extends Model {

	protected $guarded = ['id'];

	public function cityb()
	{
		return $this->belongsTo(
            'App\Models\City',
            'city'            
        );
	}

	public function provinceb()
	{
		return $this->belongsTo(
            'App\Models\Province',
            'province'            
        );
	}

	public function sub_districtb()
	{
		return $this->belongsTo(
            'App\Models\SubDistrict',
            'sub_district'            
        );
	}


}