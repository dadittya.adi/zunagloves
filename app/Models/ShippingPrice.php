<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShippingPrice extends Model {

	protected $guarded = ['id'];

	public function shipping()
	{
		return $this->belongsTo(
            'App\Models\Shipping',
            'shipping_id'            
        );
	}
}