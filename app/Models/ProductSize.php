<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductSize extends Model {

	protected $table = 'productsizes';
	protected $guarded = ['id'];
	
	public function products()
    {
    	return $this->belongsToMany(
    		'App\Models\Product',
			'products_productsizes',
			'productsize_id',
			'product_id'
    	)->withTimestamps();
	}
	
}