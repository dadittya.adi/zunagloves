<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Config;

class ProductColorImage extends Model
{
    protected $guarded = ['id'];
    protected $touches = ['productColor'];

    public function productColor()
    {
    	return $this->belongsTo(
    		'App\Models\ProductColor',
			'product_color_id'
    	);
    }

    public function getProductImageFullPath()
	{
		$value = $this->image;
		if (!$value)
			return null;

		$path = Config::get('storage.product');		
		return "{$path}/{$value}";
	}
}
