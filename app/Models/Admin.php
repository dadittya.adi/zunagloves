<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class Admin extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    protected $guarded = ['id'];
	protected $hidden = ['password', 'remember_token'];
	protected $_cache = [];

	public function blog()
	{
		return $this->hasMany(
            'App\Models\Blog',
            'writen_by'            
        );
	}

    public function groups()
    {
        return $this->belongsToMany(
            'App\Models\Group',
            'admin_groups',
            'admin_id',
            'group_id'
        );
    }

    public function hasGroup($code)
    {
        return $this->groups()->where('code',$code)->count();
    }
}
