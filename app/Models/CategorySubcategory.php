<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Config;
use StdClass;

class CategorySubcategory extends Model {

	protected $guarded = ['id'];

	public function category()
	{
		return $this->belongsTo(
            'App\Models\Category',
            'category_id'            
        );
	}

	public function products()
	{
		return $this->hasMany(
            'App\Models\Product',
            'subcategory_id'            
        );
	}

	static public function random($extension = null) 
    {
		$path = Config::get('storage.icon');
		if (!$path)
			throw Exception("Config not found [{$key}]");
		$ret = new StdClass();
		$try = 250;
		do {
			if ($try <= 0)
				throw Exception("Storage::random fails to produce randomized filename");
			$hash = Str::random(32);
			if ($extension)
				$hash = $hash . '.' . $extension;
			$file = $path . '/' . $hash;
			$try -= 1;
		} while (file_exists($file));
		$ret = array($path, $hash, $file);
		return $ret;
	}

    public function setIconAttribute($value)
	{
		$this->attributes['icon'] = null;
		if (!$value->isValid())
			return;

		$ext = $value->getClientOriginalExtension();

		$file_path = static::random($ext);
		$value->move($file_path[0], $file_path[1]);

		$this->attributes['icon'] = $file_path[1];
	}

	public function getIconFullPath()
	{
		$value = $this->icon;
		if ($value == null)
			return null;

		$path = Config::get('storage.icon');		
		return "{$path}/{$value}";
	}
}