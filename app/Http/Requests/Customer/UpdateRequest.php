<?php namespace App\Http\Requests\Customer;

use App\Http\Requests\Request;

class UpdateRequest extends Request {

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function authorize()
	{
		return true;
	}

	public function rules()
	{
		$id = intval($this->route()->getParameter('id'));

		return [
			'full_name' => 'required|max:255',
			'phone' => 'required|max:255',
			'email' => 'required|email|unique:customers,email,' . $id,
			'organization' => 'max:255',
			'address' => 'max:255',
			'city' => 'max:255',
			'province' => 'max:255',
			'country' => 'max:255',
			'postal_code' => 'max:255',
			'state' => 'required|in:NEW,ACT,BAN'
		];
	}

	public function response(array $errors)
	{
	    return $this->redirector->to($this->getRedirectUrl())
	         ->withInput($this->except($this->dontFlash))
	         ->withErrors($errors, $this->errorBag)
	         ->with('UPDATE.FAIL', true);
	}
	
}
