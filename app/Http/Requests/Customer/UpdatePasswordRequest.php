<?php namespace App\Http\Requests\Customer;

use App\Http\Requests\Request;

class UpdatePasswordRequest extends Request {

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function authorize()
	{
		return true;
	}

	public function rules()
	{
		return [
			'current_password' => 'required',
			'password' => 'required|min:8',
			'password_confirm' => 'required|same:password'
		];
	}

	public function response(array $errors)
	{
	    return $this->redirector->to($this->getRedirectUrl())
	         ->withInput($this->except($this->dontFlash))
	         ->withErrors($errors, $this->errorBag)
	         ->with('UPDATE_PASS.FAIL', true);
	}
	
}
