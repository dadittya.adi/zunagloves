<?php namespace App\Http\Requests\Customer;

use App\Http\Requests\Request;

class ConfirmPaymentRequest extends Request {

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function authorize()
	{
		return true;
	}

	public function rules()
	{
		return [
			'paid_at_date' => 'required',
			'paid_at_hour' => 'required|numeric',
			'paid_at_minute' => 'required|numeric',
			'paid_amount' => 'required|numeric',
			'bank_acc' => 'required|max:255',
			'name_acc' => 'required|max:255',
			'no_acc' => 'required|max:255'
		];
	}

	public function response(array $errors)
	{
		$id = intval($this->route()->getParameter('id'));

	    return $this->redirector->to($this->getRedirectUrl())
	         ->withInput($this->except($this->dontFlash))
	         ->withErrors($errors, $this->errorBag)
	         ->with('CONFIRM_PAY.FAIL', true)
	         ->with('error_id', $id);
	}
	
}
