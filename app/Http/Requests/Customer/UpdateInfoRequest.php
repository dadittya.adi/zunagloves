<?php namespace App\Http\Requests\Customer;

use App\Http\Requests\Request;
use Auth;

class UpdateInfoRequest extends Request {

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function authorize()
	{
		if (Auth::customer()->guest())
			return false;

		return true;
	}

	public function rules()
	{
		$user = Auth::customer()->get();

		return [
			'full_name' => 'required|max:255',
			'phone' => 'required|max:255',
			'email' => 'required|email|unique:customers,email,' . $user->id,
		];
	}

	public function response(array $errors)
	{
	    return $this->redirector->to($this->getRedirectUrl())
	         ->withInput($this->except($this->dontFlash))
	         ->withErrors($errors, $this->errorBag)
	         ->with('UPDATE_INFO.FAIL', true);
	}
	
}
