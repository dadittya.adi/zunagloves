<?php namespace App\Http\Requests\Jumbotron;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest {

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function authorize()
	{
		return true;
	}

	public function rules()
	{
		return [
			'title' => 'required|max:255',
			'image' => 'required',
			//'state' => 'required|in:DRF,PUB'
		];
	}

	public function response(array $errors)
	{
	    return $this->redirector->to($this->getRedirectUrl())
	         ->withInput($this->except($this->dontFlash))
	         ->withErrors($errors, $this->errorBag)
	         ->with('UPDATE.FAIL', true);
	}
	
}
