<?php namespace App\Http\Requests\Blog;

use App\Http\Requests\Request;

class StoreRequest extends Request {

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function authorize()
	{
		return true;
	}

	public function rules()
	{
		return [
			'title' => 'required|unique:blogs|max:255',
			'permalink' => 'required|unique:blogs|max:255|regex:/^[a-z0-9\-_]*$/',
			'content' => 'required|max:65535',
			'publish_state' => 'required|in:DRF,PUB',
		];
	}

	public function response(array $errors)
	{
	    return $this->redirector->to($this->getRedirectUrl())
	         ->withInput($this->except($this->dontFlash))
	         ->withErrors($errors, $this->errorBag)
	         ->with('UPDATE.FAIL', true);
	}
	
}
