<?php namespace App\Http\Requests\Product;

use Illuminate\Foundation\Http\FormRequest;

class EmailRequest extends FormRequest {

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function authorize()
	{
		return true;
	}

	public function rules()
	{
		return [
			'name' => 'required',
			'email' => 'required|email',
			'message' => 'required'
		];
	}

	public function response(array $errors)
	{
	    return $this->redirector->to($this->getRedirectUrl())
	         ->withInput($this->except($this->dontFlash))
	         ->withErrors($errors, $this->errorBag)
	         ->with('SEND_MAIL.FAIL', true);
	}
	
}
