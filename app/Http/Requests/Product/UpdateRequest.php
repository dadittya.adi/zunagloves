<?php namespace App\Http\Requests\Product;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest {

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function authorize()
	{
		return true;
	}

	public function rules()
	{
		return [
			'name' => 'required|max:255',
			'is_active' => 'required',
			'sku' => 'max:127',
			'genre' => 'required|in:MEN,WMN,UNI,KID',
			'description' => 'required|max:4096',
			'price_id' => 'required|numeric',
			'price_en' => 'required|numeric',
			'disc_percent' => 'nullable|numeric',
			'start_discount' => 'required_with:disc_percent',
			'end_discount' => 'required_with:disc_percent',
			'weight' => 'required|numeric|min:1'
		];
	}

	public function response(array $errors)
	{
	    return $this->redirector->to($this->getRedirectUrl())
	         ->withInput($this->except($this->dontFlash))
	         ->withErrors($errors, $this->errorBag)
	         ->with('UPDATE.FAIL', true);
	}
	
}
