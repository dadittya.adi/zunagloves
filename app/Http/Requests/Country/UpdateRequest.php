<?php namespace App\Http\Requests\Country;

use App\Http\Requests\Request;

class UpdateRequest extends Request {

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function authorize()
	{
		return true;
	}

	public function rules()
	{
		$id = intval($this->route()->getParameter('id'));

		return [
			'name_edit' => 'required|max:255|unique:countries,name,' . $id
		];
	}

	public function response(array $errors)
	{
		$id = intval($this->route()->getParameter('id'));

	    return $this->redirector->to($this->getRedirectUrl())
	         ->withInput($this->except($this->dontFlash))
	         ->withErrors($errors, $this->errorBag)
	         ->with('UPDATE.FAIL', true)
	         ->with('error_id', $id);
	}
	
}
