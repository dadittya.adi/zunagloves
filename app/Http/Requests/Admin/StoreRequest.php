<?php namespace App\Http\Requests\Admin;

use App\Http\Requests\Request;

class StoreRequest extends Request {

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function authorize()
	{
		return true;
	}

	public function rules()
	{
		return [
			'full_name' => 'required|max:255',
			'email' => 'required|email|max:255|unique:admins,email',
			'password' => 'required|max:127',
			'confirm_password' => 'required|max:127|same:password',
			'is_active' => 'required|boolean',			
			'groups' => 'required|json_array_min:1'
		];
	}

	public function response(array $errors)
	{
	    return $this->redirector->to($this->getRedirectUrl())
	         ->withInput($this->except($this->dontFlash))
	         ->withErrors($errors, $this->errorBag)
	         ->with('UPDATE.FAIL', true);
	}
	
}
