<?php namespace App\Http\Requests\Shipping;

use App\Http\Requests\Request;

class UpdateRequest extends Request {

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function authorize()
	{
		return true;
	}

	public function rules()
	{
		$id = intval($this->route()->getParameter('id'));

		return [
			'name' => 'required|max:255|unique:shippings,name,' . $id,
			'display_name' => 'required|max:255',
			'country' => 'required|numeric'
		];
	}

	public function response(array $errors)
	{
	    return $this->redirector->to($this->getRedirectUrl())
	         ->withInput($this->except($this->dontFlash))
	         ->withErrors($errors, $this->errorBag)
	         ->with('UPDATE.FAIL', true);
	}
	
}
