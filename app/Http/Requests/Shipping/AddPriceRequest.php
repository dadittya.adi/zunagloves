<?php namespace App\Http\Requests\Shipping;

use App\Http\Requests\Request;

class AddPriceRequest extends Request {

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function authorize()
	{
		return true;
	}

	public function rules()
	{
		return [
			'destination' => 'required|max:255|unique:shipping_prices,destination',
			'price' => 'required|numeric',
			'time' => 'required|numeric|min:1'
		];
	}

	public function response(array $errors)
	{
	    return $this->redirector->to($this->getRedirectUrl())
	         ->withInput($this->except($this->dontFlash))
	         ->withErrors($errors, $this->errorBag)
	         ->with('UPDATE.FAIL', true);
	}
	
}
