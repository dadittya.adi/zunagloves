<?php namespace App\Http\Requests\Shipping;

use App\Http\Requests\Request;

class UpdatePriceRequest extends Request {

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function authorize()
	{
		return true;
	}

	public function rules()
	{
		$id = intval($this->route()->getParameter('priceId'));

		return [
			'destination_edit' => 'required|max:255|unique:shipping_prices,destination,' . $id,
			'price_edit' => 'required|numeric',
			'time_edit' => 'required|numeric|min:1'
		];
	}

	public function response(array $errors)
	{
		$id = intval($this->route()->getParameter('priceId'));
		
	    return $this->redirector->to($this->getRedirectUrl())
	         ->withInput($this->except($this->dontFlash))
	         ->withErrors($errors, $this->errorBag)
	         ->with('UPDATE.FAIL', true)
	         ->with('error_id', $id);
	}
	
}
