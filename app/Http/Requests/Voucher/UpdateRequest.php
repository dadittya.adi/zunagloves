<?php namespace App\Http\Requests\Voucher;

use App\Http\Requests\Request;
use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest {

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function authorize()
	{
		return true;
	}

	public function rules()
	{
		$id = intval($this->route()->parameter('id'));
		return [
			'code' => 'required|min:5|max:25|unique:vouchers,code,' . $id,
			'promo' => 'required|max:255|unique:vouchers,promo,' . $id,
			'type' => 'required',
			'discount' => 'required|numeric|min:1',
			'start_date' => 'required',
			'end_date' => 'required',
			'minimum' => 'numeric'
		];
	}

	public function response(array $errors)
	{
	    return $this->redirector->to($this->getRedirectUrl())
	         ->withInput($this->except($this->dontFlash))
	         ->withErrors($errors, $this->errorBag)
	         ->with('UPDATE.FAIL', true);
	}
	
}
