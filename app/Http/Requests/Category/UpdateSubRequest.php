<?php namespace App\Http\Requests\Category;

use App\Http\Requests\Request;

class UpdateSubRequest extends Request {

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function authorize()
	{
		return true;
	}

	public function rules()
	{
		return [
			'subcategory_name_edit' => 'required|max:127'
		];
	}

	public function response(array $errors)
	{
		$id = intval($this->route()->getParameter('subId'));

	    return $this->redirector->to($this->getRedirectUrl())
	         ->withInput($this->except($this->dontFlash))
	         ->withErrors($errors, $this->errorBag)
	         ->with('UPDATE.FAIL', true)
	         ->with('error_id', $id);
	}
	
}
