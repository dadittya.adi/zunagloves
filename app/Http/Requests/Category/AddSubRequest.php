<?php namespace App\Http\Requests\Category;

use App\Http\Requests\Request;

class AddSubRequest extends Request {

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function authorize()
	{
		return true;
	}

	public function rules()
	{
		return [
			'subcategory_name' => 'required|max:127',
			'icon' => 'required'
		];
	}

	public function response(array $errors)
	{
	    return $this->redirector->to($this->getRedirectUrl())
	         ->withInput($this->except($this->dontFlash))
	         ->withErrors($errors, $this->errorBag)
	         ->with('UPDATE.FAIL', true)
	         ->with('ADD_SUB.FAIL', true);
	}
	
}
