<?php namespace App\Http\Requests\CityStore;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest {

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function authorize()
	{
		return true;
	}

	public function rules()
	{
		$id = intval($this->route()->parameter('id'));

		return [
			'name' => 'required|max:127|unique:city_stores,name,' . $id,
			'is_active' => 'required|boolean'
		];
	}

	public function response(array $errors)
	{
	    return $this->redirector->to($this->getRedirectUrl())
	         ->withInput($this->except($this->dontFlash))
	         ->withErrors($errors, $this->errorBag)
	         ->with('UPDATE.FAIL', true);
	}
	
}
