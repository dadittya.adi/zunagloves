<?php namespace App\Http\Requests\ProductColor;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest {

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function authorize()
	{
		return true;
	}

	public function rules()
	{
		return [
			'name' => 'required|max:127',
			'type' => 'required|in:COLOR,TEXTURE',
			'product_name' => 'required',
			'color_image' => 'required',
			'product_image' => 'required',
			'sku' => 'required|unique:product_colors,sku'
		];
	}

	public function response(array $errors)
	{
	    return $this->redirector->to($this->getRedirectUrl())
	         ->withInput($this->except($this->dontFlash))
	         ->withErrors($errors, $this->errorBag)
	         ->with('UPDATE.FAIL', true);
	}
	
}
