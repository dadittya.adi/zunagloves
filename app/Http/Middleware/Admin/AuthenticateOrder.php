<?php

namespace App\Http\Middleware\Admin;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Auth;

class AuthenticateOrder
{
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::guard('admins')->guest()) {
            if ($request->ajax()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->guest('/admin/auth/login');
            }
        }
        if (!(Auth::guard('admins')->get()->hasGroup('ORD') || Auth::admin()->get()->hasGroup('ADM'))) {
            if ($request->ajax()) {
                return response('Forbidden.', 403);
            } else {
                return abort(403);
            }
        }

        return $next($request);
    }
}