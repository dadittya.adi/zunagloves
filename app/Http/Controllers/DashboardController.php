<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\Order;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function home(Request $request)
    {   
        //update expired transaction

        $orders = Order::where('created_at', '<', Carbon::now()->subHour(24))
                        ->where('state', 'INV')->get();

        foreach ($orders as $order) {
            $temp = Order::find($order->id);
            $temp->state = 'EXP';
            $temp->save();
        }

        $transactions = Order::where('customer_id', Auth::guard('web')->user()->id)
                                ->orderBy('id', 'desc');

        $status = 'all';
        $keyword = '';
        if ($request->has('status')) {
            $status = $request->get('status');
            if ($status != 'all') {
                $transactions = $transactions->where('state', $status);
            }
        }

        if ($request->has('keyword')) {
            $keyword = $request->get('keyword');
            $transactions = $transactions->where('order_number', 'like', '%' . $keyword . '%');
        }

        $transactions = $transactions->get();

        return view('user.home')
                ->with('transactions', $transactions)
                ->with('keyword', $keyword)
                ->with('status', $status);
    }

    public function profile()
    {   
        $customer = Customer::find( Auth::guard('web')->user()->id);

        return view('user.profile')
            ->with('customer', $customer);
    }

    public function updateProfile(Request $request)
    {   
        $customer = Customer::find( Auth::guard('web')->user()->id);

        $customer->phone = $request->phone;
        $customer->save();

        $request->session()->flash('status','Your profile has been successfully updated!');

        return redirect()->route('dashboard.profile');
    }

    public function updatePassword(Request $request)
    {   
        $customer = Customer::find( Auth::guard('web')->user()->id);

        $this->validate($request, [
			'password_current' => 'required',
			'password_new' => 'required|different:password_current',
			'password_confirm' => 'required|same:password_new',
		]);

        $customer->password = bcrypt($request->password);
        $customer->save();

        $request->session()->flash('password','Your password has been successfully updated!');

        return redirect()->route('dashboard.profile');
    }

    public function address()
    {   
        $customer = Customer::find( Auth::guard('web')->user()->id);

        return view('user.address')
            ->with('customer', $customer);
    }

    public function updateAddress(Request $request)
    {   
        $customer = Customer::find( Auth::guard('web')->user()->id);

        $customer->province = $request->province;
        $customer->city = $request->city;
        $customer->sub_district = $request->district;
        $customer->postal_code = $request->postal_code;
        $customer->save();

        $request->session()->flash('address-updated','Your address has been successfully updated!');

        return view('user.address')
            ->with('customer', $customer);
    }
}
