<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \Mailjet\Resources;
use Illuminate\Support\Facades\Mail;

class EmailController extends Controller
{
    public function testMail() {
        // $data = [];
        // Mail::send('email.reminder_test', ['data' => null], function ($message) {
        //     $message->to('dadittya.id@gmail.com', 'dadittya')->subject("Welcome!");
        // });

        $mj = new \Mailjet\Client('79d88dec8fa87290816a643e43533624','4a73bddc5e7b689f4911487a47a2c4e6',true,['version' => 'v3.1']);
        $body = [
          'Messages' => [
            [
              'From' => [
                'Email' => "order@zunagloves.com",
                'Name' => "zuna"
              ],
              'To' => [
                [
                  'Email' => "dadittya.adi@gmail.com",
                  'Name' => "dadittya"
                ]
              ],
              'Subject' => "Greetings from Mailjet.",
              'TextPart' => "My first Mailjet email",
              'HTMLPart' => "<h3>Dear passenger 1, welcome to <a href='https://www.mailjet.com/'>Mailjet</a>!</h3><br />May the delivery force be with you!",
              'CustomID' => "AppGettingStartedTest"
            ]
          ]
        ];
        $response = $mj->post(Resources::$Email, ['body' => $body]);
        $response->success() && var_dump($response->getData());
    }
}
