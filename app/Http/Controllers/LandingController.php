<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Str;
use App\Models\Category;
use App\Models\CityStore;
use App\Models\DiscountRequest;
use App\Models\Product;
use App\Models\ProductColor;
use App\Models\ProductColorImage;
use App\Models\Jumbotron;
use App\Models\ProductImage;
use App\Models\ProductStock;
use App\Models\PromoPopUp;
use Illuminate\Support\Facades\Mail;
use App\Models\SubscribeEmail;
use App\Models\Voucher;
use Carbon\Carbon;
use Session;
use Jenssegers\Agent\Agent;

class LandingController extends Controller
{
    public function __construct()
    {

    }

    public function home(Request $request)
    {

        // Dummy Data
        $gallery = [
            [
                'title' => '#ZunaGloves',
                'image' => 'images/About-ZUna-gloves-43-1.jpg',
                'desc'  => '17 Agustus merupakan hari kelahiran bangsa Indonesia, dan hari yang sama di tahun 2020 akan menyaksikan ZUNA GLOVES lahir kembali dengan makna, tampilan dan lifestyle yang baru bagimu.
                            ZUNA GLOVES akan selalu menyajikan sarung tangan terbaik untuk segala kegiatan anda, dari kegemaran travelling-mu, memancing dengan kawan2, touring motor dengan teman se-hobby sampai gowes santai atau racing, ajang kompetisi di berkuda bahkan melindungi tangan2 tim rescuers Indonesia!
                            ZUNA artinya berkelimpahan tanpa batas. Maka kami akan merancang segala macam sarung tangan bagi segala kegiatanmu tanpa batas! 17th August is Indonesia’s National Day, and today it will witness the rebirth of ZUNA GLOVES with a new meaning, image and lifestyle created for you!',
                'link' => 'all' 
            ],
            [
                'title' => 'Zuna For Cycling',
                'image' => 'images/sepeda-4.jpg',
                'desc'  => 'Pak Andi dan Ibu Ina merupakan couple pesepeda sejak tahun 2006. Saat itu Pak Andi baru saja pulih dari penyakit stroke yang dimilikinya. Beliau memilih bersepeda sebagai pilihan olahraga agar tetap bugar dan sehat. Motivasi dari Pak Andi untuk tetap bersepeda yaitu ingin selalu sehat dan panjang umur. Pak Andi berharap dapat memberi semangat kepada kawan-kawan yang pernah mengalami sakit stroke, bahwasanya juga masih bisa bersepeda dengan semangat agar tetap sehat. Pak Andi serta Ibu Ina telah memakai sarung tangan ZUNA untuk menunjang hobi mereka. “Menurut kami yang hobi bersepeda sarung tangan ZUNA sangat nyaman baik dari segi bahan dapat nyerep keringat, lalu warna dan model sangat variatif, kami sangat suka memakainya”',
                'link' => 'Cycling Gloves' 
            ],
            [
                'title' => 'Zuna For Riding',
                'image' => 'images/ojek-online-1.jpg',
                'desc'  => 'Driver Gojek yang satu ini merupakan Ibu Niken. Menurut Ibu Niken mencari lapangan pekerjaan untuk saat ini semakin sulit sehingga beliau memilih menjadi driver untuk memenuhi kebutuhan rumah tangga. Sebagai driver, menurut Ibu Niken sarung tangan bukan hanya sekedar aksesoris, melainkan merupakan komponen penting dalam berkendara. Selain melindungi kulit dari teriknya matahari, sarung tangan dengan bahan yang tepat juga dapat melindungi dari gesekan kulit agar tidak lecet.“ZUNA emang mantap, enak dipakai buat driver bahanya juga bagus, terima kasih ZUNA” pungkas Ibu Niken setelah memakai sarung tangan ZUNA.',
                'link' => 'Motorbike Gloves'
            ],
            [
                'title' => 'Zuna For Horse Riding',
                'image' => 'images/horseriding-4.jpg',
                'desc'  => 'Ivana Putri Santosa merupakan atlet muda berprestasi di bidang Horse Riding atau berkuda. Ketertarikan dalam berkuda sudah tumbuh sejak Ivana masih duduk di bangku taman kanak-kanak. Cita-cita Ivana sendiri yaitu dapat mengharumkan nama Indonesia di kancah Internasional. Ivana mengatakan, dalam berkuda selain helm dan boots, sarung tangan merupakan komponen terpenting dalam berkuda. Sarung tangan dapat melindungi gesekan antar kulit agar tidak lecet dan cidera saat memegang kendali kuda (reins).',
                'link' => 'Horse Riding Gloves'
            ],
            [
                'title' => 'Zuna For Heavy Duty',
                'image' => 'images/resquer-1.jpg',
                'desc'  => 'Roy Saifudin merupakan seorang penggiat alam bebas profesional sekaligus Rescuer sejak tahun 2005. Bapak Roy memilih profesi ini karena mengikuti kata hati. Dalam menjalankan tugas, Bapak Roy selalu melakukanya dengan penuh tanggung jawab dari hati yang paling dalam. Dalam menjelajah alam bebas atau sedang melakukan tugas sebagai Rescuer sarung tangan merupakan komponen penting agar selalu aman serta nyaman. Untuk memenuhi kebutuhan sarung tangan, Bapak Roy sudah menggunakan sarung tangan ZUNA. “Comfortable dan safety saat dipakai aktifitas berat. Cukup nyaman dan sesuai dengan iklim tropis” terang Bapak Roy saat memakai sarung tangan ZUNA.',
                'link' => 'Outdoor Gloves'
            ],
            [
                'title' => 'Zuna For Billiard',
                'image' => 'images/billiard-8.jpg',
                'desc'  => 'Pak Welly merupakan salah satu penikmat hobi billiard. Biilliard sudah menjadi pilihan hidup Pak Welly. Saat ini Pak Welly mengelola berbagai tempat billiard di Semarang, Magelang hingga Yogyakarta. Selain itu, Pak Welly juga terdaftar sebagai atlet billiard Yogyakarta. Momen yang paling berkesan ketika beliau berhasil memenangkan medali emas di PORDA DIY dan menjadi juara 1 di event skala Nasional. Dalam urusan sarung tangan, Pak Welly sudah menggunakan sarung tangan ZUNA untuk bermain billiard. “Enak, elastis, tidak panas, nyaman , dan satu lagi keren ada berbagai model yang lifestyle namun tetap dalam standar yang nyaman” ungkap Pak Welly.',
                'link' => 'Billiard Gloves'
            ],
            [
                'title' => '#HandMade',
                'image' => 'images/handmade-baru.jpg',
                'desc'  => 'Di balik layar ZUNA GLOVES adalah tim produksi sarung tangan empat musim untuk negara2 asing sedunia selama lebih dari 30 tahun! Dari sarung tangan golf, batting, ski, memancing, sepeda, trekking, berburu, fotografi, bahkan sarung tangan militer! Pengalaman selama 30 tahun lebih ini yang mampu menciptakan sarung tangan berkualitas tinggi, dengan pilihan bahan baku berteknologi terbaru dan fitting yang terbaik. Sehingga mendorong ZUNA GLOVES untuk menciptakan sarung tangan untuk semua orang. Behind ZUNA GLOVES is a gloves manufacturing plant producing all sorts of gloves for global consumers for more than 30 years! Ranging from golf, batting, ski, fishing, cycling, trekking, hunting, photography to even military! ',
                'link' => 'all'
            ],
            [
                'title' => 'Zuna For Protection',
                'image' => 'images/ibu-rumah-tangga-6.jpg',
                'desc'  => 'Sebagai ibu rumah tangga yang memiliki 4 anak yang sudah mandiri. Bu Dewi tetap menjalankan perannya sebagai Ibu rumah tangga yang baik. Beliau mengajarkan anak-anaknya untuk tetap taat pada agama, rajin beribadah namun tidak mengabaikan pendidikan. Bahkan salah satu anaknya mendapatkan bea siswa ke Luar Negeri. Itu semua bisa dilakukan Bu Dewi karena pandai dalam mengatur waktu. Selain mengurus rumah tangga, Bu Dewi juga aktif di acara keagamaan dan juga sosial. Penyayang binatang dan sangat ramah. “Menurut saya harus pakai. Karena tangan adalah bagian penting buat ibu rumah tangga. Kalau dipegang tidak kasar dan kering. Selain itu dapat melindungi dari kuman dan virus” ungkap Bu Dewi saat memakai sarung tangan ZUNA"',
                'link' => 'all'
            ],
            [
                'title' => 'Zuna For Gym',
                'image' => 'images/fitness-7.jpg',
                'desc'  => 'Fitness dapat dijadikan pilihan olahraga favorit yang efektif untuk mendapatkan tubuh yang sehat dan bugar. Seperti Coach Andre dan Kak Angela Chung. Coach Andre sebagai pemilik dari Fitness Center FitNation.id dan kak Angela Chung sebagai Fashion Desainer professional percaya bahwasanya menjadi bugar dan sehat dapat meningkatkan kualitas hidup sekaligus memastikan tubuh dalam kondisi yang terbaik untuk melakukan aktifitas sehari-hari. Sarung tangan merupakan komponen penting dalam olahraga ini. Pemakaian sarung tangan yang tepat dapat melindungi tangan dari lecet akibat mengangkat beban. Selain itu memakai sarung tangan saat fitness dapat membantu tangan untuk mencengkram dumbel dan barbell dengan lebih baik. “Setelah memakai sarung tangan ZUNA selama beberapa sesi di gym, tangan saya lebih nyaman, rasa sakit berkurang, tidak ada lagi lecet, dan pengalaman angkat beban saya lebih lancar – tidak perlu berhenti untuk menyeka keringat dari tangan saya. Sarung tangan terlihat bagus, gaya, edgy, fungsional, serta mudah dipakai dan dilepas” terang mereka setelah memakai sarung tangan ZUNA.',
                'link' => 'Fitness Gloves'
            ],
            [
                'title' => 'Zuna For Motorbike',
                'image' => 'images/motorbike-6.jpg',
                'desc'  => 'Bayu dan Diana merupakan suami istri yang memiliki hobi touring menggunakan sepeda motor. Awalnya pada tahun 2015 hanya Bayu seorang diri yang memulai hobi ini. Diana menolak untuk ikut touring karena berat untuk meninggalkan anak-anak dirumah. Sampai pada  tahun 2019,untuk pertama kali Diana ikut menemani Bayu touring bersama. Dari situlah Diana mulai enjoy untuk ikut touring bersama Bayu dan mensupport penuh. Saat touring, Bayu mencari sarung tangan yang bisa menyerap keringat serta memudahkan genggaman untuk handling stang dan rem, sedangkan Diana lebih nyaman menggunakan tipe sarung tangan half finger agar memudahkan mengabadikan momen menggunakan Smartphone. “Sejak tahun 2019, sarung tangan favorit saya adalah punyanya ZUNA, karena Sejauh ini sarung tangan yang pas dan bisa menyesuaikan dengan size kita adalah ZUNA” ungkap Bayu dan Diana.',
                'link' => 'Motorbike Gloves'
            ],
            [
                'title' => 'Zuna For Travelling',
                'image' => 'images/travelling-2.jpg',
                'desc'  => 'Evani Jesslyn merupakan sosok wanita berprestasi yang berkecimpung di bidang kopi. Awalnya pada tahun 2014 Evani mencoba kopi Indonesia ketika berada di Amerika Serikat. Ketika kembali ke Indonesia ternyata kebanyakan kopi Indonesia dengan kualitas terbaik banyak yang di eskpor. Dari sini lah misi seorang Evani muncul untuk memperdalam kopi dengan sebuah misi agar semua orang Indonesia bisa menikmatii kopi kualitas terbaik dari negeri sendiri. Bicara soal prestasi, Evani telah menorehkan berbagai macam prestasi. Antara lain menjadi 1 finalist dari Asia di acara Barista Farmer Talent Show di Brazil dan memenangkan beberapa award, mendapatkan sertifikasi Q Grader, Coffee Diploma, menjadi Authorized SCA Trainer. Dalam membuat kopi terbaik, dibutuhkan ketelitian dalam membuatnya. Ada beberapa langkah yang membutuhkan  sarung tangan untuk menahan rasa panas di tangan. Menangani hal itu Evani telah mencoba sarung tangan dari ZUNA. “Nyaman, terutama ketika dipakai pada saat brewing dan memegang benda yang panas, sarung tangan ini sangat membantu sekali” terang Evani ketika mencoba sarung tangan ZUNA. ',
                'link' => 'Travelling Gloves'
            ],
            [
                'title' => 'Zuna For Fishing',
                'image' => 'images/fishing-8.jpg',
                'desc'  => 'Zulkarnain Mardizansyah merupakan jurnalis media cetak yang memiliki hobi memancing. Berawal dari ikut-ikutan, saat ini memancing sudah menjadi hobi serta kebutuhan untuk melepas penat setelah disibukan dari berbagai pekerjaan sebagai jurnalis. Pak Zulkarnain merupakan pendiri dari komunitas Wartawan Mancing Mania (Wamama) yang didirikan pada 31 Januari 2009. Wamama sendiri merupakan wadah yang dibuat Pak Zulkarnain bagi para wartawan yang memiliki hobi memancing. Sebagai komunitas memancing, Wamama telah sukses menyelenggarakan berbagai lomba memancing. Dalam urusan sarung tangan untuk memancing, Pak Zulkarnain telah memakai sarung tangan ZUNA. “Saya terkesan dengan model dan bahannya ketika kali pertama melihat dan memegang sarung tangan ZUNA. Begitu memakainya, bahannya sangat nyaman di tangan. Tidak sumuk. Modelnya juga bagus. Keren. Jika untuk memancing pasti sangat baik digunakan. Terutama kalau digunakan saat lomba atau saat kita memancing di laut” terang Pak Zulkarnain.',
                'link' => 'Fishing Gloves'
            ],

        ];

        $popup = PromoPopUp::first();
        $agent = new Agent();

        $isMobile = $agent->isMobile();

        if ($isMobile) {
            $gallery = Jumbotron::orderby('show_order_mobile')->get();
        }
        else {
            $gallery = Jumbotron::orderby('show_order')->get();
        }
        

        return view('web.home', compact('gallery', 'popup'));
    }

    public function find_us()
    {
        $title = 'Where to Find Us';
        
        // Dummy Data
        $online_store = [
            [
                'name' => 'Tokopedia',
                'img' => 'images/tokopedia.png',
                'link'  => 'https://www.tokopedia.com/zunagloves'
            ],
            [
                'name' => 'Shopee',
                'img' => 'images/shopee.png',
                'link'  => 'https://shopee.co.id/shop/70156668/'
            ],
            [
                'name' => 'Lazada',
                'img' => 'images/lazada.png',
                'link'  => 'https://s.lazada.co.id/s.byNsU'
            ],
            [
                'name' => 'Bukalapak',
                'img' => 'images/bukalapak.png',
                'link'  => 'https://www.bukalapak.com/u/wahyuningsih991'
            ],
            [
                'name' => 'Blibli',
                'img' => 'images/blibli.png',
                'link'  => 'https://www.blibli.com/merchant/zuna-sport/ZUS-24557?page=1&start=0&pickupPointCode=&cnc=&multiCategory=true&excludeProductList=true&sort=7'
            ],
        ];

        $offline_store = [
            [
                'city' => 'Bandung',
                'store' => [
                    'Chioda Sport',
                    'Net Bike',
                    'Transmart',
                    'Kurnia Bike',
                    'Tenda Biru'
                ]
            ],
            [
                'city' => 'Bekasi',
                'store' => [
                    'Transmart',
                    'Wepe Store',
                ]
            ],
            [
                'city' => 'Denpasar',
                'store' => [
                    'Transmart'
                ]
            ],
            [
                'city' => 'Garut',
                'store' => [
                    'Adidas Sport'
                ]
            ],
            [
                'city' => 'Jakarta',
                'store' => [
                    'Bike One',
                    'Marcopolo Fitness Center',
                    'Transmart',
                    'The Gym'
                ]
            ],
            [
                'city' => 'Jogya',
                'store' => [
                    'Adi Mitra',
                    'Orion Bike shop',
                    'Toko Tepat',
                    'Toko Arena',
                    'Transmart'
                ]
            ],
            [
                'city' => 'Madiun',
                'store' => [
                    'Dwi Jaya Bike',
                    'Fasto Bike',
                    'Murni Bike'
                ]
            ],
            [
                'city' => 'Magelang',
                'store' => [
                    'Cartenz',
                    'Kancaku bike shop',
                    'Transmart'
                ]
            ],
            [
                'city' => 'Makasar',
                'store' => [
                    'Velo Bike'
                ]
            ],
            [
                'city' => 'Ngawi',
                'store' => [
                    'Eka Jaya Bike'
                ]
            ],
            [
                'city' => 'Palembang',
                'store' => [
                    'Jaya Makmur',
                    'Rbike',
                    'Transmart'
                ]
            ],
            [
                'city' => 'Pangkal Pinang',
                'store' => [
                    'DRT Bike'
                ]
            ],
            [
                'city' => 'Pati',
                'store' => [
                    'Doyok Sports',
                    'Spirit Bike'
                ]
            ],
            [
                'city' => 'Purwakarta',
                'store' => [
                    'Champion Sport'
                ]
            ],
            [
                'city' => 'Purwokerto',
                'store' => [
                    'Cartenz',
                    'Rukun Makmur'
                ]
            ],
            [
                'city' => 'Semarang',
                'store' => [
                    'Bike One',
                    'Marcopolo Fitness Center',
                    'Transmart',
                    'The Gym'
                ]
            ],
            [
                'city' => 'Solo',
                'store' => [
                    'Cartenz',
                    'Dunia Sepeda',
                    'Moss'
                ]
            ],
            [
                'city' => 'Surabaya',
                'store' => [
                    'Cafe pagi pagi',
                    'Graha Sepeda',
                    'Transmart'
                ]
            ],
            [
                'city' => 'Tasikmalaya',
                'store' => [
                    'Indra Jaya Bike',
                    'Laksana Jaya'
                ]
            ],
        ];

        $CityStore = CityStore::where('is_active', '1')
                            ->orderBy('show_order')
                            ->get();
        return view('web.find_us', compact('online_store','CityStore','title'));
    }

    public function contact()
    {
        $title = 'How to Contact Us';

        return view('web.contact', compact('title'));
    }

    public function about()
    {
        $title = 'About Us';

        return view('web.about', compact('title'));
    }

    public function size()
    {
        $title = 'Size Chart';

        return view('web.size', compact('title'));
    }

    public function policy()
    {
        $title = 'Return and Claim Policy';
        return view('web.policy', compact('title'));
    }

    public function tos()
    {
        $title = 'Terms of Use';

        return view('web.tos', compact('title'));
    }

    public function faq()
    {
        $title = 'Frequently Asked Question';
        return view('web.faq', compact('title'));
    }

    public function shipping()
    {
        $title = 'Shipping And Handling';
        return view('web.shipping', compact('title'));
    }

    public function payment()
    {
        return view('web.payment');
    }

    public function category(Request $request, $slug)
    {
        $title = ucwords(str_replace('-',' ',$slug));
        $show_item_per_page = $request->get('show') != "" ? $request->get('show') : 12;
        $query_string = $request->except('page');
        $keyword = $request->keyword;

        $sort_by = [
            'new_products' => [
                'title' => 'New Products'
            ],
            'discount_products' => [
                'title' => 'Sale'
            ],
            'highest_products' => [
                'title' => 'Highest Price'
            ],
            'lowest_products' => [
                'title' => 'Lowest Price'
            ]
        ];

        /**
         * ======================================================
         *      Create New Instance
         * ======================================================
         */
        $products = Product::where('is_active','<>', 0);

        if ($slug == 'all') {
            
            $title = 'All Products';    

        } else {

            $category = Category::where('name', $title)->first();
            $products = $products->where('category_id', $category->id);
            //$products = $category->products()->where('is_active', '<>', 0);

        }

        if ($keyword) {
            $products = $products->where('name', 'like', '%'.$keyword.'%');
            $title = 'Search Result For : ' . $keyword;
        }

        /**
         * ======================================================
         *      Filter
         * ======================================================
         */

        if ($request->has('size')) {

            $size = $request->get('size');
           
            $products = $products->whereHas('productSizes', function($q) use($size){
                $q->whereIn('products_productsizes.productsize_id', $size);  
            });

           
            //$products = $products->whereIn('productsize_id', $request->get('size'));
            
        }

        if ($request->has('category')) {

            $cat = $request->get('category');
            //dd($cat);
            $products= $products->whereIn('category_id', $cat);
        }

        if ($request->get('color-filter')) {
            $selectedColor = $request->get('color-filter');
            $filteredProductColor = ProductColor::select('product_id')
                                        ->wherein('name', $selectedColor)
                                        ->distinct()
                                        ->get();
        }

        if ($request->has('genre')) {

            foreach ($request->get('genre') as $genre) {
                $products = $products->where('genre', $genre);
            }
            
        }

        if ($request->has('min-price') && $request->has('max-price')) {
            $max = 0;
            $min = 0;
            //if (is_float($request->get('min-price'))) {
                $min = $request->get('min-price');
            //}

            //if (is_float($request->get('max-price'))) {
                $max = $request->get('max-price');
            //}

            // /var_dump($min);die();

            $products = $products->whereBetween('price_id', [$min, $max]);
        }

        if (isset($filteredProductColor)) {
            $products = $products->wherein('id', $filteredProductColor);
        }

        if ($request->has('sortby')) {
            switch ($request->get('sortby')) {
                case 'lowest_products':
                    $products = $products->orderBy('price_id','asc');
                    break;

                case 'highest_products':
                    $products = $products->orderBy('price_id','desc');
                    break;

                case 'discount_products':
                    $products = $products->where('disc_percent','>',0);
                    //$products = $products->orderBy('price_id','desc');
                    break;
                
                default:
                    $products = $products->orderBy('created_at','desc');
                    break;
            }
        }


        /**
         * ======================================================
         *      Return Data
         * ======================================================
         */

         
        $products = $products->orderby("id", "desc")
                        ->paginate($show_item_per_page);
        
        $keyword = '';

        //dd($products);
        return view('web.category', compact('products', 'title', 'query_string', 'sort_by'));
    }

    public function product($category_slug, $product_slug)
    {
        $product = Product::where('slug', $product_slug)->first();
        
        $products = Product::where('category_id', $product->category_id)
        ->inRandomOrder()->take(4)->active()->get();
        return view('web.product', compact('product','products'));
    }

    public function search(Request $request)
    {
        $keyword = $request->keyword;
        $show_item_per_page = $request->get('show') != "" ? $request->get('show') : 12;
        $query_string = $request->except('page');
        $sort_by = [
            'new_products' => [
                'title' => 'New Products'
            ],
            'discount_products' => [
                'title' => 'Discount Products'
            ],
            'lowest_products' => [
                'title' => 'Lowest Price'
            ],
            'highest_products' => [
                'title' => 'Highest Price'
            ]
        ];

        /**
         * ======================================================
         *      Create New Instance
         * ======================================================
         */
            
        $title = 'Search Result For : ' . $keyword;
        $products = new Product;

        //$products = $products->where('name', 'like', '%'.$keyword.'%');
        $products = $products->where(function($query) use ($keyword) {
            $query->where('name', 'like', '%'.$keyword.'%')
                ->orWhere('description', 'like', '%'.$keyword.'%');
        });
        $products = $products->where('is_active', '>', '0');
        /**
         * ======================================================
         *      Filter
         * ======================================================
         */


        if ($request->has('sortby')) {
            switch ($request->get('sortby')) {
                case 'lowest_products':
                    $products = $products->orderBy('price_id','desc');
                    break;

                case 'highest_products':
                    $products = $products->orderBy('price_id','asc');
                    break;

                case 'discount_products':
                    $products = $products->where('disc_percent','>',0);
                    $products = $products->orderBy('price_id','desc');
                    break;
                
                default:
                    $products = $products->orderBy('created_at','desc');
                    break;
            }
        }

        /**
         * ======================================================
         *      Return Data
         * ======================================================
         */

        $products = $products->paginate($show_item_per_page);

        return view('web.category', compact('products', 'title', 'query_string', 'sort_by', 'keyword'));
    }

    public function cart()
    {
        $title = 'Cart';
        return view('web.cart', compact('title'));
    }

    public function storeCart(Request $request)
    {

        $cart_data = $request->has('cart') ? json_decode($request->cart) : $request->input();

        $cart_item = [];
        $subtotal = 0;

        foreach ($cart_data as $key => $value) {

            $value = (array) $value;

            $cart_item[$key] = (object) [
                'product_id'      => $value['id'],
                'name'            => $value['name'],
                'image'           => $value['image'],
                'productcolor_id' => $value['color'],
                'color'           => $value['color'],
                'size'            => $value['size'],
                'price'           => floatval($value['price']),
                'qty'             => intval($value['qty']),
                'subtotal'        => floatval($value['price']) * intval($value['qty']),
            ];
        }

        Session::put('cart', $cart_item);

        return route('checkout');
    }

    public function checkout(Request $request)
    {

        $cart_data = $request->session()->get('cart');

        if ( is_array($cart_data) && count($cart_data) > 0) {
            $subtotal = 0;

            foreach ($cart_data as $item) {
                $subtotal += $item->price * $item->qty;
            }

            $title = 'Billing & Shipping';

            $shipment = [
                'jne' => 'JNE',
                'jet' => 'JET',
                'jnt' => 'JNT',
                'sicepat' => 'SICEPAT',
                'pos' => 'POS',
            ];
        } else {
            return abort('404');
        }

        return view('web.checkout', compact('title','shipment', 'cart_data', 'subtotal'));
    }

    public function showColorImage($id)
	{
		$color = ProductColor::findOrFail($id);
		$path = $color->getColorImageFullPath();

		$resp = response()->download($path);
		$resp->headers->set('Content-Disposition', 'inline');
		$resp->headers->set('X-Content-Type-Options', 'nosniff');
		
		//disable cache
        $resp->headers->set('Cache-Control', 'no-cache, no-store, must-revalidate'); // HTTP 1.1.
        $resp->headers->set('Pragma', 'no-cache'); // HTTP 1.0.
        $resp->headers->set('Expires', '0'); // Proxies.

		return $resp;
    }

    public function showProductImage($id)
	{
		$image = ProductImage::findOrFail($id);
		$path = $image->getProductImageFullPath();

		$resp = response()->download($path);
		$resp->headers->set('Content-Disposition', 'inline');
		$resp->headers->set('X-Content-Type-Options', 'nosniff');
		
		//disable cache
        $resp->headers->set('Cache-Control', 'no-cache, no-store, must-revalidate'); // HTTP 1.1.
        $resp->headers->set('Pragma', 'no-cache'); // HTTP 1.0.
        $resp->headers->set('Expires', '0'); // Proxies.

		return $resp;
    }
    
    public function showProductColorImage($id)
	{
		$ProductColorImage = ProductColorImage::findOrFail($id);

		$path = $ProductColorImage->getProductImageFullPath();

		$resp = response()->download($path);
		$resp->headers->set('Content-Disposition', 'inline');
		$resp->headers->set('X-Content-Type-Options', 'nosniff');
		
		//disable cache
        $resp->headers->set('Cache-Control', 'no-cache, no-store, must-revalidate'); // HTTP 1.1.
        $resp->headers->set('Pragma', 'no-cache'); // HTTP 1.0.
        $resp->headers->set('Expires', '0'); // Proxies.

		return $resp;
    }

    public function showShowcaseImage($id)
	{
		$jumbotron = Jumbotron::findOrFail($id);

		$resp = response()->download($jumbotron->getImageFullPath());
		$resp->headers->set('Content-Disposition', 'inline');
		$resp->headers->set('Content-Type', $jumbotron->mimetype);
		$resp->headers->set('X-Content-Type-Options', 'nosniff');
		
		//disable cache
        $resp->headers->set('Cache-Control', 'no-cache, no-store, must-revalidate'); // HTTP 1.1.
        $resp->headers->set('Pragma', 'no-cache'); // HTTP 1.0.
        $resp->headers->set('Expires', '0'); // Proxies.

		return $resp;
	}

    public function showShowcaseDetailImage($id)
	{
		$jumbotron = Jumbotron::findOrFail($id);

		$resp = response()->download($jumbotron->getImageDetailFullPath());
		$resp->headers->set('Content-Disposition', 'inline');
		$resp->headers->set('Content-Type', $jumbotron->image_detail_mimetype);
		$resp->headers->set('X-Content-Type-Options', 'nosniff');
		
		//disable cache
        $resp->headers->set('Cache-Control', 'no-cache, no-store, must-revalidate'); // HTTP 1.1.
        $resp->headers->set('Pragma', 'no-cache'); // HTTP 1.0.
        $resp->headers->set('Expires', '0'); // Proxies.

		return $resp;
	}

    public function showPopupImage($id)
	{
		$jumbotron = PromoPopUp::findOrFail($id);

		$resp = response()->download($jumbotron->getImageFullPath());
		$resp->headers->set('Content-Disposition', 'inline');
		$resp->headers->set('Content-Type', $jumbotron->mimetype);
		$resp->headers->set('X-Content-Type-Options', 'nosniff');
		
		//disable cache
        $resp->headers->set('Cache-Control', 'no-cache, no-store, must-revalidate'); // HTTP 1.1.
        $resp->headers->set('Pragma', 'no-cache'); // HTTP 1.0.
        $resp->headers->set('Expires', '0'); // Proxies.

		return $resp;
	}

    public function cekData()
    {
        $file = file_get_contents(storage_path('zuna_products.json'));
        $obj = json_decode($file, true);

        usort($obj, function($a, $b) {
            return $a['category'] <=> $b['category'];
        });

        // foreach ($obj as $key => $value) {
        //     echo $value['title'] . '<br/>';
        // }
    }

    public function sendMessage(Request $request)
	{
		$this->validate($request, [
			'first_name' => 'required|max:127',
            'last_name' => 'required|max:127',
			'email' => 'required|email',
            'phone' => 'required',
			'message' => 'required',
            'g-recaptcha-response' => 'required|captcha'
		]);

		Mail::send('email.send_message', ['input' => $request->all()], function($message)
		{
			$message->to('zunagloves@gmail.com')->subject("[Zunagloves Website] New Message from Website");
		});

		return redirect()->action('LandingController@contact')
			->with('_SENT_SUCCESS', true);
	}

    public function subscribeEmail(Request $request){
		$this->validate($request, [
			'sub_email' => 'required|email|unique:subscribe_emails,email'
		]);

		$email = new SubscribeEmail();
		$email->fill(
			['email' => $request->sub_email]
		);

		$email->save();
		return redirect()->action('LandingController@home')
			->with('_SUBSCRIBE_EMAIL', true);
	}

    public function checkStock(Request $request) {
        if ($request->productId == null || $request->productColorId == null || $request->productProductSizeId == null) {
            //input not complete
            return -1;
        }

        $product = Product::find($request->productId);
        
        if ($product->is_active == 2) {
            return -2;
        }

        $stock = ProductStock::where('product_id', $request->productId)
                            ->where('product_color_id', $request->productColorId)
                            ->where('product_productsize_id', $request->productProductSizeId)
                            ->first();
        
        return $stock->stock;
    }

    public function discountRequest(Request $request) {
        $email = $request->email;
        $subscribe = $request->subscribe;

        
        $_subscribeCheck = SubscribeEmail::where('email',$email)->count();
        if ($_subscribeCheck == 0) {
            $_email = new SubscribeEmail();
            $_email->fill(
                ['email' => $request->email]
            );

            $_email->save();

            $promoPopUp = PromoPopUp::first();

        $codeLength = $promoPopUp->code_length;
        $validIn = $promoPopUp->valid_in;
        $promo = $promoPopUp->promo;
        $type = $promoPopUp->type;
        $discount = $promoPopUp->discount;
        $minimum = $promoPopUp->minimum;

        $startDate = Carbon::now();
        //$endDate = $startDate->addDays($validIn);
        $voucherCode = $this->generateRandomString($codeLength);
        $voucher = new Voucher();
        $voucher->fill([
			'code' => $voucherCode,
			'promo' => $promo,
			'type' => $type,
			'discount' => $discount,
			'start_date' => Carbon::now(),
			'end_date' =>  Carbon::now()->addDays($validIn)->endOfDay(),
			'minimum' => $minimum,
            'email' => $email,
            'is_auto_generated' => 1
		]);

        //dd($voucher);
        $voucher->save();

        Mail::send('email.promo_voucher', ['email' => $email, 'voucherCode' => $voucherCode, 'validIn' => $validIn], function($message) use ($email, $voucherCode, $validIn)
        {
            $message->to($email, 'Zunagloves Customer')->subject("[Zunagloves] Kode Voucher");
        });
                  
        }
        return redirect()->action('LandingController@home');
    }

    public function generateRandomString($length) {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
