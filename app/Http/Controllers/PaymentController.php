<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Order;

use Auth;
use Hash;
use Config;
use Carbon\Carbon;
use App\VeritransPaymentGateway;
use Log;
use App\Models\Activity;

class PaymentController extends Controller {
	public function notifVeritrans(Request $request)
	{
		//$notif = VeritransPaymentGateway::handleNotif();

		$result = file_get_contents('php://input');
        
        $order_id = $request->input('order_id');
        //var_dump($order_id);die();
        $result = json_decode($result);
		
		$transaction = $result->transaction_status;
		$type = $result->payment_type;
		$order_id = $result->order_id;
		$fraud = $result->fraud_status;

		$order = Order::where('order_number', '=', $order_id)->first();
		$order->setPaymentData('raw', $result);

		if ($transaction == 'capture' || $transaction == 'settlement') {
			if ($order->state == 'INV')
				$order->state = 'PAI';
			$order->setPaymentData('type', $type);
			$order->paid_amount = $result->gross_amount;
			if (property_exists($result, 'card_type')) {
				$order->payment_method = ucfirst($result->card_type);
			}
			if (property_exists($result, 'bank')) {
				$order->bank_acc = ucfirst($result->bank);
			}
			$order->paid_at = $result->transaction_time;
		} else if($transaction == 'pending' || $transaction == 'deny') {
			$order->state = 'INV';
			$order->setPaymentData('type', $type);
		} else if ($transaction == 'expire' || $transaction == 'cancel') {
			$order->state = 'REJ';
			$order->setPaymentData('type', $type);
		}

		if ($order->getOriginal('state') != $order->state) {
			// temporary disable
			// it's multiple state activity, not only success
			//Activity::save_activity('PAYMENT.SUCCESS|ORDER', Auth::customer()->get()->getOriginal(), $order, null, $order, 'customers');
			$order->save();
			$order->sendNotif($order->email, $order->full_name);
		} else {
			$order->save();
		}

		return response()->json([
			'timestamp' => Carbon::now()->format('c')
		]);
	}
}
