<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Promo\UpdateRequest;
use App\Http\Controllers\Admin\Authenticate;

use App\Models\Promo;
use App\Models\Activity;

use Auth;

class PromoController extends Controller {
	
	use Authenticate;

	public function index()
	{
		$promos = Promo::all();
		return view('admin.promo.list')
			->with('promos', $promos);
	}

	public function edit($id)
	{
		$promo = Promo::findOrFail($id);

		return view('admin.promo.edit')
			->with('promo', $promo);
	}

	public function update(UpdateRequest $request, $id)
	{
		$promo = Promo::findOrFail($id);
		$promo->fill([
			'title' => $request->title,
			'link' => $request->link
		]);

		if ($request->hasFile('image'))
			$promo->image = $request->file('image');

		//Activity::save_activity('UPDATE.SUCCESS|PROMO', Auth::user(), $promo, null, $promo, 'admins');
		$promo->save();

		return redirect()->action('Admin\PromoController@edit', [$promo->id])
			->with('UPDATE.OK', true);
	}
}