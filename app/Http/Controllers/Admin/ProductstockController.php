<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Models\ProductStock;

class ProductStockController extends Controller
{
    public function index(Request $request)
	{
        $productId = $request->id;

        //$productStock = ProductStock::where('product_id', $productId)->get();
        $product = Product::find($productId);
        $productName = $product->name;

        //product stock
		$productSizes = $product->productSizes;
		$productColor = $product->colors;
		
		foreach ($productColor as $color) {
			foreach ($productSizes as $size) {
				//insert to product stock when it's not there
				$productStock = ProductStock::where('product_id', '=', $product->id)
											->where('product_color_id', '=', $color->id)
											->where('product_productsize_id', '=', $size->id)
											->first();	

				if($productStock == null) {
					$_prodcutStock = new ProductStock();
					$_prodcutStock->fill([
						'product_id' => $product->id,
						'product_color_id' => $color->id,
						'product_productsize_id' => $size->id,
						'stock' => '0',
						'sku' => $color->sku . $size->size->short_name  
					]);
					$_prodcutStock->save();
				}
				
			}	
		}

		// clean up product stock
		$productStocks = ProductStock::select('product_productsize_id')
							->where('product_id', '=', $product->id)
							->distinct()
							->get();
		
		foreach ($productStocks as $value) {	
			$found = false;
			foreach ($productSizes as $size) {
				// var_dump($size->id);
				// echo " vs ";
				// var_dump($value->product_productsize_id);
				// echo " || ";
				if ($value->product_productsize_id == $size->id) {
					$found = true;
				}
			}
			if (!$found) {
				ProductStock::where('product_productsize_id', '=', $size->id)
								->delete();
			}
        }
        
        $productStock = ProductStock::where('product_id', '=', $productId)
                                ->get();
        
        
        //dd($productStock);
        
		return view('admin.productstock.list')
            ->with('productStock', $productStock)
            ->with('productId', $productId)
            ->with('productName', $productName);
            
    }
    
    public function update(Request $request, $id)
	{
        $sku = $request->sku;
		$stock = $request->stock;
		$id = $request->id;

        foreach($id as $key => $item) {
			$productStock = ProductStock::find($item);

			$productStock->fill([
				'sku' => $sku[$key],
				'stock' => $stock[$key]
			]);
			$productStock->save();
		}

		return redirect()->action('Admin\ProductController@index')
			->with('UPDATE.OK', true);
    }
}
