<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Shipping\StoreRequest;
use App\Http\Requests\Shipping\UpdateRequest;
use App\Http\Requests\Shipping\AddPriceRequest;
use App\Http\Requests\Shipping\UpdatePriceRequest;
use App\Http\Controllers\Admin\Authenticate;

use App\Models\Shipping;
use App\Models\ShippingPrice;
use App\Models\Country;
use App\Models\Activity;

Use View;
use League\Csv\Reader;
use Exception;
use DB;
use Carbon\Carbon;
use Auth;

class ShippingController extends Controller {
	
	use Authenticate;

	public function index()
	{
		$shippings = Shipping::orderBy('show_order')->paginate(10);
		return view('admin.shipping.list')
			->with('shippings', $shippings);
	}

	public function create()
	{
		$countries = Country::orderBy('name')->lists('name', 'id');
		return view('admin.shipping.create')
			->with('countries', $countries);
	}

	public function edit($id)
	{
		$shipping = Shipping::findOrFail($id);
		$countries = Country::orderBy('name')->lists('name', 'id');

		$prices = $shipping->prices()->get()
			->map(function($i) {
				return [
					'id' => $i->id,
					'destination' => $i->destination,
					'price' => (float)$i->price,
					'time' => $i->time
				];
			});

		return view('admin.shipping.edit')
			->with('shipping', $shipping)
			->with('countries', $countries)
			->with('prices', $prices);
	}

	public function store(StoreRequest $request)
	{
		$shipping = new Shipping();
		$shipping->fill([
			'name' => $request->name,
			'display_name' => $request->display_name,
			'country_id' => $request->country
		]);

		$max = Shipping::max('show_order');
		if ($max == null)
			$max = 1;
		else
			$max += 1;

		$shipping->show_order = $max;

		//Activity::save_activity('CREATE.SUCCESS|SHIPPING', Auth::user(), $request, null, null, 'admins');
		$shipping->save();

		return redirect()->action('Admin\ShippingController@edit', [$shipping->id])
			->with('UPDATE.OK', true);
	}

	public function update(UpdateRequest $request, $id)
	{
		$shipping = Shipping::findOrFail($id);
		$shipping->fill([
			'name' => $request->name,
			'display_name' => $request->display_name,
			'country_id' => $request->country
		]);

		//Activity::save_activity('UPDATE.SUCCESS|SHIPPING', Auth::user(), $shipping, null, $shipping, 'admins');
		$shipping->save();

		return redirect()->action('Admin\ShippingController@edit', [$shipping->id])
			->with('UPDATE.OK', true);
	}

	public function delete($id)
	{
		$shipping = Shipping::findOrFail($id);
		//Activity::save_activity('DELETE.SUCCESS|SHIPPING', Auth::user(), $shipping, null, null, 'admins');
		$shipping->delete();

		return redirect()->action('Admin\ShippingController@index')
			->with('DELETE.OK', true);
	}

	public function move($id, $move)
	{
		$curr = Shipping::findOrFail($id);

		$next = null;
		if ($move > 0) {
			$next = Shipping::where('show_order', '>', $curr->show_order)
				->orderBy('show_order')
				->first();
		} elseif ($move < 0) {
			$next = Shipping::where('show_order', '<', $curr->show_order)
				->orderBy('show_order', 'desc')
				->first();
		}

		if (!$next)
			App::abort('404');

		$temp = $curr->show_order;
		$curr->show_order = $next->show_order;
		$next->show_order = $temp;

		//Activity::save_activity('MOVE.SUCCESS|SHIPPING', Auth::user(), null, null, null, 'admins');
		$curr->save();
		$next->save();

		return redirect()->action('Admin\ShippingController@index')
			->with('UPDATE.OK', true);
	}

	public function addPrice(AddPriceRequest $request, $id)
	{
		$shipping = Shipping::findOrFail($id);

		$price = new ShippingPrice();
		$price->fill([
			'destination' => $request->destination,
			'price' => $request->price,
			'time' => $request->time
		]);

		//Activity::save_activity('CREATE.SUCCESS|SHIPPING PRICE', Auth::user(), $request, $shipping, null, 'admins');

		$shipping->prices()->save($price);

		return redirect()->action('Admin\ShippingController@edit', [$id])
			->with('UPDATE.OK', true);
	}

	public function updatePrice(UpdatePriceRequest $request, $id, $priceId)
	{
		$shipping = Shipping::findOrFail($id);

		$price = ShippingPrice::findOrFail($priceId);
		$price->fill([
			'destination' => $request->destination_edit,
			'price' => $request->price_edit,
			'time' => $request->time_edit
		]);

		//Activity::save_activity('UPDATE.SUCCESS|SHIPPING PRICE', Auth::user(), $price, $shipping, $price, 'admins');
		$price->save();

		return redirect()->action('Admin\ShippingController@edit', [$id])
			->with('UPDATE.OK', true);
	}

	public function deletePrice($id, $priceId)
	{
		$shipping = Shipping::findOrFail($id);
		$price = ShippingPrice::findOrFail($priceId);

		//Activity::save_activity('DELETE.SUCCESS|SHIPPING PRICE', Auth::user(), $price, $shipping, null, 'admins');
		$price->delete();

		return redirect()->action('Admin\ShippingController@edit', [$id])
			->with('DELETE.OK', true);
	}

	public function upload()
    {
            try {
                $csv = Reader::createFromPath('../docs/tarif_jne.csv');                
                
            } catch (Exception $e) {
                return redirect()->action('Admin\ShippingController@index')
                ->with('No_File', true);
            }

            //Activity::save_activity('UPLOAD.SUCCESS|SHIPPING PRICE', Auth::user(), null, null, null, 'admins');

            	$csv->setOffset(1); //because we don't want to insert the header
                //var_dump($csv);die;
                $nbInsert = $csv->each(function ($row) {
                	$data = explode(";", $row[0]);
                	$Destinasi = $data[2].'('.$data[1].')';
                	$Tarif = intval($data[4]);
                	$Day = intval($data[5]);
                	//var_dump($Destinasi);die;
                DB::statement("REPLACE INTO shipping_prices
                    (
                        shipping_id,
                        destination,
                        price,
                        time,
                        created_at,
                        updated_at
                    ) 
                    VALUES (
                        :shipping_id,
                        :destination,
                        :price,
                        :time,
                        :now1,
                        :now2
                    )", [
                         ':shipping_id' => 3,
                         ':destination' => $Destinasi,
                         ':price' => $Tarif,
                         ':time' => $Day,
                         ':now1' => Carbon::now(),
                         ':now2' => Carbon::now()
                     ]);
                return true;
                });

            return redirect()->action('Admin\ShippingController@index')
            ->with('Success', true);
    }
}