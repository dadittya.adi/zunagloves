<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Productsize\StoreRequest;
use App\Http\Requests\Productsize\UpdateRequest;
use App\Http\Controllers\Admin\Authenticate;

use App\Models\ProductSize;
use App\Models\Activity;

use Auth;

class ProductsizeController extends Controller {
	
	use Authenticate;

	public function index()
	{
		$sizes = ProductSize::orderBy('order')->get();
		return view('admin.productsize.list')
			->with('sizes', $sizes);
	}

	public function store(StoreRequest $request)
	{
		$size = new ProductSize();
		$size->fill([
			'name' => $request->name,
			'short_name' => $request->short_name
		]);

		$max = ProductSize::max('order');
		if ($max == null)
			$max = 1;
		else
			$max += 1;
		
		$size->order = $max;

		//Activity::save_activity('CREATE.SUCCESS|PRODUCT SIZE', Auth::user(), $request, null, null, 'admins');
		$size->save();

		return redirect()->action('Admin\ProductsizeController@index')
			->with('UPDATE.OK', true);
	}

	public function update(UpdateRequest $request, $id)
	{
		$size = ProductSize::findOrFail($id);
		$size->fill([
			'name' => $request->name_edit,
			'short_name' => $request->short_name_edit
		]);

		//Activity::save_activity('UPDATE.SUCCESS|PRODUCT SIZE', Auth::user(), $size, null, $size, 'admins');
		$size->save();

		return redirect()->action('Admin\ProductsizeController@index')
			->with('UPDATE.OK', true);
	}

	public function delete($id)
	{
		//TODO: check relation to products
		$size = ProductSize::findOrFail($id);

		//Activity::save_activity('DELETE.SUCCESS|PRODUCT SIZE', Auth::user(), $size, null, null, 'admins');
		$size->delete();

		return redirect()->action('Admin\ProductsizeController@index')
			->with('DELETE.OK', true);
	}

	public function move($id, $move){
		$curr = ProductSize::findOrFail($id);

		$next = null;
		if ($move > 0) {
			$next = ProductSize::where('order', '>', $curr->order)
				->orderBy('order')
				->first();
		} elseif ($move < 0) {
			$next = ProductSize::where('order', '<', $curr->order)
				->orderBy('order', 'desc')
				->first();
		}

		if (!$next)
			return redirect()->action('Admin\ProductsizeController@index')
			->with('UPDATE.FAIL', true);

		$temp = $curr->order;
		$curr->order = $next->order;
		$next->order = $temp;
		//var_dump(Auth::user());
		//Activity::save_activity('MOVE.SUCCESS|PRODUCT SIZE', Auth::user(), $curr, null, $curr, 'admins');

		$curr->save();
		$next->save();

		return redirect()->action('Admin\ProductsizeController@index')
			->with('UPDATE.OK', true);
	}
}