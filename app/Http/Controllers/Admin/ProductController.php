<?php namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\Product\StoreRequest;
use App\Http\Requests\Product\UpdateRequest;
use App\Http\Controllers\Admin\Authenticate;
use Illuminate\Support\MessageBag;
use League\CommonMark\CommonMarkConverter;

use App\Models\Activity;
use App\Models\Product;
use App\Models\Category;
use App\Models\CategorySubcategory;
use App\Models\ProductSize;
use App\Models\Media;
use App\Models\ProductImage;
use Illuminate\Support\Str;

use App\Models\ProductStock;
use Carbon\Carbon;

use Auth;
use Symfony\Component\VarDumper\VarDumper;

class ProductController extends Controller {
	
	use Authenticate;

	public function index(Request $request)
	{
		$search = $request->search;
        $genre = $request->genre;
        $stock = $request->stock;
        $state = $request->state;
        $cat = $request->category;

		$products = Product::orderBy('id');
		$category = Category::orderBy('name')->pluck('name', 'id');
        if($search){
            $products = $products->where(function($q) use ($search) {
                $q->where('products.name', 'like', "%$search%")
                ->orWhere('products.sku', 'like', "%$search%");
            });
        }
        if($genre != null && $genre != 'ALL'){
            $products = $products->where('genre','=',$genre);
        }
        if($stock != null && $stock != 'ALL'){
            $products = $products->where('stock_out','=',$stock);
        }
        if($state != null && $state != 'ALL'){
            $products = $products->where('is_active','=',$state);
        }
        if($cat != null && $cat != 'ALL'){
            $products = $products->where('category_id','=',$cat);
        }
		$products = $products->paginate(25);
		//return response($products);die();
		
		return view('admin.product.list')
			->with('products', $products)
			->with('category', $category);
	}

	public function create()
	{
		//TODO : change this or not
		//only shown category which have subcategory
		$categories = Category::orderBy('show_order')->pluck('name', 'id');

		
		$subcategories = [];
		foreach (CategorySubcategory::all() as $sub) {
			$subcategories[$sub->category_id][] = [
				'id' => $sub->id,
				'name' => $sub->name
			];
		}

		$sizes = ProductSize::all();

		$media = Media::all();
		$example = "
# Heading 1
## Heading 2

This is link example [google](https://www.google.com/).

You can add new paragraph like this. This is **bold** example. This is _italic_ example.

> Blockquote

1. item one
2. item two
   - sublist
   - sublist

`Inline code` with backticks

```
# code block
print '3 backticks or'
print 'indent 4 spaces'
```
";

		return view('admin.product.create')
			->with('categories', $categories)
			->with('subcategories', $subcategories)
			->with('example', $example)
			->with('media', $media)
			->with('sizes', $sizes)
			->with('launch_date', now());
	}

	public function edit($id)
	{
		$product = Product::findOrFail($id);

		//TODO : change this or not
		//only shown category which have subcategory
		$categories = Category::orderBy('show_order')->pluck('name', 'id');
		
		$subcategories = [];
		foreach (CategorySubcategory::all() as $sub) {
			$subcategories[$sub->category_id][] = [
				'id' => $sub->id,
				'name' => $sub->name
			];
		}

		$sizes = ProductSize::all();

		$media = Media::all();
		$example = "
# Heading 1
## Heading 2

This is link example [google](https://www.google.com/).

You can add new paragraph like this. This is **bold** example. This is _italic_ example.

> Blockquote

1. item one
2. item two
   - sublist
   - sublist

`Inline code` with backticks

```
# code block
print '3 backticks or'
print 'indent 4 spaces'
```
";

		return view('admin.product.edit')
			->with('product', $product)
			->with('categories', $categories)
			->with('subcategories', $subcategories)
			->with('example', $example)
			->with('media', $media)
			->with('sizes', $sizes);
	}

	public function store(StoreRequest $request)
	{
		$converter = new CommonMarkConverter();

		if ($request->sizes == '[]') {
			return redirect()->back()->withInput()
   				->withErrors(['product_size'=>'Minimal 1 Product Size'])
				   ->with('UPDATE.FAIL', true);
		}

		if (!$request->hasFile('product_image')){
			return redirect()->back()->withInput()
   				->withErrors(['product_image'=>'Minimal 1 Product Image'])
   				->with('UPDATE.FAIL', true);
		}

		$product = new Product();
		$product->fill([
			'name' => $request->name,
			'is_active' => $request->is_active,
			'category_id' => $request->category,
			//'subcategory_id' => $request->subcategory,
			'genre' => $request->genre,
			'description' => $request->description,
			'description_html' => $converter->convertToHtml($request->description),
			'feature_material' => $request->feature_material,
			'feature_html' => $request->feature_material ? $converter->convertToHtml($request->feature_material) : '',
			//'stock_out' => $request->stock_out,
			'price_id' => $request->price_id,
			//'price_en' => $request->price_en,
			'disc_percent' => $request->disc_percent ? $request->disc_percent : 0,
			'weight' => $request->weight,
			'slug' => Str::slug(clean($request->name)),
			'launch_date' => Carbon::createFromFormat('d/m/Y', $request->launch_date)->startOfDay()
		]);

		if ($request->disc_percent > 0) {
			$product->start_discount = Carbon::createFromFormat('d/m/Y', $request->start_discount)->startOfDay();
			$product->end_discount = Carbon::createFromFormat('d/m/Y', $request->end_discount)->endOfDay();

			if ($product->end_discount <= $product->start_discount) {
				$bag = new MessageBag();
   				$bag->add('end_discount', 'End date must be after start date.');
   				return redirect()->back()
   					->withInput()
   					->withErrors($bag)
   					->with('UPDATE.FAIL', true);
			}
		}

		if ($request->hasFile('color_image'))
			$product->image = $request->file('color_image');

		//Activity::save_activity('CREATE.SUCCESS|PRODUCT', Auth::user(), $request, null, null, 'admins');

		$product->save();

		//product size
		
		if ($request->sizes) {
			$sizes = array_map(function($i) {
				return $i->id;
			}, json_decode($request->sizes));

			$product->sizes()->sync($sizes);
		}

		if ($request->hasFile('guide_image'))
			$product->guide_image = $request->file('guide_image');

		$product->save();

		foreach ($request->product_image as $key => $value) {
			if($value != null){
				$image = new ProductImage();
				$img = $image->uploadImage($value);
				$image->fill([
					'product_id' => $product->id,
					'image' => $img
				]);
				$image->save();
			}
		}

		return redirect()->action('Admin\ProductColorController@create', [$product->id])
			->with('UPDATE.OK', true);
	}

	public function update(UpdateRequest $request, $id)
	{
		$converter = new CommonMarkConverter();
		$product = Product::findOrFail($id);
		
		$product->fill([
			'name' => $request->name,
			'is_active' => $request->is_active,
			'category_id' => $request->category,
			'genre' => $request->genre,
			'description' => $request->description,
			'description_html' => $converter->convertToHtml($request->description),
			'feature_material' => $request->feature_material,
			'feature_html' => $request->feature_material ? $converter->convertToHtml($request->feature_material) : '',
			//'stock_out' => $request->stock_out,
			'price_id' => $request->price_id,
			'price_en' => $request->price_en,
			'disc_percent' => $request->disc_percent ? $request->disc_percent : 0,
			'weight' => $request->weight,
			'slug' => Str::slug($request->name)
		]);

		if ( $request->launch_date) {
			$product->launch_date = Carbon::createFromFormat('d/m/Y', $request->launch_date)->startOfDay();
		}

		if ($request->disc_percent > 0) {
			$product->start_discount = Carbon::createFromFormat('d/m/Y', $request->start_discount)->startOfDay();
			$product->end_discount = Carbon::createFromFormat('d/m/Y', $request->end_discount)->endOfDay();

			if ($product->end_discount <= $product->start_discount) {
				$bag = new MessageBag();
   				$bag->add('end_discount', 'End date must be after start date.');
   				return redirect()->back()
   					->withInput()
   					->withErrors($bag)
   					->with('UPDATE.FAIL', true);
			}
		} else {
			$product->start_discount = null;
			$product->end_discount = null;
		}
		$productSizes = $product->productSizes;
		$productColor = $product->colors;

		//product size
		$sizes = array_map(function($i) {
			return $i->id;
		}, json_decode($request->sizes));

		$arrProdSizeId = array();
		foreach ($productSizes as $size) {
			array_push($arrProdSizeId, $size->id);
			
		}

		if (!empty($arrProdSizeId)) {
			ProductStock::where('product_id', $product->id)
										->whereNotIn('product_productsize_id', $arrProdSizeId)
										->delete();
		}
		
		$product->sizes()->sync($sizes);

		//product stock
		$productSizes = $product->productSizes;
		$productColor = $product->colors;
		
		//dd($productSizes);
		foreach ($productColor as $color) {
			foreach ($productSizes as $size) {
				//insert to product stock when it's not there
				$productStock = ProductStock::where('product_id', '=', $product->id)
											->where('product_color_id', '=', $color->id)
											->where('product_productsize_id', '=', $size->id)
											->first();	
											

				if($productStock == null) {
					$checkStock = ProductStock::where('id', $size->id)
									->first();
					if ($checkStock != null) {
						$_prodcutStock = new ProductStock();
						$_prodcutStock->fill([
							'product_id' => $product->id,
							'product_color_id' => $color->id,
							'product_productsize_id' => $size->id,
							'stock' => '0',
							'sku' => $color->sku . $size->size->short_name
						]);
						$_prodcutStock->save();
					}
				}
				
			}	
		}

		// clean up product stock
		$productStocks = ProductStock::select('product_productsize_id')
							->where('product_id', '=', $product->id)
							->distinct()
							->get();
						
		foreach ($productStocks as $value) {	
			$found = false;
			foreach ($productSizes as $size) {
				// var_dump($size->id);
				// echo " vs ";
				// var_dump($value->product_productsize_id);
				// echo " || ";
				if ($value->product_productsize_id == $size->id) {
					$found = true;
				}
			}
			if (!$found) {
				ProductStock::where('product_productsize_id', '=', $size->id)
								->delete();
			}
		}
		//die();
		

		if ($request->hasFile('guide_image')) {
			$old_guide_image = $product->getGuideImageFullPath();
			$product->guide_image = $request->file('guide_image');
		}

		if ($request->hasFile('product_image')){
			$images = $product->images;
			foreach ($images as $key => $i) {
				@unlink($i->getProductImageFullPath());
				$i->delete();
			}
			foreach ($request->product_image as $key => $value) {
				if($value != null){
					$product_image = new ProductImage();
					$img = $product_image->uploadImage($value);
					$product_image->fill([
						'product_id' => $id,
						'image' => $img
					]);
					$product_image->save();	
				}
			}
			
		} 

		//Activity::save_activity('UPDATE.SUCCESS|PRODUCT', Auth::user(), $product, null, $product, 'admins');

		if ($product->save())
			@unlink($old_guide_image);
		else
			@unlink($product->getGuideImageFullPath());

		return redirect()->action('Admin\ProductController@edit', [$id])
			->with('UPDATE.OK', true);
	}

	function clean($string) {
		$string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
	 
		return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
	 }
}