<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\BlogsMedia\StoreRequest;
use App\Http\Requests\BlogsMedia\UpdateRequest;
use App\Http\Controllers\Admin\Authenticate;

use App\Models\BlogsMedia;
use App\Models\Activity;
use Carbon\Carbon;
use Config;
use Auth;

class BlogsMediaController extends Controller {
	
	use Authenticate;

	public function index()
	{
		$media = BlogsMedia::all();

		return view('admin.blogsmedia.list')
			->with('media', $media);
	}

	public function store(StoreRequest $request)
	{
		$media = new BlogsMedia();
		$file = $request->file('media');
		$file_name = null;
		//$destinationPath = Config::get('storage.media') . '/' . Carbon::now()->year;
		$destinationPath = 'blogs/media/' . Carbon::now()->year;
		$try = 250;
		do {
			if ($try <= 0)
				throw Exception("Storage::random fails to produce randomized filename");
			
			$hash = Carbon::now()->month."-".str_random(6).".".$file->getClientOriginalExtension();
			$file_name = $hash;
			$file_check = $destinationPath . '/' . $hash;
			$try -= 1;
		} while (file_exists($file_check));

		//var_dump($request); die;
		$uploadSuccess = $file->move($destinationPath, $file_name);

		$media->fill([
			'name' => $file->getClientOriginalName(),
			'filename' => $file_name,
			'upload_at' => Carbon::now()
		]);

		$media->save();

		//Activity::save_activity('CREATE.SUCCESS|BLOG MEDIA', Auth::user(), $request, null, null, 'admins');
		return redirect()->action('Admin\BlogsMediaController@index')
			->with('UPDATE.OK', true);
	}

	public function update(UpdateRequest $request, $id)
	{
		$media = BlogsMedia::findOrFail($id);
		$media->name = $request->name;

		//Activity::save_activity('UPDATE.SUCCESS|BLOG MEDIA', Auth::user(), $media, null, $media, 'admins');
		$media->save();

		return redirect()->action('Admin\BlogsMediaController@index')
			->with('UPDATE.OK', true);
	}

	public function delete($id)
	{
		$media = BlogsMedia::findOrFail($id);
		$path = 'blogs/media/' . $media->upload_at->year . '/' . $media->filename;

		if ($media->delete())
			@unlink($path);

		//Activity::save_activity('DELETE.SUCCESS|BLOG MEDIA', Auth::user(), $media, null, null, 'admins');
		return redirect()->action('Admin\BlogsMediaController@index')
			->with('DELETE.OK', true);
	}
}