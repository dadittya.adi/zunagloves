<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Store\StoreRequest;
use App\Http\Requests\Store\UpdateRequest;
use App\Models\CityStore;
use App\Models\Store;
use Illuminate\Http\Request;

class StoreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $id)
    {
        $city = null;
		if ($id == 'all')
			$stores = Store::orderBy('city_store_id')->get();
		else {
			$city = CityStore::findOrFail($id);
			$stores = $city->stores;
		}

		return view('admin.stores.list')
			->with('stores', $stores)
			->with('city', $city);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id = null)
    {
		if(!$id)
			$city = null;
		else
			$city = CityStore::findOrFail($id);

        
		return view('admin.stores.create')
			->with('city', $city);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $store = new Store();
		$store->fill([
			'name' => $request->name,
            'url' => $request->url,
            'city_store_id' => $request->city,
			'is_active' => $request->is_active
		]);

		$max = Store::where('city_store_id', $request->city)
        ->max('show_order');
		if ($max == null)
			$max = 1;
		else
			$max += 1;

		$store->show_order = $max;

		//Activity::save_activity('CREATE.CAT.SUCCESS|CATEGORY', Auth::user(), $category, null, null, 'admins');
		$store->save();

        $city = CityStore::find($request->city);
		return redirect()->action('Admin\StoreController@create', $city->id)
			->with('UPDATE.OK', true)
            ->with('city', $city);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $store = Store::findOrFail($id);

		return view('admin.stores.edit')
			->with('store', $store);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        $store = Store::findOrFail($id);
		$store->fill([
			'name' => $request->name,
            'url' => $request->url,
			'is_active' => $request->is_active
		]);

		//Activity::save_activity('UPDATE.CAT.SUCCESS|CATEGORY', Auth::user(), $category, null, $category, 'admins');
		$store->save();

		return redirect()->action('Admin\StoreController@index', [$store->city_store_id])
			->with('UPDATE.OK', true);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function move($id, $move)
	{
		$curr = Store::findOrFail($id);

        //dd($curr);
		$next = null;
		if ($move > 0) {
			$next = Store::where('show_order', '>', $curr->show_order)
                ->where('city_store_id', $curr->city_store_id)
				->orderBy('show_order')
				->first();
		} elseif ($move < 0) {
			$next = Store::where('show_order', '<', $curr->show_order)
            ->where('city_store_id', $curr->city_store_id)
				->orderBy('show_order', 'desc')->first();
		}

		if (!$next)
			App::abort('404');

		$temp = $curr->show_order;
		$curr->show_order = $next->show_order;
		$next->show_order = $temp;

		$curr->save();
		$next->save();

		//Activity::save_activity('MOVE.CAT.SUCCESS|CATEGORY', Auth::user(), null, null, null, 'admins');
		return redirect()->action('Admin\StoreController@index', $curr->city_store_id)
			->with('UPDATE.OK', true);
	}
}
