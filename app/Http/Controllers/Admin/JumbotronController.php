<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Jumbotron\StoreRequest;
use App\Http\Requests\Jumbotron\UpdateRequest;
use App\Http\Controllers\Admin\Authenticate;
use Symfony\Component\HttpFoundation\Request;
use App\Models\Category;

use App\Models\Jumbotron;
use App\Models\Activity;

use Auth;

class JumbotronController extends Controller {

	use Authenticate;

	public function index($order)
	{
		if ($order == 'mobile') {
			$order = 'show_order_mobile';
			$view = 'admin.jumbotron.list_mobile';
		}
		else {
			$order = 'show_order';
			$view = 'admin.jumbotron.list';
		}
		$jumbotrons = Jumbotron::orderBy($order)->get();
		return view($view)
			->with('jumbotrons', $jumbotrons);
	}

	public function create()
	{

		return view('admin.jumbotron.create');
	}

	public function edit($id)
	{	
		$jumbotron = Jumbotron::findOrFail($id);
		return view('admin.jumbotron.edit')
			->with('jumbotron', $jumbotron);
	}

	public function store(Request $request)
	{
		$jumbotron = new Jumbotron();
		$jumbotron->fill([
			'title' => $request->title,
			'link' => $request->link,
			'state' => 'PUB',
			'color' => $request->color,
			'description' => $request->description,
			'font_color' => $request->font_color
		]);

		if ($request->hasFile('image'))
			$jumbotron->image = $request->file('image');

		if ($request->hasFile('image_detail'))
			$jumbotron->image_detail = $request->file('image_detail');

		$max = Jumbotron::max('show_order');
		if ($max == null)
			$max = 1;
		else
			$max += 1;

		$jumbotron->show_order = $max;
		
		//Activity::save_activity('CREATE.SUCCESS|JUMBOTRON', Auth::user(), $request, null, null, 'admins');
		if (!$jumbotron->save())
		
			@unlink($jumbotron->getImageFullPath());

		return redirect()->action('Admin\JumbotronController@create')
			->with('UPDATE.OK', true);
	}

	public function update(UpdateRequest $request, $id)
	{
		$jumbotron = Jumbotron::findOrFail($id);
		$jumbotron->fill([
			'title' => $request->title,
			'link' => $request->link,
			'state' => 'PUB',
			'color' => $request->color,
			'font_color' => $request->font_color,
			'description' => $request->description,
		]);

		if ($request->hasFile('image')) {
			$old_image = $jumbotron->getImageFullPath();
			$jumbotron->image = $request->file('image');
		}

		if ($request->hasFile('image_detail')) {
			$old_image = $jumbotron->getImageDetailFullPath();
			$jumbotron->image_detail = $request->file('image_detail');
		}

		//Activity::save_activity('UPDATE.SUCCESS|JUMBOTRON', Auth::user(), $jumbotron, null, $jumbotron, 'admins');
		if ($jumbotron->save())
			@unlink($old_image);
		else
			@unlink($jumbotron->getImageFullPath());

		return redirect()->action('Admin\JumbotronController@edit', [$id])
			->with('UPDATE.OK', true);
	}

	public function delete($id) 
	{
		$jumbotron = Jumbotron::findOrFail($id);
		$image = $jumbotron->getImageFullPath();

		//Activity::save_activity('DELETE.SUCCESS|JUMBOTRON', Auth::user(), $jumbotron, null, null, 'admins');

		if ($jumbotron->delete()) {
			@unlink($image);
			return redirect()->action('Admin\JumbotronController@index')
				->with('DELETE.OK', true);
		}

		return redirect()->action('Admin\JumbotronController@index')
				->with('DELETE.FAIL', true);
	}

	public function move($id, $move)
	{
		$curr = Jumbotron::findOrFail($id);

		$next = null;
		if ($move > 0) {
			$next = Jumbotron::where('show_order', '>', $curr->show_order)
				->orderBy('show_order')
				->first();
		} elseif ($move < 0) {
			$next = Jumbotron::where('show_order', '<', $curr->show_order)
				->orderBy('show_order', 'desc')
				->first();
		}

		if (!$next)
			abort('404');

		$temp = $curr->show_order;
		$curr->show_order = $next->show_order;
		$next->show_order = $temp;

		//Activity::save_activity('MOVE.SUCCESS|JUMBOTRON', Auth::user(), null, null, null, 'admins');

		$curr->save();
		$next->save();

		return redirect()->action('Admin\JumbotronController@index', 'desktop')
			->with('UPDATE.OK', true);
	}

	public function moveMobile($id, $move)
	{
		$curr = Jumbotron::findOrFail($id);

		$next = null;
		if ($move > 0) {
			$next = Jumbotron::where('show_order_mobile', '>', $curr->show_order_mobile)
				->orderBy('show_order_mobile')
				->first();
		} elseif ($move < 0) {
			$next = Jumbotron::where('show_order_mobile', '<', $curr->show_order_mobile)
				->orderBy('show_order_mobile', 'desc')
				->first();
		}

		if (!$next)
			abort('404');

		$temp = $curr->show_order_mobile;
		$curr->show_order_mobile = $next->show_order_mobile;
		
		$next->show_order_mobile = $temp;

		//Activity::save_activity('MOVE.SUCCESS|JUMBOTRON', Auth::user(), null, null, null, 'admins');

		$curr->save();
		$next->save();

		return redirect()->action('Admin\JumbotronController@index', 'mobile')
			->with('UPDATE.OK', true);
	}
}