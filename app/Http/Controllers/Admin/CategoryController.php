<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Category\StoreRequest;
use App\Http\Requests\Category\UpdateRequest;
use App\Http\Requests\Category\AddSubRequest;
use App\Http\Requests\Category\UpdateSubRequest;
use App\Http\Controllers\Admin\Authenticate;

use App\Models\Category;
use App\Models\CategorySubcategory;
use App\Models\Activity;

use Auth;

class CategoryController extends Controller {
	
	use Authenticate;

	public function index()
	{
		$categories = Category::orderBy('show_order')->paginate(10);
		return view('admin.category.list')
			->with('categories', $categories);
	}

	public function edit($id)
	{
		$category = Category::findOrFail($id);
		$subcategories = $category->subcategories()->get()
			->map(function($i) {
				return [
					'id' => $i->id,
					'name' => $i->name
				];
			});

		return view('admin.category.edit')
			->with('category', $category)
			->with('subcategories', $subcategories);
	}

	public function update(UpdateRequest $request, $id)
	{
		$category = Category::findOrFail($id);
		$category->fill([
			'name' => $request->name,
			'is_active' => $request->is_active
		]);

		//Activity::save_activity('UPDATE.CAT.SUCCESS|CATEGORY', Auth::user(), $category, null, $category, 'admins');
		$category->save();

		return redirect()->action('Admin\CategoryController@edit', [$category->id])
			->with('UPDATE.OK', true);
	}

	public function create()
	{

		return view('admin.category.create');
	}

	public function store(StoreRequest $request)
	{
		$category = new Category();
		$category->fill([
			'name' => $request->name,
			'is_active' => $request->is_active
		]);

		$max = Category::max('show_order');
		if ($max == null)
			$max = 1;
		else
			$max += 1;

		$category->show_order = $max;

		//Activity::save_activity('CREATE.CAT.SUCCESS|CATEGORY', Auth::user(), $category, null, null, 'admins');
		$category->save();

		return redirect()->action('Admin\CategoryController@create')
			->with('UPDATE.OK', true);
	}

	public function move($id, $move)
	{
		$curr = Category::findOrFail($id);

		$next = null;
		if ($move > 0) {
			$next = Category::where('show_order', '>', $curr->show_order)
				->orderBy('show_order')
				->first();
		} elseif ($move < 0) {
			$next = Category::where('show_order', '<', $curr->show_order)
				->orderBy('show_order', 'desc')
				->first();
		}

		if (!$next)
			App::abort('404');

		$temp = $curr->show_order;
		$curr->show_order = $next->show_order;
		$next->show_order = $temp;

		$curr->save();
		$next->save();

		//Activity::save_activity('MOVE.CAT.SUCCESS|CATEGORY', Auth::user(), null, null, null, 'admins');
		return redirect()->action('Admin\CategoryController@index')
			->with('UPDATE.OK', true);
	}

	public function addSubcategory(AddSubRequest $request, $id)
	{
		$subcategory = new CategorySubcategory();
		$subcategory->name = $request->subcategory_name;

		if ($request->hasFile('icon'))
			$subcategory->icon = $request->file('icon');

		$category = Category::findOrFail($id);
		if (!$category->subcategories()->save($subcategory))
			@unlink($subcategory->getIconFullPath());

		//Activity::save_activity('CREATE.SUBCAT.SUCCESS|SUB CATEGORY', Auth::user(), $request, $category, null, 'admins');
		return redirect()->action('Admin\CategoryController@edit', [$id])
			->with('UPDATE.OK', true);
	}

	public function updateSubcategory(UpdateSubRequest $request, $id, $subId)
	{
		$category = Category::findOrFail($id);
		
		$subcategory = CategorySubcategory::find($subId);
		$subcategory->name = $request->subcategory_name_edit;

		
		//Activity::save_activity('UPDATE.SUBCAT.SUCCESS|SUB CATEGORY', Auth::user(), $subcategory, $category, $subcategory, 'admins');
		if ($request->hasFile('icon_edit')) {
			$old_icon = $subcategory->getIconFullPath();
			$subcategory->icon = $request->file('icon_edit');
		}

		if ($subcategory->save())
			@unlink($old_icon);
		else
			@unlink($subcategory->getIconFullPath());

		return redirect()->action('Admin\CategoryController@edit', [$id])
			->with('UPDATE.OK', true);
	}

	public function deleteSubcategory($id, $subId)
	{
		//TODO: check relation product
		$category = Category::findOrFail($id);
		$subcategory = CategorySubcategory::findOrFail($subId);

		$subcategory->delete();

		//Activity::save_activity('DELETE.SUBCAT.SUCCESS|SUB CATEGORY', Auth::user(), $subcategory, $category, null, 'admins');
		return redirect()->action('Admin\CategoryController@edit', [$category->id])
			->with('DELETE.OK', true);
	}
}