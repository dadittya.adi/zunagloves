<?php namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Admin\Authenticate;

use App\Models\Activity;
use App\Models\Order;
use Carbon\Carbon;
use Response;
use Config;
use Auth;

use IntlDateFormatter;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

use Illuminate\Support\Str;

class OrderController extends Controller {
	
	use Authenticate;

	public function index(Request $request)
	{
		$orders = Order::orderBy('created_at', 'desc')->active();
		$filter_state = $request->input('filter_state', 'ALL');
		if ($filter_state != 'ALL')
			$orders = $orders->where('state', '=', $filter_state);

		$orders = $orders->paginate(10);

		return view('admin.order.list')
			->with('orders', $orders)
			->with('filter_state', $filter_state);
	}

	public function show($id)
	{
		$order = Order::findOrFail($id);
		return view('admin.order.detail')
			->with('order', $order);
	}

	public function manualPaid(Request $request, $id)
	{
		$order = Order::findOrFail($id);

		$now = Carbon::now();
		$paid_at = sprintf("%s/%s/%s %s:%s",
			$request->paid_at_day ? $request->paid_at_day : $now->format('d'),
			$request->paid_at_month ? $request->paid_at_month : $now->format('m'),
			$request->paid_at_year ? $request->paid_at_year : $now->format('Y'),
			$request->paid_at_hour ? $request->paid_at_hour : '00',
			$request->paid_at_minute ? $request->paid_at_minute : '00'
		);

		$order->fill([
			'paid_at' => Carbon::createFromFormat('d/m/Y H:i', $paid_at),
			'paid_amount' => $request->paid_amount,
			'paid_notes' => $request->paid_notes,
			'state' => 'PAI',
		]);

		//Activity::save_activity('UPDATE.SUCCESS|ORDER', Auth::user(), $order, null, $order, 'admins');
		if ($order->save())
			$order->sendNotif($order->email, $order->full_name);

		return redirect()->action('Admin\OrderController@show', [$order->id]);
	}

	public function paid($id)
	{
		$order = Order::findOrFail($id);
		$order->state = 'PAI';
		
		//Activity::save_activity('UPDATE.SUCCESS|ORDER', Auth::user(), $order, null, $order, 'admins');
		if ($order->save())
			$order->sendNotif($order->email, $order->full_name);

		return redirect()->action('Admin\OrderController@show', [$order->id]);
	}

	public function work(Request $request, $id)
	{
		$order = Order::findOrFail($id);
		$order->state = 'WRK';
		$order->work_notes = $request->work_notes;
		$order->process_date = Carbon::now();
		
		//Activity::save_activity('UPDATE.SUCCESS|ORDER', Auth::user(), $order, null, $order, 'admins');
		if ($order->save())
			$order->sendNotif($order->email, $order->full_name);

		return redirect()->action('Admin\OrderController@show', [$order->id]);
	}

	public function unpaid($id)
	{
		$order = Order::findOrFail($id);
		$order->state = 'INV';

		//Activity::save_activity('UPDATE.SUCCESS|ORDER', Auth::user(), $order, null, $order, 'admins');
		$order->save();

		return redirect()->action('Admin\OrderController@show', [$order->id]);
	}

	public function finish(Request $request, $id)
	{
		$order = Order::findOrFail($id);
		$order->state = 'DLV';
		$order->delivery_notes = $request->delivery_notes;
		$order->delivery_date = Carbon::now();
		
		//Activity::save_activity('UPDATE.SUCCESS|ORDER', Auth::user(), $order, null, $order, 'admins');
		if ($order->save())
			$order->sendNotif($order->email, $order->full_name);

		return redirect()->action('Admin\OrderController@show', [$order->id]);
	}

	public function reject(Request $request, $id)
	{
		$order = Order::findOrFail($id);
		$order->state = 'REJ';
		$order->reject_notes = $request->reject_notes;
		
		//Activity::save_activity('UPDATE.SUCCESS|ORDER', Auth::user(), $order, null, $order, 'admins');
		if ($order->save())
			$order->sendNotif($order->email, $order->full_name);

		return redirect()->action('Admin\OrderController@show', [$order->id]);
	}

	public function downloadBanknote($id)
	{
		$order = Order::findOrFail($id);

		if (!$order->banknote_image)
			return null;
	
		$resp = response()->download($order->getBanknoteImageFullPath());
		$resp->headers->set('Content-Disposition', 'inline');
		$resp->headers->set('X-Content-Type-Options', 'nosniff');
		
		//disable cache
        $resp->headers->set('Cache-Control', 'no-cache, no-store, must-revalidate'); // HTTP 1.1.
        $resp->headers->set('Pragma', 'no-cache'); // HTTP 1.0.
        $resp->headers->set('Expires', '0'); // Proxies.

        //Activity::save_activity('DOWNLOAD.BANK.SUCCESS|ORDER', Auth::user(), $order, null, null, 'admins');
		return $resp;
	}

	public function revision(Request $request, $id)
	{
		$order = Order::findOrFail($id);
		if ($order->state != 'INV')
			return redirect()->action('Admin\OrderController@show', [$id]);

		$rev_items = json_decode($request->items);
		foreach ($rev_items as $i) {
			$subtotal = (float)$i->qty * (float)$i->price;

			$discount = 0;
			$disc_percent = 0;
			if ($i->disc_percent > 0) {
				$disc_percent = (float)$i->disc_percent;
				$discount = $subtotal * $i->disc_percent / 100;
			}

			$item = $order->items()->findOrFail($i->id);
			$item->fill([
				'price' => (float)$i->price,
				'qty' => (float)$i->qty,
				'disc_percent' => $disc_percent,
				'discount' => $discount,
				'total' => $subtotal,
			]);
			$item->save();
		}

		$order->revision += 1;
		$order->weight = (float)$request->weight;
		$order->shipping_price = (float)$request->shipping;
		$order->voucher_discount = (float)$request->discount;
		
		//Activity::save_activity('UPDATE.SUCCESS|ORDER', Auth::user(), $order, null, $order, 'admins');
		if ($order->save())
			$order->sendNotif($order->email, $order->full_name, nl2br($request->mail_notes));

		return redirect()->action('Admin\OrderController@show', [$id]);
	}

	public function printInvoice($id)
	{
		$order = Order::findOrFail($id);

		//Activity::save_activity('PRINT.SUCCESS|ORDER', Auth::user(), $order, null, null, 'admins');
		return view('print_invoice')->with('order', $order);
	}

	static function filterDate($from_ori = null, $to_ori = null)
    {
        $from_date = Carbon::today();
        $to_date = Carbon::today()->endOfDay();
        
        if ($from_ori)
            $from_date = Carbon::createFromFormat('d/m/Y', $from_ori)->startOfDay();

        if ($to_ori)
            $to_date = Carbon::createFromFormat('d/m/Y', $to_ori)->endOfDay();

        if ($to_date < $from_date) 
            $to_date = Carbon::createFromFormat('d/m/Y', $from_ori)->endOfDay();

        return [$from_date, $to_date];
    }

	public function report(Request $request)
	{
		$date = static::filterDate($request->from_date, $request->to_date);
		$orders = Order::select('orders.*')
			->active()
			->where('orders.state', '=', 'DLV')
			->whereBetween('orders.created_at', [$date[0], $date[1]]);

		$q = $request->q;
		if ($q)
			$orders = $orders->leftJoin('customers', 'customers.id', '=', 'orders.customer_id')
				->where(function($query) use($q) {
					$query->where('order_number', 'like', "%$q%")
						->orWhere('full_name', 'like', "%$q%")
						->orWhere('email', 'like', "%$q%");
			});

		$orders = $orders->orderBy('orders.created_at', 'desc')
			->groupBy('orders.id')->get();

		return view('admin.order.order_report')
			->with('orders', $orders)
			->with('from_date', $date[0])
			->with('to_date', $date[1])
			->with('q', $q);
	}

	public function export(Request $request)
	{
		$date = static::filterDate($request->from_date, $request->to_date);
		$orders = Order::select('orders.*')
			->active()
			->where('orders.state', '=', 'DLV')
			->whereBetween('orders.created_at', [$date[0], $date[1]]);

		$q = $request->q;
		if ($q)
			$orders = $orders->leftJoin('customers', 'customers.id', '=', 'orders.customer_id')
				->where(function($query) use($q) {
					$query->where('order_number', 'like', "%$q%")
						->orWhere('full_name', 'like', "%$q%")
						->orWhere('email', 'like', "%$q%");
			});

		$orders = $orders->orderBy('orders.created_at', 'desc')
			->groupBy('orders.id')->get();

		$report_date = i18l_date($date[0], ['date' => IntlDateFormatter::LONG, 'time' => IntlDateFormatter::NONE]);

		if ($date[1] > $date[0]->addDay())
			$report_date .= ' to ' . i18l_date($date[1], ['date' => IntlDateFormatter::LONG, 'time' => IntlDateFormatter::NONE]);

	    // Create new PHPExcel object
	    //$objPHPExcel = new PHPExcel();
		$objPHPExcel = new Spreadsheet();

	    // Add header
	    $objPHPExcel->getActiveSheet()->getStyle('A1:H4')->getFont()->setBold(true);
	    $objPHPExcel->getActiveSheet()->getStyle('A1:H2')
	     	->getAlignment()
    		->setHorizontal('center');

	    $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:H1');
	    $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A2:H2');

	    $objPHPExcel->setActiveSheetIndex(0)
	        ->setCellValue('A1', 'Order Report')
	        ->setCellValue('A2', 'Date : ' . $report_date)
	        ->setCellValue('A4', '#')
	        ->setCellValue('B4', 'Date')
	        ->setCellValue('C4', 'Order Number')
	        ->setCellValue('D4', 'Customer')
	        ->setCellValue('E4', 'Product')
	        ->setCellValue('F4', 'Price')
	        ->setCellValue('G4', 'Qty')
	        ->setCellValue('H4', 'Subtotal');

	    //Add data from db
	    $row = 4;
	    $total = 0;
	    foreach ($orders as $key => $order) {
	    	$row++;
	    	$objPHPExcel->getActiveSheet()
				    ->getStyle("D$row")
				    ->getAlignment()
				    ->setWrapText(true);

			$objPHPExcel->getActiveSheet()
				->setCellValueExplicit("C$row", $order->order_number, DataType::TYPE_STRING);

	    	$sheet = $objPHPExcel->setActiveSheetIndex(0)
		        ->setCellValue("A$row", $key + 1)
		        ->setCellValue("B$row", $order->created_at->format('d/m/Y'))
		        ->setCellValue("D$row", $order->full_name . "\n (" . $order->email . ")");

		    foreach ($order->items as $item) {
		    	$disc = '';
		    	if ($item->discount > 0)
		    		$disc = " ( ". (float)$item->disc_percent . "%)";

		    	$row++;
		    	$sheet = $objPHPExcel->setActiveSheetIndex(0)
		    		->setCellValue("E$row", $item->product->name . $disc)
		    		->setCellValue("F$row", $item->price)
		    		->setCellValue("G$row", $item->qty)
		    		->setCellValue("H$row", $item->total - $item->discount);
		    }

		    if ($order->voucher_discount > 0) {
		    	$row++;
		    	$objPHPExcel->getActiveSheet()->getStyle("G$row:H$row")->getFont()->setBold(true);
			    $sheet = $objPHPExcel->setActiveSheetIndex(0)
			    	->setCellValue("G$row", 'Diskon')
			    	->setCellValue("H$row", $order->voucher_discount);
		    }

		    $subtotal = $order->total - $order->shipping_price;
		    $row++;
		    $objPHPExcel->getActiveSheet()->getStyle("G$row:H$row")->getFont()->setBold(true);
		    $sheet = $objPHPExcel->setActiveSheetIndex(0)
		    	->setCellValue("G$row", 'Subtotal')
		    	->setCellValue("H$row", $subtotal);

		    $total += $subtotal;
	    } 

	    $objPHPExcel->getActiveSheet()->getStyle("A5:H$row")
	    	->getAlignment()
    		->setVertical('top');

    	 $objPHPExcel->getActiveSheet()->getStyle("F5:H$row")
	     	->getAlignment()
    		->setHorizontal('right');

	    $row++;
	    $objPHPExcel->setActiveSheetIndex(0)->mergeCells("A$row:G$row");
	    $sheet = $objPHPExcel->setActiveSheetIndex(0)
	    	->setCellValue("A$row", 'TOTAL')
	    	->setCellValue("H$row", $total);
	    	
	    $objPHPExcel->getActiveSheet()->getStyle("A$row:H$row")->getFont()->setBold(true);
	    $objPHPExcel->getActiveSheet()->getStyle("A$row:G$row")
	     	->getAlignment()
    		->setHorizontal('right');

	    // Set autosize
	    for($col = 'A'; $col !== 'I'; $col++) {
		    $objPHPExcel->getActiveSheet()
		        ->getColumnDimension($col)
		        ->setAutoSize(true);
		}

	   // Rename worksheet
	    $objPHPExcel->getActiveSheet()->setTitle('Order Report');

	    // Set active sheet index to the first sheet, so Excel opens this as the first sheet
	    $objPHPExcel->setActiveSheetIndex(0);

	    $objWriter = new Xlsx($objPHPExcel, 'Excel2007');

	    $path = Config::get('storage.report');
	    $file = Str::random(32).'.xlsx';
	    $objWriter->save($path . '/' . $file);
   		
    	$resp = response()->download($path . '/' . $file);
    	$resp->headers->set('Content-Disposition', 'attachment;filename="Order Report.xlsx"');
    	$resp->headers->set('Content-Type', 'application/vnd.ms-excel');
    	$resp->headers->set('X-Content-Type-Options', 'nosniff');

    	//Activity::save_activity('EXPORT.SUCCESS|ORDER', Auth::user(), null, null, null, 'admins');    	
    	return $resp;
	}
}