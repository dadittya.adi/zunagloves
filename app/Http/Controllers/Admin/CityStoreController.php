<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CityStore\StoreRequest;
use App\Http\Requests\CityStore\UpdateRequest;
use App\Http\Controllers\Admin\Authenticate;

use App\Models\CityStore;
use Auth;

class CityStoreController extends Controller {
	
	use Authenticate;

	public function index()
	{
		$cityStore = CityStore::orderBy('show_order')->paginate(10);
		return view('admin.citystore.list')
			->with('citystore', $cityStore);
	}

	public function edit($id)
	{
		$cityStore = CityStore::findOrFail($id);

		return view('admin.citystore.edit')
			->with('city', $cityStore);
	}

	public function update(UpdateRequest $request, $id)
	{
		$cityStore = CityStore::findOrFail($id);
		$cityStore->fill([
			'name' => $request->name,
			'is_active' => $request->is_active
		]);

		//Activity::save_activity('UPDATE.CAT.SUCCESS|CATEGORY', Auth::user(), $category, null, $category, 'admins');
		$cityStore->save();

		return redirect()->action('Admin\CityStoreController@edit', [$cityStore->id])
			->with('UPDATE.OK', true);
	}

	public function create()
	{

		return view('admin.citystore.create');
	}

	public function store(StoreRequest $request)
	{
		$cityStore = new CityStore();
		$cityStore->fill([
			'name' => $request->name,
			'is_active' => $request->is_active
		]);

		$max = CityStore::max('show_order');
		if ($max == null)
			$max = 1;
		else
			$max += 1;

		$cityStore->show_order = $max;

		//Activity::save_activity('CREATE.CAT.SUCCESS|CATEGORY', Auth::user(), $category, null, null, 'admins');
		$cityStore->save();

		return redirect()->action('Admin\CityStoreController@create')
			->with('UPDATE.OK', true);
	}

	public function move($id, $move)
	{
		$curr = CityStore::findOrFail($id);

		$next = null;
		if ($move > 0) {
			$next = CityStore::where('show_order', '>', $curr->show_order)
				->orderBy('show_order')
				->first();
		} elseif ($move < 0) {
			$next = CityStore::where('show_order', '<', $curr->show_order)
				->orderBy('show_order', 'desc')
				->first();
		}

		if (!$next)
			App::abort('404');

		$temp = $curr->show_order;
		$curr->show_order = $next->show_order;
		$next->show_order = $temp;

		$curr->save();
		$next->save();

		//Activity::save_activity('MOVE.CAT.SUCCESS|CATEGORY', Auth::user(), null, null, null, 'admins');
		return redirect()->action('Admin\CityStoreController@index')
			->with('UPDATE.OK', true);
	}
}