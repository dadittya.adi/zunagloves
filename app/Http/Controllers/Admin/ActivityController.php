<?php namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Activity\StoreRequest;
use App\Http\Requests\Activity\UpdateRequest;
use App\Http\Controllers\Admin\Authenticate;

use App\Models\Activity;

use Hash;
use DB;
use Auth;

class ActivityController extends Controller {
	
	use Authenticate;

	public function index()
	{
		$activities = Activity::orderBy('created_at', 'desc')->paginate('10');
        //var_dump($request->header('User-Agent')); die;
		return view('admin.activity.list')
			->with('activities', $activities);
	}

	public function detail($id)
	{
		$activity = Activity::find($id);

        //var_dump($blogs); die;
		return view('admin.activity.detail')
			->with('activity', $activity);
	}

	public function delete($id)
	{
		$activity = Activity::findOrFail($id);
        $activity->delete();

        return redirect()->action('Admin\ActivityController@index')
			->with('DELETE.OK', true);
	}
}