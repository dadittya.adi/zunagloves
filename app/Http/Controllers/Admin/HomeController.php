<?php namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Admin\Authenticate;
use Illuminate\Support\Carbon;

use App\Models\Order;
use DB;

class HomeController extends Controller {
	
	use Authenticate;

	public function index()
	{
		//update expired transaction

        $_orders = Order::where('created_at', '<', Carbon::now()->subHour(24))
                        ->where('state', 'INV')->get();

		foreach ($_orders as $order) {
			$temp = Order::find($order->id);
			$temp->state = 'EXP';
			$temp->save();
		}
				

		$orders = Order::select('orders.state', DB::raw('count(*) as count'))
			->active()
			->whereNotIn('state',['REJ','DLV', 'EXP'])
			->groupBy('state')
			->get();
		return view('admin.home')
			->with('orders', $orders);
	}
}