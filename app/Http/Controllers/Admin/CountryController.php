<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Country\StoreRequest;
use App\Http\Requests\Country\UpdateRequest;
use App\Http\Controllers\Admin\Authenticate;

use App\Models\Country;
use App\Models\Activity;

use Auth;

class CountryController extends Controller {
	
	use Authenticate;

	public function index()
	{
		$countries = Country::orderBy('name')->get();
		return view('admin.country.list')
			->with('countries', $countries);
	}

	public function store(StoreRequest $request)
	{
		$country = new Country();
		$country->fill([
			'name' => $request->name
		]);

		$country->save();

		//Activity::save_activity('CREATE.SUCCESS|COUNTRY', Auth::user(), $request, null, null, 'admins');
		return redirect()->action('Admin\CountryController@index')
			->with('UPDATE.OK', true);
	}

	public function update(UpdateRequest $request, $id)
	{
		$country = Country::findOrFail($id);
		$country->fill([
			'name' => $request->name_edit
		]);

		//Activity::save_activity('UPDATE.SUCCESS|COUNTRY', Auth::user(), $country, null, $country, 'admins');
		$country->save();

		return redirect()->action('Admin\CountryController@index')
			->with('UPDATE.OK', true);
	}

	public function delete($id)
	{
		//TODO: check relation to products
		$country = Country::findOrFail($id);
		$country->delete();

		//Activity::save_activity('DELETE.SUCCESS|COUNTRY', Auth::user(), $country, null, null, 'admins');
		return redirect()->action('Admin\CountryController@index')
			->with('DELETE.OK', true);
	}
}