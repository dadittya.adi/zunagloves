<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Tag\StoreRequest;
use App\Http\Requests\Tag\UpdateRequest;
use App\Http\Controllers\Admin\Authenticate;

use App\Models\Activity;
use App\Models\Tag;
use App\Models\Blog;

use Auth;

class TagController extends Controller {
	
	use Authenticate;

	public function index()
	{
		$tags = Tag::orderBy('id')->paginate(10);
		return view('admin.tag.list')
			->with('tags', $tags);
	}

	public function create()
	{
		return view('admin.tag.add');
	}

	public function store(StoreRequest $request)
	{
		$tag = new Tag();
		$tag->fill([
			'name' => $request->name,
			'state' => $request->state,
			'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
		]);

		$tag->save();
		//Activity::save_activity('CREATE.SUCCESS|TAG', Auth::user(), $request, null, null, 'admins');
		return redirect()->action('Admin\TagController@index')
			->with('UPDATE.OK', true);
	}

	public function edit($id)
	{
		$tag = Tag::findOrFail($id);

		return view('admin.tag.edit')
			->with('tag', $tag);
	}

	public function update(UpdateRequest $request, $id)
	{
		$tag = Tag::findOrFail($id);
		$cek = Tag::where('tags.name', $request->name)->where('tags.id','!=',$id)->count();
		//var_dump($cek); die;
        if($cek > 0){
        	return redirect()->action('Admin\TagController@edit',$id)
				->with('UPD.TAG.FAIL', true);
        }
		$tag->fill([
			'name' => $request->name,
			'state' => $request->state,
		]);

		//Activity::save_activity('UPDATE.SUCCESS|TAG', Auth::user(), $tag, null, $tag, 'admins');
		$tag->save();

		return redirect()->action('Admin\TagController@index')
			->with('UPDATE.OK', true);
	}

	public function delete($id)
    {
        $tag = Tag::findOrFail($id);
        $tag->delete();

        //Activity::save_activity('DELETE.SUCCESS|TAG', Auth::user(), $tag, null, null, 'admins');
        return redirect()->action('Admin\TagController@index')
			->with('UPDATE.OK', true);
    }
}