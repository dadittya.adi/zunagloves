<?php namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;

use App\Models\Activity;

use Auth;
use Hash;

class AuthController extends Controller {

	/**
	 * The Guard implementation.
	 *
	 * @var \Illuminate\Contracts\Auth\Guard
	 */
	protected $auth;

	/**
	 * Create a new authentication controller instance.
	 *
	 * @param  \Illuminate\Contracts\Auth\Guard  $auth
	 * @param  \Illuminate\Contracts\Auth\Registrar  $registrar
	 * @return void
	 */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;

		$this->middleware('admin.guest', ['except' => [
			'getLogout',
			'getChangePassword',
			'postChangePassword'
		]]);
	}
	
	/**
	 * Show the application login form.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function getLogin()
	{
		return view('admin.auth.login');
	}

	/**
	 * Handle a login request to the application.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function postLogin(Request $request)
	{
		$this->validate($request, [
			'email' => 'required', 'password' => 'required',
		]);

		$credentials = $request->only('email', 'password');
		// limit active admin is able to login
		$credentials['is_active'] = 1;

		
		if (Auth::guard('admins')->attempt($credentials))
		{
			Activity::save_activity('LOGIN.SUCCESS|AUTHENTICATION', Auth::guard('admins')->user(), null, null, null, 'admins');
			return redirect()->intended(action('Admin\HomeController@index'));
		}
		
		return redirect()
			->action('Admin\AuthController@getLogin')
			->withInput($request->only('email', 'remember'))
			->withErrors([
				'email' => $this->getFailedLoginMessage(),
			]);
	}

	/**
	 * Get the failed login message.
	 *
	 * @return string
	 */
	protected function getFailedLoginMessage()
	{
		return 'Invalid email or password.';
	}

	/**
	 * Log the user out of the application.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function getLogout()
	{
		Activity::save_activity('LOGOUT.SUCCESS|AUTHENTICATION', Auth::guard('admins')->user(), null, null, null, 'admins');
		Auth::guard('admins')->logout();

		return redirect()->action('Admin\AuthController@getLogin');;
	}

	public function getChangePassword(Request $request)
	{
		return view('admin.auth.change_password');
	}

	public function postChangePassword(Request $request)
	{
		$this->validate($request, [
			'password_current' => 'required',
			'password_new' => 'required|different:password_current',
			'password_confirm' => 'required|same:password_new',
		]);

		if (!Hash::check($request->password_current, Auth::guard('admins')->user()->password))
			return redirect()
				->action('Admin\AuthController@getChangePassword')
				->with('PASSWORD_CHANGED.FAIL', true);

		$user = Auth::guard('admins')->user();
		$user->password = Hash::make($request->input('password_new'));
		$user->save();

		return redirect()
			->action('Admin\AuthController@getChangePassword')
			->with('PASSWORD_CHANGED.OK', true);
	}
}