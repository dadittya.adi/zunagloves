<?php namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Admin\Authenticate;

use App\Models\Activity;
use App\Models\SubscribeEmail;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Illuminate\Support\Str;

use Config;
use Auth;

class SubscribedEmailController extends Controller {
	
	use Authenticate;

	public function index(Request $request)
	{
        $search = $request->search;
        $emails = SubscribeEmail::orderBy('email');
        if($search){
            $emails = $emails->where(function($q) use ($search) {
                $q->where('subscribe_emails.email', 'like', "%$search%");
            });
        }
		$emails = $emails->get();
		return view('admin.subscribed_email.list')
			->with('emails', $emails);
	}

    public function delete($id)
    {
        $email = SubscribeEmail::findOrFail($id);
        //dd($email);
        //Activity::save_activity('DELETE.SUCCESS|SUBSCRIBED', Auth::user(), $email, null, null, 'admins');
        $email->delete();

        return redirect()->action('Admin\SubscribedEmailController@index')
            ->with('DELETE.OK', true);
    }

	public function export(Request $request)
	{
        $search = $request->search;
        $emails = SubscribeEmail::orderBy('email');
        if($search){
            $emails = $emails->where(function($q) use ($search) {
                $q->where('subscribe_emails.email', 'like', "%$search%");
            });
        }
        $emails = $emails->get();
        
        
// $spreadsheet = new Spreadsheet();
// $sheet = $spreadsheet->getActiveSheet();
// $sheet->setCellValue('A1', 'Hello World !');

// $writer = new Xlsx($spreadsheet);
// $writer->save('hello world.xlsx');

        $objPHPExcel = new Spreadsheet();

        // Add header
        $objPHPExcel->getActiveSheet()->getStyle('A1:B3')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A1:B3')
            ->getAlignment()
            ->setHorizontal('center');

        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:B1');

        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Daftar Subscribed Email')
            ->setCellValue('A3', '#')
            ->setCellValue('B3', 'EMAIL');

        //Add data from db
        $key = 0;
        foreach ($emails as $key => $email) {
            $row = $key + 4;

            $sheet = $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A' . $row, $key + 1)
                ->setCellValue('B' . $row, $email->email);
        }

        //var_dump($pbms_data->toArray());die;
        // Set autosize
        for($col = 'A'; $col !== 'C'; $col++) {
            $objPHPExcel->getActiveSheet()
                ->getColumnDimension($col)
                ->setAutoSize(true);
        }

       // Rename worksheet
        $objPHPExcel->getActiveSheet()->setTitle('Subscribed Email');

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);

        // $writer = new Xlsx($spreadsheet);
        // $writer->save('hello world.xlsx');
        //$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter = new Xlsx($objPHPExcel);

        $path = Config::get('storage.subscribe');
        $file = Str::random(32).'.xlsx';
        $objWriter->save($path . '/' . $file);
   
        $resp = response()->download($path . '/' . $file);
        $resp->headers->set('Content-Disposition', 'attachment;filename="Subscribed_Email.xlsx"');
        $resp->headers->set('Content-Type', 'application/vnd.ms-excel');
        $resp->headers->set('X-Content-Type-Options', 'nosniff');

        Activity::save_activity('EXPORT.SUCCESS|SUBSCRIBED', Auth::user(), null, null, null, 'admins');    
        return $resp;
	}
	
}