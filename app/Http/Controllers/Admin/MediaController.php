<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Media\StoreRequest;
use App\Http\Requests\Media\UpdateRequest;
use App\Http\Controllers\Admin\Authenticate;

use App\Models\Media;
use App\Models\Activity;
use Carbon\Carbon;
use Config;
use Auth;

class MediaController extends Controller {
	
	use Authenticate;

	public function index()
	{
		$media = Media::all();
		return view('admin.media.list')
			->with('media', $media);
	}

	public function store(StoreRequest $request)
	{
		$media = new Media();
		$file = $request->file('media');
		$file_name = null;
		$destinationPath = Config::get('storage.media') . '/' . Carbon::now()->year;
		
		$try = 250;
		do {
			if ($try <= 0)
				throw Exception("Storage::random fails to produce randomized filename");
			
			$hash = Carbon::now()->month."-".str_random(6).".".$file->getClientOriginalExtension();
			$file_name = $hash;
			$file_check = $destinationPath . '/' . $hash;
			$try -= 1;
		} while (file_exists($file_check));

		$uploadSuccess = $file->move($destinationPath, $file_name);
		$media->fill([
			'name' => $file->getClientOriginalName(),
			'filename' => $file_name,
			'upload_at' => Carbon::now()
		]);

		//Activity::save_activity('CREATE.SUCCESS|MEDIA', Auth::user(), $request, null, null, 'admins');
		$media->save();

		return redirect()->action('Admin\MediaController@index')
			->with('UPDATE.OK', true);
	}

	public function update(UpdateRequest $request, $id)
	{
		$media = Media::findOrFail($id);
		$media->name = $request->name;

		//Activity::save_activity('UPDATE.SUCCESS|MEDIA', Auth::user(), $media, null, $media, 'admins');
		$media->save();

		return redirect()->action('Admin\MediaController@index')
			->with('UPDATE.OK', true);
	}

	public function delete($id)
	{
		$media = Media::findOrFail($id);
		$path = Config::get('storage.media') . '/' . $media->upload_at->year . '/' . $media->filename;

		//Activity::save_activity('DELETE.SUCCESS|MEDIA', Auth::user(), $media, null, null, 'admins');
		if ($media->delete())
			@unlink($path);

		return redirect()->action('Admin\MediaController@index')
			->with('DELETE.OK', true);
	}
}