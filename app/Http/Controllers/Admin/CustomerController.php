<?php namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\Customer\StoreRequest;
use App\Http\Requests\Customer\UpdateRequest;
use App\Http\Controllers\Admin\Authenticate;

use App\Models\Activity;
use App\Models\Customer;
use App\Models\Country;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Illuminate\Support\Str;

use Hash;
use Config;
use Auth;

class CustomerController extends Controller {
	
	use Authenticate;

	public function index(Request $request)
	{
        $search = $request->search;
        $country = $request->country;
        $state = $request->state;

		$customers = Customer::orderBy('full_name');
        if($search){
            $customers = $customers->where(function($q) use ($search) {
                $q->where('customers.full_name', 'like', "%$search%")
                ->orWhere('customers.email', 'like', "%$search%")
                ->orWhere('customers.phone', 'like', "%$search%");
            });
        }
        if($country && $country != 'ALL'){
            $customers = $customers->where('country','=',$country);
        }
        if($state && $state != 'ALL'){
            $customers = $customers->where('state','=',$state);
        }

        $customers = $customers->paginate(20);
        //var_dump($customers->toArray());die;
		return view('admin.customer.list')
			->with('customers', $customers);
	}

	public function create()
	{
		$countries = Country::orderBy('name')->lists('name', 'name');
		return view('admin.customer.create');
	}

	public function edit($id)
	{
		$customer = Customer::findOrFail($id);

		return view('admin.customer.edit')
			->with('customer', $customer);
	}

	public function store(StoreRequest $request)
	{
		$customer = new Customer();
		$customer->fill([
			'full_name' => $request->full_name,
			'phone' => $request->phone,
			'email' => $request->email,
			'organization' => $request->organization,
			'address' => $request->address,
			'city' => $request->city,
			'province' => $request->province,
			'country' => $request->country,
			'postal_code' => $request->postal_code,
			'state' => $request->state,
			'password' => Hash::make('*')
		]);

        //Activity::save_activity('CREATE.SUCCESS|CUSTOMER', Auth::user(), $request, null, null, 'admins');
		$customer->save();

		return redirect()->action('Admin\CustomerController@create')
			->with('UPDATE.OK', true);
	}

	public function update(UpdateRequest $request, $id)
	{
		$customer = Customer::findOrFail($id);
		$customer->fill([
			'full_name' => $request->full_name,
			'phone' => $request->phone,
			'email' => $request->email,
			'organization' => $request->organization,
			'address' => $request->address,
			'city' => $request->city,
			'province' => $request->province,
			'country' => $request->country,
			'postal_code' => $request->postal_code,
			'state' => $request->state,
		]);

        //Activity::save_activity('UPDATE.SUCCESS|CUSTOMER', Auth::user(), $customer, null, $customer, 'admins');
		$customer->save();

		return redirect()->action('Admin\CustomerController@index')
			->with('UPDATE.OK', true);
	}

	public function detail($id)
	{
		$customer = Customer::findOrFail($id);
		return view('admin.customer.detail')
			->with('customer', $customer);
	}

	public function export(Request $request)
    {
        $search = $request->search;
        $state = $request->state;

        $customers = Customer::orderBy('full_name');
      
        if($search){
            $customers = $customers->where(function($q) use ($search) {
                $q->where('customers.full_name', 'like', "%$search%")
                ->orWhere('customers.email', 'like', "%$search%")
                ->orWhere('customers.phone', 'like', "%$search%");
            });
        }

        if($state && $state != 'ALL'){
            $customers = $customers->where('state','=',$state);
        }

        $customers = $customers->get();
        $objPHPExcel = new Spreadsheet();


        // Add header
        $objPHPExcel->getActiveSheet()->getStyle('A1:K3')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A1:K3')
            ->getAlignment()
            ->setHorizontal('center');

        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:K1');

        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Daftar Customer Zuna Gloves')
            ->setCellValue('A3', '#')
            ->setCellValue('B3', 'NAMA')
            ->setCellValue('C3', 'EMAIL')
            ->setCellValue('D3', 'TELEPON')
            ->setCellValue('E3', 'ALAMAT')
            //->setCellValue('F3', 'KECAMATAN')
            ->setCellValue('F3', 'KOTA')
            ->setCellValue('G3', 'PROVINSI')
            ->setCellValue('H3', 'KODE POS');

        //Add data from db
        $key = 0;
        foreach ($customers as $key => $customer) {
            $row = $key + 4;
            // $objPHPExcel->getActiveSheet()->setCellValueExplicit('F' . $row, $customer->phone, PHPExcel_Cell_DataType::TYPE_STRING);
            // $objPHPExcel->getActiveSheet()->setCellValueExplicit('C' . $row, $customer->member_number, PHPExcel_Cell_DataType::TYPE_STRING);
            	$state = '';
                if ($customer->state == 'NEW')
                    $state = 'New';
                elseif ($customer->state == 'ACT')
                    $state = 'Active';
                elseif ($customer->state == 'BAN')
                    $state = 'Banned';

            $sheet = $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A' . $row, $key + 1)
                ->setCellValue('B' . $row, $customer->full_name)
                ->setCellValue('C' . $row, $customer->email)
                ->setCellValue('D' . $row, $customer->phone)
                ->setCellValue('E' . $row, $customer->address)
                //->setCellValue('F' . $row, $customer->sub_district)
                ->setCellValue('F' . $row, $customer->city != null ? $customer->cityb->city_name : "")
                ->setCellValue('G' . $row, $customer->province != null ? $customer->provinceb->province : "")
                ->setCellValue('H' . $row, $customer->postal_code);
        }

        //var_dump($pbms_data->toArray());die;
        // Set autosize
        for($col = 'A'; $col !== 'H'; $col++) {
            $objPHPExcel->getActiveSheet()
                ->getColumnDimension($col)
                ->setAutoSize(true);
        }

       // Rename worksheet
        $objPHPExcel->getActiveSheet()->setTitle('Customer_List');

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);

        $objWriter = new Xlsx($objPHPExcel);

        $path = Config::get('storage.customer');
        $file = Str::random(32).'.xlsx';
        $objWriter->save($path . '/' . $file);
   
        $resp = response()->download($path . '/' . $file);
        $resp->headers->set('Content-Disposition', 'attachment;filename="Customer_List.xlsx"');
        $resp->headers->set('Content-Type', 'application/vnd.ms-excel');
        $resp->headers->set('X-Content-Type-Options', 'nosniff');
        //Activity::save_activity('EXPORT.SUCCESS|CUSTOMER', Auth::user(), null, null, null, 'admins');     
        return $resp;
    }
}