<?php namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Voucher\StoreRequest;
use App\Http\Requests\Voucher\UpdateRequest;
use App\Http\Controllers\Admin\Authenticate;
use Illuminate\Support\MessageBag;

use App\Models\Activity;
use App\Models\Voucher;
use Carbon\Carbon;

use Auth;

class VoucherController extends Controller {
	
	use Authenticate;

	public function index()
	{	
		$vouchers = Voucher::where('is_auto_generated', 0)
						->paginate(10);
		return view('admin.voucher.list')
			->with('vouchers', $vouchers);
	}

	public function create()
	{
		return view('admin.voucher.create');
	}

	public function edit($id)
	{
		$voucher = Voucher::findOrFail($id);
		return view('admin.voucher.edit')
			->with('voucher', $voucher);
	}

	public function store(StoreRequest $request)
	{
		$voucher = new Voucher();
		$voucher->fill([
			'code' => $request->code,
			'promo' => $request->promo,
			'type' => $request->type,
			'discount' => $request->discount,
			'start_date' => Carbon::createFromFormat('d/m/Y', $request->start_date)->startOfDay(),
			'end_date' => Carbon::createFromFormat('d/m/Y', $request->end_date)->endOfDay(),
			'minimum' => $request->minimum,
		]);

		if ($voucher->end_date <= $voucher->start_date) {
			$bag = new MessageBag();
			$bag->add('end_date', 'End date must be after start date.');
			return redirect()->back()
				->withInput()
				->withErrors($bag)
				->with('UPDATE.FAIL', true);
		}

		//Activity::save_activity('CREATE.SUCCESS|VOUCHER', Auth::user(), $request, null, null, 'admins');
		$voucher->save();

		return redirect()->action('Admin\VoucherController@index')
			->with('UPDATE.OK', true);
	}

	public function update(UpdateRequest $request, $id)
	{
		$voucher = Voucher::findOrFail($id);
		$voucher->fill([
			'code' => $request->code,
			'promo' => $request->promo,
			'type' => $request->type,
			'discount' => $request->discount,
			'start_date' => Carbon::createFromFormat('d/m/Y', $request->start_date)->startOfDay(),
			'end_date' => Carbon::createFromFormat('d/m/Y', $request->end_date)->endOfDay(),
			'minimum' => $request->minimum,
		]);

		if ($voucher->end_date <= $voucher->start_date) {
			$bag = new MessageBag();
			$bag->add('end_date', 'End date must be after start date.');
			return redirect()->back()
				->withInput()
				->withErrors($bag)
				->with('UPDATE.FAIL', true);
		}

		//Activity::save_activity('UPDATE.SUCCESS|VOUCHER', Auth::user(), $voucher, null, $voucher, 'admins');
		$voucher->save();

		return redirect()->action('Admin\VoucherController@edit', [$id])
			->with('UPDATE.OK', true);
	}

	public function delete($id)
	{
		$voucher = Voucher::findOrFail($id);
		//Activity::save_activity('DELETE.SUCCESS|VOUCHER', Auth::user(), $voucher, null, null, 'admins');
		$voucher->delete();

		return redirect()->action('Admin\VoucherController@index')
			->with('DELETE.OK', true);
	}
}