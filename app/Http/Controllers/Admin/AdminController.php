<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreRequest;
use App\Http\Requests\Admin\UpdateRequest;
use App\Http\Controllers\Admin\Authenticate;

use App\Models\Admin;
use App\Models\Group;
use App\Models\Activity;

use Hash;
use Auth;

class AdminController extends Controller {

	use Authenticate;

	public function index()
	{
		$admins = Admin::orderBy('full_name')->paginate('10');
		return view('admin.admin.list')
			->with('admins', $admins);
	}

	public function create()
	{
		$admin = new Admin();
		$groups = Group::orderBy('name')->get();
		return view('admin.admin.create')
			->with('admin', $admin)
            ->with('groups', $groups);
	}

	public function edit($id)
	{	
		$admin = Admin::findOrFail($id);
		$groups = Group::orderBy('name')->get();

		return view('admin.admin.edit')
			->with('admin', $admin)
			->with('groups', $groups);
	}

	public function store(StoreRequest $request)
	{
		$admin = new Admin();
		$admin->fill([
			'full_name' => $request->full_name,
			'email' => $request->email,
			'password' => Hash::make($request->password),
			'is_active' => $request->is_active
		]);

		$admin->save();

		$groups = json_decode($request->groups);
        $groups = array_map(function ($i) {
            return $i->id;
        }, $groups);

        //TODO: logchild
        //$log->logChild();
        $admin->groups()->sync($groups);

        //Activity::save_activity('CREATE.SUCCESS|ADMIN', Auth::user(), $request, null, null, 'admins');
		return redirect()->action('Admin\AdminController@create')
			->with('UPDATE.OK', true);
	}

	public function update(UpdateRequest $request, $id)
	{
		$admin = Admin::findOrFail($id);
		$before = with (clone $admin);
		$admin->fill([
			'full_name' => $request->full_name,
			'email' => $request->email,
			'is_active' => $request->is_active
		]);

		if ($request->password)
			$admin->password = Hash::make($request->password);

		//Activity::save_activity('UPDATE.SUCCESS|ADMIN', Auth::user(), $before, null, $admin, 'admins');

		$admin->save();

		$groups = json_decode($request->groups);
        $groups = array_map(function ($i) {
            return $i->id;
        }, $groups);

        //TODO: logchild
        //$log->logChild();
        $admin->groups()->sync($groups);

		return redirect()->action('Admin\AdminController@edit', [$id])
			->with('UPDATE.OK', true);
	}
}