<?php namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProductColor\StoreRequest;
use App\Http\Requests\ProductColor\UpdateRequest;
use App\Http\Controllers\Admin\Authenticate;

use App\Models\Product;
use App\Models\ProductColor;
use App\Models\ProductColorImage;
use App\Models\Activity;
use App\Models\ProductStock;
use Auth;

class ProductColorController extends Controller {
	
	use Authenticate;

	public function index(Request $request, $id = 'all')
	{
		$product = null;
		if ($id == 'all')
			$colors = ProductColor::orderBy('product_id')->get();
		else {
			$product = Product::findOrFail($id);
			$colors = $product->colors;
		}

		$auto_product = Product::select('id', 'name')->get();
		$products = Product::orderBy('name')->pluck('name', 'id');

		return view('admin.productcolor.list')
			->with('colors', $colors)
			->with('products', $products)
			->with('product_id', $id)
			->with('auto_product', $auto_product)
			->with('product', $product);
	}

	public function create(Request $request, $id = null)
	{
		$products = Product::orderBy('name')->pluck('name', 'id');
		
		if(!$id)
			$product = null;
		else
			$product = Product::findOrFail($id);

		$auto_product = Product::select('id', 'name')->get();

		return view('admin.productcolor.create')
			->with('products', $products)
			->with('selected_product', $id)
			->with('auto_product', $auto_product)
			->with('product', $product);
	}

	public function edit($id)
	{
		$color = ProductColor::findOrFail($id);
		$products = Product::orderBy('name')->pluck('name', 'id');

		return view('admin.productcolor.edit')
			->with('color', $color)
			->with('products', $products);
	}

	public function store(StoreRequest $request)
	{	
		
		$pro = Product::where('name','=',$request->product_name)->where('id','=', $request->product)->first(); 
		if(!$pro)
			return redirect()->back()->withInput()
   				->withErrors(['pro'=>'Product not found'])
   				->with('UPDATE.FAIL', true);

		if (!$request->hasFile('product_image')){
			return redirect()->back()->withInput()
   				->withErrors(['product_image'=>'Minimal 1 Product Image'])
   				->with('UPDATE.FAIL', true);
		}
		
		$product = Product::findOrFail($request->product);

		$color = new ProductColor();
		$color->fill([
			'name' => $request->name,
			'type' => $request->type,
			'sku' => $request->sku,
			'stock_out' => $request->stock_out,

		]);


		if ($request->hasFile('color_image'))
			$color->color_image = $request->file('color_image');

		/*if ($request->hasFile('product_image'))
			$color->product_image = $request->file('product_image');*/
		
		$flag = false;
		foreach ($request->product_image as $key => $val) {
			if(!$flag && $val != null){
				$color->product_image = $val;
				$flag = true;
			}
		}
		
		Activity::save_activity('CREATE.SUCCESS|PRODUCT COLOR', Auth::user(), $request, $product, null, 'admins');

		$product->colors()->save($color);
		
		foreach ($request->product_image as $key => $value) {
			if($value != null){
				//var_dump($value->isValid());
				if($value->isValid()){
					//var_dump($value->isValid());
					$col = $color->uploadImage($value);

					$color_image = new ProductColorImage();
					$color_image->fill([
						'product_color_id' => $color->id,
						'image' => $col
					]);
					$color_image->save();
				}else{
					$color_image = new ProductColorImage();
					$color_image->fill([
						'product_color_id' => $color->id,
						'image' => $color->product_image
					]);
					$color_image->save();
				}
				
			}
		}
		
		//dd($product->productSizes);
		foreach ($product->productSizes as $key => $value) {
			$productStock = new ProductStock();
			$productStock->fill([
				'product_id' =>  $product->id,
				'product_productsize_id' => $value->id,
				'product_color_id' =>$color->id,
				'sku' => $color->sku . $value->size->short_name
			]);
			$productStock->save();
		}

		return redirect()->action('Admin\ProductColorController@index', [$product->id])
			->with('UPDATE.OK', true);
	}

	public function update(UpdateRequest $request, $id)
	{

		$color = ProductColor::findOrFail($id);
		$color->fill([
			'name' => $request->name,
			'type' => $request->type,
			'sku' => $request->sku,
			'stock_out' => $request->stock_out,
		]);

		if ($request->hasFile('color_image')) {
			$old_color_image = $color->getColorImageFullPath();
			$color->color_image = $request->file('color_image');
		}

		if ($request->hasFile('product_image')){
			$old_product_image = $color->getProductImageFullPath();
			$flag = false;
			foreach ($request->product_image as $key => $val) {
				if(!$flag && $val != null){
					$color->product_image = $val;
					$flag = true;
				}
			}
		} 
		

		Activity::save_activity('UPDATE.SUCCESS|PRODUCT COLOR', Auth::user(), $color, null, $color, 'admins');
		
		if ($color->save()) {
			@unlink($old_color_image);
			//@unlink($old_product_image);
		} else {
			@unlink($color->getColorImageFullPath());
			//@unlink($color->getProductImageFullPath());
		}

		if ($request->hasFile('product_image')){
			$color_images = $color->colorImages;
			foreach ($color_images as $key => $i) {
				@unlink($i->getProductImageFullPath());
				$i->delete();
			}
			foreach ($request->product_image as $key => $value) {
				if($value != null){
					//var_dump($value->isValid());
					if($value->isValid()){
						//var_dump($value->isValid());
						$col = $color->uploadImage($value);
						//var_dump($col);
						

						$color_image = new ProductColorImage();
						$color_image->fill([
							'product_color_id' => $color->id,
							'image' => $col
						]);
						$color_image->save();
					}else{
						$color_image = new ProductColorImage();
						$color_image->fill([
							'product_color_id' => $color->id,
							'image' => $color->product_image
						]);
						$color_image->save();
					}
					
				}
			}
			
		} 

		return redirect()->action('Admin\ProductColorController@edit', [$id])
			->with('UPDATE.OK', true);
	}

	public function delete($id)
	{
		$color = ProductColor::findOrFail($id);
		$color->productStocks()->delete();
		$color_images = $color->colorImages;
		foreach ($color_images as $key => $i) {
			@unlink($i->getProductImageFullPath());
			$i->delete();
		}

		Activity::save_activity('DELETE.SUCCESS|PRODUCT COLOR', Auth::user(), $color, null, null, 'admins');
		
		if ($color->delete()) {
			@unlink($color->getProductImageFullPath());
			//@unlink($old_product_image);
		}

		return redirect()->action('Admin\ProductColorController@index',[$color->product_id])
			->with('DELETE.OK', true);
	}
}