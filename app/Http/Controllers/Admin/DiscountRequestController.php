<?php namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\PopUpSetting\UpdateRequest;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Admin\Authenticate;

use App\Models\Activity;
use App\Models\DiscountRequest;
use App\Models\PromoPopUp;
use App\Models\SubscribeEmail;

use PHPExcel;
use PHPExcel_IOFactory;
use PHPExcel_Cell_DataType;

use Config;
use Auth;

class DiscountRequestController extends Controller {
	
	use Authenticate;

	public function index(Request $request)
	{
        $search = $request->search;
        $emails = DiscountRequest::orderBy('email');
        if($search){
            $emails = $emails->where(function($q) use ($search) {
                $q->where('customer_discounts.email', 'like', "%$search%");
            });
        }
		$emails = $emails->get();
		return view('admin.discount_request.list')
			->with('emails', $emails);
	}

    public function delete($id)
    {
        $email = DiscountRequest::findOrFail($id);
        //Activity::save_activity('DELETE.SUCCESS|SUBSCRIBED', Auth::user(), $email, null, null, 'admins');
        $email->delete();

        return redirect()->action('Admin\SubscribedEmailController@index')
            ->with('DELETE.OK', true);
    }

	public function export(Request $request)
	{
        $search = $request->search;
        $search = $request->search;
        $emails = DiscountRequest::orderBy('email');
        if($search){
            $emails = $emails->where(function($q) use ($search) {
                $q->where('customer_discounts.email', 'like', "%$search%");
            });
        }
        $emails = $emails->get();
        
        $objPHPExcel = new PHPExcel();

        // Add header
        $objPHPExcel->getActiveSheet()->getStyle('A1:B3')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A1:B3')
            ->getAlignment()
            ->setHorizontal('center');

        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:B1');

        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Daftar Subscribed Email')
            ->setCellValue('A3', '#')
            ->setCellValue('B3', 'EMAIL');

        //Add data from db
        $key = 0;
        foreach ($emails as $key => $email) {
            $row = $key + 4;

            $sheet = $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A' . $row, $key + 1)
                ->setCellValue('B' . $row, $email->email);
        }

        //var_dump($pbms_data->toArray());die;
        // Set autosize
        for($col = 'A'; $col !== 'C'; $col++) {
            $objPHPExcel->getActiveSheet()
                ->getColumnDimension($col)
                ->setAutoSize(true);
        }

       // Rename worksheet
        $objPHPExcel->getActiveSheet()->setTitle('Subscribed Email');

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        $path = Config::get('storage.subscribe');
        $file = str_random(32).'.xlsx';
        $objWriter->save($path . '/' . $file);
   
        $resp = response()->download($path . '/' . $file);
        $resp->headers->set('Content-Disposition', 'attachment;filename="Subscribed_Email.xlsx"');
        $resp->headers->set('Content-Type', 'application/vnd.ms-excel');
        $resp->headers->set('X-Content-Type-Options', 'nosniff');

        //Activity::save_activity('EXPORT.SUCCESS|SUBSCRIBED', Auth::user(), null, null, null, 'admins');    
        return $resp;
	}

    public function getImage()
	{	
		$jumbotron = PromoPopUp::first();
		return view('admin.discount_request.edit')
			->with('jumbotron', $jumbotron);
	}
	
    public function updateImage(UpdateRequest $request, $id)
	{
		$promoPopUp = PromoPopUp::findOrFail($id);
        // /var_dump($request->minimum);die();
		if ($request->hasFile('image')) {
            
			$old_image = $promoPopUp->getImageFullPath();
			$promoPopUp->image = $request->file('image');
		}

        $promoPopUp->code_length = $request->code_legth;
        $promoPopUp->valid_in = $request->valid_in;
        $promoPopUp->promo = $request->promo;
        $promoPopUp->type = $request->type;
        $promoPopUp->discount = $request->discount;
        $promoPopUp->minimum = $request->minimum;    
        $promoPopUp->code_length = $request->code_length;


		//Activity::save_activity('UPDATE.SUCCESS|JUMBOTRON', Auth::user(), $jumbotron, null, $jumbotron, 'admins');
		if ($promoPopUp->save())
			@unlink($old_image);
		else
			@unlink($promoPopUp->getImageFullPath());

		return redirect()->action('Admin\DiscountRequestController@getImage', [$id])
			->with('UPDATE.OK', true);
	}
}