<?php namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Blog\StoreRequest;
use App\Http\Requests\Blog\UpdateRequest;
use App\Http\Controllers\Admin\Authenticate;
use League\CommonMark\CommonMarkConverter;

use App\Models\Activity;
use App\Models\Blog;
use App\Models\BlogsMedia;
use App\Models\Tag;

use Hash;
use DB;
use Auth;

class BlogController extends Controller {
	
	use Authenticate;

	public function index()
	{
		$blogs = DB::table('blogs')->get();
        //var_dump($request->header('User-Agent')); die;
		$tags = Tag::orderBy('name');
		return view('admin.blog.list')
			->with('blogs', $blogs)
			->with('tags', $tags);
	}

	public function create()
	{
		$media = BlogsMedia::all();
		$tags = DB::table('tags')
                        ->where('tags.state', '=', 1)
                        ->orderBy('name', 'asc')
                        ->get();
		$active = 'create';
		$converter = new CommonMarkConverter();
		//Don't change this example
		$example = "
		# Try CommonMark (Heading 1)
		## Heading 2
		![Image](https://pbs.twimg.com/profile_images/2149314222/square_normal.png)

		You can try CommonMark here.  This dingus is powered by
		[commonmark.js](https://github.com/jgm/commonmark.js), the
		JavaScript reference implementation.

		(new paragraph)You can try CommonMark here.  This dingus is powered by
		[commonmark.js](https://github.com/jgm/commonmark.js), the
		JavaScript reference implementation.

		> Blockquote

		1. item one
		2. item two
		   - sublist
		   - sublist

		Horizontal line

		---
		`Inline code` with backticks

		```
		# code block
		print '3 backticks or'
		print 'indent 4 spaces'
		```
		";
		//var_dump(Auth::admin()->get()->id); die;
		return view('admin.blog.add')
			->with('tags', $tags)
			->with('active', $active)
			->with('example', $example)
			->with('media', $media)
			->with('converter',$converter);
	}

	public function store(StoreRequest $request)
	{
		$writter = Auth::admin()->get()->id;

		$content = $request->content;
		$converter = new CommonMarkConverter();
		$content_html = $converter->convertToHtml($content);
		$title = ucwords($request->title);
		$blog = new Blog();
		$blog->fill([
			'title' => $title,
			'permalink' => $request->permalink,
			'content' => $content,
			'content_html' => $content_html,
			'publish_state' => $request->publish_state,
			'publish_at' => date('Y-m-d H:i:s'),
			'writen_by' => $writter,
			'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
		]);
		//var_dump($request->ip()); die;
		$tags = $request->tags;

		if($blog->save() && ($tags != null || $tags != '')){
			foreach($tags as $key=>$tag){
				$tagIn = Tag::find($tag);
				$blog->tag()->attach($tag);
			}
		}

		//Activity::save_activity('CREATE.SUCCESS|BLOG', Auth::user(), $request, null, null, 'admins');

		return redirect()->action('Admin\BlogController@index')
			->with('UPDATE.OK', true);
	}

	public function edit($id)
	{
		$active = 'create';
		$media = BlogsMedia::all();
		$blogs = DB::table('blogs')
                ->where('blogs.id', '=', $id)
                ->get();
		
		$tags = DB::table('tags')
			->where('tags.state', '=', 1)
			->get();
		
		$blog_tags = DB::table('blog_tag')
                    ->where('blog_tag.blog_id', '=', $id)
                    ->get();
        $tagselected = array();
        foreach($blog_tags as $key => $blog_tag){
        	$tagselected[$blog_tag->tag_id] = $blog_tag->blog_id;
        }

        //var_dump($blogs); die;
		return view('admin.blog.edit')
			->with('blogs', $blogs)
			->with('tags', $tags)
			->with('active', $active)
			->with('media', $media)
			->with('tagselected', $tagselected);
	}

	public function update(UpdateRequest $request, $id)
	{
		$blog = Blog::findOrFail($id);
		$before = clone($blog);
		$writter = Auth::admin()->get()->id;
		$cek = DB::table('blogs')
                        ->where('blogs.permalink', '=', $request->permalink)
                        ->where('blogs.id', '!=', $id)
                        ->count();
		//var_dump($request); die;
        if($cek > 0){
        	return redirect()->action('Admin\BlogController@edit',$id)
				->with('UPD.BLOG.FAIL', true);
        }
		$converter = new CommonMarkConverter();
		$content_html = $converter->convertToHtml($request->content);
		$title = ucwords($request->title);
		
		$blog->fill([
			'title' => $title,
			'permalink' => $request->permalink,
			'content' => $request->content,
			'content_html' => $content_html,
			'publish_state' => $request->publish_state,
			'writen_by' => $writter,
		]);
		//var_dump($request->tags); die;
		$tags = $request->tags;
		if($tags == null || $tags == ''){
			DB::table('blog_tag')->where('blog_id', $id)->delete();
		}
		//Activity::save_activity('UPDATE.SUCCESS|BLOG', Auth::user(), $before, null, $blog, 'admins');
		if($blog->save() && ($tags != null || $tags != '')){
			DB::table('blog_tag')->where('blog_id', $id)->delete();
			foreach($tags as $key=>$tag){
				$tagIn = Tag::find($tag);
				$blog->tag()->attach($tag);
			}
		}

		//var_dump($before->getDirty());die;

		return redirect()->action('Admin\BlogController@index')
			->with('UPDATE.OK', true);
	}

	public function delete($id)
	{
		$blog = Blog::findOrFail($id);
        $blog->delete();
        DB::table('blog_tag')->where('blog_id', $id)->delete();

       // Activity::save_activity('DELETE.SUCCESS|BLOG', Auth::user(), $blog, null, null, 'admins');

        return redirect()->action('Admin\BlogController@index')
			->with('DELETE.OK', true);
	}
}