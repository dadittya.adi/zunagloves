<?php namespace App\Http\Controllers\Admin;

trait Authenticate {
	
	public function __construct()
	{
		$this->middleware('admin.auth');
	}
}