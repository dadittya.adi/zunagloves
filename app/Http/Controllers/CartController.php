<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Activity;
use App\Models\Product;
use App\Models\ProductSize;
use App\Models\ProductProductsize;
use App\Models\Country;
use App\Models\Customer;
use App\Models\Shipping;
use App\Models\ShippingPrice;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Address;
use App\Models\Voucher;
use App\Models\SubscribeEmail;

use App\Veritrans\Midtrans;

use StdClass;
use Session;
use Auth;
use App;
use App\Models\ProductColor;
use Config;
use Carbon\Carbon;
use Log;

class CartController extends Controller {

	public function __construct()
    {
        // $this->middleware('auth', ['only' => [
        // 	'showCheckout',
        // 	'doCheckout'
        // ]]);

		Midtrans::$serverKey = Config::get('payment.veritrans.server_key');
        //set is production to true for production mode
        Midtrans::$isProduction = Config::get('payment.veritrans.production');
    }

	public function index()
	{
		$cart = $this->getCart();
		//$voucher = $this->getSessionVoucher();		

		return response()->json($cart, 200); 
	}

	static public function getCart()
	{
		return Session::get('cart', array());
	}

	static function getSessionVoucher()
	{
		$voucher = Session::get('voucher', array());
		if ($voucher)
			$voucher = json_decode($voucher);

		return $voucher;
	}

	public function add(Request $request, $id)
	{
		$product = Product::findOrFail($id);

		$details = new StdClass();
		$details->version = 1;
		$details->id = $id;
		
		if ($request->size) {
			$size = $product->sizes()->where('productsize_id', '=', $request->size)->first();
			if ($size)
				$details->size = $size->name;
		}

		if ($request->color) {
			$color = $product->colors()->where('id', '=', $request->color)->first();
			if ($color) {
				$details->color = $color->name;
				$details->color_id = $request->color;
			}

			if ($color->stock_out)
				return redirect()->back()
					->with('_CART.OUT_OF_STOCK', true);
		}
		
		$details->name = $product->name;
		$details->weight = (float)$product->weight;
		$details->qty = (float)$request->qty;
		$details->price = $product->price;
		$details->total = $details->qty * $details->price;

		if ($product->discount > 0) {
			$details->price_discount = $product->price_discount;
			$details->disc_percent = $product->discount;
			$details->disc_amount = $details->total * $details->disc_percent / 100;
			$details->total -= $details->disc_amount;
		}
		$this->putProduct($details);

		return redirect()->action('CartController@index');
	}

	protected function putProduct($item)
	{
		$cart = $this->getCart();
		$cart[] = $item;
		Session::put('cart', $cart);
	}

	static public function toProduct($item)
	{
		if (isset(static::$_cache[$item->id]))
			return static::$_cache[$item->id];

		$p = Product::find($item->id);
		if (!$p)
			return new Product();
		
		static::$_cache[$item->id] = $p;
		return $p;
	}

	public function remove($id)
	{
		$this->discardProduct($id);
		return redirect()->action('CartController@index');
	}

	protected function discardProduct($key)
	{
		$cart = $this->getCart();
		if ($key >= count($cart))
			return redirect()->action('CartController@index');

		$item = $cart[$key];

		unset($cart[$key]);
		$cart = array_values($cart);

		Session::put('cart', $cart);
	}

	public function showCheckout()
	{
		$cart = $this->getCart();
		$voucher = $this->getSessionVoucher();		

		if (count($cart) == 0)
			return redirect()->action('CartController@index');

		$check = Session::get('check');

		$countries = Country::orderBy('name')->lists('name', 'name');
		$customer = Auth::customer()->check() ? Auth::customer()->get() : new Customer();

		$providers = [];
		foreach (Shipping::orderBy('show_order')->get() as $i) {
			$providers[$i->country->name][] = [
				'id' => $i->id,
				'display_name' => $i->display_name
			];
		}

		$shippings = [];
		foreach (ShippingPrice::orderBy('shipping_id')->orderBy('destination')->get() as $i) {
			$shippings[$i->shipping_id][] = [
				'id' => $i->id,
				'destination' => $i->destination,
				'price' => (float)$i->price,
				'time' => $i->time
			];
		}

		return view('zuna.shopping_checkout')
			->with('cart', $cart)
			->with('voucher', $voucher)
			->with('check', $check)
			->with('countries', $countries)
			->with('providers', $providers)
			->with('shippings', $shippings)
			->with('customer', $customer);
	}

	static function putCheckoutSession($request)
	{
		$check = new StdClass();
		$check->bill = [
			'country' => $request->billing_country,
			'full_name' => $request->billing_full_name,
			'phone' => $request->billing_phone,
			'organization' => $request->billing_organization,
			'address' => $request->billing_address,
			'city' => $request->billing_city,
			'province' => $request->billing_province,
			'postal_code' => $request->billing_postal_code,
		];

		if ($request->use_billing_address)
			$check->ship = $check->bill;
		else
			$check->ship = [
				'country' => $request->shipping_country,
				'full_name' => $request->shipping_full_name,
				'phone' => $request->shipping_phone,
				'organization' => $request->shipping_organization,
				'address' => $request->shipping_address,
				'city' => $request->shipping_city,
				'province' => $request->shipping_province,
				'postal_code' => $request->shipping_postal_code,
			];

		$dest_id = $request->destination;
		$destination = ShippingPrice::find($dest_id);
		if (!$destination)
			return redirect()->action('CartController@showCheckout');

		$check->provider = $destination->shipping->name;
		$check->dest_id = $dest_id;

		Session::put('check', $check);
	}

	public function getVoucher(Request $request)
	{	
		$code = $request->code;
		

		$voucher = Voucher::where('code', '=', $code)->first();
		if (!$voucher || $voucher->state == 'Inactive' || $voucher->state == 'Expired')
			return response()->json([
				'status' => 'fail',
				'message' => 'Voucher is not available'
			], 200);
		
		if ($voucher->is_auto_generated && $voucher->counter > 0) {
			return response()->json([
				'status' => 'fail',
				'message' => 'Voucher is already used'
			], 200);
		}

		$amount = $request->amount;
		if ($voucher->minimum > $amount)
		return response()->json([
			'status' => 'fail',
			'message' => 'Minimum transaction has not been reached'
		], 200);

		$saving = $voucher->discount;
		if ($voucher->type == 'PERCENT') {
			$saving = $amount * ($voucher->discount / 100);
		}
		$voucher->discount = $saving;
		Session::put('voucher', $voucher->toJson());
		return response()->json([
			'status' => 'available',
			'message' => "Voucher $voucher->promo successfully applied! You save " . number_format($voucher->discount, 0, ',', '.'),
			'data' => $voucher
		], 200);
	
	}

	public function removeVoucher()
	{
		Session::forget('voucher');
		return redirect()->action('CartController@index');
	}

	public function doCheckout(Request $request)
	{

		// return response()->json($request, 200);

		// static::putCheckoutSession($request);
		// var_dump(Midtrans::$serverKey);
		// var_dump(Midtrans::$isProduction);
		// var_dump(Midtrans::getBaseUrl());
		// die();
		$cart = $this->getCart();

		if (count($cart) == 0)
			return redirect()->action('CartController@index');

		checkIfProvinceExist( $request->province);
		checkIfCityExist( $request->city);
		checkIfSubdistrictExist( $request->district);
		
		// data billing
		$bill = [
			'country' => 1,
			'full_name' => $request->full_name,
			'phone' => $request->phone,
			'organization' => null,
			'address' => $request->address,
			'city' => $request->city,
			'sub_district' => $request->district,
			'province' => $request->province, 
			'postal_code' => $request->post_code,
			'type' => 'BILL',
		];

		$billaddr = new Address();
		$billaddr->fill($bill);
		$billaddr->save();

		// shipping data
		$ship = [
			'country' => 1,
			'full_name' => $request->full_name,
			'phone' => $request->phone,
			'organization' => null,
			'address' => $request->address,
			'city' => $request->city,  
			'sub_district' => $request->district,
			'province' => $request->province,
			'postal_code' => $request->post_code,
			'type' => 'SHIP',
		];

		$shipaddr = new Address();
		$shipaddr->fill($ship);
		$shipaddr->save();

		//save address
		$saveaddr = $request->save_address;

		if (Auth::check()) {
			$customer = Auth::guard('web')->user();
		}
		else {
			$customer = null;
		}

		
		if ($saveaddr) {
			$customer->address = $request->address;
			$customer->city = $request->city;
			$customer->province = $request->province;
			$customer->postal_code = $request->post_code;

			// $customer->fill([
			// 	'country' => 1,
			// 	'organization' => null,
			// 	'address' => $request->address,
			// 	'city' => $request->city,
			// 	'province' => $request->province,
			// 	'postal_code' => $request->post_code,
			// ]);
			$customer->save();

		}

		//data order
		// $provider = Shipping::findOrFail($request->provider);
		// $shipping = $provider->prices()->where('id', '=', $request->dest_id)->where('destination','=',$request->dest)->first();
		// if (!$shipping)
		// 	return redirect()->back()->withInput()
		// 		->withErrors(['dest_error'=>'Destination tidak terdaftar'])
   		// 		->with('UPDATE.FAIL', true);

		$order = new Order();
		$order->fill([
			'domain' => Config::get('app.domain'),
			'state' => 'INV',
			'revision' => 0,
			'billing_addr' => $billaddr->id,
			'shipping_addr' => $billaddr->id,
			'use_billing_addr' => false,
			'provider' => $request->courier,
			'full_name'    => $request->full_name,
			'email'         => $request->email,
			'phone'         => $request->phone,
			'shipping_price' => $request->ongkir,
			'shipping_time' => '0' // todolist : ambil dari etd shiping yang dipilih oleh user
		]);
		if ($customer != null) {
			$order->customer_id = $customer->id;
			
			$customer->dob = $request->dob;
			$customer->address = $request->address;
			$customer->city = $request->city;
			$customer->province = $request->province;
			$customer->postal_code = $request->post_code;
			$customer->phone = $request->phone;

			// $customer->fill([
			// 	'country' => 1,
			// 	'organization' => null,
			// 	'address' => $request->address,
			// 	'city' => $request->city,
			// 	'province' => $request->province,
			// 	'postal_code' => $request->post_code,
			// ]);
			$customer->save();

			Activity::save_activity('CHECKOUT.SUCCESS|ORDER', Auth::guard('web')->user(), $order, null, $order, 'customers');
		}

		$order->save();

		//order number
		$now = Carbon::now()->format('YmdHi');
		$order->order_number = $now . substr('00' . e($order->id), -2, 2);

		
		$weight = 0; $total = 0;
		//data order items
		//dd($cart);
		$item_details = array();
		foreach ($cart as $i) {
			$subtotal = $i->qty * $i->price;

			$discount = 0;
			$disc_percent = 0;
			if (isset($i->disc_percent)) {
				$disc_percent = $i->disc_percent;
				$discount = $subtotal * $i->disc_percent / 100;
			}
			
			$product = Product::find($i->product_id);

			$productColor = ProductColor::where('name', $i->color)
								->where('product_id', $i->product_id)
								->first();
			$size = ProductSize::where('short_name', $i->size)->first();
			$productProductSize = ProductProductsize::where('product_id', $i->product_id)
									->where('productsize_id', $size->id)
									->first();

			$item = new OrderItem();
			$item->fill([
				'product_id' => $i->product_id,
				'product_color_id' => $productColor->id,
				'product_productsize_id' => $size->id,
				'color' => isset($i->color) ? $i->color : null,
				'size' => isset($i->size) ? $i->size : null,
				'qty' => $i->qty,
				'price' => $i->price,
				'disc_percent' => $disc_percent,
				'discount' => $discount,
				'total' => $subtotal,
				'notes' => isset($i->notes) ? $i->notes : null
			]);

			$itemList = array();
			$itemList['id'] = '';
			$itemList['price'] = $i->price;
			$itemList['quantity'] = $i->qty;
			$itemList['name'] = $product->name;

			array_push($item_details, $itemList);

			$order->items()->save($item);
			$weight += ($product->weight * $i->qty); //todolist : ambil weight sesuai dengan barang yang dipilih oleh user
			$total = $total + $subtotal - $discount;
		}

		$itemList = array();
		$itemList['id'] = '';
		$itemList['price'] = $request->ongkir;
		$itemList['quantity'] = 1;
		$itemList['name'] = 'Shipping Fee';

		array_push($item_details, $itemList);
	

		$order->weight = ceil($weight/1000);
		$voucher = $this->getSessionVoucher();

		$disc_voucher = 0;
		if ($voucher && $total >= $voucher->minimum) {
			$order->voucher_id = $voucher->id;
			$order->voucher_code = $voucher->code;
			$order->voucher_promo = $voucher->promo;
			$order->voucher_discount = $voucher->discount;
			$order->voucher_minimum = $voucher->minimum;

			$itemList = array();
			$itemList['id'] = '';
			$itemList['price'] =  $voucher->discount * - 1;
			$itemList['quantity'] = 1;
			$itemList['name'] = 'Discount';

			array_push($item_details, $itemList);

			$disc_voucher = $voucher->discount;

		}

		$payload = [
			'transaction_details' => [
			  'order_id'      => $order->order_number,
			  'gross_amount'  => $total + $request->ongkir - $disc_voucher,
			],
			'credit_card'=> [
				'secure'=> true
			],
			'customer_details' => [
			  'first_name'    => $request->full_name,
			  'email'         => $request->email,
			  'phone'         => $request->phone,
			],
			'item_details' => $item_details
		  ];
	
		if ($request->voucher != null) {
			$voucher = Voucher::where('code', $request->voucher)->first();
			if ($voucher) {
				$voucher->counter = $voucher->counter+1;
				$voucher->save();
			}
		}
	
		$snapToken = Midtrans::getSnapToken($payload);
		$order->snap_token = $snapToken;

		$order->save();
		$order->sendNotif($request->email, $request->full_name);

		$_subscribeCheck = SubscribeEmail::where('email', $request->email)->count();
		if ($_subscribeCheck == 0) {
			$_email = new SubscribeEmail();
			$_email->fill(
				['email' => $request->email]
			);

			$_email->save();	
		}

		return response()->json($snapToken);
	}

	public function showOrderComplete($order_number)
	{
		$order = Order::where('order_number', '=', $order_number)->first();
		if (!$order)
			App::abort('404');

		return view('zuna.shopping_complete')
			->with('order', $order);
	}
}