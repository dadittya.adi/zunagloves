<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Controllers\LandingController;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Socialite;
use App\Models\Customer;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;
use Illuminate\Routing\Route;

class LoginController extends LandingController
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        if ($request->has('cart')) {
            $this->storeCart($request);
            $this->redirectTo = '/checkout';
        }
        $this->middleware('guest')->except('logout');
    }

    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->stateless()->redirect();
    }

    public function handleProviderCallback($provider)
    {
        $userSocial = Socialite::driver($provider)->stateless()->user();

        if ($userSocial->getEmail() != null) {
            $users       =   Customer::where(['email' => $userSocial->getEmail()])->first();
            if($users) {
                Auth::login($users);
                return redirect()->route('web.home');
            }
            else {
            $user = Customer::create([
                    'full_name'     => $userSocial->getName(),
                    'email'         => $userSocial->getEmail(),
                    'photo'         => $userSocial->getAvatar(),
                    'provider_id'   => $userSocial->getId(),
                    'provider'      => $provider,
                    'state'         => 'ACT'
                ]);
                Auth::login($user);
                return redirect()->route('web.home');
            }
        }
        else {
            return redirect()->route('web.home');
        }
    }
}
