<?php

use \App\Vendor\Durenworks\I18lFilter;
use App\Models\Blog;
use Carbon\Carbon;
use App\Models\City;
use App\Models\Province;
use App\Models\SubDistrict;

if (!function_exists('i18l_date'))
{
	function i18l_date($value, $pattern = array(), $locale = null, $timezone = null) {
		return I18lFilter::date($value, $pattern, $locale, $timezone);
	}
}

if (!function_exists('i18l_currency'))
{
	function i18l_currency($value, $locale = null) {
		$a = str_replace("IDR", "IDR ", I18lFilter::currency($value, $locale));
		$a = str_replace("SGD", "SGD ", $a);
		return $a;
	}
}

if (!function_exists('i18l_number'))
{
	function i18l_number($value, $pattern = null, $locale = null) {
		return I18lFilter::number($value, $pattern, $locale);
	}
}

if (!function_exists('action_cached'))
{
	function action_cached($timestamp, $action, $param) {
		$url = action($action, $param);

		$query = parse_url($url, PHP_URL_QUERY);

		if ($query) {
		    $url .= '&_cached=';
		} else {
		    $url .= '?_cached=';
		}

		return $url . $timestamp->format('YmdHi');
	}
}

if (!function_exists('blogs_footer'))
{
	function blogs_footer() {
		$blogs = Blog::where('publish_state', 'PUB')
				->orderBy('publish_at', 'desc')
				->get()->take(2);

		return $blogs;
	}
}

if (!function_exists('blogs_date'))
{
	function blogs_date($str,$opt) {
		$day = Carbon::createFromFormat('Y-m-d H:i:s', $str)->day;
		$monthNum = Carbon::createFromFormat('Y-m-d H:i:s', $str)->month;
		$dateObj   = DateTime::createFromFormat('!m', $monthNum);
		$monthName = $dateObj->format('F');
		$monthName = strtoupper(substr($monthName, 0, 3));
		if($opt == 'day'){
			return $day;
		}else if($opt == 'month'){
			return $monthName;
		}else{
			return '';
		}
	}
}

if (!function_exists('blogs_content_footer'))
{
	function blogs_content_footer($str) {
		$cleaned = clean($str);
		$rest = substr($cleaned, 0, 55);
		$pos = strrpos($rest, ' ');
		$rest = substr($rest, 0, $pos);

		return $rest;
	}
}

if (!function_exists('blogs_content'))
{
	function blogs_content($str) {
		$rest = substr($str, 0, 512);
		$pos1 = strrpos($rest, '. ');
		$rest = substr($rest, 0, $pos1+1);

		return $rest;
	}
}

if (!function_exists('substr_act_msg'))
{
	function substr_act_msg($str) {
		$rest = substr($str, 0, 40);
		$pos1 = strrpos($rest, ' ');
		$rest = substr($rest, 0, $pos1);

		return $rest;
	}
}

if (!function_exists('getProvince'))
{
    function getProvince($id = null) {

        $key = Config::get('app.rajaongkir');
        $url = "http://pro.rajaongkir.com/api/province";
        if($id != null){
            $url .= '?id='.$id;
        }

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "key:".$key
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return "cURL Error #:" . $err;
        } else {
            // var_dump($response);die;
            return $response;
        }
    }
}

if (!function_exists('getProvinceForInsert'))
{
    function getProvinceForInsert() {
        $getProvince = json_decode(getProvince(), true);

        $aaa = array('province_id' => '0', 'province' => '-- Select Province --');

        if ($getProvince != null) {
            array_unshift( $getProvince['rajaongkir']['results'], $aaa );
            $provinces = [];
            // $cities[0] = ['','','','','-- Select City --',''];
            if ($getProvince['rajaongkir']['status']['code'] === 200) {
                $provinces = $getProvince['rajaongkir']['results'];
            }
        }
        else {
            $provinces = $aaa;
        }

        //$provinces = json_encode($provinces);

        //return $provinces;
        return collect($provinces)->pluck('province', 'province_id');
    }
}

if (!function_exists('getCity'))
{
    function getCity($province_id = null, $city_id = null) {
        $key = Config::get('app.rajaongkir');
        $url = "http://pro.rajaongkir.com/api/city";
        if($province_id != null && $city_id == null){
            $url .= '?province='.$province_id;
        }elseif ($province_id != null && $city_id != null) {
            $url .= '?id='.$city_id.'&province='.$province_id;
        }elseif ($city_id != null) {
            $url .= '?id='.$city_id;
        }

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "key:".$key
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return "cURL Error #:" . $err;
        } else {
            return $response;
        }
    }
}

if(!function_exists('getSubdistrict')){

    function getSubdistrict($city_id = null, $subdistrict_id = null){

        $key = Config::get('app.rajaongkir');
        $url = "http://pro.rajaongkir.com/api/subdistrict";
        if($city_id != null && $subdistrict_id == null){
            $url .= '?city='.$city_id;
        }elseif ($city_id != null && $subdistrict_id != null) {
            $url .= '?id='.$subdistrict_id.'&city='.$city_id;
        }elseif ($subdistrict_id != null) {
            $url .= '?id='.$subdistrict_id;
        }
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "key:".$key
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return "cURL Error #:" . $err;
        } else {
            return $response;
        }
    }
}

if (!function_exists('getRajaongkirCost'))
{
    function getRajaongkirCost($origin_id, $originType, $destination, $destinationType, $weight, $courier) {
        $key = Config::get('app.rajaongkir');
        $url = "https://pro.rajaongkir.com/api/cost";
        
        $curl = curl_init();

        $post = [
            'origin' => $origin_id,
            'originType' => $originType,
            'destination'   => $destination,
            'destinationType' => $destinationType,
            'weight' => $weight,
            'courier' => $courier
        ];

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => $post,
            CURLOPT_HTTPHEADER => array(
                "key:".$key
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return "cURL Error #:" . $err;
        } else {
            return $response;
        }
    }
}

if (!function_exists('checkIfProvinceExist')) {
    function checkIfProvinceExist($id)
    {
        if (!Province::find($id)) {
            $province = json_decode(getProvince($id), true);

            if($province['rajaongkir']['status']['code'] === 200){
                $province = $province['rajaongkir']['results'];

                Province::create([
                    'province_id' => $province['province_id'],
                    'province' => $province['province']
                ]);
            }
        }
    }
}

if (!function_exists('checkIfCityExist')) {
    function checkIfCityExist($id)
    {
        if (!City::find($id)) {
            $city = json_decode(getCity(null, $id), true);

            //var_dump($city);die();
            if($city['rajaongkir']['status']['code'] === 200){
                $city = $city['rajaongkir']['results'];

                City::create([
                    'city_id' => $city['city_id'],
                    'province_id' => $city['province_id'],
                    'type' => $city['type'],
                    'city_name' => $city['city_name'],
                    'postal_code' => $city['postal_code']
                ]);
            }
        }
    }
}

if (!function_exists('checkIfSubdistrictExist')) {
    function checkIfSubdistrictExist($id)
    {
        if (!SubDistrict::find($id)) {
            $subDistrict = json_decode(getSubdistrict(null, $id), true);


            if($subDistrict['rajaongkir']['status']['code'] === 200){
                $subDistrict = $subDistrict['rajaongkir']['results'];

                SubDistrict::create([
                    'subdistrict_id' => $subDistrict['subdistrict_id'],
                    'province_id' => $subDistrict['province_id'],
                    'city_id' => $subDistrict['city_id'],
                    'subdistrict_name' => $subDistrict['subdistrict_name'],
                ]);

            }
        }
    }
}
