<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['as' => 'web.'], function () {
    
    Route::get('/', 'LandingController@home')->name('home');
    Route::get('find-us', 'LandingController@find_us')->name('find_us');
    Route::get('contact', 'LandingController@contact')->name('contact');
    Route::get('about', 'LandingController@about')->name('about');
    Route::get('size', 'LandingController@size')->name('size');
    Route::get('shipping', 'LandingController@shipping')->name('shipping');
    Route::get('policy', 'LandingController@policy')->name('policy');
    Route::get('tos', 'LandingController@tos')->name('tos');
    Route::get('faq', 'LandingController@faq')->name('faq');
    
    
    Route::get('product/{category}', 'LandingController@category')->name('category');
    Route::get('product/{category}/{product}', 'LandingController@product')->name('product');
    Route::get('product/', 'LandingController@search')->name('product.search');

    Route::get('cart', 'LandingController@cart')->name('cart');
    Route::post('cart', 'LandingController@storeCart')->name('cart.store');

    Route::get('/content/color/image/{id}', 'LandingController@showColorImage');
    Route::get('/content/showcase/image/{id}', 'LandingController@showShowcaseImage')->name('showcase.image');
    Route::get('/content/showcase/image/detail/{id}', 'LandingController@showShowcaseDetailImage')->name('showcase.detail.image');
    Route::get('/content/popup/image/{id}', 'LandingController@showPopupImage')->name('popup.image');
    Route::get('/content/product/{id}/imagecolor', 'LandingController@showProductColorImage');
    Route::get('/content/product/{id}/image', 'LandingController@showProductImage');

    Route::get('testemail', 'EmailController@testMail')->name('testMail');

    Route::post('/message/send', 'LandingController@sendMessage')->name('contact_us');

});

Auth::routes();

Route::get('checkout', 'LandingController@checkout')->name('checkout');
Route::post('checkout', 'CartController@doCheckout')->name('checkout.store');
Route::get('payment', 'LandingController@payment')->name('payment');

Route::get('/cart/voucher', 'CartController@getVoucher')->name('voucher.check');
Route::get('/cart/voucher/remove', 'CartController@removeVoucher'); 

Route::post('/subscribe/email', 'LandingController@subscribeEmail')->name('subscribe.store');
Route::post('/disc_request', 'LandingController@discountRequest')->name('discount.request');


/**
 * Route to get region
 */
Route::get('/get-regions/city/db', 'RegionController@getCityFromDb')->name('region.get_city.db');
Route::get('/get-regions/subdistrict/db', 'RegionController@getSubDistrictFromDb')->name('region.get_subdistrict.db');

Route::post('/get-regions/city/external', 'RegionController@getCity')->name('region.get_city');
Route::post('/get-regions/subdistrict/external', 'RegionController@getSubDistrict')->name('region.get_subdistrict');
Route::post('/get-regions/cost', 'RegionController@getCost')->name('region.get_cost');


Route::group(['prefix' => 'dashboard','as' => 'dashboard.'], function () {
    Route::get('/', 'DashboardController@home')->name('home');
    Route::get('/profile', 'DashboardController@profile')->name('profile');
    Route::put('/profile', 'DashboardController@updateProfile')->name('profile.update');
    Route::put('/update/password', 'DashboardController@updatePassword')->name('password.update');
    Route::get('/address', 'DashboardController@address')->name('address');
    Route::put('/address', 'DashboardController@updateAddress')->name('address.update');
});

Route::post('checkstock', 'LandingController@checkStock');
Route::get('login/{provider}/social', 'Auth\LoginController@redirectToProvider')->name('login.social');
Route::get('login/{provider}/callback', 'Auth\LoginController@handleProviderCallback');

Auth::routes();

/*
 * Payment Gateway
 */
// Veritrans notification
Route::post('/api/payment/notif/veritrans', 'PaymentController@notifVeritrans');



/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function ()
{
    Route::get('/', 'HomeController@index');

    Route::get('/auth/login', 'AuthController@getLogin');
    Route::get('/auth/logout', 'AuthController@getLogout');
    Route::get('/auth/password/change', 'AuthController@getChangePassword');
    
    Route::post('/auth/login', 'AuthController@postLogin');
    Route::post('/auth/password/change', 'AuthController@postChangePassword');

    //product 
    Route::get('/product', 'ProductController@index');

    Route::get('/product/create', [
    	//'middleware' => 'auth.app.product',
    	'uses' => 'ProductController@create'
		]);
    Route::get('/product/{id}', [
            //'middleware' => 'auth.app.product',
            'uses' => 'ProductController@edit'
            ]);

    Route::post('/product/create', [
            //'middleware' => 'auth.app.product',
            'uses' => 'ProductController@store'
            ]);
    Route::post('/product/{id}', [
            //'middleware' => 'auth.app.product',
            'uses' => 'ProductController@update'
            ]);

    /*
    * Productsize
    */
    Route::get('/productsize', [
        //'middleware' => 'auth.app.product',
        'uses' => 'ProductsizeController@index'
        ]);

    Route::post('productsize', [
        //'middleware' => 'auth.app.product',
        'uses' => 'ProductsizeController@store'
        ]);
    Route::post('productsize/{id}', [
        //'middleware' => 'auth.app.product',
        'uses' => 'ProductsizeController@update'
        ]);
    Route::post('/productsize/{id}/delete', [
        //'middleware' => 'auth.app.product',
        'uses' => 'ProductsizeController@delete'
        ]);
    Route::post('productsize/{id}/move/{move}', [
        //'middleware' => 'auth.app.product',
        'uses' => 'ProductsizeController@move'
        ]);


    /*
    * ProductColor
    */
    Route::get('productcolor/create/{id?}', [
        //'middleware' => 'auth.app.product',
        'uses' => 'ProductColorController@create'
        ]);
    Route::get('productcolor/{id?}', [
        //'middleware' => 'auth.app.product',
        'uses' => 'ProductColorController@index'
        ]);
    Route::get('productcolor/{id}/edit', [
        //'middleware' => 'auth.app.product',
        'uses' => 'ProductColorController@edit'
        ]);

    Route::post('productcolor', [
        //'middleware' => 'auth.app.product',
        'uses' => 'ProductColorController@store'
        ]);
    Route::post('productcolor/{id}', [
        //'middleware' => 'auth.app.product',
        'uses' => 'ProductColorController@update'
        ]);
    Route::post('productcolor/{id}/delete', [
        //'middleware' => 'auth.app.product',
        'uses' => 'ProductColorController@delete'
        ]);

    /*
    * ProductStock
    */
    Route::get('productstock/{id?}', [
        //'middleware' => 'auth.app.product',
        'uses' => 'ProductStockController@index'
        ]);

    Route::post('productstock/{id?}/update', [
        //'middleware' => 'auth.app.product',
        'uses' => 'ProductStockController@update'
        ]);
    /*
    * Media
    */
    Route::get('/media', [
        //'middleware' => 'auth.app.product',
        'uses' => 'MediaController@index'
        ]);

    Route::post('media/create', [
        //'middleware' => 'auth.app.product',
        'uses' => 'MediaController@store'
        ]);
    Route::post('media/{id}', [
        //'middleware' => 'auth.app.product',
        'uses' => 'MediaController@update'
        ]);
    Route::post('media/{id}/delete', [
        //'middleware' => 'auth.app.product',
        'uses' => 'MediaController@delete'
        ]);

    /*
    * Category
    */
    Route::get('/category', [
        //'middleware' => 'auth.app.product',
        'uses' => 'CategoryController@index'
        ]);

        Route::get('/category/create', [
            //'middleware' => 'auth.app.product',
            'uses' => 'CategoryController@create'
            ]);
    
        Route::post('/category/create', [
            //'middleware' => 'auth.app.product',
            'uses' => 'CategoryController@store'
            ]);
    Route::get('/category/{id}', [
        //'middleware' => 'auth.app.product',
        'uses' => 'CategoryController@edit'
        ]);

    Route::post('/category/{id}', [
        //'middleware' => 'auth.app.product',
        'uses' => 'CategoryController@update'
        ]);
    
    Route::post('/category/{id}', [
        //'middleware' => 'auth.app.product',
        'uses' => 'CategoryController@update'
        ]);
    Route::post('/category/{id}/move/{move}', [
        //'middleware' => 'auth.app.product',
        'uses' => 'CategoryController@move'
        ]);

    Route::post('/category/{id}/add', [
        //'middleware' => 'auth.app.product',
        'uses' => 'CategoryController@addSubcategory'
        ]);
    Route::post('/category/{id}/update/{subId}', [
        //'middleware' => 'auth.app.product',
        'uses' => 'CategoryController@updateSubcategory'
        ]);
    Route::post('/category/{id}/delete/{subId}', [
        //'middleware' => 'auth.app.product',
        'uses' => 'CategoryController@deleteSubcategory'
        ]);


         /*
    * City Store
    */
    Route::get('/citystore', [
    //'middleware' => 'auth.app.product',
    'uses' => 'CityStoreController@index'
    ]);

    Route::get('/citystore/create', [
        //'middleware' => 'auth.app.product',
        'uses' => 'CityStoreController@create'
        ]);

    Route::post('/citystore/create', [
        //'middleware' => 'auth.app.product',
        'uses' => 'CityStoreController@store'
        ]);
    Route::get('/citystore/{id}', [
    //'middleware' => 'auth.app.product',
    'uses' => 'CityStoreController@edit'
    ]);

    Route::post('/citystore/{id}', [
    //'middleware' => 'auth.app.product',
    'uses' => 'CityStoreController@update'
    ]);

    Route::post('/citystore/{id}', [
    //'middleware' => 'auth.app.product',
    'uses' => 'CityStoreController@update'
    ]);
    Route::post('/citystore/{id}/move/{move}', [
    //'middleware' => 'auth.app.product',
    'uses' => 'CityStoreController@move'
    ]);

    /*
    * Store
    */
    Route::get('stores/create/{id?}', [
        //'middleware' => 'auth.app.product',
        'uses' => 'StoreController@create'
        ]);
    Route::get('stores/{id?}', [
        //'middleware' => 'auth.app.product',
        'uses' => 'StoreController@index'
        ]);
    Route::get('stores/{id}/edit', [
        //'middleware' => 'auth.app.product',
        'uses' => 'StoreController@edit'
        ]);

    Route::post('stores', [
        //'middleware' => 'auth.app.product',
        'uses' => 'StoreController@store'
        ]);
    Route::post('stores/{id}', [
        //'middleware' => 'auth.app.product',
        'uses' => 'StoreController@update'
        ]);
    Route::post('stores/{id}/delete', [
        //'middleware' => 'auth.app.product',
        'uses' => 'StoreController@delete'
        ]);
    Route::post('stores/{id}/move/{move}', [
        //'middleware' => 'auth.app.product',
        'uses' => 'StoreController@move'
        ]);

    /*
    * Showcase Image
    */
    Route::get('/showcase/{order}/order', [ 
        //'middleware' => 'auth.app.admin',
        'uses' => 'JumbotronController@index'
        ])->name('admin.showcase.index');
    Route::get('/showcase/create', [
        //'middleware' => 'auth.app.admin',
        'uses' => 'JumbotronController@create'
        ]);
    Route::get('/showcase/{id}', [
        //'middleware' => 'auth.app.admin',
        'uses' => 'JumbotronController@edit'
        ]);

    Route::post('/showcase/create', [
        //'middleware' => 'auth.app.admin',
        'uses' => 'JumbotronController@store'
        ]);
    Route::post('/showcase/{id}', [
        //'middleware' => 'auth.app.admin',
        'uses' => 'JumbotronController@update'
        ]);
    Route::post('/showcase/{id}/delete', [
        //'middleware' => 'auth.app.admin',
        'uses' => 'JumbotronController@delete'
        ]);
    Route::post('/showcase/{id}/move/{move}', [
        //'middleware' => 'auth.app.admin',
        'uses' => 'JumbotronController@move'
        ]);

    Route::post('/showcase/{id}/move/mobile/{move}', [
        //'middleware' => 'auth.app.admin',
        'uses' => 'JumbotronController@moveMobile'
        ]);

    Route::get('/admin', 'AdminController@index');
    Route::get('/activity', 'ActivityController@index');
    
    
    /*
    * Order
    */
    Route::get('/order', [
        //'middleware' => 'auth.app.order',
        'uses' => 'OrderController@index'
        ]);
    Route::get('/order/report', [
        //'middleware' => 'auth.app.order',
        'uses' => 'OrderController@report'
        ]);
    Route::get('/order/export', [
        //'middleware' => 'auth.app.order',
        'uses' => 'OrderController@export'
        ]);
    Route::get('/order/{id}', [
        //'middleware' => 'auth.app.order',
        'uses' => 'OrderController@show'
        ]);
    Route::get('/order/{id}/banknote', [
        //'middleware' => 'auth.app.order',
        'uses' => 'OrderController@downloadBanknote'
        ]);
    Route::get('/order/{id}/invoice', [
        //'middleware' => 'auth.app.order',
        'uses' => 'OrderController@printInvoice'
        ]);

    Route::post('/order/{id}/manual_paid', [
        //'middleware' => 'auth.app.order',
        'uses' => 'OrderController@manualPaid'
        ]);
    Route::post('/order/{id}/paid', [
        //'middleware' => 'auth.app.order',
        'uses' => 'OrderController@paid'
        ]);
    Route::post('/order/{id}/work', [
        //'middleware' => 'auth.app.order',
        'uses' => 'OrderController@work'
        ]);
    Route::post('/order/{id}/unpaid', [
        //'middleware' => 'auth.app.order',
        'uses' => 'OrderController@unpaid'
        ]);
    Route::post('/order/{id}/finish', [
        //'middleware' => 'auth.app.order',
        'uses' => 'OrderController@finish'
        ]);
    Route::post('/order/{id}/reject', [
        //'middleware' => 'auth.app.order',
        'uses' => 'OrderController@reject'
        ]);
    Route::post('/order/{id}/revision', [
        //'middleware' => 'auth.app.order',
        'uses' => 'OrderController@revision'
        ]);

    
    /*
    * Voucher
    */
    Route::get('/voucher', [
        //'middleware' => 'auth.app.product',
        'uses' => 'VoucherController@index'
        ]);
    Route::get('/voucher/create', [
        //'middleware' => 'auth.app.product',
        'uses' => 'VoucherController@create'
        ]);
    Route::get('/voucher/{id}', [
        //'middleware' => 'auth.app.product',
        'uses' => 'VoucherController@edit'
        ]);

    Route::post('/voucher/create', [
        //'middleware' => 'auth.app.product',
        'uses' => 'VoucherController@store'
        ]);
    Route::post('/voucher/{id}', [
        //'middleware' => 'auth.app.product',
        'uses' => 'VoucherController@update'
        ]);
    Route::post('/voucher/{id}/delete', [
        //'middleware' => 'auth.app.product',
        'uses' => 'VoucherController@delete'
        ]);

    /*
	Subscribed Email
    */
    Route::get('/email/subscribed', [
        //'middleware' => 'auth.app.admin',
        'uses' => 'SubscribedEmailController@index'
        ]);
    Route::get('/email/subscribed/export', [
        //'middleware' => 'auth.app.admin',
        'uses' => 'SubscribedEmailController@export'
        ]);
    Route::post('/subscribed/{id}/delete', [
        //'middleware' => 'auth.app.admin',
        'uses' => 'SubscribedEmailController@delete'
        ]);

     /*
	Discount Request
    */
    Route::get('/discount/request', [
        //'middleware' => 'auth.app.admin',
        'uses' => 'DiscountRequestController@index'
        ]);
    Route::get('/discount/request/export', [
        //'middleware' => 'auth.app.admin',
        'uses' => 'DiscountRequestController@export'
        ]);
    Route::post('/discount/request/{id}/delete', [
        // /'middleware' => 'auth.app.admin',
        'uses' => 'DiscountRequestController@delete'
        ]);
    Route::get('/popup/image', [
        //'middleware' => 'auth.app.admin',
        'uses' => 'DiscountRequestController@getImage'
        ]);
    Route::post('/popup/image/{id}', [
        //'middleware' => 'auth.app.admin',
        'uses' => 'DiscountRequestController@updateImage'
        ]);

    /*
    * Customer
    */
    Route::get('/customer', [
        //'middleware' => 'auth.app.order',
        'uses' => 'CustomerController@index'
        ]);
    Route::get('/customer/create', [
        //'middleware' => 'auth.app.order',
        'uses' => 'CustomerController@create'
        ]);
    Route::get('/customer/export', [
        //'middleware' => 'auth.app.order',
        'uses' => 'CustomerController@export'
        ]);
    Route::get('/customer/{id}', [
        //'middleware' => 'auth.app.order',
        'uses' => 'CustomerController@edit'
        ]);
    Route::get('/customer/{id}/detail', [
        //'middleware' => 'auth.app.order',
        'uses' => 'CustomerController@detail'
        ]);

    Route::post('/customer', [
        //'middleware' => 'auth.app.order',
        'uses' => 'CustomerController@store'
        ]);
    Route::post('/customer/{id}', [
        //'middleware' => 'auth.app.order',
        'uses' => 'CustomerController@update'
        ]);




});    